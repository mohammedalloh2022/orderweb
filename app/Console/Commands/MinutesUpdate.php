<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\PublicOrder;
use App\Notifications\SignupActivate;
use App\User;
use Carbon\Carbon;
use Illuminate\Console\Command;

class MinutesUpdate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'publicOrder:cancel';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Cancel public order after 5min';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $client=User::findOrFail(253);
//        $message = [
//            'msg' => "Order test ",
//            'title' => "4station",
//            'public_order_id' => '',
//            'type' => 'public',
//        ];
//        if ($client->fcm_token) {
//            sendFCM($client->fcm_token, $message);
//        }


//         get all public order status pending and left time is 5 minutes
        $carbonBefore=Carbon::now()->subMinutes(7)->format('Y-m-d H:m:i');
        $allPublicOrders=PublicOrder::where('status','pending')->where('created_at','<',$carbonBefore)->get();
        if(isset($allPublicOrders)){
            foreach ($allPublicOrders as $key=>$order){
                $order->status='cancelled';
                $order->save();

                $client=User::findOrFail($order->client_id);
                $message = [
                    'msg' => "order #".$order->invoice_number." cancelled because no one picked it",
                    'title' => "4station",
                    'public_order_id' => $order->id,
                    'type' => 'public',
                    'country_id'=>@$order->country_id
                ];
                if ($client->fcm_token) {
                    sendFCM($client->fcm_token, $message);
                }

                $client->notify(new SignupActivate($message));
            }
        }

        $carbonBefore5min=Carbon::now()->subMinutes(3)->format('Y-m-d H:m:i');
        $allNotPickedOrders=Order::where('status','ready')->where('type_of_receive','home')->where('updated_at','<',$carbonBefore5min)->get();
        if($allNotPickedOrders) {
            foreach ($allNotPickedOrders as $key => $order) {
                $order->is_suspended=true;
                $order->save();
            }
        }
    }
}
