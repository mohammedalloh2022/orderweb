<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $casts=[ 'created_at' => 'datetime:Y-m-d H:m:i'];
    protected  $appends=['extra_item_count'];
    public  function item(){
        return $this->belongsTo(Item::class,'item_id');
    }
    public function extraItems(){
       return $this->hasMany(OrderExtraItem::class,'order_item_id');
    }

    public function getExtraItemCountAttribute(){
        return $this->hasMany(OrderExtraItem::class,'order_item_id')->count();
    }
}
