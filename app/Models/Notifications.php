<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\DatabaseNotification;

class Notifications extends DatabaseNotification
{
    protected $casts = [
        'created_at' => 'timestamp',
        'data' => 'array',
        'read_at' => 'datetime'
    ];

}
