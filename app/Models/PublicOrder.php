<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Storage;
class PublicOrder extends Model
{
//    protected $casts=[ 'created_at' => 'datetime:U',];
    protected $casts=[ 'created_at' => 'datetime:Y-m-d H:m:i','cancel_reasons_id'=>'string'];

    protected $appends=['order_pic_url','created_timestamp','status_translation'];
    public function client(){
        return $this->belongsTo(User::class,'client_id');
    }
    public function driver(){
        return $this->belongsTo(User::class,'driver_id');
    }
    public function getOrderPicUrlAttribute()
    {
        if($this->order_pic)
            return Storage::url('publicOrders/'.$this->order_pic);
        else  return null;
    }

    public function attachments()
    {
        return $this->morphMany(Image::class, 'content');
    }

    public function getCreatedTimestampAttribute(){
        return  strtotime($this->created_at);

    }
    public function cancelReason(){
        return $this->belongsTo(RejectReasons::class,'cancel_reasons_id');
    }


    public function getStatusTranslationAttribute(){
        $status_en=null;
        $status_ar=null;
        $status_translation=null;
        switch ($this->status){
            case "pending" :
                $status_en="Pending order";
                $status_ar="الطلب في الانتظار";
            break;
            case "in_the_way_to_store" :
                $status_en="Order in the way to store";
                $status_ar="الطلب في الطريق للمتجر";
                break;
            case "in_the_way_to_client":
                $status_en="Order in the way to client";
                $status_ar="الطلب في الطريق للزبون";
                break;
            case "cancelled" :
                $status_en="Cancelled order";
                $status_ar="الطلب ملغي";
                break;
            case "delivered" :
                $status_en="Delivered order";
                $status_ar="تم توصيل الطلب  ";
                break;
            case "paid" :
                $status_en="Paid order";
                $status_ar="تم دفع قيمة الطلب";
                break;

        }
        if(app()->getLocale()=='ar'){
            $status_translation=$status_ar;
        }elseif (app()->getLocale()=='en'){
            $status_translation=$status_en;
        }
        return $status_translation;
    }

    public function country(){
        return $this->belongsTo(Country::class,'country_id')->withTrashed();
    }
}
