<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Storage;
class Item extends Model
{
    protected $appends=['name','desc','image_url','extra_items_count','offer_orders_count'];
    protected $casts=[ 'created_at' => 'datetime:Y-m-d H:m:i',];


    public function getImageUrlAttribute()
    {

        $image = Image::where('content_type', Item::class)->where('content_id', $this->id)->where('is_default', true)->first();
        if (isset($image['image_url'])) {
            return $image['image_url'];
        }
        else{
            return null;
        }
    }
    public function getNameAttribute()
    {
        $name='';
        if(app()->getLocale()=='ar'){
            $name= $this->getAttribute('name_ar');
        }elseif (app()->getLocale()=='en'){
            $name= $this->getAttribute('name_en');
        }
        return $name;
    }
    public function getDescAttribute()
    {
        $name='';
        if(app()->getLocale()=='ar'){
            $name= $this->getAttribute('desc_ar');
        }elseif (app()->getLocale()=='en'){
            $name= $this->getAttribute('desc_en');
        }
        return $name;
    }

    public function images()
    {
        return $this->morphMany(Image::class, 'content');
    }
    public function notDefaultImages()
    {
        return $this->morphMany(Image::class, 'content')->where('is_default','false');
    }
    public function category(){
        return $this->belongsTo(CategoriesContent::class,'classification_id');
    }

    public function shop(){
        return $this->belongsTo(CategoriesContent::class,'classification_id');
    }

    public function extraItems(){
        return $this->hasMany(Item::class,'parent_id');
    }
    public function getExtraItemsCountAttribute(){
        return $this->hasMany(Item::class,'parent_id')->count();
    }
    public function getOfferOrdersCountAttribute(){
        if($this->type=='offer'){
            return $this->hasMany(Order::class,'offer_id')->count();
        }
        return 0;
    }

}
