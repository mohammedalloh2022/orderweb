<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class CategoriesContent extends Model
{
    use SoftDeletes;

//    public function  __construct(array $attributes = [])
//    {
//        parent::__construct($attributes);
//    }

    protected $appends = ['name', 'logo_url', 'cover_image_url', 'address', 'rate', 'branch_count', 'offer_count',
        'category_count', 'order_count', 'meals_count', 'store_wallet', 'is_open'];
    protected $casts = ['created_at' => 'datetime:Y-m-d H:m:i',];


    public function getIsOpenAttribute()
    {
        $business_hours = json_decode($this->business_hours);
        if (isset($business_hours)) {
            $today = Carbon::today()->format('l');
            $dayNo = $this->getDayNo($today);
            if (isset($business_hours->$dayNo) && isset($business_hours->$dayNo->open) && isset($business_hours->$dayNo->close)) {
                $open = (int)$business_hours->$dayNo->open;
                $close = (int)$business_hours->$dayNo->close;

                $hour = (int)Carbon::now()->format('H');
                if ($close > $open)
                    return ($open <= $hour && $close > $hour);
                else if (($open <= $hour && $hour <= 24) || ($close > $hour))
                    return true;

            }

//            dd($business_hours, $today, $business_hours->$dayNo);

        }
        return false;
    }

    public function getDayNo($day)
    {
        switch ($day) {
            case "Saturday":
                return 1;
            case "Sunday":
                return 2;
            case "Monday":
                return 3;
            case "Tuesday":
                return 4;
            case "Wednesday":
                return 5;
            case "Thursday":
                return 6;
            default:
                return 7;
        }
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'content_id');
    }

    public function contentable()
    {
        return $this->morphTo();
    }


    public function getNameAttribute()
    {
        $name = '';
        if (app()->getLocale() == 'ar') {
            $name = $this->getAttribute('name_ar');
        } elseif (app()->getLocale() == 'en') {
            $name = $this->getAttribute('name_en');
        }
        return $name;
    }

    public function getBranchCountAttribute()
    {
        return $this->hasMany(CategoriesContent::class, 'parent_id', 'id')->count();
    }

    public function getAddressAttribute()
    {
        $name = '';
        if (app()->getLocale() == 'ar') {
            $name = $this->getAttribute('address_ar');
        } elseif (app()->getLocale() == 'en') {
            $name = $this->getAttribute('address_en');
        }
        return $name;
    }

    public function getLogoUrlAttribute()
    {
        if ($this->image) {
            return Storage::url('shops/' . $this->image);
        } else {
            return null;
        }

    }

    public function getCoverImageUrlAttribute()
    {
        if ($this->cover_image) {

            return Storage::url('shops/' . $this->cover_image);
        } else {
            return null;
        }
    }

    public function favorites()
    {
        return $this->morphMany(Favorite::class, 'content');
    }

    public function ratings()
    {
        return $this->morphMany(Rating::class, 'content');
    }

    public function getRateAttribute()
    {
//        $sum= $this->morphMany(Rating::class, 'content')->sum('rate');
        $sum = $this->morphMany(Rating::class, 'content')->select(\DB::raw("SUM(rate) as sum_rate"))->pluck('sum_rate')[0];;
        $count = $this->morphMany(Rating::class, 'content')->count();
        if ($count) {
            return (float)$sum / $count;

        } else {
            return 0;
        }
    }

    public function getOrderCountAttribute()
    {
        $shopBranchList = CategoriesContent::where('parent_id', $this->id)->get()->pluck('id')->toArray();
        $countShopBranchOrder = Order::whereIn('shop_id', $shopBranchList)->where(function ($qq) {
            $qq->where('status', 'cancelled')->orWhere('status', 'delivered');
        })->count();
        $countShopOrder = $this->hasMany(Order::class, 'shop_id')->where(function ($qq) {
            $qq->where('status', 'cancelled')->orWhere('status', 'delivered');
        })->count();

        return $countShopOrder + $countShopBranchOrder;
    }

    public function orders()
    {
        return $this->hasMany(Order::class, 'shop_id', 'id');
    }

    public function branch()
    {
        return $this->hasMany(CategoriesContent::class, 'parent_id', 'id');
    }

    public function branchParent()
    {
        return $this->belongsTo(CategoriesContent::class, 'parent_id');
    }

    public function subCategory()
    {
        return $this->hasMany(SubCategory::class, 'shop_id', 'id')->whereHas('meals', function ($q) {
            $q->where("is_active", 1);
        });
    }

    public function getCategoryCountAttribute()
    {
        return $this->hasMany(SubCategory::class, 'shop_id', 'id')->count();
    }

    public function offers()
    {
        return $this->hasMany(Item::class, 'classification_id', 'id')
            ->where('type', 'offer')->where('parent_id', null);
    }

    public function activeOffers()
    {
        $today = Carbon::now()->toDayDateTimeString();
        return $this->hasMany(Item::class, 'classification_id', 'id')
            ->where('type', 'offer')->where('parent_id', null)
            ->where('status', 'active')
            ->where('is_active', 1);
    }

    public function getOfferCountAttribute()
    {
        return $this->hasMany(Item::class, 'classification_id', 'id')
            ->where('type', 'offer')->where('is_active', 1)->where('parent_id', null)->count();
    }

    public function meals()
    {
        return $this->hasMany(Item::class, 'classification_id', 'id')
            ->where('type', 'normal')->where('parent_id', null);
    }

    public function getMealsCountAttribute()
    {
        return $this->hasMany(Item::class, 'classification_id', 'id')
            ->where('type', 'normal')->where('parent_id', null)->count();
    }

    public function hasBranches()
    {
        return $this->hasMany(CategoriesContent::class, 'parent_id', 'id')->count();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getStoreWalletAttribute()
    {
        $shop = $this->id;
        $shopBranchList = CategoriesContent::where('parent_id', $shop)->get()->pluck('id')->toArray();
        $wallet = Order::where(function ($q) use ($shop, $shopBranchList) {
            $q->where('shop_id', $shop)->orWhereIn('shop_id', $shopBranchList);
        })->where('status', 'delivered')->where('accounted_with_app', 0)->sum('order_store_wallet');

        return $wallet;
    }

    public function scopeDistance($lat2, $lon2, $unit = "K")
    {
        $lat1 = $this->lat;
        $lon1 = $this->lng;

        if ($lat1 && $lon1 && $lat2 && $lon2) {

            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
        return 0;
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id')->withTrashed();
    }

    public function getIsOpenedAttribute()
    {

    }
}
