<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;

class Image extends Model
{
    protected $appends = ['image_url'];
    protected $casts = ['created_at' => 'datetime:Y-m-d H:m:i',];

    public function getImageUrlAttribute()
    {
        if ($this->value == "public_order") {
            return Storage::url('publicOrders/' . $this->name);
        } else {

            return Storage::url('items/' . $this->name);
        }
    }

    public function content()
    {
        return $this->morphTo();
    }
}
