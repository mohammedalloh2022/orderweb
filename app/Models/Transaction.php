<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $casts=[ 'created_at' => 'datetime:Y-m-d H:m:i',];
    public function country(){
        return $this->belongsTo(Country::class,'country_id')->withTrashed();
    }
}
