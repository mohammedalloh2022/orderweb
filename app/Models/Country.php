<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\App;

class Country extends Model
{
    use SoftDeletes;
    protected $appends=['cities_count','name'];
    public function setting(){


        return $this->hasOne(\App\Models\AppSetting::class,'country_id');
    }

    public function cities(){
        return $this->hasMany(City::class,'country_id');
    }
    public function getCitiesCountAttribute(){
        return $this->hasMany(City::class,'country_id')->count();
    }

    public function getNameAttribute()
    {
        if(App::getLocale()=='ar'){
            return $this->name_ar;
        }
        return $this->name_en;
    }
}
