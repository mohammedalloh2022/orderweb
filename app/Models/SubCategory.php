<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Storage;
class SubCategory extends Model
{
    protected $appends=['name','image_url','meals_count'];
    protected $casts=[ 'created_at' => 'datetime:Y-m-d H:m:i',];
    public function getNameAttribute()
    {
        $name='';
        if(app()->getLocale()=='ar'){
            $name= $this->getAttribute('name_ar');
        }elseif (app()->getLocale()=='en'){
            $name= $this->getAttribute('name_en');
        }
        return $name;
    }
    public function getImageUrlAttribute()
    {
        if($this->image)
            return Storage::url('subcategories/'.$this->image);
        else return null;
    }

    public function getMealsCountAttribute()
    {
      return $this->hasMany(Item::class,'sub_cat_id')->where('parent_id',null)->count();

    }
    public function meals(){
        return $this->hasMany(Item::class,'sub_cat_id')->where('parent_id',null);

    }
    public function country(){
        return $this->belongsTo(Country::class,'country_id')->withTrashed();
    }
}
