<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class ContactReply extends Model
{

    protected $casts=[ 'created_at' => 'datetime:Y-m-d H:m:i'];


    public function contact(){
        return $this->belongsTo(Contact::class,'contact_id');
    }
}
