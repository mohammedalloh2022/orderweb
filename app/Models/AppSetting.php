<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppSetting extends Model
{
    protected $hidden=['app_order_commission','offer_periods_hr','linkedin_link','created_at','updated_at','id'];
    protected $casts=[ 'created_at' => 'datetime:Y-m-d H:m:i'];
    public function country(){
        return $this->belongsTo(Country::class,'country_id')->withTrashed();
    }
}
