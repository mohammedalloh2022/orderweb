<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DriverWalletTransaction extends Model
{
    protected $casts=[ 'created_at' => 'datetime:Y-m-d H:m:i'];
}
