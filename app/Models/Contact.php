<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $casts=[ 'created_at' => 'datetime:Y-m-d H:m:i',];
    protected $appends=['replies_count'];
    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function replies(){
        return $this->hasMany(ContactReply::class,'contact_id');
    }
    public function getRepliesCountAttribute(){
        return $this->hasMany(ContactReply::class,'contact_id')->count();
    }
    public function userRole(){
        return $this->belongsTo(User::class,'user_id');
    }

    public function country(){
        return $this->belongsTo(Country::class,'country_id')->withTrashed();
    }
}
