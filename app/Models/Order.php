<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\Types\Null_;

class Order extends Model
{
    protected $appends=['item_count','created_timestamp','status_translation'];
    protected $casts=[ 'created_at' => 'datetime:Y-m-d H:m:i','total'=>'float(10,2)'];
    public function coupon(){
        return $this->belongsTo(Coupon::class,'coupon_id');
    }
    public function orderItems(){
        return $this->hasMany(OrderItem::class,'order_id');
    }
    public function client(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function deliveryUser(){
        return $this->belongsTo(User::class,'driver_id');
    }
    public function driver(){
        return $this->belongsTo(User::class,'driver_id');
    }
    public function shop(){
        return $this->belongsTo(CategoriesContent::class,'shop_id');
    }
    public function getItemCountAttribute(){
        return $this->hasMany(OrderItem::class,'order_id')->whereHas('item',function ($q){
            $q->where('parent_id',null);
        })->count();
    }
    public function getCreatedTimestampAttribute(){
       return  strtotime($this->created_at);
       return  $this->created_at->timestamp();
       return Carbon::parse($this->created_at)->timestamp();
    }
    public function cancelReason(){

        return  $this->belongsTo(RejectReasons::class,'cancel_reason_id');


    }

    public function getStatusTranslationAttribute(){
        $status_en=null;
        $status_ar=null;
        $status_translation=null;
        switch ($this->status){
            case "pending" :
                $status_en="Pending order";
                $status_ar="الطلب في الانتظار";
                break;

            case "on_the_way":
                $status_en="Order in the way to client";
                $status_ar="الطلب في الطريق للزبون";
            break;
            case "cancelled" :
                $status_en="Cancelled";
                $status_ar="الطلب ملغي";
                break;
            case "delivered" :
                $status_en="Delivered order";
                $status_ar="تم توصيل الطلب  ";
                break;
            case "paid" :
                $status_en="Paid order";
                $status_ar="تم دفع قيمة الطلب";
                break;
            case "ready" :
                $status_en="Ready";
                $status_ar="طلب جاهز";
            break;
            case "preparing" :
                $status_en="preparing";
                $status_ar="قيد التجهيز";
            break;


        }
        if(app()->getLocale()=='ar'){
            $status_translation=$status_ar;
        }elseif (app()->getLocale()=='en'){
            $status_translation=$status_en;
        }
        return $status_translation;
    }

    public function country(){
        return $this->belongsTo(Country::class,'country_id')->withTrashed();
    }


}
