<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Wallet extends Model
{

    protected $appends=['payment_type','invoice_number'];
    public function user(){
        return $this->belongsTo(User::class,'driver_id');
    }
    public function getPaymentTypeAttribute(){
        if($this->order_id){

          return $this->belongsTo(Order::class,'order_id')->pluck('payment_type')->first();
        }else{
            return 'System transaction';
        }
    }
    public function getInvoiceNumberAttribute(){
        if($this->order_id) {
            return $this->belongsTo(Order::class, 'order_id')->pluck('invoice_number')->first();
        }else{
            return 'App manger';
        }
    }
    public function country(){
        return $this->belongsTo(Country::class,'country_id')->withTrashed();
    }
}
