<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class City extends Model
{
    protected $fillable=[
        'name_en','name_ar','lat','lng','country_id'
    ];
    protected $appends=['name'];
    public function country(){
        return $this->belongsTo(Country::class,'country_id');
    }

    public function getNameAttribute()
    {
        if(App::getLocale()=='ar'){
            return $this->name_ar;
        }
        return $this->name_en;
    }
}
