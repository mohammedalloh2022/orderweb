<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Favorite extends Model
{
    protected $casts=[ 'created_at' => 'datetime:Y-m-d H:m:i',];
    public function content()
    {
        return $this->morphTo();
    }
//    public  function content(){
//        return $this->belongsTo(CategoriesContent::class,'content_id');
//    }
}
