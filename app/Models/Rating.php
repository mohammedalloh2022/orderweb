<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Rating extends Model
{
    protected $appends=['created_timestamp'];
    protected $casts=[ 'created_at' => 'datetime:Y-m-d H:m:i',];
    public function content()
    {
        return $this->morphTo();
    }

    public function user(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function getCreatedTimestampAttribute(){
        return  strtotime($this->created_at);

    }
    public function order(){
        if($this->type=='private'):
        return $this->belongsTo(Order::class,'order_id');
        else:
        return $this->belongsTo(PublicOrder::class,'order_id');
        endif;
    }
}
