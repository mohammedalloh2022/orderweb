<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AppContent extends Model
{
    protected $fillable=[
        "facebook_link",
"instagram_link",
"linkedin_link",
"twitter_link",
"mobile",
"address",
"email",
"country_id"
    ];
    public function country(){
        return $this->belongsTo(Country::class,'country_id')->withTrashed();
    }
}
