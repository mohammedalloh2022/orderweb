<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Storage;

class AdvertiseSlider extends Model
{

    protected $appends=['image_url','name'];
    public function getImageUrlAttribute()
    {
        if($this->image){

            return Storage::url('advertise/'.$this->image);
        }else{
            return Storage::url('advertise/default.jfif');

        }
    }
    public function getNameAttribute()
    {
        $name='';
        if(app()->getLocale()=='ar'){
            $name= $this->getAttribute('name_ar');
        }elseif (app()->getLocale()=='en'){
            $name= $this->getAttribute('name_en');
        }
        return $name;
    }
    public function country(){
        return $this->belongsTo(Country::class,'country_id')->withTrashed();
    }

}
