<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderExtraItem extends Model
{
    protected $appends=['name'];
    public  function item(){
        return $this->belongsTo(Item::class,'item_id');
    }
    public function orderItem(){
        return $this->belongsTo(OrderItem::class,'order_item_id');
    }

    public function getNameAttribute()
    {
        $name='';
        if(app()->getLocale()=='ar'){
            $name= $this->getAttribute('name_ar');
        }elseif (app()->getLocale()=='en'){
            $name= $this->getAttribute('name_en');
        }
        return $name;
    }

}
