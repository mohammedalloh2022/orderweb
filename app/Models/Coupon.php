<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $casts = ['created_at' => 'datetime:Y-m-d H:m:i'];
    protected $appends = [
        'count_of_use',
        'city_name'
    ];

    public function getCountOfUseAttribute()
    {
        return $this->hasMany(Order::class, 'coupon_id')->where('status', '!=', 'cancelled')->count();
    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     * @author WeSSaM
     */
    public function city()
    {
        return $this->belongsTo(City::class, 'city_id', 'id')->withDefault(['name' => '']);
    }

    /**
     * @return mixed
     * @author WeSSaM
     */
    public function getCityNameAttribute()
    {
        return $this->city->name;
    }
}
