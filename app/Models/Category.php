<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
class Category extends Model
{
    use SoftDeletes;
    protected $appends=['name','image_url','shops_count'];
    protected $casts=[ 'created_at' => 'datetime:Y-m-d H:m:i',];
    protected $table = 'categories';

    public function getNameAttribute()
    {
        $name='';
        if(app()->getLocale()=='ar'){
            $name= $this->getAttribute('name_ar');
        }elseif (app()->getLocale()=='en'){
            $name= $this->getAttribute('name_en');
        }
        return $name;
    }
    public function getImageUrlAttribute()
    {
        if($this->image){

        return Storage::url('categories/'.$this->image);
        }else{
        return Storage::url('categories/default.jfif');

        }
    }

    public function content()
    {
        return $this->morphMany(CategoriesContent::class, 'contentable');
    }

    public function getShopsCountAttribute()
    {
        return $this->hasMany(CategoriesContent::class,'content_id')->where('parent_id',null)->where('is_active',1)->count();
    }
    public function shops(){
        return $this->hasMany(CategoriesContent::class,'content_id');
    }
    public function country(){
        return $this->belongsTo(Country::class,'country_id')->withTrashed();
    }
}
