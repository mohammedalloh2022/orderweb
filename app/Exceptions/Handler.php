<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Session\TokenMismatchException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Throwable $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Throwable $exception
     * @return \Symfony\Component\HttpFoundation\Response
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $exception)
    {

//        dd($exception);
        if ($exception instanceof TokenMismatchException && !$request->expectsJson())
            return redirect()->to('/login');

//        $errors = (array)@ $exception->validator->errors();
////        dd($errors);
////        $new = [];
////        if (isset($errors[0]) && count($errors[0] ) > 0)
////            dd($errors[0]);
////        foreach ($errors[0] as $key => $error) {
////            $new[(strpos($key, '.') !== false) ? explode('.', $key)[0] : $key] = $error;
////        }
        if ($request->expectsJson())
            return response()->json(['status' => false, 'message' => $exception->getMessage(), 'errors' => []]);

        return parent::render($request, $exception);
    }
}
