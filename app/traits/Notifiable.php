<?php

namespace App\traits;

use App\Models\Notifications;
use Illuminate\Notifications\Notifiable as BaseNotifiable;

trait Notifiable
{
    use BaseNotifiable;

    /**
     * Get the entity's notifications.
     */
    public function notifications()
    {
        return $this->morphMany(Notifications::class, 'notifiable')
            ->orderBy('created_at', 'desc');
    }
}
