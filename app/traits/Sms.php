<?php
namespace App\traits;

use Twilio\Exceptions\ConfigurationException;
use Twilio\Rest\Client as TwilioClient;

trait Sms
{


    /**
     * Get the entity's notifications.
     */
    public static function sendSMSMessage($message, $recipients)
    {
        $account_sid = config("services.twilio.TWILIO_SID");
        $auth_token = config("services.twilio.TWILIO_AUTH_TOKEN");
        $twilio_number = config("services.twilio.TWILIO_NUMBER");

        try {
            $client = new TwilioClient($account_sid, $auth_token);
            $client->messages->create(
                ("+".$recipients),
                [
                    'from' => $twilio_number,
                    'body' => $message
                ]);
        } catch (ConfigurationException $e) {
            return $e->getMessage();
        }

    }
}
