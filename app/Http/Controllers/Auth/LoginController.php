<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
//    protected $redirectTo = RouteServiceProvider::HOME;
    /** @var TYPE_NAME $this */
    /** @var TYPE_NAME $this */
//    protected $redirectTo = $this->redirectTo();

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string|email|exists:users,email',
            'password' => 'required|string',
        ]);


    }
    public function credentials(Request $request)
    {
        return [
            'email'     => $request->email,
            'password'  => $request->password,
            'active' => '1'
        ];
    }
    public function unActiveUserCredentials(Request $request)
    {
        return [
            'email'     => $request->email,
            'password'  => $request->password,
            'active' => '0'
        ];
    }


    protected function sendUnActiveFailedLoginResponse(Request $request)
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    protected function authenticated(Request $request, $user)
    {
        if($user->isAdmin()) {
            return redirect(route('home.index'));
        }
        else if($user->isShop()) {
            return redirect(route('shop.dashboard'));
        }
        else if($user->isBranch()){
            return redirect(route('branch.dashboard'));
        }
        abort(404);
    }

//    protected function redirectTo(){
//        // User role
//        if(Auth::check()){
//            $role = Auth::user()->role;
//            // Check user role
//            switch ($role) {
//                case 'admin':
//                    return '/dashboard';
//                    break;
//                case 'branch':
//                    return '/branch/dashboard';
//                    break;
//                case 'shop':
//                    return '/shop/dashboard';
//                break;
////                default:
////                    return '/login';
////                break;
//            }
//        }else{
//            return '/login';
//        }
//    }


}
