<?php

namespace App\Http\Controllers;

use App\Models\CategoriesContent;
use App\Models\Category;
use App\Models\Item;
use App\Models\Order;
use App\Models\Rating;
use App\Models\SubCategory;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

use Image;
use Storage;
use File;

use DB;
use Validator;
use Avatar;

class SingleShopController extends Controller
{
    public function CateogryIndex(){
        $shop=auth()->user()->shop;
        $page='cat';
        return view('shop.layout',compact('shop','page'));
    }

    public function branch(){
        $shop=auth()->user()->shop;
        $page='branch';
        return view('shop.layout',compact('shop','page'));
    }
    public function orders(){
        $shop=auth()->user()->shop;
        $page='order';
        return view('shop.layout',compact('shop','page'));
    }

    public function pendingOrders(){
        $shop=auth()->user()->shop;
        $page='pendingOrders';
        return view('shop.layout',compact('shop','page'));
    }

    public function offers(){
        $shop=auth()->user()->shop;
        $page='offer';
        return view('shop.layout',compact('shop','page'));
    }
    public function transactions(){
        $shop=auth()->user()->shop;
        $page='transactions';
        return view('shop.layout',compact('shop','page'));
    }
    public function settings(){
        $shop=auth()->user()->shop;
        return view('shop.settings',compact('shop'));
    }

    public function revenue(){
        return view('shop.revenue');
    }
    public function previousOrderView(){
        $shop=auth()->user()->shop;
        return view('shop.preOrders',compact('shop'));
    }

    public function getPervOrders(Request $request,CategoriesContent $shop){

        $shopBranchList = CategoriesContent::where('parent_id', $shop->id)->get()->pluck('id')->toArray();

        $orders = new Order();
        if ( !empty($request->filter)) {
            $filter = request('filter');
            $orders = $orders->where('invoice_number', 'LIKE', "%$filter");


        }
        $orders= $orders->where('status', '!=', 'pending');
        if($shop->parent_id){
            $orders=$orders->where('wallet_calc_with_store_branch',1);
        }else{
            $orders=$orders->where('accounted_with_app',1);
        }
        $orders = $orders->with("client", "deliveryUser", "shop");

        $orders = $orders->where(function ($q) use ($shop, $shopBranchList) {
            $q->where('shop_id', $shop->id)->orWhereIn('shop_id', $shopBranchList);
        });
//        $orders=$orders->where('type', 'normal');
        $orders = $orders->orderBy('created_at', 'desc');
        $wallet=null;
        if( isset($request->report_period)) {

//            \Illuminate\Pagination\Paginator::currentPageResolver(function ()  {
//                return 1;
//            });

            if($request->report_period=='custom'){
                $dates=explode(',',$request->custom_period);
                $periodDate=['start_at'=>Carbon::parse($dates[0])->format('Y-m-d 00:00:00'),'end_at'=>Carbon::parse($dates[1])->format('Y-m-d 23:59:59')];
            }else{

                $periodDate=  filterPeriod($request->report_period);
            }
            $wallet = $orders->where('status', 'delivered')->where('created_at','>=',$periodDate['start_at'])->where('created_at','<=',$periodDate['end_at'])->sum('order_store_wallet');
            $orders=$orders->where('created_at','>=',$periodDate['start_at'])->where('created_at','<=',$periodDate['end_at']) ;
            $orders = $orders->paginate(20);
        }else{
            $wallet=$orders->where('status', '!=', 'pending')->sum('order_store_wallet');
            $orders = $orders->paginate(20);
        }
        $totalWallet=$orders->where('status', 'delivered')->sum('order_store_wallet');
        $wallet= number_format($wallet,2);
        return response()->json(compact('orders','wallet','totalWallet'));
    }

    public function paymentTransaction(){
        $shop=auth()->user()->shop;
        return view('shop.paymentTransaction',compact('shop'));
    }

    public function getRevenueFilter(Request $request){
        $shop= auth()->user()->shop;
            $orders = new Order();

            $orders=$orders->where('status','delivered');
        $shopBranchList = CategoriesContent::where('parent_id', $shop->id)->get()->pluck('id')->toArray();

        $orders = $orders->where(function ($q) use ($shop, $shopBranchList) {
            $q->where('shop_id', $shop->id)->orWhereIn('shop_id', $shopBranchList);
        });
            if(request()->has('filter') && request('filter') ) {
                $orders = $orders->where('invoice_number',"like","%$request->filter%");
//            $orders = $orders->where(function($q) use($filter){
//                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
//            });

            }
            $app_revenue=null;
            if( isset($request->report_period)) {

//            \Illuminate\Pagination\Paginator::currentPageResolver(function ()  {
//                return 1;
//            });

                if($request->report_period=='custom'){
                    $dates=explode(',',$request->custom_period);
                    $periodDate=['start_at'=>Carbon::parse($dates[0])->format('Y-m-d 00:00:00'),'end_at'=>Carbon::parse($dates[1])->format('Y-m-d 23:59:59')];
                }else{

                    $periodDate=  filterPeriod($request->report_period);
                }
                $app_revenue=Order::where('status','delivered')->where(function ($q) use ($shop, $shopBranchList) {
                    $q->where('shop_id', $shop->id)->orWhereIn('shop_id', $shopBranchList);
                })->where('created_at','>=',$periodDate['start_at'])->where('created_at','<=',$periodDate['end_at'])->sum('shop_revenue');
                $orders=$orders->where('created_at','>=',$periodDate['start_at'])->where('created_at','<=',$periodDate['end_at']) ;
                $orders = $orders->orderBy('created_at','desc')->paginate(20);
            }else{
                $app_revenue=Order::where('status','delivered')->where(function ($q) use ($shop, $shopBranchList) {
                    $q->where('shop_id', $shop->id)->orWhereIn('shop_id', $shopBranchList);
                })->sum('shop_revenue');
                $orders = $orders->orderBy('created_at','desc')->paginate(20);
            }





//        $orders = $orders->orderBy('created_at','desc')->paginate(20);
            return response()->json(compact('orders','app_revenue'));


    }
}
