<?php

namespace App\Http\Controllers;

use App\AppContent;
use Illuminate\Http\Request;

class AppContentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\AppContent  $appContent
     * @return \Illuminate\Http\Response
     */
    public function show(AppContent $appContent)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\AppContent  $appContent
     * @return \Illuminate\Http\Response
     */
    public function edit(AppContent $appContent)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\AppContent  $appContent
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AppContent $appContent)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\AppContent  $appContent
     * @return \Illuminate\Http\Response
     */
    public function destroy(AppContent $appContent)
    {
        //
    }
}
