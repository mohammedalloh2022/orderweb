<?php

namespace App\Http\Controllers;


use App\Models\CategoriesContent;
use App\Models\Category;
use http\Url;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\Storage;
use File;
class CategoryController extends Controller
{

    public function index(){
        $categories=Category::paginate(10);
        return view('category.index',compact('categories'));
    }


    public function list(){
        $categories=Category::all();
        return response()->json(compact('categories'));
    }

    public function search()
    {
        $categories = new Category();

        if(request()->has('filter') && request('filter') ) {
            $filter = request('filter');
            $categories = $categories->where(function($q) use($filter){
                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
            });
        }
        if(request()->has('sort')) {
            $sort = json_decode(request('sort'), true);
//            $categories = $categories->orderBy(($sort['fieldName'] ?? 'id'), $sort['order']);
        }
         $categories = $categories->orderBy('created_at','desc')->paginate(15);
        return response()->json(compact('categories'));
    }
//categories
    public function store(Request $request){
        $request->validate([
            'cat_name_en' => 'required',
            'cat_name_ar' => 'required',
            'cat_image' => 'required',
        ]);

        \DB::beginTransaction();
        try {
            $category = new Category();
            $category->name_en = $request->cat_name_en;
            $category->name_ar = $request->cat_name_ar;

//            if ($request->hasFile('cat_image')) {
                // Get just ext
            $img = Image::make($request->cat_image);
//                $extension =$img->getClientOriginalExtension();
            $extension = explode('/', $img->mime)[1];
                //uniqid()
            $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
            $destinationPath = public_path('uploads/categories/');
            File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
            $img->save($destinationPath . $fileNameToStore);
                $category->image = $fileNameToStore;

//            $data=base64_decode($request->cat_image);
//            Storage::disk('local')->put($fileNameToStore, $data);
//            Storage::disk('local')->put($fileNameToStore, base64_decode($data));
//                Storage::put('categories', $data, $fileNameToStore);
//            }
            $category->save();
            \DB::commit();



        } catch (\Exception $e) {
//            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }
        $message='Successfully created category!';
        return response()->json(compact('message'),200);
   }

    public function update(Request $request,$id){
        $request->validate([
            'cat_name_en' => 'required',
            'cat_name_ar' => 'required',
//            'cat_image' => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $category =  Category::find($id);
            $category->name_en = $request->cat_name_en;
            $category->name_ar = $request->cat_name_ar;

//            if ($request->hasFile('cat_image')) {
            // Get just ext
            if (!filter_var($request->cat_image, FILTER_VALIDATE_URL)) {

//            if($request->cat_image){

            $img = Image::make($request->cat_image);
//                $extension =$img->getClientOriginalExtension();
            $extension = explode('/', $img->mime)[1];
            //uniqid()
            $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
            $destinationPath = public_path('uploads/categories/');
            File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
            $img->save($destinationPath . $fileNameToStore);
            $category->image = $fileNameToStore;
            }
//            $data=base64_decode($request->cat_image);
//            Storage::disk('local')->put($fileNameToStore, $data);
//            Storage::disk('local')->put($fileNameToStore, base64_decode($data));
//                Storage::put('categories', $data, $fileNameToStore);
//            }
            $category->save();
            \DB::commit();



        } catch (\Exception $e) {
//            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }
        $message='Successfully update category!';
        return response()->json(compact('message'),200);
   }

    public function destroy( Category $category)
    {
        $category->delete();
        $message ="Category deleted successfully";
        return response()->json(compact('message'));
        return response()->json(['message' =>'Item deleted successfully'],200);

    }

    public function changeStatus(Request $request) {
        $request->validate([
            'id' => 'required',
            'status' => 'required',
        ]);
        $category = Category::findOrFail($request->id);
        $category->is_active = $request->status;
        $category->save();
        return response()->json([
            'status' => 'success',
            'msg' => 'Status changed successfully .'
        ], 200);
    }

}
