<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Coupon;
use App\Models\Order;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    public function index($countryId)
    {
        $country = Country::findOrFail($countryId);
        if (!auth()->user()->can('Coupons management ' . $country->code)) {
            abort(404);
        }
        $coupons = Coupon::paginate(10);
        return view('coupons.index', compact('coupons', 'countryId'));

    }


    /**
     * return create view resource
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author WeSSaM
     */
    public function create($id)
    {
        $country = Country::findOrFail($id);
        return view('coupons.create', ['country' => $country]);
    }

    /**
     *
     * @param $countryId
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @author WeSSaM
     */
    public function edit($countryId, $id)
    {
        $country = Country::findOrFail($countryId);
        $item = Coupon::findOrFail($id);
        return view('coupons.create', ['country' => $country, 'item' => $item]);
    }


    public function viewOrders($coupon)
    {
        return view('coupons.orders', compact('coupon'));
    }

    public function orders($coupon)
    {

        $orders = new Order();
        $orders = $orders->where('coupon_id', $coupon);
        $orders = $orders->with("client", "deliveryUser", "shop");

        $orders = $orders->orderBy('created_at', 'desc')->paginate(15);
        return response()->json(compact('orders'));
    }

    public function list($countryId)
    {
        $coupons = new Coupon();
        $coupons = $coupons->where('country_id', $countryId);
        if (request()->has('filter') && request('filter')) {
            $filter = request('filter');
            $coupons = $coupons->where('code', 'like', "%$filter%");
        }
        $coupons = $coupons->orderBy('created_at', 'desc')->paginate(10);
        return response()->json(compact('coupons'));

    }

    public function store(Request $request)
    {
        $request->validate([
            'max_use_no' => 'required|integer',
            'code' => 'required|unique:coupons,code',
            'value' => 'required|integer',
            'expire_at' => 'required',
            'country_id' => 'required',
        ]);
        \DB::beginTransaction();
        try {
            $coupon = new Coupon();
            $coupon->max_use_no = $request->max_use_no;
            $coupon->code = $request->code;
            $coupon->value = $request->value;
            $coupon->expire_at = $request->expire_at ? $request->expire_at : $request->expire_date;
            $coupon->country_id = $request->country_id;
            $coupon->city_id = $request->city_id;
            $coupon->active = 1;
            $coupon->save();
            \DB::commit();
        } catch (\Exception $e) {
            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }
        $message = 'Successfully created coupon!';
        return response()->json(['status'=> true,'message'=> $message], 200);
    }

    public function changeStatus(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'status' => 'required',
        ]);
        $coupon = Coupon::findOrFail($request->id);
        $coupon->active = $request->status;
        $coupon->save();
        return response()->json([
            'status' => 'success',
            'msg' => 'Status changed successfully .'
        ], 200);
    }


}
