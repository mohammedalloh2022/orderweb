<?php

namespace App\Http\Controllers;

use App\Models\AppContent;
use App\Models\AppSetting;
use App\Models\Country;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AppSettingController extends Controller
{
    public function index($countryId=null){
        $country=Country::findOrFail($countryId);
        if(!auth()->user()->can('App setting '.$country->code)){
            abort(404);
        }

        if($countryId){

        $settings=AppSetting::where('country_id',$countryId)->first();
        }else{
            abort('404');
        }
        return view('settings.index',compact('settings','countryId'));
    }

    public function store(Request $request){
        $validator = \Validator::make($request->all(), [
            'driver_wallet_limit' => 'required|digit',
            'shop_wallet_limit' => 'required|digit',
//            'offer_periods_hr' => 'required',
            'app_order_commission' => 'required|digit',
            'tax' => 'required|digit',
            'delivery_cost' => 'required|digit',
            'app_delivery_commission' => 'required|digit',
            'app_commission_public_delivery' => 'required|digit',
            'public_delivery_cost_km' => 'required|digit',
            'shop_distance_limit_km' => 'required|digit',
            'driver_distance_limit_km' => 'required|digit',
            'client_hand_payment_limit' => 'required|digit',
        ]);



        \DB::beginTransaction();
        try {
            if($request->country_id){
                $app=AppSetting::where('country_id',$request->country_id)->first();
                if(!$app){
                    $app=new AppSetting();
                }
            }
//            $app =AppSetting::findOrFail(1);
            $app->driver_wallet_limit=($request->driver_wallet_limit>0?-$request->driver_wallet_limit:$request->driver_wallet_limit);
            $app->shop_wallet_limit=($request->shop_wallet_limit>0?-$request->shop_wallet_limit:$request->shop_wallet_limit);
//            $app->shop_wallet_limit=(-$request->shop_wallet_limit);
            $app->offer_periods_hr=$request->offer_periods_hr;
            $app->app_order_commission=$request->app_order_commission;
            $app->shop_distance_limit_km=$request->shop_distance_limit_km;
            $app->driver_distance_limit_km=$request->driver_distance_limit_km;
            $app->tax=$request->tax;
            $app->country_id=$request->country_id;
            $app->app_commission_public_delivery=$request->app_commission_public_delivery;
            $app->public_delivery_cost_km=$request->public_delivery_cost_km;
            $app->delivery_cost=$request->delivery_cost;
            $app->app_delivery_commission=$request->app_delivery_commission;
            $app->client_hand_payment_limit=$request->client_hand_payment_limit;
            $app->save();
            
        } catch (\Exception $e) {
            \DB::rollback();
            return $e;
//            return response()->json([
//                'success' => false,
//                'message' => 'Something going wrong'
//            ], 422);
            return redirect()->back()->withInput()->with(['message'=>"Error updated successfully"]);
        }

        return redirect()->back()->withInput()->with(['message'=>"Settings updated successfully"]);

    }
    public function content($countryId){
        $settings=AppContent::where('country_id',$countryId)->first();

        return view('settings.content',compact('settings','countryId'));
    }


    public function privacyContent(){
        $settings=AppContent::findOrFail(1);
        return view('settings.privacy',compact('settings'));
    }



    public function contentStore(Request $request){
//        $validator = \Validator::make($request->all(), [
//            'driver_wallet_limit' => 'required',
//            'shop_wallet_limit' => 'required',
//            'offer_periods_hr' => 'required',
//            'app_order_commission' => 'required',
//            'tax' => 'required',
//            'delivery_cost' => 'required',
//        ]);
        \DB::beginTransaction();
        try {

                $app =AppContent::findOrFail(1);


            $app->facebook_link=$request->facebook_link;
            $app->instagram_link=$request->instagram_link;
            $app->linkedin_link=$request->linkedin_link;
            $app->twitter_link=$request->twitter_link;
            $app->mobile=$request->mobile;
            $app->address=$request->address;
            $app->email=$request->email;
//            $app->country_id=$request->country_id;
            $app->driver_privacy_title_ar=$request->driver_privacy_title_ar;
            $app->driver_privacy_title_en=$request->driver_privacy_title_en;
            $app->driver_privacy_content_en=$request->driver_privacy_content_en;
            $app->driver_privacy_content_ar=$request->driver_privacy_content_ar;

            $app->client_privacy_title_en=$request->client_privacy_title_en;
            $app->client_privacy_title_ar=$request->client_privacy_title_ar;
            $app->client_privacy_content_en=$request->client_privacy_content_en;
            $app->client_privacy_content_ar=$request->client_privacy_content_ar;

            $app->save();

        } catch (\Exception $e) {
            \DB::rollback();
            return $e;
//            return response()->json([
//                'success' => false,
//                'message' => 'Something going wrong'
//            ], 422);
            return redirect()->back()->withInput()->with(['message'=>"Error updated successfully"]);
        }
        return redirect()->back()->withInput()->with(['message'=>"Settings updated successfully"]);
    }


    public function appContentStore(Request $request){
//        $validator = \Validator::make($request->all(), [
//            'driver_wallet_limit' => 'required',
//            'shop_wallet_limit' => 'required',
//            'offer_periods_hr' => 'required',
//            'app_order_commission' => 'required',
//            'tax' => 'required',
//            'delivery_cost' => 'required',
//        ]);
        \DB::beginTransaction();
        try {

            $data['facebook_link']=$request->facebook_link;
            $data['instagram_link']=$request->instagram_link;
            $data['linkedin_link']=$request->linkedin_link;
            $data['twitter_link']=$request->twitter_link;
            $data['mobile']=$request->mobile;
            $data['address']=$request->address;
            $data['email']=$request->email;
            $data['country_id']=$request->country_id;

            AppContent::updateOrCreate(['country_id'=>$request->country_id],$data);


        } catch (\Exception $e) {
            \DB::rollback();
            return $e;
//            return response()->json([
//                'success' => false,
//                'message' => 'Something going wrong'
//            ], 422);
            return redirect()->back()->withInput()->with(['message'=>"Error updated successfully"]);
        }
        return redirect()->back()->withInput()->with(['message'=>"Settings updated successfully"]);
    }
    public function privacyStore(Request $request){
//        $validator = \Validator::make($request->all(), [
//            'driver_wallet_limit' => 'required',
//            'shop_wallet_limit' => 'required',
//            'offer_periods_hr' => 'required',
//            'app_order_commission' => 'required',
//            'tax' => 'required',
//            'delivery_cost' => 'required',
//        ]);
        \DB::beginTransaction();
        try {
            $app =AppContent::findOrFail(1);

            $app->driver_privacy_title_ar=$request->driver_privacy_title_ar;
            $app->driver_privacy_title_en=$request->driver_privacy_title_en;
            $app->driver_privacy_content_en=$request->driver_privacy_content_en;
            $app->driver_privacy_content_ar=$request->driver_privacy_content_ar;

            $app->client_privacy_title_en=$request->client_privacy_title_en;
            $app->client_privacy_title_ar=$request->client_privacy_title_ar;
            $app->client_privacy_content_en=$request->client_privacy_content_en;
            $app->client_privacy_content_ar=$request->client_privacy_content_ar;
            $app->save();

        } catch (\Exception $e) {
            \DB::rollback();
            return $e;
//            return response()->json([
//                'success' => false,
//                'message' => 'Something going wrong'
//            ], 422);
            return redirect()->back()->withInput()->with(['message'=>"Error updated successfully"]);
        }
        return redirect()->back()->withInput()->with(['message'=>"Privacy updated successfully"]);
    }

}
