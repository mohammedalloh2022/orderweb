<?php

namespace App\Http\Controllers;

use App\Models\CategoriesContent;
use App\Models\Country;
use App\Models\Order;
use App\Models\PublicOrder;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Kreait\Firebase\Factory;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $orders_count=Order::where('status','delivered')->count();
        $clients_count=User::where('role','client')->count();
        $drivers_count=User::where('role','delivery_driver')->count();
        $stores_count=CategoriesContent::count();
        return view('welcome2');
    }

    public function welcome($countryId)
    {
        $country=Country::findOrFail($countryId);
        if(!auth()->user()->can('Super admin dashboard '.$country->code)){
            abort(404);
        }

        $now = Carbon::now();

        \Carbon\Carbon::setWeekStartsAt(\Carbon\Carbon::SATURDAY);
        \Carbon\Carbon::setWeekEndsAt(\Carbon\Carbon::FRIDAY);

        $weekStartDate = Carbon::now()->startOfWeek()->format('Y-m-d H:i');
        $weekEndDate = Carbon::now()->endOfWeek()->format('Y-m-d H:i');

        $monthStartDate=Carbon::now()->startOfMonth()->format('Y-m-d H:i');
        $monthEndDate=Carbon::now()->endOfMonth()->format('Y-m-d H:i');

        $yearStartDate=Carbon::now()->startOfYear()->format('Y-m-d H:i');
        $yearEndDate=Carbon::now()->endOfYear()->format('Y-m-d H:i');

        $Order_today=Order::where('status','delivered')->where('country_id',$countryId);
        $order_today_count=$Order_today->where('created_at','LIKE',"{$now->format('Y-m-d')}%")->count();



        $order_today_amount=$Order_today->where('created_at','LIKE',`{$now->format('Y-m-d')}%`)
            ->select(\DB::raw("SUM(total) as today_total"))->pluck('today_total')[0];
        $order_today_app_revenue=$Order_today->where('created_at','LIKE',`{$now->format('Y-m-d')}%`)
             ->select(\DB::raw("SUM(app_revenue) as app_revenue"))->pluck('app_revenue')[0];
        $Order_week=Order::where('status','delivered')->where('country_id',$countryId);
        $week_orders=$Order_week->where(function ($q) use ($weekStartDate,$weekEndDate){
            $q->where('created_at','>=',$weekStartDate)->where('created_at','<=',$weekEndDate);
        });
        $order_week_count=$week_orders->count();
        $order_week_amount=$week_orders->select(\DB::raw("SUM(total) as today_total"))->pluck('today_total')[0];
        $order_week_app_revenue=$week_orders->select(\DB::raw("SUM(app_revenue) as app_revenue"))->pluck('app_revenue')[0];;
        $Order_month=Order::where('status','delivered')->where('country_id',$countryId);
        $month_orders=$Order_month->where(function ($q) use ($monthStartDate,$monthEndDate){
            $q->where('created_at','>=',$monthStartDate)->where('created_at','<=',$monthEndDate);
        });
        $order_month_count=$month_orders->count();
        $order_month_amount=$month_orders->select(\DB::raw("SUM(total) as app_total"))->pluck('app_total')[0];
        $order_month_app_revenue=$month_orders->select(\DB::raw("SUM(app_revenue) as app_revenue"))->pluck('app_revenue')[0];
        $Order_year=Order::where('status','delivered')->where('country_id',$countryId);
        $year_orders=$Order_year->where(function ($q) use ($yearStartDate,$yearEndDate){
            $q->where('created_at','>=',$yearStartDate)->where('created_at','<=',$yearEndDate);
        });
        $order_year_count=$year_orders->count();
        $order_year_amount=$year_orders->select(\DB::raw("SUM(total) as today_total"))->pluck('today_total')[0];;
        $order_year_app_revenue=$year_orders->select(\DB::raw("SUM(app_revenue) as app_revenue"))->pluck('app_revenue')[0];
//        return $order_today_count;
        $statistics=[
            'order_today_count'=>$order_today_count??0,
            'order_today_amount'=>$order_today_amount??0,
            'order_today_app_revenue'=>$order_today_app_revenue??0,

            'order_week_count'=>$order_week_count??0,
            'order_week_amount'=>$order_week_amount??0,
            'order_week_app_revenue'=>$order_week_app_revenue??0,

            'order_month_count'=>$order_month_count??0,
            'order_month_amount'=>$order_month_amount??0,
            'order_month_app_revenue'=>$order_month_app_revenue??0,

            'order_year_count'=>$order_year_count??0,
            'order_year_amount'=>$order_year_amount??0,
            'order_year_app_revenue'=>$order_year_app_revenue??0,
        ];

        $orders_count=Order::where('status','delivered')->where('country_id',$countryId)->count();
        $clients_count=User::where('role','client')->where('country_id',$countryId)->count();
        $drivers_count=User::where('role','delivery_driver')->where('country_id',$countryId)->where('is_documented',1)->count();
        $stores_count=CategoriesContent::where('parent_id',null)->where('country_id',$countryId)->count();
        $clients=User::where('role',"client")->where('country_id',$countryId)->orderBy('created_at','desc')->take(10)->get();
        $orders=Order::orderby('created_at','desc')->where('country_id',$countryId)->with('client','shop')->take(10)->get();
        $four_station_revenue=Order::where('status','delivered')->where('country_id',$countryId)->select(\DB::raw("SUM(app_revenue) as app_revenue"))->pluck('app_revenue')[0];
        $public_order_revenue=PublicOrder::where('status','delivered')->where('country_id',$countryId)->select(\DB::raw("SUM(app_revenue) as app_revenue"))->pluck('app_revenue')[0];
        return view('welcome2', compact('countryId','orders_count','clients_count',
            'drivers_count','stores_count','clients','orders','statistics','four_station_revenue','public_order_revenue'));
//   else:
//       if(auth()->user()->can('category.index'))
//        return redirect(route('category.index'));
//       else if(auth()->user()->can('Store list'))
//          return  redirect(route('shops.index'));
//       else if(auth()->user()->can('Store list'))
//         return   redirect(route('shops.index'));
//       else if(auth()->user()->can('New offers list'))
//         return   redirect(route('mange.offers.index'));
//       else if(auth()->user()->can('Offers list'))
//           return   redirect(route('mange.offers.list.index'));
//       else if(auth()->user()->can('Order list'))
//           return   redirect(route('mange.orders.index'));
//       else if(auth()->user()->can('New order list'))
//           return   redirect(route('mange.pending.orders.index'));
//       else if(auth()->user()->can('Client list'))
//           return   redirect(route('mange.client.index'));
//       else if(auth()->user()->can('Drivers list'))
//           return   redirect(route('mange.delivery.index'));
//       else if(auth()->user()->can('New driver list'))
//           return   redirect(route('mange.delivery.documented'));
//       else
////           $this->guard()->logout();
//           abort(403);
//
////       else if(auth()->user()->can('App setting'))
////           return   redirect(route('mange.delivery.documented'));
////       else if(auth()->user()->can('App privacy content'))
////           return   redirect(route('mange.delivery.documented'));
//   endif;

    }

    public function test($countryId){
        $countries= Country::get()->pluck('code');
        return $countries;
        $factory = (new Factory)->withServiceAccount(public_path().'/orderstation-33cc9-firebase-adminsdk-2lozq-97491469b9.json');
        $database = $factory->createDatabase();
        $data = $database->getReference('delivery_app_tracking')->orderByChild('country_id')->equalTo((int)$countryId)->getValue();

        dd($data) ;
    }
    public function shopDashbaord(){
        $shop= auth()->user()->shop;
        $now = Carbon::now();

        \Carbon\Carbon::setWeekStartsAt(\Carbon\Carbon::SATURDAY);
        \Carbon\Carbon::setWeekEndsAt(\Carbon\Carbon::FRIDAY);

        $weekStartDate = Carbon::now()->startOfWeek()->format('Y-m-d H:i');
        $weekEndDate = Carbon::now()->endOfWeek()->format('Y-m-d H:i');

        $monthStartDate=Carbon::now()->startOfMonth()->format('Y-m-d H:i');
        $monthEndDate=Carbon::now()->endOfMonth()->format('Y-m-d H:i');

        $yearStartDate=Carbon::now()->startOfYear()->format('Y-m-d H:i');
        $yearEndDate=Carbon::now()->endOfYear()->format('Y-m-d H:i');
        $shopBranchList = CategoriesContent::where('parent_id', $shop->id)->get()->pluck('id')->toArray();
        $Order_today=Order::where('status','delivered')->where(function ($q) use ($shop, $shopBranchList) {
            $q->where('shop_id', $shop->id)->orWhereIn('shop_id', $shopBranchList);
        });
        $order_today_count=$Order_today->where('created_at','LIKE',"{$now->format('Y-m-d')}%")->count();
        $order_today_amount=$Order_today->where('created_at','LIKE',`{$now->format('Y-m-d')}%`)
            ->select(\DB::raw("SUM(shop_revenue) as shop_revenue"))->pluck('shop_revenue')[0];
//        $order_today_app_revenue=$Order_today->where('created_at','LIKE',`{$now->format('Y-m-d')}%`)->sum('app_revenue');
        $Order_week=Order::where('status','delivered')->where(function ($q) use ($shop, $shopBranchList) {
            $q->where('shop_id', $shop->id)->orWhereIn('shop_id', $shopBranchList);
        });
        $week_orders=$Order_week->where(function ($q) use ($weekStartDate,$weekEndDate){
            $q->where('created_at','>=',$weekStartDate)->where('created_at','<=',$weekEndDate);
        });
        $order_week_count=$week_orders->count();
        $order_week_amount=$week_orders->select(\DB::raw("SUM(shop_revenue) as shop_revenue"))->pluck('shop_revenue')[0];
//        $order_week_app_revenue=$week_orders->sum('app_revenue');
        $Order_month=Order::where('status','delivered')->where(function ($q) use ($shop, $shopBranchList) {
            $q->where('shop_id', $shop->id)->orWhereIn('shop_id', $shopBranchList);
        });
        $month_orders=$Order_month->where(function ($q) use ($monthStartDate,$monthEndDate){
            $q->where('created_at','>=',$monthStartDate)->where('created_at','<=',$monthEndDate);
        });
        $order_month_count=$month_orders->count();
        $order_month_amount=$month_orders->select(\DB::raw("SUM(shop_revenue) as shop_revenue"))->pluck('shop_revenue')[0];
//        $order_month_app_revenue=$month_orders->sum('app_revenue');
        $Order_year=Order::where('status','delivered')->where(function ($q) use ($shop, $shopBranchList) {
            $q->where('shop_id', $shop->id)->orWhereIn('shop_id', $shopBranchList);
        });
        $year_orders=$Order_year->where(function ($q) use ($yearStartDate,$yearEndDate){
            $q->where('created_at','>=',$yearStartDate)->where('created_at','<=',$yearEndDate);
        });
        $order_year_count=$year_orders->count();
        $order_year_amount=$year_orders->select(\DB::raw("SUM(shop_revenue) as shop_revenue"))->pluck('shop_revenue')[0];
//        $order_year_app_revenue=$year_orders->sum('app_revenue');
//        return $order_today_count;
        $statistics=[
            'order_today_count'=>$order_today_count??0,
            'order_today_amount'=>$order_today_amount??0,
//            'order_today_app_revenue'=>$order_today_app_revenue,

            'order_week_count'=>$order_week_count??0,
            'order_week_amount'=>$order_week_amount??0,
//            'order_week_app_revenue'=>$order_week_app_revenue,

            'order_month_count'=>$order_month_count??0,
            'order_month_amount'=>$order_month_amount??0,
//            'order_month_app_revenue'=>$order_month_app_revenue,

            'order_year_count'=>$order_year_count??0,
            'order_year_amount'=>$order_year_amount??0,
//            'order_year_app_revenue'=>$order_year_app_revenue,
        ];

        $orders_count=Order::where('status','delivered')->where(function ($q) use ($shop, $shopBranchList) {
            $q->where('shop_id', $shop->id)->orWhereIn('shop_id', $shopBranchList);
        })->count();
        $branch_count=CategoriesContent::where('parent_id',$shop->id)->count();
        $orders=Order::with('client','shop')->where('shop_id',$shop->id)->orderby('created_at','desc')->take(10)->get();
//        $wallet=Order::where('shop_id',$shop->id)->where('wallet_calc_with_store',0)->select(\DB::raw("SUM(order_store_wallet) as storeWallet"))->pluck('storeWallet')[0];

        $shopBranchList = CategoriesContent::where('parent_id',$shop->id)->get()->pluck('id')->toArray();
        $wallet =Order::where(function ($q) use ($shop, $shopBranchList) {
            $q->where('shop_id', $shop->id)->orWhereIn('shop_id', $shopBranchList);
        })->where('status', 'delivered')->where('accounted_with_app',0)->sum('order_store_wallet');

        return view('shopDashboard', compact('orders_count','branch_count','statistics','orders','wallet','shop'));

    }

    public function home(){
        return view('home');
    }
    public function check(){
        $carbonBefore=Carbon::now()->subMinutes(10)->format('Y-m-d H:m:i');
        return $carbonBefore;
        return now()->diffInMinutes('2020-06-18 07:30:00');
    }


    public function branchDashbaord(){
        $shop= auth()->user()->shop;
        $now = Carbon::now();

        \Carbon\Carbon::setWeekStartsAt(\Carbon\Carbon::SATURDAY);
        \Carbon\Carbon::setWeekEndsAt(\Carbon\Carbon::FRIDAY);

        $weekStartDate = Carbon::now()->startOfWeek()->format('Y-m-d H:i');
        $weekEndDate = Carbon::now()->endOfWeek()->format('Y-m-d H:i');

        $monthStartDate=Carbon::now()->startOfMonth()->format('Y-m-d H:i');
        $monthEndDate=Carbon::now()->endOfMonth()->format('Y-m-d H:i');

        $yearStartDate=Carbon::now()->startOfYear()->format('Y-m-d H:i');
        $yearEndDate=Carbon::now()->endOfYear()->format('Y-m-d H:i');

        $Order_today=Order::where('status','delivered')->where('shop_id',$shop->id);
        $order_today_count=$Order_today->where('created_at','LIKE',"{$now->format('Y-m-d')}%")->count();



        $order_today_amount=$Order_today->where('created_at','LIKE',`{$now->format('Y-m-d')}%`)
            ->select(\DB::raw("SUM(total) as total"))->pluck('total')[0];
        $order_today_app_revenue=$Order_today->where('created_at','LIKE',`{$now->format('Y-m-d')}%`)
            ->select(\DB::raw("SUM(shop_revenue) as app_revenue"))->pluck('app_revenue')[0];
        $Order_week=Order::where('status','delivered')->where('shop_id',$shop->id);
        $week_orders=$Order_week->where(function ($q) use ($weekStartDate,$weekEndDate){
            $q->where('created_at','>=',$weekStartDate)->where('created_at','<=',$weekEndDate);
        });
        $order_week_count=$week_orders->count();
        $order_week_amount=$week_orders->select(\DB::raw("SUM(total) as total"))->pluck('total')[0];
        $order_week_app_revenue=$week_orders   ->select(\DB::raw("SUM(shop_revenue) as app_revenue"))->pluck('app_revenue')[0];
        $Order_month=Order::where('status','delivered')->where('shop_id',$shop->id);
        $month_orders=$Order_month->where(function ($q) use ($monthStartDate,$monthEndDate){
            $q->where('created_at','>=',$monthStartDate)->where('created_at','<=',$monthEndDate);
        });
        $order_month_count=$month_orders->count();
        $order_month_amount=$month_orders->select(\DB::raw("SUM(total) as total"))->pluck('total')[0];
        $order_month_app_revenue=$month_orders->select(\DB::raw("SUM(shop_revenue) as app_revenue"))->pluck('app_revenue')[0];
        $Order_year=Order::where('status','delivered')->where('shop_id',$shop->id);
        $year_orders=$Order_year->where(function ($q) use ($yearStartDate,$yearEndDate){
            $q->where('created_at','>=',$yearStartDate)->where('created_at','<=',$yearEndDate);
        });
        $order_year_count=$year_orders->count();
        $order_year_amount=$year_orders->select(\DB::raw("SUM(total) as total"))->pluck('total')[0];
        $order_year_app_revenue=$year_orders->select(\DB::raw("SUM(shop_revenue) as app_revenue"))->pluck('app_revenue')[0];;
//        return $order_today_count;
        $statistics=[
            'order_today_count'=>$order_today_count??0,
            'order_today_amount'=>$order_today_amount??0,
            'order_today_app_revenue'=>$order_today_app_revenue??0,

            'order_week_count'=>$order_week_count??0,
            'order_week_amount'=>$order_week_amount??0,
            'order_week_app_revenue'=>$order_week_app_revenue??0,

            'order_month_count'=>$order_month_count??0,
            'order_month_amount'=>$order_month_amount??0,
            'order_month_app_revenue'=>$order_month_app_revenue??0,

            'order_year_count'=>$order_year_count??0,
            'order_year_amount'=>$order_year_amount??0,
            'order_year_app_revenue'=>$order_year_app_revenue??0,
        ];

        $orders_count=Order::where('status','delivered')->where('shop_id',$shop->id)->count();
        $orders=Order::orderby('created_at','desc')->with('client','shop')->where('shop_id',$shop->id)->take(10)->get();
//        $wallet=$shop->wallet;
//        $wallet=Order::where('shop_id',$shop->id)->where('wallet_calc_with_store_branch',0)->select(\DB::raw("SUM(order_store_wallet) as storeWallet"))->pluck('storeWallet')[0];

        return view('branchDashboard', compact('orders_count','statistics','orders','shop'));

    }
}
