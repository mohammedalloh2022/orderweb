<?php

namespace App\Http\Controllers;

use App\Models\AppSetting;
use App\Models\CategoriesContent;
use App\Models\Country;
use App\Models\Order;
use App\Models\Rating;
use App\User;
use Avatar;
use Carbon\Carbon;
use DB;
use File;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;
use Image;
use Storage;
use Validator;
use function foo\func;

class ShopController extends Controller
{


    public function index()
    {
        $shops = CategoriesContent::paginate(10);
        return view('shops.index', compact('shops'));
    }

    public function getCountryIndex($countryId)
    {
        $country = Country::findOrFail($countryId);
        if (!auth()->user()->can('Store list ' . $country->code)) {
            abort(404);
        }
        $shops = CategoriesContent::where('country_id', $countryId)->paginate(10);
        return view('shops.index', compact('shops', 'countryId'));
    }

    public function view(CategoriesContent $shop)
    {
        return view('shops.view', compact('shop'));
    }

    public function branchview(CategoriesContent $shop)
    {
        return view('shops.branch.view', compact('shop'));
    }

    public function editTimes(CategoriesContent $shop)
    {
        return view('shops.times', compact('shop'));
    }

    public function updateTimes(Request $request, CategoriesContent $shop)
    {
        $array = [];
        foreach ($request->open as $key => $value) {
            if (isset($value) && isset($request->close[$key]))
                $array[$key] = ['open' => $value, 'close' => $request->close[$key]];
        }
        if ($array > 0) {
            $shop->business_hours = json_encode($array);
            $shop->save();
            session()->flash('success', 'تم حفظ اوقات العمل بنجاح');
            return redirect()->back();
        }
        session()->flash('error', 'تم حفظ اوقات العمل بنجاح');
        return redirect()->back();

    }

    public function search(Request $request)
    {
        $shops = new CategoriesContent();
        $shops = $shops->where('parent_id', null)->with('category', 'user', 'country');

        if (request()->has('filter') && request('filter')) {
            $filter = request('filter');
            $shops = $shops->where(function ($q) use ($filter) {
                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
            })->orWhere('mobile', 'LIKE', "%$filter%");
        }
        if (isset($request->cat_id)) {
            $shops = $shops->where('content_id', $request->cat_id);
        }
        if ($request->country_id) {
            $shops = $shops->where('country_id', $request->country_id);
        }
        if (request()->has('name') && request('name')) {
            $filter = request('name');
            $shops = $shops->where(function ($q) use ($filter) {
                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
            });
        }
        if (request()->has('mobile') && request('mobile')) {
            $filter = request('mobile');
            $shops = $shops->where('mobile', $filter);
        }
        if (request()->has('wallet') && request('wallet')) {
            $filter = request('wallet');
            if ($filter == 'exceed_station_wallet') {

                $shop_wallet_limit = AppSetting::where('country_id', $request->country_id)->latest()->pluck('shop_wallet_limit')->first();
                if ($shop_wallet_limit > 0) {
                    $shops = $shops->where('wallet', '>=', $shop_wallet_limit);
                } else {
                    $shops = $shops->where('wallet', '<=', $shop_wallet_limit);
                }
            }
        }
//        if (request()->has('sort')) {
//            $sort = json_decode(request('sort'), true);
////            $categories = $categories->orderBy(($sort['fieldName'] ?? 'id'), $sort['order']);
//        }


        $shops = $shops->orderBy('created_at', 'desc')->paginate(15);

        return response()->json(compact('shops'));
    }

    public function shopsList($countryId)
    {
        $shops = CategoriesContent::where('parent_id', null)->where('country_id', $countryId)->get();
        return response()->json(compact('shops'));
    }

    public function branches($shop)
    {
        $shops = new CategoriesContent();
        $shops = $shops->with('user');
        $shops = $shops->where('parent_id', $shop);
        if (request()->has('filter') && request('filter')) {
            $filter = request('filter');
            $shops = $shops->where(function ($q) use ($filter) {
                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
            });
        }
        if (request()->has('sort')) {
            $sort = json_decode(request('sort'), true);
//            $categories = $categories->orderBy(($sort['fieldName'] ?? 'id'), $sort['order']);
        }
        $shops = $shops->orderBy('created_at', 'desc')->paginate(15);
        return response()->json(compact('shops'));
    }

    public function orders(Request $request, CategoriesContent $shop)
    {
        $shopBranchList = CategoriesContent::where('parent_id', $shop->id)->get()->pluck('id')->toArray();

        $orders = new Order();
        $orders = $orders->where('shop_id', $shop->id)->where('status', '!=', 'pending');


        if ($request->filter) {
            $filter = request('filter');
            $orders = $orders->where('invoice_number', 'LIKE', "%$filter");
        }
//        $orders= $orders->where(function($query){
//            $query->where('status','delivered')->orWhere('status','cancelled');
//        });

        $orders = $orders->with("cancelReason", "client", "deliveryUser", "shop");

        $orders = $orders->where(function ($q) use ($shop, $shopBranchList) {
            $q->where('shop_id', $shop->id)->orWhereIn('shop_id', $shopBranchList);
        });


        $orders = $orders->orderBy('created_at', 'desc');
        $wallet = null;
        if (($request->filled('report_period'))) {
            if ($request->report_period == 'custom') {
                $dates = explode(',', $request->custom_period);
                $periodDate = ['start_at' => Carbon::parse($dates[0])->format('Y-m-d 00:00:00'), 'end_at' => Carbon::parse($dates[1])->format('Y-m-d 23:59:59')];
            } else {
                $periodDate = filterPeriod($request->report_period);
            }
//            $wallet = $orders->where('status', 'delivered')
//                ->where('created_at','>=',$periodDate['start_at'])
//                ->where('created_at','<=',$periodDate['end_at'])
//                ->sum('order_store_wallet');
            $orders = $orders->where('created_at', '>=', $periodDate['start_at'])->where('created_at', '<=', $periodDate['end_at']);
            $orders = $orders->paginate(20);
        } else {
//            $wallet=$orders->where('status', 'delivered')->sum('order_store_wallet');
            $orders = $orders->paginate(20);
        }
        $totalWallet = $orders->where('status', 'delivered')->sum('order_store_wallet');
        $wallet = number_format($shop->wallet, 2);


        return response()->json(compact('orders', 'wallet', 'totalWallet'));
    }

    public function branchOrders(Request $request, $shop)
    {
        $shopBranchList = CategoriesContent::where('parent_id', $shop)->get()->pluck('id')->toArray();

        $orders = new Order();
        if (!empty($request->filter)) {
            $filter = request('filter');
            $orders = $orders->where('invoice_number', 'LIKE', "%$filter%");


        }
        $orders = $orders->where('status', '!=', 'pending')->where('wallet_calc_with_store_branch', 0);
        $orders = $orders->with("client", "deliveryUser", "shop", 'cancelReason');

        $orders = $orders->where(function ($q) use ($shop, $shopBranchList) {
            $q->where('shop_id', $shop)->orWhereIn('shop_id', $shopBranchList);
        });
        $orders = $orders->orderBy('created_at', 'desc');
        $wallet = null;
        if (isset($request->report_period)) {

//            \Illuminate\Pagination\Paginator::currentPageResolver(function ()  {
//                return 1;
//            });

            if ($request->report_period == 'custom') {
                $dates = explode(',', $request->custom_period);
                $periodDate = ['start_at' => Carbon::parse($dates[0])->format('Y-m-d 00:00:00'), 'end_at' => Carbon::parse($dates[1])->format('Y-m-d 23:59:59')];
            } else {

                $periodDate = filterPeriod($request->report_period);
            }
            $wallet = $orders->where('created_at', '>=', $periodDate['start_at'])->where('created_at', '<=', $periodDate['end_at'])->sum('order_store_wallet');
            $orders = $orders->where('created_at', '>=', $periodDate['start_at'])->where('created_at', '<=', $periodDate['end_at']);
            $orders = $orders->paginate(20);
        } else {
            $wallet = $orders->where('status', '!=', 'pending')->sum('order_store_wallet');
            $orders = $orders->paginate(20);
        }
        $totalWallet = $orders->where('status', '!=', 'pending')->sum('order_store_wallet');
        $wallet = number_format($wallet, 2);
        return response()->json(compact('orders', 'wallet', 'totalWallet'));
    }

    public function pendingOrders($shop)
    {
        $shopBranchList = CategoriesContent::where('parent_id', $shop)->get()->pluck('id')->toArray();

        $orders = new Order();
        $orders = $orders->where('status', 'pending');
        $orders = $orders->with("client", "deliveryUser", "shop");
        $orders = $orders->where(function ($q) use ($shop, $shopBranchList) {
            $q->where('shop_id', $shop)->orWhereIn('shop_id', $shopBranchList);
        });
        $orders = $orders->orderBy('created_at', 'desc')->paginate(15);
        return response()->json(compact('orders'));
    }

    public function reviews($shop)
    {
        $reviews = new Rating();
        $reviews = $reviews->where('content_id', $shop)->where('content_type', CategoriesContent::class)->with('user');
//        if(request()->has('filter') && request('filter') ) {
//            $filter = request('filter');
//            $shops = $shops->where(function($q) use($filter){
//                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
//            });
//
//        }
//        if(request()->has('sort')) {
//            $sort = json_decode(request('sort'), true);
////            $categories = $categories->orderBy(($sort['fieldName'] ?? 'id'), $sort['order']);
//        }
        $reviews = $reviews->orderBy('created_at', 'desc')->paginate(15);

        return response()->json(compact('reviews'));
    }


    public function changeStatus(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'status' => 'required',
        ]);
        $shop = CategoriesContent::with('user', 'branch.user')->findOrFail($request->id);
        $shop->is_active = $request->status;
        $shop->save();

        if ($shop->user) {
            $user = User::find($shop->user->id);
            $user->active = $request->status;
            $user->save();
        }

        if (!empty($shop->branch)) {


            foreach ($shop->branch as $bbranch) {
                $bbranch = CategoriesContent::find($bbranch->id);
                $bbranch->is_active = $request->status;
                $bbranch->save();
                if ($bbranch->user) {
                    $user = User::find($bbranch->user->id);
                    $user->active = $request->status;
                    $user->save();
                }

            }
        }

        return response()->json([
            'status' => 'success',
            'msg' => 'Status changed successfully .'
        ], 200);
    }

    public function storeBranch(Request $request)
    {

        $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
            'address_en' => 'required',
            'address_ar' => 'required',
            'country_id' => 'required',
            'user_email' => 'required|string|email|unique:users,email',
            'user_password' => 'required',
            'user_role' => 'required',
            'mobile' => 'required|unique:users,mobile',
        ]);
        if (!$request->shop_id) {
            $request->validate([
                'cat_id' => 'required',
            ]);
        }
        \DB::beginTransaction();
        try {

            $user = new User();
            $user->name = $request->name_en;
            $user->email = $request->user_email;
            $user->role = $request->user_role;
            $user->password = bcrypt($request->user_password);
            $user->active = true;
            $user->save();
//            $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
//            Storage::put('users/' . $user->id . '/avatar.png', (string)$avatar);
            $register_number = null;
            $reg_number = CategoriesContent::latest()->pluck('reg_no')->first();

            if ((int)$reg_number) {
                $register_number = (int)$reg_number + 1;
            } else {
                $register_number = 30000000001;
            }
            $shop = new CategoriesContent();
            $shop->name_en = $request->name_en;
            $shop->country_id = $request->country_id;
            $shop->name_ar = $request->name_ar;
            $shop->address_en = $request->address_en;
            $shop->address_ar = $request->address_ar;
            $shop->mobile = $request->mobile;
//            $shop->reg_no = $register_number;
            $shop->lat = $request->lat;
            $shop->lng = $request->lng;
            $shop->user_id = $user->id;
            if ($request->shop_id) {
                $shop->parent_id = $request->shop_id;
                $cat_id = CategoriesContent::find($request->shop_id);
                $shop->content_id = $cat_id->content_id;
            } else {
                $shop->content_id = $request->cat_id;
            }
            $shop->save();
            \DB::commit();


        } catch (\Exception $e) {

            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }
        $message = 'Successfully created new branch!';
        return response()->json(compact('message'), 200);
    }

    public function updateBranch(Request $request, $shop)
    {

        $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
            'address_en' => 'required',
            'address_ar' => 'required',
//            'country_id' => 'required',
//            'shop_logo' => 'required',
//            'shop_cover' => 'required',
//            'start_work_at' => 'required',
//            'end_work_at' => 'required',
            'email' => Rule::unique('users')->ignore($request->user_id, 'id'),
            'mobile' => 'required',
            'mobile' => Rule::unique('users')->ignore($request->user_id, 'id'),
//            'user_email' => 'required|string|email|unique:users,email',
//            'user_password' => 'required',
        ]);
        if (!$request->shop_id) {

            $request->validate([
                'cat_id' => 'required',
            ]);
        }
        \DB::beginTransaction();
        try {

            $user = User::findOrFail($request->user_id);
            if ($request->user_password) {
                $user->password = bcrypt($request->user_password);
//                $user->password = Hash::make($request->user_password);
            }
            if ($request->email) {
                $user->email = $request->email;
            }
            $user->save();


            $shop = CategoriesContent::findOrFail($shop);
            $shop->name_en = $request->name_en;
            $shop->name_ar = $request->name_ar;
            $shop->address_en = $request->address_en;
            $shop->address_ar = $request->address_ar;
//            $shop->start_work_at = $request->start_work_at;
//            $shop->end_work_at = $request->end_work_at;
            $shop->country_id = @$request->country_id;
            $shop->mobile = $request->mobile;
            $shop->lat = $request->lat;
            $shop->lng = $request->lng;
            if (!$request->shop_id) {
                $shop->content_id = $request->cat_id;

            }


            $shop->save();
            \DB::commit();

        } catch (\Exception $e) {

            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }
        $message = 'Branch updated successfully!';
        return response()->json(compact('message'), 200);
    }

    public function store(Request $request)
    {

        $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
            'address_en' => 'required',
            'address_ar' => 'required',
            'shop_logo' => 'required',
            'shop_cover' => 'required',
            'start_work_at' => 'required',
            'end_work_at' => 'required',
            'country_id' => 'required',
            'cat_id' => 'required',
            'app_commission' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',

            'mobile' => 'required',
            'user_email' => 'required|string|email|unique:users,email',
            'user_password' => 'required',
            'user_role' => 'required',
            'mobile' => 'required|unique:users,mobile',
        ]);
        if (!$request->shop_id) {
            $request->validate([
                'cat_id' => 'required',
            ]);
        }
        \DB::beginTransaction();
        try {

            $user = new User();
            $user->name = $request->name_en;
            $user->email = $request->user_email;
            $user->role = $request->user_role;
            $user->password = bcrypt($request->user_password);
            $user->active = true;
            $user->save();

//            $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
//            Storage::put('users/' . $user->id . '/avatar.png', (string)$avatar);

            $register_number = null;
            $reg_number = CategoriesContent::latest()->pluck('reg_no')->first();

            if ((int)$reg_number) {
                $register_number = (int)$reg_number + 1;
            } else {
                $register_number = 30000000001;
            }
            $shop = new CategoriesContent();
            $shop->name_en = $request->name_en;
            $shop->name_ar = $request->name_ar;
            $shop->address_en = $request->address_en;
            $shop->address_ar = $request->address_ar;
            $shop->start_work_at = $request->start_work_at;
            $shop->end_work_at = $request->end_work_at;
            $shop->country_id = @$request->country_id;
            $shop->mobile = $request->mobile;
            $shop->reg_no = $register_number;
            $shop->country_id = $request->country_id;
            $shop->app_commission = $request->app_commission;
            $shop->lat = $request->lat;
            $shop->lng = $request->lng;
            $shop->user_id = $user->id;
            if ($request->shop_id) {
                $shop->parent_id = $request->shop_id;
                $cat_id = CategoriesContent::find($request->shop_id);
                $shop->content_id = $cat_id->content_id;
            } else {
                $shop->content_id = $request->cat_id;
            }
            if ($request->shop_logo) {
                $img = Image::make($request->shop_logo);
                $extension = explode('/', $img->mime)[1];
                $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                $destinationPath = public_path('uploads/shops/');
                File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                $img->save($destinationPath . $fileNameToStore);
                $shop->image = $fileNameToStore;
            }

            if ($request->shop_cover) {
                $img = Image::make($request->shop_cover);
                $extension = explode('/', $img->mime)[1];
                $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                $destinationPath = public_path('uploads/shops/');
                File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                $img->save($destinationPath . $fileNameToStore);
                $shop->cover_image = $fileNameToStore;
            }

            $shop->save();
            \DB::commit();


        } catch (\Exception $e) {

            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }
        $message = 'Successfully created new Shop!';
        return response()->json(compact('message'), 200);
    }

    public function update(Request $request, $shop)
    {

        $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
            'address_en' => 'required',
            'address_ar' => 'required',
            'shop_logo' => 'required',
            'shop_cover' => 'required',
            'start_work_at' => 'required',
            'end_work_at' => 'required',
            'app_commission' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'email' => Rule::unique('users')->ignore($request->user_id, 'id'),
            'mobile' => 'required',
            'mobile' => Rule::unique('users')->ignore($request->user_id, 'id'),
//            'user_email' => 'required|string|email|unique:users,email',
//            'user_password' => 'required',
        ]);
        if (!$request->shop_id) {

            $request->validate([
                'cat_id' => 'required',
            ]);
        }
        \DB::beginTransaction();
        try {
            $data = [
                "id" => $request->user_id,
            ];
            if ($request->password) {
                $data['password'] = bcrypt($request->password);

            }
            if ($request->email) {
                $data['email'] = $request->email;;

            }
            $user = User::updateOrCreate(['id' => $request->user_id], $data);


            $shop = CategoriesContent::findOrFail($shop);
            $shop->name_en = $request->name_en;
            $shop->name_ar = $request->name_ar;
            $shop->address_en = $request->address_en;
            $shop->address_ar = $request->address_ar;
            $shop->start_work_at = $request->start_work_at;
            $shop->end_work_at = $request->end_work_at;
            $shop->mobile = $request->mobile;
            $shop->lat = $request->lat;
            $shop->app_commission = $request->app_commission;
            $shop->lng = $request->lng;
            if (!$request->shop_id) {
                $shop->content_id = $request->cat_id;

            }


            if (!filter_var($request->shop_logo, FILTER_VALIDATE_URL)) {
                if ($request->shop_logo) {
                    $img = Image::make($request->shop_logo);
                    $extension = explode('/', $img->mime)[1];
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/shops/');
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . $fileNameToStore);
                    $shop->image = $fileNameToStore;
                }
            }
            if (!filter_var($request->shop_cover, FILTER_VALIDATE_URL)) {
                if ($request->shop_cover) {
                    $img = Image::make($request->shop_cover);
                    $extension = explode('/', $img->mime)[1];
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/shops/');
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . $fileNameToStore);
                    $shop->cover_image = $fileNameToStore;
                }
            }

            $shop->save();
            \DB::commit();

        } catch (\Exception $e) {

            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }
        $message = 'Shop updated successfully!';
        return response()->json(compact('message'), 200);
    }

    public function destroy(CategoriesContent $shop)
    {
        $shop->delete();
        $message = "Shop deleted successfully";
        return response()->json(compact('message'));
    }
}
