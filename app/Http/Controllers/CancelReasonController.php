<?php

namespace App\Http\Controllers;

use App\Models\RejectReasons;
use App\Models\Category;
use Illuminate\Http\Request;
class CancelReasonController extends Controller
{
    public function index(){
        $categories=RejectReasons::paginate(10);
        return view('cancelReason.index',compact('categories'));
    }

    public function list(){
        $categories=RejectReasons::all();
        return response()->json(compact('categories'));
    }

    public function search()
    {
        $categories = new RejectReasons();

        if(request()->has('filter') && request('filter') ) {
            $filter = request('filter');
            $categories = $categories->where(function($q) use($filter){
                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
            });
        }
        if(request()->has('sort')) {
            $sort = json_decode(request('sort'), true);
//            $categories = $categories->orderBy(($sort['fieldName'] ?? 'id'), $sort['order']);
        }
        $categories = $categories->orderBy('created_at','desc')->paginate(15);
        return response()->json(compact('categories'));
    }
//categories
    public function store(Request $request){
        $request->validate([
            'cat_name_en' => 'required',
            'cat_name_ar' => 'required',
        ]);

        \DB::beginTransaction();
        try {
            $category = new RejectReasons();
            $category->reason_en = $request->cat_name_en;
            $category->reason_ar = $request->cat_name_ar;
            $category->save();
            \DB::commit();



        } catch (\Exception $e) {
//            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }
        $message='Successfully created reason!';
        return response()->json(compact('message'),200);
    }

    public function update(Request $request,$id){
        $request->validate([
            'cat_name_en' => 'required',
            'cat_name_ar' => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $category =  RejectReasons::find($id);
            $category->reason_en = $request->cat_name_en;
            $category->reason_ar = $request->cat_name_ar;


            $category->save();
            \DB::commit();



        } catch (\Exception $e) {
//            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }
        $message='Successfully update reason!';
        return response()->json(compact('message'),200);
    }

    public function destroy( RejectReasons $ad)
    {
        $ad->delete();
        $message ="Reason deleted successfully";
        return response()->json(compact('message'));
        return response()->json(['message' =>'Item deleted successfully'],200);

    }
    public function changeStatus(Request $request) {
        $request->validate([
            'id' => 'required',
            'status' => 'required',
        ]);
        $category = RejectReasons::findOrFail($request->id);
        $category->is_active = $request->status;
        $category->save();
        return response()->json([
            'status' => 'success',
            'msg' => 'Status changed successfully .'
        ], 200);
    }

}
