<?php

namespace App\Http\Controllers;

use App\Models\CategoriesContent;
use App\Models\Order;
use App\Models\ShopTransaction;
use App\Models\Transaction;
use App\Notifications\SignupActivate;
use App\User;
use Illuminate\Http\Request;

class ShopTransactionController extends Controller
{
    public function index(CategoriesContent $shop){

        return view('shops.payments',compact('shop'));
    }

    public function walletTransactions(Request $request,$shopId){
        $wallet_transactions=ShopTransaction::where('shop_id',$shopId)->paginate(10);
        return response()->json(compact('wallet_transactions'));
    }

    public function export(Request $request){
        $transactions=ShopTransaction::where('shop_id',$request->shop_id)->paginate(10);
        return response(view('excel.shop-transactions',compact('transactions')));
    }
    public function storeWalletTransAction(Request $request,CategoriesContent $shop)
    {
        $request->validate([
            'amount' => 'required',
//            'invoice_no' => 'required',
            'type' => 'required',
        ]);

        if ($shop) {
            $prev_wallet=$shop->wallet?(float)$shop->wallet:0;
//
            if($request->type=='withdraw'){
                $shop->wallet -= ((float)$request->amount);
            }else{
                $shop->wallet += ((float)$request->amount);
            }

            $shop->save();
            if($shop->wallet==0){
                $shopBranchList = CategoriesContent::where('parent_id', $shop->id)->get()->pluck('id')->toArray();
                $orders = new Order();

                  if($shop->parent_id==null && empty($shop->parent_id)){
                      $orders->where(function ($q) use ($shop, $shopBranchList) {
                          $q->where('shop_id', $shop->id)->orWhereIn('shop_id', $shopBranchList);
                      })->update(['accounted_with_app'=>true]);
                  }

                  if($shop->parent_id != null && !empty($shop->parent_id)){
                          $orders->where('shop_id',$shop->id)->update(['wallet_calc_with_store_branch'=>true]);
                  }





            }
            $transaction =new ShopTransaction();
            $transaction->prev_wallet=$prev_wallet;
            $transaction->amount=$request->amount;
            $transaction->invoice_no=$request->invoice_no;
            $transaction->next_wallet=$shop->wallet;
            $transaction->type=$request->type;
            $transaction->shop_id=$shop->id;
            $transaction->save();

            if($shop->wallet==0) {
                $user = User::find($shop->user_id);
                if ($user) {
                    $message = [
                        'msg' => "Your wallet calculated #" . $request->invoice_no,
                        'title' => "Your wallet calculated #" . $request->invoice_no,
                        'order_id' => "",
                        'order_no' => "",
                        'type' => '4station',
                        'type_of_receive' => '',
                        'country_id'=>@$shop->country_id
                    ];
                    $user->notify(new SignupActivate($message));
                    sendFcmWeb($user->fcm_token, $message);
                }
            }
            return response()->json([
                'status' => 'success',
                'msg' => ' Transaction stored  successfully .'
            ], 200);
        }
        return response()->json([
            'status' => 'failed',
            'msg' => 'failed'
        ], 422);
    }

}
