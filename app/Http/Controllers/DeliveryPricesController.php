<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\DeliveryPrice;
use Illuminate\Http\Request;

class DeliveryPricesController extends Controller
{
    public function index($countryId){
//        $categories=Category::paginate(10);
        return view('delivery-price',compact('countryId'));
    }


    public function list(){
        $categories=DeliveryPrice::all();
        return response()->json(compact('categories'));
    }

    public function search($countryId)
    {
        $categories = new DeliveryPrice();
        if ($countryId) {
            $categories = $categories->where('country_id', $countryId);
        }

        $categories = $categories->orderBy('created_at','desc')->paginate(15);
        return response()->json(compact('categories'));
    }
//categories
    public function store(Request $request){
        $request->validate([
            'price_from' => 'required',
            'price_to' => 'required',
            'price' => 'required',
            'countryId' => 'required',
        ]);

        \DB::beginTransaction();
        try {
            $price = new DeliveryPrice();
            $price->from = $request->price_from;
            $price->to = $request->price_to;
            $price->price = $request->price;
            $price->country_id = $request->countryId;
            $price->save();
        } catch (\Exception $e) {
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }

        \DB::commit();

        $message='Successfully created new delivery price !';
        return response()->json(compact('message'),200);
    }

    public function update(Request $request,$id){
        $request->validate([
            'price_from' => 'required',
            'price_to' => 'required',
            'price' => 'required',
                'countryId' => 'required',
//            'cat_image' => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $price =  DeliveryPrice::find($id);
            $price->from = $request->price_from;
            $price->to = $request->price_to;
            $price->price = $request->price;
             $price->country_id = $request->countryId;
            $price->save();




        } catch (\Exception $e) {
//            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }
        \DB::commit();
        $message='Successfully update delivery price !';
        return response()->json(compact('message'),200);
    }

    public function destroy( DeliveryPrice $category)
    {
        $category->delete();
        $message ="Category deleted successfully";
        return response()->json(compact('message'));
        return response()->json(['message' =>'Item deleted successfully'],200);

    }

    public function changeStatus(Request $request) {
        $request->validate([
            'id' => 'required',
            'status' => 'required',
        ]);
        $category = DeliveryPrice::findOrFail($request->id);
        $category->is_active = $request->status;
        $category->save();
        return response()->json([
            'status' => 'success',
            'msg' => 'Status changed successfully .'
        ], 200);
    }

}
