<?php

namespace App\Http\Controllers;

use App\Models\CategoriesContent;
use App\Models\Country;
use App\Models\Coupon;
use App\Models\Item;
use App\Models\Order;
use App\Notifications\SignupActivate;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Image;
use Storage;
use File;

use DB;
use Validator;
use Avatar;

class OffersController extends Controller
{
    public function index($countryId){
        $country=Country::findOrFail($countryId);
        if(!auth()->user()->can('New offers list '.$country->code)){
            abort(404);
        }
        $notifications=auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\NewOffer');
        foreach ($notifications as $notification){
            $notification->markAsRead();

        }
        return view('offers.index',compact('countryId'));
    }
    public function viewOrderOffers($offer){
        return view('offers.orders',compact('offer'));
    }
    public function search(Request $request,$countryId){
        $items = new Item();
        $items=$items->where('country_id',$countryId)->with('shop')->where("status","pending")->where('type','offer');
        if(request()->has('filter') && request('filter') ) {
            $filter = request('filter');
            $items = $items->where(function($q) use($filter){
                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
            });
        }

        if(request()->has('sort')) {
            $sort = json_decode(request('sort'), true);
//            $items = $items->orderBy(($sort['fieldName'] ?? 'id'), $sort['name_en']);
        }
        $items = $items->orderBy('created_at','desc')->paginate(15);
        return response()->json(compact('items'));
    }
    public  function showOffersList($countryId){

        $country=Country::findOrFail($countryId);
        if(!auth()->user()->can('Offers list '.$country->code)){
            abort(404);
        }
        return view('offers.offers',compact('countryId'));
    }
    public function offerList(Request $request,$countryId){
        $items = new Item();
        $items=$items->where('country_id',$countryId)->with('shop')->where("status",'!=',"pending")->where('type','offer');
//        if(request()->has('filter') && request('filter') ) {
//            $filter = request('filter');
//            $items = $items->where(function($q) use($filter){
//                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
//            });
//        }
        if(request()->has('store') && request('store') ) {
            $storee = request('store');
            $items = $items->where('classification_id',$storee);
        }
        if( isset($request->report_period)) {

//            \Illuminate\Pagination\Paginator::currentPageResolver(function ()  {
//                return 1;
//            });

            if($request->report_period=='custom'){
                $dates=explode(',',$request->custom_period);
                $periodDate=['start_at'=>Carbon::parse($dates[0])->format('Y-m-d 00:00:00'),'end_at'=>Carbon::parse($dates[1])->format('Y-m-d 23:59:59')];
            }else{

                $periodDate=  filterPeriod($request->report_period);
            }
            $items=$items->where('created_at','>=',$periodDate['start_at'])->where('created_at','<=',$periodDate['end_at']) ;

        }

        $items = $items->orderBy('created_at','desc')->paginate(15);
        return response()->json(compact('items'));
    }
    public function offersOrders( Request $request,$offer){
        $orders = new Order();
        $orders=$orders->where('type','offer')->where("offer_id",$offer)->with("client", "deliveryUser","shop");

        if($request->driver_id){
            $orders=$orders->where('driver_id',$request->driver_id);
        }
        if($request->client_id){
            $orders=$orders->where('user_id',$request->client_id);
        }

//        if(request()->has('filter') && request('filter') ) {
//            $filter = request('filter');
//            $shops = $shops->where(function($q) use($filter){
//                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
//            });
//
//        }
        if(request()->has('sort')) {
            $sort = json_decode(request('sort'), true);
//            $categories = $categories->orderBy(($sort['fieldName'] ?? 'id'), $sort['order']);
        }
        $orders = $orders->orderBy('created_at','desc')->paginate(15);
        return response()->json(compact('orders'));
    }
    public function changeStatus(Request $request)
    {
        $item = Item::findOrFail($request->id);
        $shop = CategoriesContent::with('user', 'branchParent.user')->findOrFail($item->classification_id);
        $user = User::find($shop->user['id']);

        if ($item) {
            $item->status = $request->status;
            $item->save();
            if($item->status=='active'){
                $msg = 'Your offer '.$item->name_en." accepted";
            }else{
                $msg = 'Your offer '.$item->name_en." rejected";
            }
            $message = [
                'msg' => $msg,
                'title' => $msg,
                'order_id' => 0,
                'order_no' =>0,
                'type' => '4station',
                'type_of_receive'=>0,
                'country_id'=>@$item->country_id
            ];
            $user->notify(new SignupActivate($message));
            return response()->json([
                'status' => 'success',
                'msg' => ' changed successfully .'
            ], 200);

        }

        return response()->json([
            'status' => 'failed',
            'msg' => 'failed'
        ], 422);
    }


    public function exportExcel(Request  $request){
        $orders=Order::get();
        $orders=$orders->where('status','!=','pending');
        if ($request->driver_id) {
            $orders = $orders->where('driver_id', $request->driver_id);
        }
        if ($request->client_id) {
            $orders = $orders->where('user_id', $request->client_id);
        }
        if(request()->has('store_id') && $request->store_id ) {
            $storee = request('store_id');
            $orders = $orders->where('shop_id',$storee);
        }
        if( isset($request->report_period)) {


            if($request->report_period=='custom'){
                $dates=explode(',',$request->custom_period);
                $periodDate=['start_at'=>Carbon::parse($dates[0])->format('Y-m-d 00:00:00'),'end_at'=>Carbon::parse($dates[1])->format('Y-m-d 23:59:59')];
            }else{

                $periodDate=  filterPeriod($request->report_period);
            }
            $orders=$orders->where('created_at','>=',$periodDate['start_at'])->where('created_at','<=',$periodDate['end_at']) ;

        }
        return  response(view('excel.orders',compact('orders')));




    }

}
