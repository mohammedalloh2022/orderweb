<?php

namespace App\Http\Controllers;

use App\Models\AppSetting;
use App\Models\CategoriesContent;
use App\Models\Country;
use App\Models\Coupon;
use App\Models\Item;
use App\Models\SubCategory;
use App\Notifications\NewOffer;
use App\Notifications\SignupActivate;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Image;
use Storage;
use File;

use DB;
use Validator;
use Avatar;

class ItemController extends Controller
{
    public function index(){
        $meals=Item::whereNull('parent_id')->paginate(10);

        return view('items.index',compact('meals'));
    }

    public function changeStatus(Request $request) {
        $request->validate([
            'id' => 'required',
            'status' => 'required',
        ]);
        $Item = Item::findOrFail($request->id);
        $Item->is_active = $request->status?1:0;
        $Item->save();
        return response()->json([
            'status' => 'success',
            'msg' => 'Status changed successfully .'
        ], 200);
    }

    public function mealsByCategory( CategoriesContent $shop,$cat){

        return view('shops.catmeals',compact('shop','cat'));
    }
    public function meals(Request $request,$shop){
        $items = new Item();

        $items=$items->with('images')->where('classification_id',$shop)->where('type','normal')->whereNull('parent_id')->with('extraItems');
        if($request->get('cat_id')){
            $items=$items->where('sub_cat_id',$request->cat_id);
        }
        if(request()->has('filter') && request('filter') ) {
            $filter = request('filter');
            $items = $items->where(function($q) use($filter){
                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
            });
        }
        if(request()->has('sort')) {
            $sort = json_decode(request('sort'), true);
//            $categories = $categories->orderBy(($sort['fieldName'] ?? 'id'), $sort['order']);
        }

        $items = $items->orderBy('created_at','desc')->paginate(15);

        return response()->json(compact('items'));
    }

    public function getMealsByCat(Request $request,$shop,$cat){
        $items = new Item();

        $items=$items->with('notDefaultImages')->where('classification_id',$shop)->where('type','normal')
            ->whereNull('parent_id')->with('extraItems');

        $items=$items->where('sub_cat_id',$cat);

        if(request()->has('filter') && request('filter') ) {
            $filter = request('filter');
            $items = $items->where(function($q) use($filter){
                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
            });
        }
        if(request()->has('sort')) {
            $sort = json_decode(request('sort'), true);
//            $categories = $categories->orderBy(($sort['fieldName'] ?? 'id'), $sort['order']);
        }

        $items = $items->orderBy('created_at','desc')->paginate(15);

        return response()->json(compact('items'));
    }


    public function store(Request $request){

    $request->validate([
        'name_en' => 'required',
        'name_ar' => 'required',
        'desc_ar' => 'required',
        'desc_en' => 'required',
        'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
        'meal_image' => 'required',
//        'cat_id' => 'required',
        'type' => 'required',
        'shop_id' => 'required',
        'country_id' => 'required',

    ]);
    if($request->type=='normal'){
        $request->validate([
            'cat_id'=>'required'
        ]);
    }
    \DB::beginTransaction();
    try {

        $meal = new Item();
        $meal->name_en = $request->name_en;
        $meal->name_ar = $request->name_ar;
        $meal->desc_ar = $request->desc_ar;
        $meal->desc_en = $request->desc_en;
        $meal->price = $request->price;
        $meal->sub_cat_id = $request->cat_id;
        $meal->country_id = $request->country_id;

        $meal->type = $request->type;
        $meal->classification_id = $request->shop_id;
        $meal->save();


        if(!empty($request->extraItems)){
            foreach($request->extraItems as $Item){
                $extraMeal = new Item();
                $extraMeal->name_en = $Item['name_en'];
                $extraMeal->name_ar = $Item['name_ar'];
                $extraMeal->desc_ar = $Item['desc_ar'];
                $extraMeal->desc_en = $Item['desc_en'];
                $extraMeal->price = $Item['price'];
                $extraMeal->sub_cat_id = $request->cat_id;
                $extraMeal->type = $request->type;
                $extraMeal->classification_id = $request->shop_id;
                $extraMeal->parent_id = $meal->id;
                $extraMeal->save();
            }
        }

        if($request->meal_image){
            $img = Image::make($request->meal_image);
            $extension = explode('/', $img->mime)[1];
            $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
            $destinationPath = public_path('uploads/items/');
            File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
            $img->save($destinationPath . $fileNameToStore);
            $image=new \App\Models\Image();
            $image->name=$fileNameToStore;
            $image->content_id=$meal->id;
            $image->content_type=Item::class;
            $image->is_default=true;
            $image->save();
        }

        foreach( $request->ImagesList as $key=>$base64Image) {

            $img = Image::make($base64Image);
            $extension = explode('/', $img->mime)[1];
            $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
            $destinationPath = public_path('uploads/items/');
            File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
            $img->save($destinationPath . $fileNameToStore);
            $image=new \App\Models\Image();
            $image->name=$fileNameToStore;
            $image->content_id=$meal->id;
            $image->content_type=Item::class;
            $image->is_default=false;
            $image->save();
//            $fileName=$this->saveBase64Image($base64Image);
//            if($fileName ){
//                $image=new ImageModel();
//                $image->content_id=$machine->id;
//                $image->content_type=Machine::class;
//                $image->name=$fileName;
//                $image->save();
//            };
//            $fileName=null;
        }
        $meal->save();
        \DB::commit();

    } catch (\Exception $e) {


        \DB::rollback();
        return response()->json([
            'success' => false,
            'message' => 'Something going wrong',
            'error'=>$e->getMessage()
        ], 422);

    }
    $message='Successfully created new meal!';
    return response()->json(compact('message'),200);
}

    public function update(Request $request,$item){

        $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
            'desc_ar' => 'required',
            'desc_en' => 'required',
            'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'meal_image' => 'required',

            'type' => 'required',
            'shop_id' => 'required',

        ]);
        if($request->type=='normal'){
              $request->validate([
                  'cat_id'=>'required'
              ]);
        }
        \DB::beginTransaction();
        try {

            $meal =  Item::findOrFail($item);
            $meal->name_en = $request->name_en;
            $meal->name_ar = $request->name_ar;
            $meal->desc_ar = $request->desc_ar;
            $meal->desc_en = $request->desc_en;
            $meal->price = $request->price;
            $meal->sub_cat_id = $request->cat_id;

            $meal->type = $request->type;
            $meal->classification_id = $request->shop_id;

            $meal->save();

            if(!empty($request->extraItems)){
                foreach($request->extraItems as $Item){
                    if($Item['id']){
                    $extraMeal =  Item::findOrFail($Item['id']);
                    }else{

                    $extraMeal = new Item();
                    }
                    $extraMeal->name_en = $Item['name_en'];
                    $extraMeal->name_ar = $Item['name_ar'];
                    $extraMeal->desc_ar = $Item['desc_ar'];
                    $extraMeal->desc_en = $Item['desc_en'];
                    $extraMeal->price = $Item['price'];
                    $extraMeal->sub_cat_id = $request->cat_id;
                    $extraMeal->type = $request->type;
                    $extraMeal->classification_id = $request->shop_id;
                    $extraMeal->parent_id = $meal->id;
                    $extraMeal->save();
                }
            }
            if(!empty($request->deletedExtraItem)){
                foreach ($request->deletedExtraItem as $item){
                    Item::findOrFail($item)->delete();
                }
            }
            if (!filter_var($request->meal_image, FILTER_VALIDATE_URL)) {
                if($request->meal_image){
                    $img = Image::make($request->meal_image);
                    $extension = explode('/', $img->mime)[1];
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/items/');
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . $fileNameToStore);
                    $image=\App\Models\Image::where('content_id',$meal->id)->where('content_type',Item::class)->first();
                    $image->name=$fileNameToStore;
                    $image->save();
                }
            }

            if( $request->removedImagesList && count($request->removedImagesList)){
                \App\Models\Image::destroy($request->removedImagesList);

            }
            if(!empty($request->ImagesList)){

            foreach( $request->ImagesList as $key=>$base64Image) {
                if(!isset($base64Image['id'])){
                    $img = Image::make($base64Image);
                    $extension = explode('/', $img->mime)[1];
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/items/');
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . $fileNameToStore);
                    $image=new \App\Models\Image();
                    $image->name=$fileNameToStore;
                    $image->content_id=$meal->id;
                    $image->content_type=Item::class;
                    $image->is_default=false;
                    $image->save();
                }
            }
            }


            $meal->save();
            \DB::commit();

        } catch (\Exception $e) {


            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong',
                'error'=>$e->getMessage()
            ], 422);

        }
        if($meal->type=='offer'){

        $message='Offer updated successfully!';
        }else{
        $message='Meal updated successfully!';

        }
        return response()->json(compact('message'),200);
    }

    public function offers($shop){
        $items = new Item();
        $items=$items->where('classification_id',$shop)->where('type','offer');
        if(request()->has('filter') && request('filter') ) {
            $filter = request('filter');
            $items = $items->where(function($q) use($filter){
                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
            });

        }
        if(request()->has('sort')) {
            $sort = json_decode(request('sort'), true);
//            $categories = $categories->orderBy(($sort['fieldName'] ?? 'id'), $sort['order']);
        }
        $items = $items->orderBy('created_at','desc')->paginate(15);
        return response()->json(compact('items'));
    }
    public function storeOffer(Request $request){
        $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
            'desc_ar' => 'required',
            'desc_en' => 'required',
            'price' => 'required|regex:/^[0-9]+(\.[0-9][0-9]?)?$/',
            'meal_image' => 'required',
            'shop_id' => 'required',
            'country_id' => 'required',
        ]);

        \DB::beginTransaction();
        try {
            $durationhr=AppSetting::find(1)->pluck('offer_periods_hr')[0];
            $meal = new Item();
            $meal->name_en = $request->name_en;
            $meal->name_ar = $request->name_ar;
            $meal->desc_ar = $request->desc_ar;
            $meal->desc_en = $request->desc_en;
            $meal->price = $request->price;
            $meal->country_id = $request->country_id;
            $meal->sub_cat_id = null;

            $meal->expire_at=Carbon::now()->addHours($durationhr)->format('Y-m-d H:i:s');

            $meal->type = "offer";
            $meal->classification_id = $request->shop_id;
            $meal->save();

                $admins=User::where('role','admin')->get();
                $shop=CategoriesContent::find($request->shop_id);



            foreach ($admins as $admin){
                if( $admin->can('New offers requests notifications')){
                    $message = [
                        'msg' => "new offer entered",
                        'title' => "new offer entered",
                        'offer_id' => $meal->id,
                        'shop_id' => $request->shop_id,
                        'type' => 'admin',
                        'country_id'=>$shop->country_id
                    ];
                    $admin->notify(new NewOffer($message));
                }else{
                    $countries= Country::get()->pluck('code');
                    foreach($countries as $country) {
                        if ($admin->can('New offers requests notifications '.$country)) {
                            $message = [
                                'msg' => "new offer approve request",
                                'title' => "new offer approve request",
                                'offer_id' => $meal->id,
                                'shop_id' => $request->shop_id,
                                'type' => 'admin',
                                'country_id'=>$shop->country_id
                            ];
                            $admin->notify(new NewOffer($message));
                            break;
                        }

                    }
                }


            }

            if($request->meal_image){
                $img = Image::make($request->meal_image);
                $extension = explode('/', $img->mime)[1];
                $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                $destinationPath = public_path('uploads/items/');
                File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                $img->save($destinationPath . $fileNameToStore);
                $image=new \App\Models\Image();
                $image->name=$fileNameToStore;
                $image->content_id=$meal->id;
                $image->content_type=Item::class;
                $image->is_default=true;
                $image->save();
            }
            $meal->save();
            \DB::commit();

        } catch (\Exception $e) {

            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }
        $message='Successfully created new offer!';
        return response()->json(compact('message'),200);
    }
    public function destroy( Item $item)
    {
        $item->delete();
        $message ="Meal deleted successfully";
        return response()->json(compact('message'));


    }
}
