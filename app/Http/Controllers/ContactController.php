<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Contact;
use App\Models\ContactReply;
use App\Models\Country;
use App\Notifications\SignupActivate;
use App\User;
use Illuminate\Http\Request;
use Mail;
class ContactController extends Controller
{
    public function index($countryId){

        $country=Country::findOrFail($countryId);
        if(!auth()->user()->can('Contact messages '.$country->code)){
            abort(404);
        }
        $contacts=Contact::with('userRole')->paginate(10);
        $notifications=auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\NewMessage');
        foreach ($notifications as $notification){
            $notification->markAsRead();

        }
        return view('contacts.index',compact('contacts','countryId'));
    }

    public function search($countryId)
    {
        $contacts = new Contact();
        $contacts = $contacts->with('user')->where('country_id',$countryId);
        $contacts = $contacts->orderBy('created_at','desc')->paginate(15);
        return response()->json(compact('contacts'));
    }

    public  function getReplies(Request $request,$contId){
        $contactReplies=ContactReply::where('contact_id',$contId)->get();
        return response()->json(compact('contactReplies'));
    }

    public function reply(Request $request,$contId){
        $request->validate([
            'replay' => 'required',
        ]);
        \DB::beginTransaction();
        try {
            $contact=Contact::findOrFail($contId);
            $ContactReply = new ContactReply();
            $ContactReply->replay = $request->replay;
//            $ContactReply->replay = $request->replay;
            $ContactReply->contact_id = $contId;
            $ContactReply->save();

            \DB::commit();
            $newuser = User::findOrFail($contact->user_id);
            if ($newuser) {
                $message = [
                    'msg' =>  ''.$request->replay,
                    'title' => "Reply to your message",
                    'type' => 'public',
                    'country_id'=>@$contact->country_id

                ];
                if ($newuser->fcm_token) {
                    sendFCM($newuser->fcm_token, $message);
                }
                $newuser->notify(new SignupActivate($message));
            }

//            $data['title'] = "This is first mail from 4orderStation.com";
//
//            Mail::send('emails.contactReply', $data, function($message) {
//
//                $message->to('dev.abdallahalkahlout@gmail.com', 'Abdalalh Alkahlout')
//
//                    ->subject('Test message');
//            });
//
//            if (Mail::failures()) {
//                return response()->Fail('Sorry! Please try again latter');
//            }else{
//                return response()->success('Great! Successfully send in your mail');
//            }

        } catch (\Exception $e) {
//            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong',
                "error"=>$e->getMessage()
            ], 422);

        }
        $message='Replay sent successfully!';
        return response()->json(compact('message'),200);
    }
}
