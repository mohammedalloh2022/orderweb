<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\DriverWalletTransaction;
use App\Models\MobileNotification;
use App\Notifications\SignupActivate;
use App\User;
use Illuminate\Http\Request;

class MobileNotificationController extends Controller
{
    public function index(Request $request,$countryId){
        $country=Country::findOrFail($countryId);
        if(!auth()->user()->can('Send notification '.$country->code)){
            abort(404);
        }
        return view('notifications.index',compact('countryId'));
    }
    public function search(Request $request,$countryId){
        $wallet_transactions=MobileNotification::where('country_id',$countryId)->orderBy('created_at','desc')->paginate(10);
        return response()->json(compact('wallet_transactions'));
    }
    public function send(Request $request)
    {
        $request->validate([
            'message' => 'required',
            'sent_to' => 'required',
            'country_id' => 'required',
            'custom_clients'=>'',
            'custom_drivers'=>'',
        ]);


        $message =new MobileNotification();
        $message->message=$request->message;
        $message->sent_to=$request->sent_to;
        $message->country_id=$request->country_id;
        $message->save();

        $users=null;

        switch ($request->sent_to){
            case 'all':
                $users=User::where('country_id',$request->country_id)->where(function($q){
                    $q->where('role','client')->orWhere('role','delivery_driver');
                })->where("fcm_token",'!=',null)->where('active',1)->get();
                break;
            case 'all_clients':
                $users=User::where('country_id',$request->country_id)->where('role','client')
                ->where("fcm_token",'!=',null)->where('active',1)->get();
                break;
            case 'all_drivers':
                $users=User::where('country_id',$request->country_id)->where('role','delivery_driver')
                    ->where("fcm_token",'!=',null)->where('active',1)->get();
                break;
            case 'custom_clients':
                $customeclient=collect($request->custom_clients)->pluck('id')->toArray();
                $users=User::findOrFail($customeclient);
                break;
            case 'custom_drivers':

                $dasfds=collect($request->custom_drivers)->pluck('id')->toArray();
                $users=User::findOrFail($dasfds);
                break;
        }
        if(!empty($users)){
            foreach($users as $user){

//                if($request->sent_to=='custom_clients'||$request->sent_to=='custom_drivers'){
//                     $user=User::find($user['id']);
//                }
                $message = [
                    'msg'=>$request->message,
                    'title'=>"Administrative message",
                    'type'=>'4station',
                    'country_id'=>@$message->country_id,
                ];
                if($user->fcm_token){
                    sendFCM($user->fcm_token,$message);
                }
                $user->notify(new SignupActivate($message));
            }
        }

        return response()->json([
                'status' => 'success',
                'msg' => ' Notification sent successfully .'
            ], 200);


    }
}
