<?php

namespace App\Http\Controllers;

use App\Models\AppSetting;
use App\Models\Country;
use Illuminate\Http\Request;

class CountryController extends Controller
{
    public function list(){
        $countries=Country::all();
        return response()->json(compact('countries'));
    }

    public function index(){
//        $categories=Category::paginate(10);
        return view('countries.index');
    }




    public function search()
    {
        $countries = new Country();

        if(request()->has('filter') && request('filter') ) {
            $filter = request('filter');
            $countries = $countries->where(function($q) use($filter){
                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
            });
        }

        $countries = $countries->orderBy('created_at','desc')->paginate(15);
        return response()->json(compact('countries'));
    }
//categories
    public function store(Request $request){
        $request->validate([
            'name_en' => 'required',
            'name_ar' => 'required',
            'code' => 'required|alpha',
            'currency_code' => 'required|alpha',
            'phone_code' => 'required|digits_between:3,4',
            'phone_length' => 'required|digits_between:1,2',
        ]);

        \DB::beginTransaction();
        try {
            $country = new Country();
            $country->name_en = $request->name_en;
            $country->name_ar = $request->name_ar;
            $country->code = $request->code;
            $country->currency_code = $request->currency_code;
            $country->phone_code = $request->phone_code;
            $country->phone_length = $request->phone_length;
            $country->is_active = false;


            $country->save();

                if($country->id){
                    $app=new AppSetting();
                    $app->driver_wallet_limit=-23;
                    $app->shop_wallet_limit=-354;

                    $app->offer_periods_hr=10;
                    $app->app_order_commission=10;
                    $app->shop_distance_limit_km=10;
                    $app->driver_distance_limit_km=10;
                    $app->tax=4;
                    $app->app_commission_public_delivery=10;
                    $app->public_delivery_cost_km=10;
                    $app->delivery_cost=10;
                    $app->app_delivery_commission=10;
                    $app->country_id=$country->id;
                    $app->save();
                }
            \DB::commit();

        } catch (\Exception $e) {
            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }
        $message='Added successfully!';
        return response()->json(compact('message'),200);
    }

    public function update(Request $request,$id){
        $request->validate([
            'cat_name_en' => 'required',
            'cat_name_ar' => 'required',
//            'cat_image' => 'required',
        ]);

        \DB::beginTransaction();
        try {

            $category =  Country::find($id);
            $category->name_en = $request->cat_name_en;
            $category->name_ar = $request->cat_name_ar;

//            if ($request->hasFile('cat_image')) {
            // Get just ext
            if (!filter_var($request->cat_image, FILTER_VALIDATE_URL)) {

//            if($request->cat_image){

                $img = Image::make($request->cat_image);
//                $extension =$img->getClientOriginalExtension();
                $extension = explode('/', $img->mime)[1];
                //uniqid()
                $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                $destinationPath = public_path('uploads/categories/');
                File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                $img->save($destinationPath . $fileNameToStore);
                $category->image = $fileNameToStore;
            }
//            $data=base64_decode($request->cat_image);
//            Storage::disk('local')->put($fileNameToStore, $data);
//            Storage::disk('local')->put($fileNameToStore, base64_decode($data));
//                Storage::put('categories', $data, $fileNameToStore);
//            }
            $category->save();
            \DB::commit();



        } catch (\Exception $e) {
//            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }
        $message='Successfully update category!';
        return response()->json(compact('message'),200);
    }

    public function destroy( Country $category)
    {
        $category->delete();
        $message ="Category deleted successfully";
        return response()->json(compact('message'));
        return response()->json(['message' =>'Item deleted successfully'],200);

    }

    public function changeStatus(Request $request) {
        $request->validate([
            'id' => 'required',
            'status' => 'required',
        ]);
        $category = Country::findOrFail($request->id);
        $category->is_active = $request->status;
        $category->save();
        return response()->json([
            'status' => 'success',
            'msg' => 'Status changed successfully .'
        ], 200);
    }

}
