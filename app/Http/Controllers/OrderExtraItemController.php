<?php

namespace App\Http\Controllers;

use App\OrderExtraItem;
use Illuminate\Http\Request;

class OrderExtraItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderExtraItem  $orderExtraItem
     * @return \Illuminate\Http\Response
     */
    public function show(OrderExtraItem $orderExtraItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderExtraItem  $orderExtraItem
     * @return \Illuminate\Http\Response
     */
    public function edit(OrderExtraItem $orderExtraItem)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderExtraItem  $orderExtraItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, OrderExtraItem $orderExtraItem)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderExtraItem  $orderExtraItem
     * @return \Illuminate\Http\Response
     */
    public function destroy(OrderExtraItem $orderExtraItem)
    {
        //
    }
}
