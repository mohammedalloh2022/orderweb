<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Order;
use App\Models\PublicOrder;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Image;
use Storage;
use File;


use Avatar;
use Illuminate\Validation\Rule;

class UserController extends Controller
{
    public function clientIndex($countryId)
    {
        $country=Country::findOrFail($countryId);
        if(!auth()->user()->can('Client management '.$country->code)){
            abort(404);
        }
        return view('users.clients',compact('countryId'));
    }

    public function adminIndex()
    {
        return view('users.admin');
    }
    public function allClients($countryId){
        $clients=User::where('country_id',$countryId)->where('role','client')->where("fcm_token",'!=',null)->where('active',1)->get();
        return response()->json(compact('clients'));
    }
    public function allDrivers($countryId){
        $drivers=User::where('country_id',$countryId)->where('role','delivery_driver')
//            ->where("fcm_token",'!=',null)
            ->where("active",1)->where("is_documented",1)->get();
        return response()->json(compact('drivers'));
    }
    public function lastTenClient(){
        $clients=User::where('role','client')->where('active',1)->orderBy('created_at','desc')->paginate(10);
        return response()->json(compact('users'));

    }
    public function allBusyDrivers(){
        $busy_drivers=Order::where('status','=','on_the_way')->pluck('driver_id')->toArray();
        $busy_public_drivers=PublicOrder::where(function($q){
            $q->where('status','')->orWhere('status','');
        })->pluck('driver_id')->pluck('driver_id')->toArray();
        $drivers=array_merge($busy_drivers,$busy_public_drivers);
        return response()->json(compact('drivers'));
    }
    public function search(Request $request,$countryId)
    {
        $users = new User();
        $users = $users->where('country_id',$countryId)->where('role', 'client')->where('id','!=',auth()->user()->id);
        if (request()->has('filter') && request('filter')) {
            $filter = request('filter');
            $users = $users->where('name', 'LIKE', "%$filter%");
        }

        if(request()->has('name') && request('name') ) {
            $filter = request('name');
            $users = $users->where('name', 'LIKE', "%$filter%");
        }
        if(request()->has('mobile') && request('mobile') ) {
            $filter = request('mobile');
            $users = $users->where('mobile', 'LIKE',$filter);
        }
        if( isset($request->report_period)) {
            if($request->report_period=='custom'){
                $dates=explode(',',$request->custom_period);
                $periodDate=['start_at'=>Carbon::parse($dates[0])->format('Y-m-d 00:00:00'),'end_at'=>Carbon::parse($dates[1])->format('Y-m-d 23:59:59')];
            }else{

                $periodDate=  filterPeriod($request->report_period);
            }
            $users=$users->where('created_at','>=',$periodDate['start_at'])->where('created_at','<=',$periodDate['end_at']) ;

        }
        if (request()->has('sort')) {
            $sort = json_decode(request('sort'), true);
//            $users = $users->orderBy(($sort['fieldName'] ?? 'id'), $sort['name_en']);
        }
        $users = $users->orderBy('created_at', 'desc')->paginate(13);
        return response()->json(compact('users'));
    }

    public function adminSearch()
    {
        $users = new User();
        $users = $users->where('role', 'admin');
        if (request()->has('filter') && request('filter')) {
            $filter = request('filter');
            $users = $users->where('name', 'LIKE', "%$filter%");
        }
        if (request()->has('sort')) {
            $sort = json_decode(request('sort'), true);
//            $users = $users->orderBy(($sort['fieldName'] ?? 'id'), $sort['name_en']);
        }
        $users = $users->orderBy('created_at', 'desc')->paginate(13);
        return response()->json(compact('users'));
    }

    public function changeStatus(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'status' => 'required',
        ]);
        $user = User::findOrFail($request->id);
        $user->active = $request->status;
        $user->save();
        if(!$request->status){
            if (isset($user) && $user->tokens) {
                $message = [
                    'msg' => "Your account has been blocked contact app manger",
                    'title' => "Your account has been blocked contact app manger",
                    'type' => 'driver_approved',
                ];

                if ($user->fcm_token) {
                    sendFCM($user->fcm_token, $message);
                }
                foreach ($user->tokens as $token) {
                    $token->revoke();
                }
            }

        }else{
            $message = [
                'msg' => "Your account has been activated",
                'title' => "Your account has been activated",
                'type' => 'driver_approved',
            ];

            if ($user->fcm_token) {
                sendFCM($user->fcm_token, $message);
            }
        }





        return response()->json([
            'status' => 'success',
            'msg' => 'Status changed successfully .'
        ], 200);
    }




    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required|string|email|unique:users',
            'mobile' => 'required|unique:users',
            'password' => 'required',
            'address' => 'required',
            'role' => 'required',
            'user_avatar' => 'required',
        ]);

        if($request->role=="delivery_driver"){
            $request->validate([
                'vehicle_type' => 'required',
                'vehicle_plate' => 'required',
                'vehicle_pic' => 'required',
                'license_pic' => 'required',
                'insurance_pic' => 'required',
                'id_pic' => 'required',
            ]);
        }

        \DB::beginTransaction();
        try {
            $register_number = null;



            $reg_number = User::where('role', $request->role)->latest()->pluck('reg_no')->first();
            if (isset($reg_number)) {
                    $register_number = $reg_number + 1;
            } else {
                if ($request->role == 'client') {
                    $register_number = 10000000001;
                }
                if ($request->role == 'delivery_driver') {
                    $register_number = 20000000001;
                }
                if ($request->role == 'admin') {
                    $register_number = 30000000001;
                }
            }

            $user = new User([
                'name' => $request->name,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'address' => $request->address,
                'role' => $request->role,
                'reg_no' => $register_number,
                'password' => bcrypt($request->password),
            ]);
            if ($request->role == 'delivery_driver') {
                $user->active=0;
            }

//        $user->notify(new SignupActivate($user));
            $user->save();

            if($request->role=="admin"){
                if(isset($request->systemRoleId)){
//                    $rolesList=collect($request->admin_role)->pluck('id');
                    $user->syncRoles($request->systemRoleId);
                }
            }
            if ($request->user_avatar) {
                $img = Image::make($request->user_avatar);
                $extension = explode('/', $img->mime)[1];
                $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                $destinationPath = public_path('uploads/users/' . $user->id . '/');
                File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                $img->save($destinationPath . $fileNameToStore);
                $user->avatar = $fileNameToStore;
            } else {
                $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
                Storage::put('users/' . $user->id . '/avatar.png', (string)$avatar);
            }

            if($request->role=="delivery_driver"){
                $user->vehicle_type = $request->vehicle_type;
                $user->vehicle_plate = $request->vehicle_plate;
                if ($request->vehicle_pic) {
                    $img = Image::make($request->vehicle_pic);
                    $extension = explode('/', $img->mime)[1];
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id . '/');
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . $fileNameToStore);
                    $user->vehicle_pic = $fileNameToStore;
                }

                if ($request->license_pic) {
                    $img = Image::make($request->license_pic);
                    $extension = explode('/', $img->mime)[1];
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id . '/');
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . $fileNameToStore);
                    $user->drive_license = $fileNameToStore;
                }

                if ($request->insurance_pic) {
                    $img = Image::make($request->insurance_pic);
                    $extension = explode('/', $img->mime)[1];
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id . '/');
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . $fileNameToStore);
                    $user->insurance_license = $fileNameToStore;
                }

                if ($request->id_pic) {
                    $img = Image::make($request->id_pic);
                    $extension = explode('/', $img->mime)[1];
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id . '/');
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . $fileNameToStore);
                    $user->id_pic = $fileNameToStore;
                }
            }

            $user->save();
            \DB::commit();


        } catch (\Exception $e) {

            return $e;
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
        $message = 'Successfully created new user!';
        return response()->json(compact('message'), 200);


    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => Rule::unique('users')->ignore($request->id, 'id'),
            'mobile' => 'required',
            'address' => 'required',
            'user_avatar' => 'required',
        ]);

        if($request->role=="delivery_driver"){
            $request->validate([
                'vehicle_type' => 'required',
                'vehicle_plate' => 'required',
                'vehicle_pic' => 'required',
                'license_pic' => 'required',
                'insurance_pic' => 'required',
                'vehicle_license_pic' => 'required',
                'id_pic' => 'required',
            ]);
        }
        \DB::beginTransaction();
        try {
            $user = User::findOrFail($request->id);
            $user->name = $request->name;
            $user->mobile = $request->mobile;
            $user->address = $request->address;
            if ($request->password) {
                $user->password = bcrypt($request->password);
            }

            $user->save();
            if (!filter_var($request->user_avatar, FILTER_VALIDATE_URL)) {
                if ($request->user_avatar) {
                    $img = Image::make($request->user_avatar);
                    $extension = explode('/', $img->mime)[1];
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id . '/');
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . $fileNameToStore);
                    $user->avatar = $fileNameToStore;
                } else {
                    $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
                    Storage::put('users/' . $user->id . '/avatar.png', (string)$avatar);
                }

            }

            if($request->role=="delivery_driver"){
                $user->vehicle_type = $request->vehicle_type;
                $user->vehicle_plate = $request->vehicle_plate;
                if (!filter_var($request->vehicle_pic, FILTER_VALIDATE_URL)) {
                    if ($request->vehicle_pic) {
                        $img = Image::make($request->vehicle_pic);
                        $extension = explode('/', $img->mime)[1];
                        $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                        $destinationPath = public_path('uploads/users/' . $user->id . '/');
                        File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                        $img->save($destinationPath . $fileNameToStore);
                        $user->vehicle_pic = $fileNameToStore;
                    }
                }
                if (!filter_var($request->vehicle_license_pic, FILTER_VALIDATE_URL)) {
                    if ($request->vehicle_license_pic) {
                        $img = Image::make($request->vehicle_license_pic);
                        $extension = explode('/', $img->mime)[1];
                        $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                        $destinationPath = public_path('uploads/users/' . $user->id . '/');
                        File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                        $img->save($destinationPath . $fileNameToStore);
                        $user->vehicle_license = $fileNameToStore;
                    }
                }
                if (!filter_var($request->license_pic, FILTER_VALIDATE_URL)) {
                    if ($request->license_pic) {
                        $img = Image::make($request->license_pic);
                        $extension = explode('/', $img->mime)[1];
                        $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                        $destinationPath = public_path('uploads/users/' . $user->id . '/');
                        File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                        $img->save($destinationPath . $fileNameToStore);
                        $user->drive_license = $fileNameToStore;
                    }
                }
                if (!filter_var($request->insurance_pic, FILTER_VALIDATE_URL)) {
                    if ($request->insurance_pic) {
                        $img = Image::make($request->insurance_pic);
                        $extension = explode('/', $img->mime)[1];
                        $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                        $destinationPath = public_path('uploads/users/' . $user->id . '/');
                        File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                        $img->save($destinationPath . $fileNameToStore);
                        $user->insurance_license = $fileNameToStore;
                    }
                }
                if (!filter_var($request->id_pic, FILTER_VALIDATE_URL)) {
                    if ($request->id_pic) {
                        $img = Image::make($request->id_pic);
                        $extension = explode('/', $img->mime)[1];
                        $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                        $destinationPath = public_path('uploads/users/' . $user->id . '/');
                        File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                        $img->save($destinationPath . $fileNameToStore);
                        $user->id_pic = $fileNameToStore;
                    }
                }
            }
            if($request->role=="admin"){
                if(isset($request->systemRoleId)){
//                    $rolesList=collect($request->admin_role)->pluck('id');
                    $user->syncRoles($request->systemRoleId);
                }
            }
            $user->save();
            \DB::commit();


        } catch (\Exception $e) {

//            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong',
                "error"=>$e->getMessage()
            ], 422);
        }
        $message = 'User data updated Successfully !';
        return response()->json(compact('message'), 200);
    }


    public function destroy(User $user)
    {
        $user->delete();
        $message = "Restaurant deleted successfully";
        return response()->json(compact('message'));
    }
    public function markReadNotification(Request $request){
        if($request->tagret){
                switch ($request->target){

                    case 'newOrders':
                        $notifications=auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\SignupActivate');
                        foreach ($notifications as $notification){
                            $notification->markAsRead();
                            $notification->save();
                        }
                        break;
                    case 'newOffers':
                        $notifications=auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\NewOffer');
                        foreach ($notifications as $notification){
                            $notification->markAsRead();
                            $notification->save();
                        }
                        break;
                    case 'newMessages':

                         $notifications=auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\NewMessage');
                        foreach ($notifications as $notification){
                            $notification->markAsRead();
                            $notification->save();
                        }
                        break;
                    case 'newDrivers':
                        $notifications=auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\SignupDrivers');
                        foreach ($notifications as $notification){
                            $notification->markAsRead();
                            $notification->save();
                        }
                        break;
                }
        }
        $message = "Notifications marked read ";
        return response()->json(compact('message'));
    }

    public function exportUsersExcel(Request $request){

        $users = new User();

        $users = $users->where('country_id',$request->country_id)->where('role', 'client');
        if (request()->has('filter') && request('filter')) {
            $filter = request('filter');
            $users = $users->where('name', 'LIKE', "%$filter%");
        }

        if(request()->has('name') && request('name') ) {
            $filter = request('name');
            $users = $users->where('name', 'LIKE', "%$filter%");
        }
        if(request()->has('mobile') && request('mobile') ) {
            $filter = request('mobile');
            $users = $users->where('mobile', 'LIKE',$filter);
        }
        if( isset($request->report_period)) {
            if($request->report_period=='custom'){
                $dates=explode(',',$request->custom_period);
                $periodDate=['start_at'=>Carbon::parse($dates[0])->format('Y-m-d 00:00:00'),'end_at'=>Carbon::parse($dates[1])->format('Y-m-d 23:59:59')];
            }else{

                $periodDate=  filterPeriod($request->report_period);
            }
            $users=$users->where('created_at','>=',$periodDate['start_at'])->where('created_at','<=',$periodDate['end_at']) ;

        }

        $clients = $users->orderBy('created_at', 'desc')->get();

        return response(view('excel.clients',compact('clients')));

    }


}
