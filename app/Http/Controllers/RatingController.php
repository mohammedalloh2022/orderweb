<?php

namespace App\Http\Controllers;

use App\Models\CategoriesContent;
use App\Models\Rating;
use Illuminate\Http\Request;

class RatingController extends Controller
{
    public function destroy( Rating $rate)
    {
        $rate->delete();
        $message ="Rate deleted successfully";
        return response()->json(compact('message'));


    }
}
