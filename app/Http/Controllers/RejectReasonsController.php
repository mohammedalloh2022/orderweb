<?php

namespace App\Http\Controllers;

use App\RejectReasons;
use Illuminate\Http\Request;

class RejectReasonsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\RejectReasons  $rejectReasons
     * @return \Illuminate\Http\Response
     */
    public function show(RejectReasons $rejectReasons)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\RejectReasons  $rejectReasons
     * @return \Illuminate\Http\Response
     */
    public function edit(RejectReasons $rejectReasons)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\RejectReasons  $rejectReasons
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RejectReasons $rejectReasons)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\RejectReasons  $rejectReasons
     * @return \Illuminate\Http\Response
     */
    public function destroy(RejectReasons $rejectReasons)
    {
        //
    }
}
