<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\CategoriesContent;
use App\Models\Order;
use App\Models\PublicOrder;
use App\Models\Rating;
use App\Notifications\SignupActivate;
use App\User;
use Illuminate\Http\Request;
use Avatar;
use Storage;
use Carbon\Carbon;
use DB;
class RatingController extends Controller
{
    public function index(Request $request, $category){
        $ratings= Rating::whereHasMorph('content',CategoriesContent::class)->where('content_id',$category)->with('user')->paginate(10);
//        $ratings=CategoriesContent::with('ratings')->findOrFail($category);
        return response()->json(compact('ratings'));
    }

    public function driversRatings(Request $request){
       $ratings= Rating::whereHasMorph('content',User::class)->where('content_id',$request->user('api')->id)->with('user')->paginate(10);
//        $ratings=CategoriesContent::with('ratings')->findOrFail($category);
        return response()->json(compact('ratings'));
    }

    public function store(Request $request){

        $validator = \Validator::make($request->all(), [
            'content_id' => 'required',
            'rate' => 'required',
            'review' => 'required',
        ]);

        if ($validator->fails()){
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }

        DB::beginTransaction();
        try{
            $previousOrder=Order::where('shop_id',$request->content_id)->where('user_id',$request->user()->id)->where('status','delivered')->first();

            if(isset($previousOrder) ){
                $fav=new Rating();
                $fav->user_id=$request->user()->id;
                $fav->content_id=$request->content_id;
                $fav->content_type=CategoriesContent::class;
                $fav->rate=$request->rate;
                $fav->review=$request->review;
                $fav->save();
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'You must have at least 1 delivered order from this shop to rate it'
                ], 422);
            }

            DB::commit();

        }catch (\Exception $e){
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
        return response()->json([
            'success' => true,
            'message' => 'Successfully send your review !'
        ], 200);
    }

    public function storeRateDriver(Request $request){


        $validator = \Validator::make($request->all(), [
            'driver_id' => 'required',
            'rate' => 'required',
            'review' => 'required',
            'type' => 'required',
            'order_id'=>'required'
        ]);
        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }

        DB::beginTransaction();
        try{
            $previousOrder=null;
            if($request->type =='private'){

               $previousOrder=Order::where('driver_id',$request->driver_id)->where('user_id',$request->user()->id)->where('status','delivered')->first();
            }else{
                $previousOrder=PublicOrder::where('driver_id',$request->driver_id)->where('client_id',$request->user()->id)->where('status','delivered')->first();

            }
            if($previousOrder ){
                $fav=new Rating();
                $fav->user_id=$request->user()->id;
                $fav->content_id=$request->driver_id;
                $fav->content_type=User::class;
                $fav->rate=$request->rate;
                $fav->review=$request->review;
                $fav->type=$request->type;
                $fav->order_id=$request->order_id;
                $fav->save();

                $driver = User::find($previousOrder->driver_id);
                if($driver){
                    $message = [
                        'msg' => "Your order #".$previousOrder->invoice_number.' rated '.$fav->rate ,
                        'title' => "Your order #".$previousOrder->invoice_number.' rated '.$fav->rate ,
                        "type"=>"rate",
                        "status"=>$previousOrder->status,
                        'country_id'=>@$driver->country_id
                    ];
                    if ($driver->fcm_token) {
                        sendFCM($driver->fcm_token, $message);
                    }
                    $driver->notify(new SignupActivate($message));
                }
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'You must have at least one completed order'
                ], 422);
            }
            DB::commit();
        }catch (\Exception $e){
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong',
                'errors'=>$e->getMessage()
            ], 422);
        }
        return response()->json([
            'success' => true,
            'message' => 'Successfully send your review !'
        ], 200);
    }



}
