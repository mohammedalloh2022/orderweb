<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\AppSetting;
use App\Models\CategoriesContent;
use App\Models\Category;
use Avatar;
use Illuminate\Http\Request;
use Storage;
use DB;

class CategoryController extends Controller
{

    public function index(Request $request)
    {
        $user_lat=$request->lat;
        $user_lng=$request->lng;
        $countryId=$request->country_id;
        $shop_wallet_limit=(float)AppSetting::where('country_id',$countryId)->first()->pluck('shop_wallet_limit')[0];
        if($user_lat && $user_lng ){
            $shop_distance_limit_km=AppSetting::where('country_id',$countryId)->first()->pluck('shop_distance_limit_km')[0];
            $shops = DB::table("categories_contents")->where('country_id',$countryId);
            $shops = $shops->select("*", DB::raw("6371 * acos(cos(radians(" . $user_lat . "))
                                * cos(radians(lat)) * cos(radians(lng) - radians(" . $user_lng . "))
                                + sin(radians(" . $user_lat . ")) * sin(radians(lat))) AS distance"));


            $shops = $shops->having('distance', '<=',$shop_distance_limit_km)->where('parent_id',null)
                ->where('is_active',1);
            $mainShopsids=$shops->pluck('id')->toArray();
            $shopBranches = DB::table("categories_contents")->where('country_id',$countryId);
            $shopBranches = $shopBranches->select("*", DB::raw("6371 * acos(cos(radians(" . $user_lat . "))
                                * cos(radians(lat)) * cos(radians(lng) - radians(" . $user_lng . "))
                                + sin(radians(" . $user_lat . ")) * sin(radians(lat))) AS distance"));


            $shopBranches = $shopBranches->having('distance', '<=',$shop_distance_limit_km)
                ->where('parent_id','!=',"null")->where('is_active',1);
            $branchShopsIds=$shopBranches->pluck('parent_id')->toArray();

            $shopsIds=array_merge($branchShopsIds,$mainShopsids);
            $categories = Category::where('is_active',1)->where(function($q){
                $q->whereHas("shops.meals",function ($q){
                    $q->where('is_active',1);
                })->orWhereHas("shops.offers",function ($q){
                    $q->where('is_active',1)->where('status','active');
                });
            })->whereHas('shops',function ($qq) use($shop_wallet_limit,$shopsIds,$countryId){
                $qq->where('wallet','>=',$shop_wallet_limit)->where('country_id',$countryId)->where('is_active',1)->whereIn('id',$shopsIds);
            })->get();
            return response()->json(compact('categories'));
        }


    }


    public function shops(Request $request, $category)
    {
        $user_lat=$request->lat;
        $user_lng=$request->lng;
        $countryId=$request->country_id;
//        $user=$request->user('api');
        $shop_wallet_limit=AppSetting::where('country_id',$countryId)->first()->shop_wallet_limit;
        if($user_lat && $user_lng ) {
            $shop_distance_limit_km = AppSetting::where('country_id',$countryId)->first()->shop_distance_limit_km;
            $shops = DB::table("categories_contents")->where('country_id',$countryId);
            $shops = $shops->select("*", DB::raw("6371 * acos(cos(radians(" . $user_lat . "))
                                * cos(radians(lat)) * cos(radians(lng) - radians(" . $user_lng . "))
                                + sin(radians(" . $user_lat . ")) * sin(radians(lat))) AS distance"));


            $shops = $shops->having('distance', '<=',$shop_distance_limit_km)->where('parent_id',null)
                ->where('is_active',1);
            $mainShopsids=$shops->pluck('id')->toArray();
            $shopBranches = DB::table("categories_contents")->where('country_id',$countryId);
            $shopBranches = $shopBranches->select("*", DB::raw("6371 * acos(cos(radians(" . $user_lat . "))
                                * cos(radians(lat)) * cos(radians(lng) - radians(" . $user_lng . "))
                                + sin(radians(" . $user_lat . ")) * sin(radians(lat))) AS distance"));


            $shopBranches = $shopBranches->having('distance', '<=',$shop_distance_limit_km)
                ->where('parent_id','!=',"null")->where('is_active',1);

            $branchShopsIds=$shopBranches->pluck('parent_id')->toArray();
            $shopsIds=array_merge($branchShopsIds,$mainShopsids);
            $shops = CategoriesContent::where(function ($q) {
                $q->whereHas('meals', function ($qq) {
                    $qq->where('is_active', 1);
                })->orWhereHas('offers', function ($qq) {
                    $qq->where('is_active',1)->where('status','active');
                });
            })->whereIn('id',$shopsIds)
                ->where('country_id',$countryId)
                ->where('wallet','>',$shop_wallet_limit)
                ->where('is_active',1)
                ->where('content_id', $category)
                ->where('parent_id',null)
                ->paginate(100);
            return response()->json(compact('shops'));

        }


    }


}
