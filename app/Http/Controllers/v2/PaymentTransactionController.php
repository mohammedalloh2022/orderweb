<?php


namespace App\Http\Controllers\v2;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaymentTransactionController extends  Controller{


    public function payment(){
        return $this->hyperPayPaymentProcess();
    }

    // this function which used in merchant
    public function getCheckOutId(Request  $request){
         return getCheckOutId($request->get('amount'),$request->get('currency'));
    }
    public function getMadaCheckout(Request  $request){
        return getMadaCheckOutId($request->get('amount'),$request->get('currency'));
    }
    // this function which used in merchant status.

    public function paymentStatus(Request  $request){
         return heyperPayPaymentStatus($request->get('resourcePath'));
    }
   public function madaPaymentStatus(Request  $request){
         return heyperMadaPayPaymentStatus($request->get('resourcePath'));
    }


    protected function hyperPayPaymentProcess() {
        $url = "https://test.oppwa.com/v1/checkouts";
        $data = "entityId=8ac7a4ca742436880174263a59be1095" .
            "&amount=92.00" .
            "&currency=SAR" .
            "&paymentType=DB" ;
//            "&paymentBrand=VISA" .
//            "&card.number=4200000000000000" .
//            "&card.holder=Jane Jones" .
//            "&card.expiryMonth=05" .
//            "&card.expiryYear=2034" .
//            "&card.cvv=123";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer OGFjN2E0Y2E3NDI0MzY4ODAxNzQyNjM4ZjRjZjEwOGZ8c242a04zZmFDcQ=='));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        $response= json_decode($responseData,true);
        return $response;
    }



    protected function hyperPayPaymentProcessOld() {
        $url = "https://test.oppwa.com/v1/checkouts";
        $data = "entityId=8ac7a4ca742436880174263a59be1095" .
            "&amount=92.00" .
            "&currency=SAR" .
            "&paymentType=DB";

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer OGFjN2E0Y2E3NDI0MzY4ODAxNzQyNjM4ZjRjZjEwOGZ8c242a04zZmFDcQ='));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if(curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        return $responseData;
    }


}
