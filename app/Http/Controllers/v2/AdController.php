<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\AdvertiseSlider;
use App\Models\AppContent;
use App\Models\AppSetting;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Avatar;
use Storage;
use Carbon\Carbon;
use DB;
class AdController extends Controller
{
    public function index(Request $request,$countryId){

      $ads=AdvertiseSlider::where('is_active',true)->where('country_id',$countryId)->get();
        return response()->json(compact('ads'));
    }


}
