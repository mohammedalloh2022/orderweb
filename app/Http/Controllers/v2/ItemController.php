<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\AppSetting;
use App\Models\CategoriesContent;
use App\Models\Item;
use Avatar;
use DB;
use Illuminate\Http\Request;
use Storage;

class ItemController extends Controller
{


    public function index(Request $request, $category)
    {

        $items = Item::with('images')->where('type', 'normal')
            ->where('parent_id', null)
            ->where('classification_id', $category)
            ->where('is_active', 1)
            ->paginate(10);
        return response()->json(compact('items'));
    }

    public function getItemsBySubCategory(Request $request, $shop, $subcategory)
    {
        $items = Item::where('type', 'normal')->where('parent_id', null)->where('classification_id', $shop)->where('sub_cat_id', $subcategory)->where('is_active', 1)
            ->with('images', 'shop', 'extraItems')->paginate(50);
        return response()->json(compact('items'));
    }

    public function item(Request $request, $item)
    {
        $item = Item::with('extraItems', 'images', 'shop')->find($item);
        if ($item) {
            return response()->json(compact('item'));
        } else {
            return response()->json([
                'success' => false,
                'message' => 'There are no data'
            ], 422);
        }
    }

    public function offersSliders(Request $request)
    {
        $user_lat = $request->lat;
        $user_lng = $request->lng;
        $countryId = $request->country_id;
        if ($user_lat && $user_lng) {
            $shop_distance_limit_km = AppSetting::where('country_id', $countryId)->first()->shop_distance_limit_km;
            $shops = CategoriesContent::where("country_id", $countryId)->whereHas('category', function ($qq) {
                $qq->where('is_active', 1);
            });
            $shops = $shops->select("*", DB::raw("6371 * acos(cos(radians(" . $user_lat . "))
                                * cos(radians(lat)) * cos(radians(lng) - radians(" . $user_lng . "))
                                + sin(radians(" . $user_lat . ")) * sin(radians(lat))) AS distance"));

            $shops = $shops->having('distance', '<=', $shop_distance_limit_km)->where('parent_id', null)
                ->where('is_active', 1);
            $mainShopsids = $shops->pluck('id')->toArray();
            $shopBranches = CategoriesContent::where("country_id", $countryId)
                ->whereHas('category', function ($qq) {
                    $qq->where('is_active', 1);
                });
            $shopBranches = $shopBranches->select("*", DB::raw("6371 * acos(cos(radians(" . $user_lat . "))
                                * cos(radians(lat)) * cos(radians(lng) - radians(" . $user_lng . "))
                                + sin(radians(" . $user_lat . ")) * sin(radians(lat))) AS distance"));


            $shopBranches = $shopBranches->having('distance', '<=', $shop_distance_limit_km)
                ->where('parent_id', '!=', "null")->where('is_active', 1);
            $branchShopsIds = $shopBranches->pluck('parent_id')->toArray();

            $shopsIds = array_merge($branchShopsIds, $mainShopsids);
            $shop_wallet_limit = AppSetting::where('country_id', $countryId)->first()->shop_wallet_limit;
            $offers = Item::where('status', 'active')->where("is_active", 1)
                ->where('type', 'offer')->where('parent_id', null)->whereHas('shop', function ($q) use ($shop_wallet_limit, $shopsIds) {
                    $q->where('wallet', '>=', $shop_wallet_limit)->where('is_active', 1)->whereIn('id', $shopsIds);;
                })->with('shop')->get();
            return response()->json(compact('offers'));
        }
    }

    public function offersByCategory(Request $request, $category)
    {

        $countryId = $request->country_id;
        $offers = Item::where('status', 'active')->where("is_active", 1)->where('type', 'offer')->where('parent_id', null)->with('category')
            ->where('classification_id', $category)->whereHas('shop', function ($q) use ($countryId) {
                $q->where('country_id', $countryId);
            })
            ->get();
        return response()->json(compact('offers'));
    }

}
