<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\AppSetting;
use App\Models\CategoriesContent;
use App\Models\Country;
use App\Models\Coupon;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderExtraItem;
use App\Models\OrderItem;
use App\Models\RejectReasons;
use App\Models\Wallet;
use App\Notifications\SignupActivate;
use App\User;
use Avatar;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Storage;
use Validator;

class OrderController extends Controller
{
    public function generateRandomAndUniqueNumber()
    {

        $last_invoice = Order::latest()->pluck('invoice_number')->first();

        if ($last_invoice) {
            $serial = $last_invoice + 1;
            $serial = sprintf("%000006d", $serial);
        } else {
            $serial = 000001;
            $serial = sprintf("%000006d", $serial);
        }

        return $serial;

    }

    public function placeOrder(Request $request)
    {
        $validator = \Validator::make($request->all(), [

            'type_of_receive' => 'required',
            'type' => 'required', // normal or offer
            'sub_total_1' => 'required',
            'discount' => 'required',
            'sub_total_2' => 'required',
            'delivery' => 'required',
            'shop_id' => 'required',
            'total' => 'required',
            'country_id' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'delivery_type' => 'required',
//            'delivery_at' => 'required',
//            'tax' => 'required'
//            'items' => 'required|in:id,qty',
//            'qty.*' => 'required_if:contact,qty',
//            'id.*' => 'required_if:contact,id',
//            'coupon_id' => 'required',
//            'notes' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }
        if ($request->payment_type == 'credit_card' && !$request->transaction_id) {
            return response()->json([
                'success' => false,
                "message" => "Payment transaction failed"
            ]);
        }

//        if($request->coupon_id && ($request->total<0) ){
//            return response()->json([
//                'success'=>false,
//                "message"=>"Coupons discount value must be less than total"
//            ]);
//        }
        DB::beginTransaction();
        try {
            $time = Carbon::now()->subHours(2)->format('H:i:s');
//            $q->where('start_work_at','<=',$time)->where('end_work_at','>=',$time);
            $shop = CategoriesContent::findOrFail($request->shop_id);

//            $condition = false;
//            if ($shop->start_work_at > $shop->end_work_at) {
//                $startDate = Carbon::createFromFormat('H:i:s', '08:00 AM');
//                $endDate = Carbon::createFromFormat('H:i:s', '07:00 PM');
//
//                $condition = Carbon::now()->between($startDate, $endDate, true);
//
//
//            }
//            dd($shop, $time, $shop->start_work_at > $time, ($shop->end_work_at < $time), ($shop->start_work_at > $time) || ($shop->end_work_at < $time));

//            $condition = ($shop->end_work_at > $time) || ($shop->start_work_at < $time);
//            dd($condition);
            if (((($shop->start_work_at > $time) || ($shop->end_work_at < $time)) && ($shop->start_work_at < $shop->end_work_at))  ) {
                return response()->json([
                    'success' => false,
                    'message' => 'The restaurant close now,open at ' . $shop->start_work_at,

                ], 200);
            }

//            if ($shop->start_work_at > $shop->end_work_at) {
//
//            }

            $InvoiceSerial = null;

            $last_invoice = Order::where('country_id', $request->country_id)->latest()->pluck('invoice_number')->first();

            if ($last_invoice) {
                $InvoiceSerial = (int)$last_invoice + 1;
                $InvoiceSerial = sprintf("%000000009d", $InvoiceSerial);
            } else {
                $InvoiceSerial = 000001;
                $InvoiceSerial = sprintf("%000000009d", $InvoiceSerial);
            }

            $userLat = $request->lat;
            $userLng = $request->lng;
            $shopId = $request->shop_id;
            $branch = $this->nearestShop($userLat, $userLng, $request->shop_id, $request->country_id);
            if (isset($branch)) {
                $shopId = $branch;
            }
            $order = new Order();
            $order->type_of_receive = $request->type_of_receive;
            $order->type = $request->type;
            $order->country_id = $request->country_id;
            $order->invoice_number = $InvoiceSerial;
            $order->discount = (double)$request->discount;
            $order->sub_total_1 = (double)$request->sub_total_1;
            $order->sub_total_2 = (double)$request->sub_total_2;
            $order->total = (double)$request->total;
            $order->tax = (double)$request->tax;
            $order->coupon_id = (int)$request->coupon_id;
            $order->notes = $request->notes;
            $order->delivery = $request->delivery;
            $order->destination_lat = $request->destination_lat;
            $order->destination_lng = $request->destination_lng;
            $order->destination_address = $request->destination_address;
            $order->payment_type = $request->payment_type;
            $order->transaction_id = $request->transaction_id;
            $order->card_brand = $request->typeCard;
            $order->delivery_type = $request->delivery_type;
            $order->delivery_at = $request->delivery_at;
            $order->shop_id = $shopId;
            $order->status = 'pending';
            $order->user_id = $request->user('api')->id;
//            $order->created_at->timezone($this->getTimezone($request))->toDateTimeString();
//            \Timezone::convertToLocal($order->created_at);
            if ($request->type == 'offer') {
                if (!empty($request->items)) {
                    $order->offer_id = $request->items[0]['id'];
                }
            }
            $order->save();

            if (!empty($request->items) && count($request->items)) {
                foreach ($request->items as $item) {

                    $orderItem = new OrderItem();
                    $orderItem->order_id = $order->id;
                    $orderItem->item_id = (int)$item['id'];
                    $orderItem->price = (float)$item['price'];
                    $orderItem->qty = (int)$item['qty'];
                    $orderItem->total = (((int)$item['qty']) * ((float)$item['price']));
                    $orderItem->save();
                    if (isset($item['extra']) && !empty($item['extra'])) {


                        foreach ($item['extra'] as $extraItem) {
                            $itemData = Item::findOrFail($extraItem['item_id']);
                            $orderExtraItem = new OrderExtraItem();
                            $orderExtraItem->name_ar = $itemData->name_ar;
                            $orderExtraItem->name_en = $itemData->name_en;
                            $orderExtraItem->order_item_id = $orderItem->id;
                            $orderExtraItem->item_id = $extraItem['item_id'];
                            $orderExtraItem->price = $extraItem['price'];
                            $orderExtraItem->qty = $extraItem['qty'];
                            $orderExtraItem->total = ($extraItem['qty'] * $extraItem['price']);
                            $orderExtraItem->save();
                        }
                    }
                }
            }


            // send notification to all admin
            //
            $admins = User::where('role', 'admin')->get();

            foreach ($admins as $admin) {
                if ($admin->can('New orders notifications')) {
                    $message = [
                        'msg' => "new order #" . $InvoiceSerial,
                        'title' => "new order #" . $InvoiceSerial,
                        'order_id' => $order->id,
                        'order_no' => $InvoiceSerial,
                        'type' => '4station',
                        'type_of_receive' => $order->type_of_receive,
                        'country_id' => @$order->country_id
                    ];
                    $admin->notify(new SignupActivate($message));

                    sendFcmWeb($admin->fcm_token, $message);
                } else {
                    $countries = Country::get()->pluck('code');
                    foreach ($countries as $country) {


                        if ($admin->can('New orders notifications ' . $country)) {
                            $message = [
                                'msg' => "new order #" . $InvoiceSerial,
                                'title' => "new order #" . $InvoiceSerial,
                                'order_id' => $order->id,
                                'order_no' => $InvoiceSerial,
                                'type' => '4station',
                                'type_of_receive' => $order->type_of_receive,
                                'country_id' => @$order->country_id
                            ];
                            $admin->notify(new SignupActivate($message));
                            sendFcmWeb($admin->fcm_token, $message);
                            break;
                        }

                    }
                }

            }
            $shop = CategoriesContent::with('user', 'branchParent.user')->findOrFail($shopId);
//
            $user = User::find($shop->user['id']);

            if ($user) {
                $message = [
                    'msg' => "new order #" . $InvoiceSerial,
                    'title' => "new order #" . $InvoiceSerial,
                    'order_id' => $order->id,
                    'order_no' => $InvoiceSerial,
                    'type' => '4station',
                    'type_of_receive' => $order->type_of_receive,
                    'country_id' => @$order->country_id
                ];
                $user->notify(new SignupActivate($message));
                sendFCM($user->fcm_token, $message);
            }
//
            if (!empty($shop->branchParent)) {
                $user = User::find($shop->branchParent->user['id']);
                if ($user) {
                    $message = [
                        'msg' => "new order #" . $InvoiceSerial,
                        'title' => "new order #" . $InvoiceSerial,
                        'order_id' => $order->id,
                        'order_no' => $InvoiceSerial,
                        'type' => '4station',
                        'type_of_receive' => $order->type_of_receive,
                        'country_id' => @$order->country_id
                    ];
                    $user->notify(new SignupActivate($message));
                    sendFcmWeb($user->fcm_token, $message);
                }

            }

            return response()->json([
                'success' => true,
                'message' => 'Order placed successfully',
                'order_id' => $order->id,
                "type" => '4station',
                'order_number' => $InvoiceSerial,
                'order_timestamp' => strtotime($order->created_at)
            ], 200);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong',
                'errors' => $e->getMessage()
            ], 422);
        }

    }


    public function index(Request $request)
    {

        $orders = Order::with("orderItems.item", 'orderItems.extraItems.item')->where('user_id', $request->user()->id)->paginate(10);
        return response()->json(compact('orders'));
    }

    public function getSingleOrderDetails($order)
    {

        $order = Order::with('orderItems.item.images', 'orderItems.extraItems.item', 'client', 'coupon', 'deliveryUser', 'shop')->find($order);
        if ($order) {
            return response()->json(compact('order'));
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Can\'t find order  '
            ], 402);
        }
    }

    public function checkCoupon(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'country_id' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }

        $coupon = Coupon::where('country_id', $request->country_id)->where('code', '=', $request->code)->where('active', 1)
            ->where('expire_at', '>=', Carbon::now()->format("Y-m-d"))
            ->first();
        if ($coupon) {
            if ($coupon->count_of_use >= $coupon->max_use_no) {
                return response()->json([
                    'success' => false,
                    'message' => 'Coupon unavailable'
                ], 402);
            }
//            $appComitions=AppSetting::find(1)->pluck('app_order_commission')[0];
//            $appDeliveryComitions=AppSetting::find(1)->pluck('app_delivery_commission')[0];
//
//            $app_commition_amount=(float)$request->sub_total_1*((float)$appComitions/100);
//            $app_delivery_comission=(float)$request->delivery*((float)$appDeliveryComitions/100);
//
//            $app_revenue=($app_commition_amount+$app_delivery_comission);
//            if($coupon->value>$app_revenue){
//                return response()->json([
//                    'success' => false,
//                    'message' => 'Coupon unable to use at this order'
//                ], 402);
//            }
            return response()->json([
                'success' => true,
                'message' => 'Coupon able to use',
                "discount_value" => $coupon->value,
                "coupon_id" => $coupon->id
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Coupon unavailable'
            ], 402);
        }
    }

    public function checkCouponByCity(Request $request)
    {
        $user = $request->user('api');
        $city_id = $user->city_id;
        $validator = \Validator::make($request->all(), [
            'code' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }

        if (!$city_id) {
            return response()->json([
                'success' => false,
                'message' => "هذا المستخدم غير مدرج تحت مدينة معينة"], 422);
        }
        $coupon = Coupon::where('city_id', $city_id)->where('code', '=', $request->code)->where('active', 1)
            ->where('expire_at', '>=', Carbon::now()->format("Y-m-d"))
            ->first();
        if ($coupon) {
            if ($coupon->count_of_use >= $coupon->max_use_no) {
                return response()->json([
                    'success' => false,
                    'message' => 'Coupon unavailable'
                ], 402);
            }

            return response()->json([
                'success' => true,
                'message' => 'Coupon able to use',
                "discount_value" => $coupon->value,
                "coupon_id" => $coupon->id
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Coupon unavailable'
            ], 402);
        }
    }


    public function getAllPendingOrders(Request $request)
    {
        $orders = Order::where("status", 'pending')->where('driver_id', null)->orderBy('created_at', 'desc')->with('client')->get();
        return response()->json(compact('orders'));
    }

    public function getOrderDetials(Request $request, $id)
    {

        $order = Order::with("orderItems.item", 'orderItems.extraItems', "client", "shop", "deliveryUser")->find($id);
        return response()->json($order);
    }

    public function pickupOrder(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'order_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }

        DB::beginTransaction();

        try {
            $currentOrder = Order::where('driver_id', $request->user()->id)->where('status', 'on_the_way')->first();
            if (isset($currentOrder)) {
                return response()->json(
                    [
                        'success' => false,
                        'message' => "You must delivered on the way  previous order first",
                    ], 402);
            }
            $order = Order::findOrFail($request->order_id);
            $message = null;
            if ($order->status == 'ready') {
                $order->driver_id = $request->user('api')->id;
                $order->status = "on_the_way";
                $order->save();
                $driver = User::findOrFail($request->user('api')->id);
                $driver->orders_balance -= 1;
                $driver->save();
                if ($driver->orders_balance == 0) {
                    $message = [
                        'msg' => "Your achieve last order balance contact management ",
                        'title' => "Your achieve last order balance contact management ",
                        'type' => '4station',
                        'country_id' => @$driver->country_id
                    ];
                    if ($driver->fcm_token) {
                        sendFCM($driver->fcm_token, $message);
                    }
                    $driver->notify(new SignupActivate($message));
                }
                $newuser = User::find($order->user_id);
                if ($newuser) {
                    $message = [
                        'msg' => "your order #" . $order->invoice_number . " in the way",
                        'title' => "your order #" . $order->invoice_number . " in the way",
                        'order_id' => $order->id,
                        'type' => '4station',
                        'status' => $order->status,
                        'type_of_receive' => $order->type_of_receive,
                        'country_id' => @$order->country_id
                    ];
                    if ($newuser->fcm_token) {
                        sendFCM($newuser->fcm_token, $message);
                    }
                    $newuser->notify(new SignupActivate($message));
                }
                return response()->json([
                        'success' => true,
                        'message' => "order picked up successfully",
                    ]
                    , 200);
            } else {
                return response()->json([
                        'success' => false,
                        'message' => "order not longer available to pickup",
                    ]
                    , 422);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong',
                'error' => $e->getMessage()
            ], 422);
        }
        return response()->json([
                'success' => true,
                'message' => "order picked up successfully",

            ]
            , 200);

    }

    public function DeliveredOrder(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'order_id' => 'required|integer',
        ]);
        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }

        DB::beginTransaction();
        try {
            $order = Order::where('driver_id', $request->user('api')->id)->find($request->order_id);
            if ($order->status == 'on_the_way') {


                $ordershop = CategoriesContent::with('branchParent')->findOrFail($order->shop_id);
                if ($ordershop->branchParent) {
                    $appComitions = $ordershop->branchParent->app_commission;
                } else {
                    $appComitions = $ordershop->app_commission;
                }

                $appDeliveryComitions = AppSetting::where('country_id', $order->country_id)->get()->pluck('app_delivery_commission')->first();

                $app_commition_amount = (float)$order->sub_total_1 * ((float)$appComitions / 100);
                $app_delivery_comission = (float)$order->delivery * ((float)$appDeliveryComitions / 100);
                $order->app_delivery_commission = $app_delivery_comission;
                $order->app_shop_commission = $app_commition_amount;
                $order->app_revenue = ((float)$order->tax + $app_commition_amount + $app_delivery_comission);
                $order->app_commission = $order->app_revenue;
                if ($order->discount) {
                    $order->app_revenue -= (float)$order->discount;
                }
                $order->driver_revenue = ((float)$order->delivery - $app_delivery_comission);
                $order->shop_revenue = (float)$order->sub_total_1 - $app_commition_amount;
                $order->save();


                // driver wallet
                $driver = User::find($order->driver_id);
                $wallet = new Wallet();
                $wallet->order_id = $order->id;
                $wallet->order_cost = $order->total;
                $wallet->delivery_cost = $order->delivery;
                $wallet->driver_id = $order->driver_id;
                if ($order->payment_type == 'on_delivery') {
                    $wallet->in = $order->total;
                    $wallet->total = (-(float)$order->total + (float)$order->driver_revenue);
                    $driver->wallet += (-(float)$order->total + (float)$order->driver_revenue);
                    $driver->save();
                } else {
                    $wallet->total = (float)$order->driver_revenue;
                    $driver->wallet += (float)$order->driver_revenue;
                    $driver->save();
                }
                $wallet->save();
                $driver_wallet_limit = AppSetting::where('country_id', $order->country_id)->latest()->pluck('driver_wallet_limit')->first();

                if ($driver->wallet <= $driver_wallet_limit) {
                    $message = [
                        'msg' => "Your wallet exceeded the credit limit",
                        'title' => "Your wallet exceeded the credit limit",
                        'type' => '4station',
                        'country_id' => @$driver->country_id
                    ];
                    if ($driver->fcm_token) {
                        sendFCM($driver->fcm_token, $message);
                    }
                    $driver->notify(new SignupActivate($message));
                }


                $shop = CategoriesContent::findOrFail($order->shop_id);
                $shop->wallet += ($order->shop_revenue);
                $shop->save();
                $order->order_store_wallet = (float)($order->shop_revenue);
                $order->save();
                if ($shop->parent_id) {
                    $parentShop = CategoriesContent::findOrFail((int)$shop->parent_id);
                    $parentShop->wallet += ($order->shop_revenue);
                    $parentShop->save();
                }


                $order->status = "delivered";
                if ($order->save()) {
                    $newuser = User::findOrFail($order->user_id);
                    $message = [
                        'msg' => "Your order #" . $order->invoice_number . " has been delivered ",
                        'title' => "Your order #" . $order->invoice_number . "has been delivered ",
                        'order_id' => $order->id,
                        'type' => '4station',
                        'status' => $order->status,
                        'type_of_receive' => $order->type_of_receive,
                        'country_id' => @$order->country_id
                    ];
                    if ($newuser->fcm_token) {
                        sendFCM($newuser->fcm_token, $message);
                    }
                    $newuser->notify(new SignupActivate($message));

                }


            } else {
                return response()->json([
                    'success' => false,
                    'message' => "Something going Wrong",
                ], 422);
            }
            return response()->json([
                'success' => true,
                'message' => "order delivered successfully",
            ], 200);

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
        return response()->json([
            'success' => false,
            'message' => 'Something going wrong'
        ], 402);

    }

    public function nearestShop($latitude, $longitude, $shopId, $countryId)
    {
//        $latitude       =       "28.418715";
//        $longitude      =       "77.0478997";

        $shop_distance_limit_km = AppSetting::where('country_id', $countryId)->get()->pluck('shop_distance_limit_km')->first();
        $hasBranches = CategoriesContent::findOrFail($shopId)->hasBranches();
        if ($hasBranches && $latitude && $longitude) {
            $shops = DB::table("categories_contents");
            $shops = $shops->select("*", DB::raw(" 111.045 * acos(cos(radians(" . $latitude . "))
                                * cos(radians(lat)) * cos(radians(lng) - radians(" . $longitude . "))
                                + sin(radians(" . $latitude . ")) * sin(radians(lat))) AS distance"));
//            $shops          =       $shops->having('distance', '<', 100);
            $shops = $shops->where('is_active', 1)->where(function ($q) use ($shopId) {
                $q->where('parent_id', $shopId)->orWhere('id', $shopId);
            });

            $shops = $shops->orderBy('distance', 'asc');
            $shops = $shops->where('country_id', $countryId);
            $shops = $shops->having('distance', '<=', $shop_distance_limit_km);
            $shops = $shops->get();
            $shop = null;
            foreach ($shops as $key => $shopy) {
                if ($key == 0) {
                    $shop = $shopy->id;
                }
                break;
            }
            return $shop;
        } else {
            return null;
        }
    }

    public function getClientOrders(Request $request)
    {
        $delivered_orders = Order::with(['shop', 'orderItems.extraItems', 'orderItems.item'])->where(function ($q) {
            $q->where('status', 'delivered')->orWhere('status', 'cancelled');
        })->where('user_id', $request->user()->id)
            ->orderBy('created_at', 'desc')->get();

        $in_progress_orders = Order::with(['shop', 'orderItems.extraItems', 'orderItems.item'])
            ->where('status', '!=', 'delivered')
            ->where('status', '!=', 'cancelled')
            ->where('user_id', $request->user()->id)->orderBy('created_at', 'desc')->get();

        return response()->json([
            'success' => true,
            'delivered_orders' => $delivered_orders,
            'in_progress_orders' => $in_progress_orders,

        ], 200);
    }

    public function getDriverOrders(Request $request)
    {
        $delivered_orders = Order::with('shop', 'client')->where('driver_id', $request->user()->id)->orderBy('created_at', 'desc')->get();

        return response()->json([
            'success' => true,
            'orders' => $delivered_orders,


        ], 200);
    }

    public function clientCancelOrder(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'order_id' => 'required|integer',
            'cancel_reasons_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }

        DB::beginTransaction();

        try {

            $cancelReason = RejectReasons::findOrFail($request->cancel_reasons_id);
            $order = Order::where('user_id', $request->user()->id)->where(function ($q) {
                // $q->where('status','pending')->orWhere('status','preparing');
                $q->where('status', '!=', 'on_the_way')->orWhere('status', 'delivered');

            })->find($request->order_id);

            if ($order) {
                $order->status = "cancelled";
                $order->cancel_reason_id = $request->cancel_reasons_id;
                $order->save();
                if ($order->driver_id) {
                    $driver = User::find($order->driver_id);
                    if ($driver) {
                        $message = [
                            'msg' => "Client cancel order #" . $order->invoice_number . " " . $cancelReason->reason_en,
                            'title' => "4station",
                            "type" => "public",
                            'public_order_id' => $order->id,
                            "status" => $order->status,
                            "country_id" => $order->country_id,
                        ];
                        if ($driver->fcm_token) {
                            sendFCM($driver->fcm_token, $message);
                        }
                        $driver->notify(new SignupActivate($message));
                    }
                }
                $user = User::find($order->user_id);
                if ($user) {
                    $message = [
                        'msg' => "Client cancel order #" . $order->invoice_number . " " . $cancelReason->reason_en,
                        'title' => "Client cancel order #" . $order->invoice_number . " " . $cancelReason->reason_en,
                        "type" => "public",
                        'public_order_id' => $order->id,
                        "status" => $order->status,
                        "country_id" => $order->country_id,
                    ];
                    if ($user->fcm_token) {
                        sendFCM($user->fcm_token, $message);
                    }
                    $user->notify(new SignupActivate($message));
                }


                return response()->json([
                    'success' => true,
                    'message' => "You order canceled successfully",
                ], 200);
            } else {
                return response()->json([
                        'success' => false,
                        'message' => "order not longer available",
                    ]
                    , 422);
            }
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong',
                'error' => $e->getMessage()
            ], 422);
        }
        return response()->json(
            ['success' => true,
                'message' => "You order canceled successfully",], 200);

    }
}
