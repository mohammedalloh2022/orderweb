<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\AppContent;
use App\Models\AppSetting;
use App\Models\City;
use App\Models\Country;
use App\Models\DeliveryPrice;
use http\Env\Response;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Avatar;
use Storage;
use Carbon\Carbon;
use DB;
class AppSettingController extends Controller
{
    public function index(Request $request){

        $validator = \Validator::make($request->all(), [
            'country_id' => 'required',
        ]);
        if($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data ",
                    'errors' => $validator->messages()]
                , 422);
        }

        $settings=AppSetting::where('country_id',$request->country_id)->first();
        if ($settings){
            return response()->json(compact('settings'));
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }

    }

    public function appContent(Request $request){



        $appContent=AppContent::select([
            "facebook_link",
        "instagram_link",
        "linkedin_link",
        "twitter_link",
        "mobile",
        "address",
        "email",
        "country_id"
        ])->where('country_id',$request->country_id)->first();


        return response()->json(['status'=>true,'data'=>$appContent]);

    }

    public function clientContent(){

        if(app()->getLocale()=='ar'){

        $privacy_title=AppContent::findOrFail(1)->pluck('client_privacy_title_ar')[0];
        $privacy_content=AppContent::findOrFail(1)->pluck('client_privacy_content_ar')[0];
        }elseif (app()->getLocale()=='en'){

        $privacy_title=AppContent::findOrFail(1)->pluck('client_privacy_title_en')[0];
        $privacy_content=AppContent::findOrFail(1)->pluck('client_privacy_content_en')[0];

        }

        return response()->json(compact('privacy_title','privacy_content'));
    }

    public function driverContent(){

        if(app()->getLocale()=='ar'){
            $privacy_title=AppContent::findOrFail(1)->pluck('driver_privacy_title_ar')[0];
            $privacy_content=AppContent::findOrFail(1)->pluck('driver_privacy_content_ar')[0];
        }elseif (app()->getLocale()=='en'){
            $privacy_title=AppContent::findOrFail(1)->pluck('driver_privacy_title_en')[0];
            $privacy_content=AppContent::findOrFail(1)->pluck('driver_privacy_content_en')[0];
        }
        return response()->json(compact('privacy_title','privacy_content'));
    }
    public function getCountriesList(){
    $countries=Country::with('setting')->where('is_active',true)->get();
     return response()->json(compact('countries'));
    }
    public function getCitiesList($country_id){

    $country=Country::find((int)$country_id);
    $cities=$country->cities;
     return response()->json(compact('cities'));
    }

    public function getDeliveryCost(Request $request){

        $validator = \Validator::make($request->all(), [
            'distance' => 'required',
            'country_id' => 'required',
        ]);
        if($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }
        $distance=$request->distance;

        $price = DeliveryPrice::where(function ($query) use ($distance) {
            $query->where('from', '<=', $distance);
            $query->where('to', '>=', $distance);
        })->where('country_id',$request->country_id)->first();
        if($price){
            return response()->json(['status'=>true,'delivery_cost'=>$price->price]);
        }
        return response()->json(['status'=>false,'delivery_cost'=>0]);
    }
}
