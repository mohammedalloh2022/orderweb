<?php

namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\CategoriesContent;
use App\Models\Category;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\SubCategory;
use Avatar;
use Illuminate\Http\Request;
use Storage;
use Carbon\Carbon;
use DB;

class CartController extends Controller
{
    public function storeCard(Request $request){
        $request->validate([
            'type_of_receive' => 'required',
            'type' => 'required', // normal or offer
            'sub_total_1' => 'required',
            'discount' => 'required',
            'sub_total_2' => 'required',
            'delivery' => 'required',
            'shop_id' => 'required',
            'total' => 'required',
        ]);

//    return $request->all();
        DB::beginTransaction();
        try{
            $userLat=$request->user('api')->lat;
            $userLng=$request->user('api')->lng;
            $shopId=$request->shop_id;

            $branch=  $this->nearestShop($userLat,$userLng,$request->shop_id);
            if(isset($branch)){
                $shopId=$branch;
            }
            $order= new Order();
            $order->type_of_receive=$request->type_of_receive;
            $order->type=$request->type;
            $order->invoice_number=$this->generateRandomAndUniqueNumber();
            $order->discount=(float)$request->discount;
            $order->sub_total_1=(float)$request->sub_total_1;
            $order->sub_total_2=(float)$request->sub_total_2;
            $order->total=(float)$request->total;
            $order->tax=(float)$request->tax;
            $order->coupon_id=$request->coupon_id;
            $order->notes=$request->notes;
            $order->destination_lat=$request->destination_lat;
            $order->destination_lng=$request->destination_lng;
            $order->destination_address=$request->destination_address;
            $order->shop_id=$shopId;
            $order->user_id=$request->user('api')->id;
            $order->save();
            if(!empty($request->items) && count($request->items)){
                foreach($request->items as $item){
                    $orderItem= new OrderItem();
                    $orderItem->order_id=$order->id;
                    $orderItem->item_id=$item['id'];
                    $orderItem->price=(float)$item['price'];
                    $orderItem->qty=(int)$item['qty'];
                    $orderItem->total=(((int)$item['qty']) *((float)$item['price']));
                    $orderItem->save();
                }
            }
            $invoice_number=mb_strlen($order->invoice_number);
            switch ($invoice_number){
                case 1:
                    $invoice_number=(string)'00000'+$order->invoice_number;
                    break;
                case 2:
                    $invoice_number=(string)'0000'+$order->invoice_number;
                    break;
                case 3:
                    $invoice_number=(string)'000'+$order->invoice_number;
                    break;
                case 4:
                    $invoice_number=(string)'00'+$order->invoice_number;
                    break;
                case 5:
                    $invoice_number=(string)'0'+$order->invoice_number;
                    break;

            }
            return response()->json([
                'success' => true,
                'message' => 'Order placed successfully',
                'order_id'=>$order->id,
                'order_number'=>$invoice_number,
                'order_timestamp'=>strtotime($order->created_at)
            ], 200);
            DB::commit();
        }catch (\Exception $e){
            DB::rollback();
            return $e->getMessage();

            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }

    }


    public function placeOrder2(Request $request){
        $request->validate([
//            'items' => 'required|in:id,qty',
//            'qty.*' => 'required_if:contact,qty',
//            'id.*' => 'required_if:contact,id',
            'type_of_receive' => 'required',
            'type' => 'required', // normal or offer
            'sub_total_1' => 'required',
            'discount' => 'required',
            'sub_total_2' => 'required',
//            'tax' => 'required',
            'delivery' => 'required',
            'shop_id' => 'required',
            'total' => 'required',

//            'coupon_id' => 'required',
//            'notes' => 'required',

        ]);

//    return $request->all();
        DB::beginTransaction();
        try{
            $userLat=$request->user('api')->lat;
            $userLng=$request->user('api')->lng;
            $shopId=$request->shop_id;

            $branch=  $this->nearestShop($userLat,$userLng,$request->shop_id);
            if(isset($branch)){
                $shopId=$branch;
            }
            $order= new Order();
            $order->type_of_receive=$request->type_of_receive;
            $order->type=$request->type;
            $order->invoice_number=$this->generateRandomAndUniqueNumber();
            $order->discount=(float)$request->discount;
            $order->sub_total_1=(float)$request->sub_total_1;
            $order->sub_total_2=(float)$request->sub_total_2;
            $order->total=(float)$request->total;
            $order->tax=(float)$request->tax;
            $order->coupon_id=$request->coupon_id;
            $order->notes=$request->notes;
            $order->destination_lat=$request->destination_lat;
            $order->destination_lng=$request->destination_lng;
            $order->destination_address=$request->destination_address;
            $order->shop_id=$shopId;
            $order->user_id=$request->user('api')->id;
            $order->save();
            if(!empty($request->items) && count($request->items)){
                foreach($request->items as $item){
                    $orderItem= new OrderItem();
                    $orderItem->order_id=$order->id;
                    $orderItem->item_id=$item['id'];
                    $orderItem->price=(float)$item['price'];
                    $orderItem->qty=(int)$item['qty'];
                    $orderItem->total=(((int)$item['qty']) *((float)$item['price']));
                    $orderItem->save();
                }
            }
            $invoice_number=mb_strlen($order->invoice_number);
            switch ($invoice_number){
                case 1:
                    $invoice_number=(string)'00000'+$order->invoice_number;
                    break;
                case 2:
                    $invoice_number=(string)'0000'+$order->invoice_number;
                    break;
                case 3:
                    $invoice_number=(string)'000'+$order->invoice_number;
                    break;
                case 4:
                    $invoice_number=(string)'00'+$order->invoice_number;
                    break;
                case 5:
                    $invoice_number=(string)'0'+$order->invoice_number;
                    break;

            }
            return response()->json([
                'success' => true,
                'message' => 'Order placed successfully',
                'order_id'=>$order->id,
                'order_number'=>$invoice_number,
                'order_timestamp'=>strtotime($order->created_at)
            ], 200);
            DB::commit();
        }catch (\Exception $e){
            DB::rollback();
            return $e->getMessage();

            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }

    }


}
