<?php


namespace App\Http\Controllers\v2;

use App\Http\Controllers\Controller;
use App\Models\AppSetting;
use App\Models\Order;
use App\Models\PublicOrder;
use App\Models\RejectReasons;
use App\Notifications\SignupActivate;
use App\User;
use DB;
use File;
use Illuminate\Http\Request;
use Image;
use Kreait\Firebase\Factory;
use Storage;

class PublicOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function driverWallet(Request $request)
    {
        $driver = User::findOrFail($request->user()->id);
        $total_orders_amount = (double)(PublicOrder::where('status', 'delivered')->where('accounted_with_driver', 0)->where('driver_id', $request->user()->id)
            ->select(\DB::raw("SUM(total) as total"))->pluck('total')[0]);
        $total_driver_revenue = (PublicOrder::where('status', 'delivered')->where('accounted_with_driver', 0)->where('driver_id', $request->user()->id)
            ->select(\DB::raw("SUM(driver_revenue) as total"))->pluck('total')[0]);
        $total_app_revenue = PublicOrder::where('status', 'delivered')->where('accounted_with_driver', 0)->where('driver_id', $request->user()->id)
            ->select(\DB::raw("SUM(app_revenue) as total"))->pluck('total')[0];
        $totalClientBills = PublicOrder::where('status', 'delivered')->where('accounted_with_driver', 0)->where('driver_id', $request->user()->id)
            ->select(\DB::raw("SUM(purchase_invoice_value) as total"))->pluck('total')[0];
        $all_orders = PublicOrder::where('status', 'delivered')->where('accounted_with_driver', 0)->where('driver_id', $request->user()->id)->orderBy('created_at', 'desc')->get();
        $total_driver_revenue = (double)(number_format($total_driver_revenue, 2, '.', ','));
        $wallet = (double)($driver->public_wallet);
        $total_app_revenue = (double)(number_format($total_app_revenue, 2, '.', ','));
        $totalClientBills = (double)(number_format($totalClientBills, 2, '.', ','));
        $total_orders_amount = (double)(number_format($total_orders_amount, 2, '.', ','));

        return response()->json(compact('total_orders_amount',
            'total_driver_revenue',
            'total_app_revenue',
            'totalClientBills',
            'all_orders',
            'wallet'));
    }

    public function getOrderDetails(Request $request, $order)
    {
        $order = PublicOrder::with('client', 'driver', 'attachments')->findOrFail($order);
        return response()->json(compact('order'));
    }

    public function pendingOrders(Request $request)
    {
        $orders = PublicOrder::with('client', 'driver')->where('status', 'pending')->get();
        return response()->json(compact('orders'));
    }

    public function driverOrdersList(Request $request)
    {
        $user = $request->user('api');
        if ($user) {
            $orders = PublicOrder::where('driver_id', $user->id)->orderBy('created_at', 'desc')->paginate(7);
            return response()->json(compact('orders'));
        }
        return response()->json([
            'status' => false,
            'message' => "Unable to handle user api"
        ]);
    }


    public function clientOrdersList(Request $request)
    {
        $orders = PublicOrder::
        where('client_id', $request->user('api')->id)
            ->orderBy('created_at', 'desc')
            ->paginate(7);
        return response()->json(compact('orders'));
    }

    public function orderDetails(Request $request, $orderId)
    {
        $orders = PublicOrder::findOrFail($orderId);
        return response()->json(compact('orders'));
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function placeOrder(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'store_name' => 'required',
            'country_id' => 'required',
//            'place_id' => 'required',
            'delivery_cost' => 'required',
            'tax' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }
//    return $request->all();
        DB::beginTransaction();
        try {
            $InvoiceSerial = null;
            $last_invoice = PublicOrder::latest()->pluck('invoice_number')->first();

            if ($last_invoice) {
                $InvoiceSerial = $last_invoice + 1;
                $InvoiceSerial = sprintf("%000006d", $InvoiceSerial);
            } else {
                $InvoiceSerial = 000001;
                $InvoiceSerial = sprintf("%000006d", $InvoiceSerial);
            }

            $order = new PublicOrder();
            $order->store_name = $request->store_name;
            $order->country_id = $request->country_id;
            $order->place_id = $request->place_id;
            $order->invoice_number = $InvoiceSerial;
            $order->delivery_cost = round((double)$request->delivery_cost, 2);
            $order->tax = round((double)$request->tax, 2);
            $order->total = round(((double)$request->tax + (double)$request->delivery_cost), 2);

            $order->note = $request->note;

//            $appComitions=AppSetting::where('country_id',$request->country_id)->get()->pluck('app_commission_public_delivery')[0];
            $appComitions = AppSetting::where('country_id', $request->country_id)->latest()->pluck('app_commission_public_delivery')->first();
            $app_commition_amount = round(((double)$request->delivery_cost * ((double)$appComitions / 100)), 2);
            $order->app_tax = round((double)$request->tax, 2);
            $order->app_delivery_commission = round($app_commition_amount, 2);
            $order->app_revenue = round(((double)$request->tax + $app_commition_amount), 2);
            $order->driver_revenue = round(((double)$request->delivery_cost - $app_commition_amount), 2);
//            $order->total=(float)$request->total;

            $order->destination_lat = $request->destination_lat;
            $order->destination_lng = $request->destination_lng;
            $order->destination_address = $request->destination_address;

            $order->store_lat = $request->store_lat;
            $order->store_lng = $request->store_lng;
            $order->store_address = @$request->store_address;

            $order->client_id = $request->user('api')->id;

            $order->save();

            if ($request->hasFile('attachment')) {

                $files = $request->file('attachment');

                foreach ($files as $file) {
                    if ($file) {
                        $img = Image::make($file);
                        $extension = explode('/', $img->mime)[1];
                        if (in_array($extension, ['jpg', 'jpeg', 'gif', 'bmp', 'png'])) {
                            $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                            $destinationPath = public_path('uploads/publicOrders/');
                            File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                            $img->save($destinationPath . '/' . $fileNameToStore);

                            $image = new \App\Models\Image();
                            $image->name = $fileNameToStore;
                            $image->content_id = $order->id;
                            $image->content_type = PublicOrder::class;
                            $image->is_default = false;
                            $image->value = 'public_order';
                            $image->save();
                        } else {
                            return response()->json([
                                'success' => false,
                                'message' => 'Supported image types jpg,jpeg,gif,bmp &png',

                            ], 422);
                            $order->forceDelete();
                        }
                    }


                }
            }

            try {
                $this->sendOrderTodriverDelivery($order);
            } catch (\Exception $e) {
                // dd($e);
                return response()->json([
                    'status' => 'failed',
                    'msg' => 'failed to send order to drivers',
                    'faild_msg' => $e->getMessage(),

                ], 422);
            }


            return response()->json([
                'success' => true,
                'message' => 'Public order placed successfully',
                'public_order_id' => $order->id,
                "type" => 'public',
                'order_number' => $InvoiceSerial,
                'order_timestamp' => strtotime($order->created_at)
            ], 200);
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
//            return $e->getMessage();

            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }

    }


    protected function sendOrderTodriverDelivery($order)
    {

        $nearestdriverArray = $this->nearsetDriversLocation($order);
        $wallet_limit = AppSetting::where('country_id', $order->country_id)->latest()->pluck('driver_wallet_limit')->first();
        $drivers = User::where('is_documented', 1)->where('active', 1)->where('is_online', 1)->where('role', 'delivery_driver')->whereDoesntHave('driverOrder', function ($q) {
            $q->where('status', "on_the_way");
        })->whereDoesntHave('driverPublicOrder', function ($q) {
            $q->where('status', "in_the_way_to_store")->orWhere('status', 'in_the_way_to_client');
        })->where('wallet', '>=', $wallet_limit)->where("orders_balance", ">", 0)->whereIn('id', $nearestdriverArray)->get();

        if (!empty($drivers)) {
            foreach ($drivers as $driver) {


                try {
                    $message = [
                        'msg' => "new order",
                        'title' => "new order",
                        'invoice_number' => $order->invoice_number,
                        'public_order_id' => $order->id,
                        "type" => 'public',
                        "status" => $order->status,
                        "store_name" => $order->store_name,
                        "destination_address" => $order->destination_address,
                        "destination_lng" => $order->destination_lng,
                        "destination_lat" => $order->destination_lat,
                        "country_id" => $order->country_id,

                    ];
                    if ($driver->fcm_token) {
                        sendFCMWithTimeExpiration($driver->fcm_token, $message);
                    }
                    $driver->notify(new SignupActivate($message));
                } catch (\Exception $e) {
                    return $e->getMessage();
                }

            }
        }
    }

    protected function nearsetDriversLocation($shop)
    {
        // dd(app_path('Firebase/' . env('FIREBASE_SDK', 'order-station-27464-firebase-adminsdk-lukht-ec4052f08a.json')));
        $factory = (new Factory)->withServiceAccount(config('firebase.firebase_file_path'))
            ->withDatabaseUri(config('firebase.database_url'));
        $database = $factory->createDatabase();
        $reference = $database->getReference('delivery_app_tracking');
        $data = $database->getReference('delivery_app_tracking')->orderByChild('country_id')->equalTo((int)$shop->country_id)->getValue();
        // dd($data);
        $onlineDrivers = collect($data)->where('status', '=', 'online')->where('country_id', $shop->country_id);
        $driver_distance_limit_km = AppSetting::where('country_id', $shop->country_id)->latest()->pluck('driver_distance_limit_km')->first();
        $subset = $onlineDrivers->filter(function ($item) use ($shop, $driver_distance_limit_km) {
            $distanceBetween = distanceBetweenTwoPoints($shop->store_lat, $shop->store_lng, $item['lat'], $item['lng'], 'K');
            return $distanceBetween <= $driver_distance_limit_km;
        });
        $nearestDriver = $subset->all();
        $nearestDriver2 = collect($nearestDriver)->pluck('driver_id');
        return $nearestDriver2;
    }


    public function pickupOrder(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'order_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }

        DB::beginTransaction();
        try {
            $currentOrder = Order::where('driver_id', $request->user()->id)->where('status', 'on_the_way')->first();
            $publicOrder = PublicOrder::where('driver_id', $request->user()->id)->where(function ($query) {
                $query->where('status', "in_the_way_to_store")->orWhere('status', 'in_the_way_to_client');
            })->first();

            if ($currentOrder || $publicOrder) {
                return response()->json([
                        'success' => false,
                        'message' => "You must delivered on the way  previous order first",
                    ]
                    , 402);
            }
            $order = PublicOrder::find($request->order_id);
            $message = null;
            if ($order->status == "pending") {
                $order->driver_id = $request->user()->id;
                $order->status = "in_the_way_to_store";
                $order->save();
                $driver = User::findOrFail($request->user('api')->id);
                $driver->orders_balance -= 1;
                $driver->save();
                if ($driver->orders_balance == 0) {
                    $message = [
                        'msg' => "Your achieve last order balance contact management ",
                        'title' => "Your achieve last order balance contact management ",
                        'type' => '4station',
                        'country_id' => @$driver->country_id
                    ];
                    if ($driver->fcm_token) {
                        sendFCM($driver->fcm_token, $message);
                    }
                    $driver->notify(new SignupActivate($message));
                }

                $client = User::findOrFail($order->client_id);

                if ($client) {
                    $message = [
                        'msg' => "Accept & Delivery boy in the way to store",
                        'title' => "4station",
                        "type" => "public",
                        'public_order_id' => $order->id,
                        "status" => $order->status,
                        "country_id" => @$order->country_id
                    ];
                    if ($client->fcm_token) {
                        sendFCM($client->fcm_token, $message);
                    }
                    $client->notify(new SignupActivate($message));
                }


            } else {
                return response()->json([
                        'success' => false,
                        'message' => "Order picked from another driver ",]
                    , 422);
            }
            return response()->json([
                    'success' => true,
                    'message' => "order picked up successfully",

                ]
                , 200);

            DB::commit();
        } catch (\Exception $e) {
            return $e;
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
        return response()->json([
            'success' => true,
            'message' => "order picked up successfully"
        ], 200);

    }

    public function cancelOrder(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'order_id' => 'required|integer',
            'cancel_reasons_id' => 'required',

        ]);

        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }

        DB::beginTransaction();
        try {
            $cancelReason = RejectReasons::findOrFail($request->cancel_reasons_id);
            $publicOrder = PublicOrder::where('client_id', $request->user()->id)->findOrFail($request->order_id);
            if ($publicOrder->status == 'in_the_way_to_store' || $publicOrder->status == 'pending') {
                $publicOrder->status = 'cancelled';
                $publicOrder->cancel_reason = $request->cancel_reason;
                $publicOrder->cancel_reasons_id = $request->cancel_reasons_id;
                $publicOrder->save();

                if ($publicOrder->driver_id) {
                    $driver = User::find($publicOrder->driver_id);
                    if ($driver) {
                        $message = [
                            'msg' => "Client cancel order #" . $publicOrder->invoice_number . ' ' . $cancelReason->reason_en,
                            'title' => '4station',

                            "type" => "public",
                            'public_order_id' => $publicOrder->id,
                            "status" => $publicOrder->status,
                            "country_id" => @$publicOrder->country_id
                        ];
                        if ($driver->fcm_token) {
                            sendFCM($driver->fcm_token, $message);
                        }
                        $driver->notify(new SignupActivate($message));
                    }
                }
                $user = User::find($publicOrder->user_id);
                if ($user) {
                    $message = [
                        'msg' => "Client cancel order #" . $publicOrder->invoice_number . " " . $cancelReason->reason_en,
                        'title' => "4station",
                        "type" => "public",
                        'public_order_id' => $publicOrder->id,
                        "status" => $publicOrder->status,
                        "country_id" => @$publicOrder->country_id
                    ];
                    if ($user->fcm_token) {
                        sendFCM($user->fcm_token, $message);
                    }
                    $user->notify(new SignupActivate($message));
                }

            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'You can\'t cancel your order in the way '
                ], 422);
            }
            DB::commit();

            return response()->json([
                    'success' => true,
                    'message' => "Order cancelled successfully",

                ]
                , 200);
        } catch (\Exception $e) {


            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong',
                'errors' => $e->getMessage()
            ], 422);
        }


    }


    public function sendInvoiceValue(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'order_id' => 'required|integer',
            'invoice_value' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }

        DB::beginTransaction();
        try {
            $publicOrder = PublicOrder::where('driver_id', $request->user()->id)->findOrFail($request->order_id);
            if ($publicOrder) {
                $publicOrder->purchase_invoice_value = $request->invoice_value;
                $publicOrder->total = ((float)$request->invoice_value + (float)$publicOrder->tax + (float)$publicOrder->delivery_cost);
                $publicOrder->save();
                $client = User::findOrFail($publicOrder->client_id);
                $message = [
                    'msg' => "Invoice amount entered for order #" . $publicOrder->invoice_number,
                    'title' => "4station",
                    'public_order_id' => $publicOrder->id,
                    'type' => 'public',
                    "status" => $publicOrder->status,
                    "country_id" => @$publicOrder->country_id
                ];
                if ($client->fcm_token) {
                    sendFCM($client->fcm_token, $message);
                }
                $client->notify(new SignupActivate($message));

            }


            DB::commit();

            return response()->json([
                    'success' => true,
                    'message' => "Client invoice amount send  successfully",

                ]
                , 200);
        } catch (\Exception $e) {


            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
        return response()->json([
            'success' => false,
            'message' => 'Something going wrong'
        ], 402);

    }


    public function changeToInTheWayOrder(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'order_id' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }

        DB::beginTransaction();
        try {
            $publicOrder = PublicOrder::where('driver_id', $request->user()->id)->findOrFail($request->order_id);
            if ($publicOrder) {
                $publicOrder->status = 'in_the_way_to_client';
                $publicOrder->save();
                $client = User::findOrFail($publicOrder->client_id);
                if ($client) {
                    $message = [
                        'msg' => "Your order #" . $publicOrder->invoice_number . " in to way to you ",
                        'title' => "4station",
                        "type" => "public",
                        'public_order_id' => $publicOrder->id,
                        "status" => $publicOrder->status,
                        "country_id" => @$publicOrder->country_id
                    ];
                    if ($client->fcm_token) {
                        sendFCM($client->fcm_token, $message);
                    }
                    $client->notify(new SignupActivate($message));
                }
            }
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => "Client order changed to in the way successfully",
            ], 200);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
        return response()->json([
            'success' => false,
            'message' => 'Something going wrong'
        ], 402);

    }


    public function deliveredOrder(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'order_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }

        DB::beginTransaction();
        try {
            $publicOrder = PublicOrder::where('driver_id', $request->user()->id)->findOrFail((double)$request->order_id);
            if ($publicOrder) {
                $publicOrder->status = 'delivered';
//                $publicOrder->driver_revenue+= ((float)$publicOrder->purchase_invoice_value);
                $publicOrder->save();
//                $appComitions=AppSetting::find(1)->pluck('app_commission_public_delivery')[0];
                $driver = User::findOrFail($request->user()->id);
                if ($publicOrder->payment_type == 'on_delivery') {
                    $driver->public_wallet -= ($publicOrder->app_revenue);
                } else {
                    $driver->public_wallet += ($publicOrder->purchase_invoice_value + $publicOrder->driver_revenue);
                }
                $driver->save();
                $client = User::findOrFail($publicOrder->client_id);
                $message = [
                    'msg' => "The driver confirmed his delivery of the order #" . $publicOrder->invoice_number,
                    'title' => "4station",
                    'public_order_id' => $publicOrder->id,
                    'type' => 'public',
                    "status" => $publicOrder->status,
                    "country_id" => @$publicOrder->country_id
                ];
                if ($client->fcm_token) {
                    sendFCM($client->fcm_token, $message);
                }
                $client->notify(new SignupActivate($message));

            }

            DB::commit();

            return response()->json([
                    'success' => true,
                    'message' => "Client order has been delivered  successfully",

                ]
                , 200);
        } catch (\Exception $e) {

            return $e->getMessage();
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
        return response()->json([
            'success' => true,
            'message' => "Client order has been delivered  successfully",
        ], 402);

    }

    public function clientConfirmDeliverd(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'order_id' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }

        DB::beginTransaction();
        try {
            $publicOrder = PublicOrder::where('client_id', $request->user()->id)->findOrFail($request->order_id);
            if ($publicOrder) {
                $publicOrder->client_deliverd = 1;
                $publicOrder->save();
                $driver = User::findOrFail($publicOrder->driver_id);
                $message = [
                    'msg' => "The customer confirmed receipt of the order #" . $publicOrder->invoice_number,
                    'title' => "4station",
                    'public_order_id' => $publicOrder->id,
                    'type' => 'public',
                    "country_id" => @$publicOrder->country_id
                ];
                if ($driver->fcm_token) {
                    sendFCM($driver->fcm_token, $message);
                }
                $driver->notify(new SignupActivate($message));


            }

            DB::commit();

            return response()->json([
                    'success' => true,
                    'message' => "Client order has been delivered  successfully",

                ]
                , 200);
        } catch (\Exception $e) {

            return $e->getMessage();
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
        return response()->json([
            'success' => true,
            'message' => "Client order has been delivered  successfully",
        ], 402);

    }

    public function cancelReasons()
    {
        $cancelOrderReasonsList = RejectReasons::where('is_active', true)->get();
        return response()->json(compact('cancelOrderReasonsList'));
    }

    public function clientConfirmPayment(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'order_id' => 'required',
            'payment_type' => 'required'
        ]);
        if ($request->payment_type == "online") {
            $validator = \Validator::make($request->all(), [
                'transaction_id' => 'required',
            ]);
        }
        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "you must send  order id with requested",
                    'errors' => $validator->messages()]
                , 422);
        }
        DB::beginTransaction();
        try {
            $publicOrder = PublicOrder::where('client_id', $request->user()->id)->findOrFail($request->order_id);
            if ($publicOrder) {
                $publicOrder->client_paid_invoice = 1;
                $publicOrder->payment_type = $request->payment_type;
                $publicOrder->transaction_id = @$request->transaction_id;
//                $publicOrder->status = "invoice_paid";
                $publicOrder->save();
                $driver = User::findOrFail($publicOrder->driver_id);

                if ($request->payment_type == "online") {
                    $message = [
                        'msg' => "Client make online payment for order #" . $publicOrder->invoice_number,
                        'title' => "4station",
                        'public_order_id' => $publicOrder->id,
                        'type' => 'public',
                        "status" => $publicOrder->status,
                        "payment_type" => $publicOrder->payment_type,
                        'country_id' => $publicOrder->status
                    ];
                    if ($driver->fcm_token) {
                        sendFCM($driver->fcm_token, $message);
                    }
                    $driver->notify(new SignupActivate($message));
                } else if ($request->payment_type == "on_delivery") {
                    $message = [
                        'msg' => "Client will pay on delivery#" . $publicOrder->invoice_number,
                        'title' => "4station",
                        'public_order_id' => $publicOrder->id,
                        'type' => 'public',
                        "status" => $publicOrder->status,
                        "payment_type" => $publicOrder->payment_type,
                        'country_id' => $publicOrder->status
                    ];
                    if ($driver->fcm_token) {
                        sendFCM($driver->fcm_token, $message);
                    }
                    $driver->notify(new SignupActivate($message));
                }
            }

            DB::commit();

            return response()->json([
                    'success' => true,
                    'message' => "Client send payment way successfully",

                ]
                , 200);
        } catch (\Exception $e) {

            return $e->getMessage();
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }


    }

    public function driverCancelOrder(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'order_id' => 'required|integer',
//            'cancel_reason' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "you must send order id with request",
                    'errors' => $validator->messages()]
                , 422);
        }

        DB::beginTransaction();
        try {

            $publicOrder = PublicOrder::where('driver_id', $request->user('api')->id)->findOrFail($request->order_id);
            if (!$publicOrder->client_paid_invoice && $publicOrder->status != 'in_the_way_to_client' && $publicOrder->status != 'delivered') {
                $publicOrder->status = 'cancelled';
//                $publicOrder->status = @$request->cancel_reasons_id;
                $publicOrder->save();


                $client = User::findOrFail($publicOrder->client_id);
                $message = [
                    'msg' => "Delivery driver cancel order  #" . $publicOrder->invoice_number,
                    'title' => "4station",
                    'public_order_id' => $publicOrder->id,
                    'type' => 'public',
                    'status' => $publicOrder->status,
                    'country_id' => $publicOrder->country_id
                ];
                if ($client->fcm_token) {
                    sendFCM($client->fcm_token, $message);
                }
                $client->notify(new SignupActivate($message));
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'You can\'t cancel  this order #' . $publicOrder->invoice_number
                ], 422);
            }

            DB::commit();
            return response()->json([
                'success' => true,
                'message' => 'Order cancelled successfully '
            ], 200);

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }


    }

    public function sendNotificationChatIsBegin(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'user_id' => 'required|integer',
            'order_id' => 'required|integer',
            'invoice_no' => 'required',
            'type' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "you must send order id and receiver user id  with request",
                    'errors' => $validator->messages()]
                , 422);
        }

        try {
            $user = User::findOrFail($request->user_id);
            if ($user) {

                if ($request->type == "4station") {
                    $message = [
                        'msg' => "You have new message in order #" . $request->invoice_no,
                        'title' => "4station",
                        "type" => "4station",
                        'order_id' => (double)$request->order_id,
                        "status" => "new_message",
                        "country_id" => @$user->country_id
                    ];
                } else {
                    $message = [
                        'msg' => "You have new message in order #" . $request->invoice_no,
                        'title' => "4station",
                        "type" => "public",
                        'public_order_id' => (int)$request->order_id,
                        "status" => "new_message",
                        "country_id" => @$user->country_id
                    ];
                }


                if ($user->fcm_token) {
                    sendFCM($user->fcm_token, $message);
                }
                $user->notify(new SignupActivate($message));
            }
        } catch (exception $e) {
//            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong',
                'error' => $e->getMessage()
            ], 422);

        }
    }
}
