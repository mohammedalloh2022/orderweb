<?php

namespace App\Http\Controllers;

use App\Models\CategoriesContent;
use App\Models\Country;
use App\Models\Item;
use App\Models\Order;
use App\Models\PublicOrder;
use App\Models\Transaction;
use App\Notifications\SignupActivate;
use App\User;
use Illuminate\Http\Request;

class PublicOrderController extends Controller
{
    public function index($countryId){
        $country=Country::findOrFail($countryId);
        if(!auth()->user()->can('Public stores orders '.$country->code)){
            abort(404);
        }
        return view('publicOrder.index',compact('countryId'));
    }

    public function driverWallet(Request $request,$driver){

        return view('publicOrder.drivers.payment',compact('driver'));
    }
    public function driverWalletTransaction(Request $request,$driver){
        $wallet_transactions=Transaction::where('driver_id',$driver)->paginate(10);
        return response()->json(compact('wallet_transactions'));
    }
    public function search(Request $request,$countryId)
    {
        $orders = new PublicOrder();
        $orders=$orders->where('country_id',$countryId)->with("client", "driver",'cancelReason');

        if($request->driver_id){
            $orders=$orders->where('driver_id',$request->driver_id);
        }
        if($request->client_id){
            $orders=$orders->where('user_id',$request->client_id);
        }

        if(request()->has('filter') && request('filter') ) {
            $orders = $orders->where('invoice_number',"like","%$request->filter%");

        }
        if(request()->has('sort')) {
            $sort = json_decode(request('sort'), true);
//            $categories = $categories->orderBy(($sort['fieldName'] ?? 'id'), $sort['order']);
        }
        $orders = $orders->orderBy('created_at','desc')->paginate(15);
        return response()->json(compact('orders'));
    }

    public function view( $order){
        $order=PublicOrder::with('client','driver','attachments')->findOrFail($order);
        return view('publicOrder.view',compact('order'));
    }



    public function changeStatus(Request $request)
    {
        $order = Order::findOrFail($request->id);
        if ($order) {
            $order->status = $request->status;
            $order->save();
            return response()->json([
                'status' => 'success',
                'msg' => ' changed successfully .'
            ], 200);
        }
        return response()->json([
            'status' => 'failed',
            'msg' => 'failed'
        ], 422);
    }
    public function storeWalletTransAction(Request $request,$driver)
    {



        $request->validate([
            'amount' => 'required',
//            'invoice_no' => 'required',

            'type' => 'required',

        ]);


        $user = User::findOrFail((int)$driver);
        if ($user) {
            $prev_wallet=$user->public_wallet?(float)$user->public_wallet:0;
            if($request->type=='deposit'){
                $user->public_wallet = ($prev_wallet+(float)$request->amount);
            }elseif($request->type=='withdraw'){
                $user->public_wallet = ($prev_wallet-(double)$request->amount);
            }

            $user->save();
            $transaction =new Transaction();
            $transaction->prev_wallet=$prev_wallet;
            $transaction->amount=$request->amount;
            $transaction->invoice_no=$request->invoice_no;
            $transaction->next_wallet=$user->public_wallet;
            $transaction->type=$request->type;
            $transaction->driver_id=$user->id;
            $transaction->save();

            if($user->public_wallet == 0.00 || $user->public_wallet == 0 ){
               $orders= PublicOrder::where('driver_id',$user->id)->get();
               if($orders){
                   foreach ($orders as $order) {

                       $order->accounted_with_driver=true;
                       $order->save();
                   }
               }
                $message = [
                    'msg' => "your previous public order wallet accounted ",
                    'title' => "4station",
                    'public_order_id' => 0,
                    'type' => 'wallet',
                    'country_id'=>@$order->country_id
                ];
                if ($user->fcm_token) {
                    sendFCM($user->fcm_token, $message);
                }

                $user->notify(new SignupActivate($message));
            }

            return response()->json([
                'status' => 'success',
                'msg' => ' Transaction stored   successfully .'
            ], 200);


        }
        return response()->json([
            'status' => 'failed',
            'msg' => 'failed'
        ], 422);
    }

    public function driverPublicOrdersList($countryId){
        $country=Country::findOrFail($countryId);
        if(!auth()->user()->can('Public drivers management '.$country->code)){
            abort(404);
        }
    return view('publicOrder.drivers.index',compact('countryId'));
    }
    public function driverOrdersList($driver){
        return view('publicOrder.drivers.ordersList',compact('driver'));
    }


    public function driverOrdersSearch($driver){
        $orders=PublicOrder::with('cancelReason')->where('driver_id',$driver)->paginate(10);
        return  response()->json(compact('orders'));
    }

    public function driverPublicOrdersSearch(Request $request,$countryId){
        $users=User::where('country_id',$countryId)->withCount('publicOrders')->withCount([
            'publicOrders AS public_order_sum_total' => function ($query) {
                $query->where('accounted_with_driver',0)->where('status','delivered')->select(\DB::raw("SUM(total) as sumtotal"));
            }
        ])->withCount([
            'publicOrders AS driver_revenue_sum' => function ($query) {
                $query->where('accounted_with_driver',0)->where('status','delivered')->select(\DB::raw("SUM(driver_revenue) as driver_revenue"));
            }
        ])->withCount([
            'publicOrders AS app_revenue_sum' => function ($query) {
                $query->where('accounted_with_driver',0)->where('status','delivered')->select(\DB::raw("SUM(app_revenue) as app_revenue"));
            }
        ])->where('role','delivery_driver');
        if(request()->has('filter') && request('filter') ) {
            $users = $users->where('name',"like","%$request->filter%");
        }
        $users=$users->paginate(10);
        return response()->json(compact('users'));
    }
}
