<?php

namespace App\Http\Controllers;

use App\Models\CategoriesContent;
use App\Models\Country;
use App\Models\Item;
use App\Models\Order;
use App\Models\PublicOrder;
use App\Models\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class AppRevenueController extends Controller
{
    public function index($countryId){
        $country=Country::findOrFail($countryId);
        if(!auth()->user()->can('App revenue '.$country->code)){
            abort(404);
        }
        return view('appRevenue.index',compact('countryId'));
    }


    public function search(Request $request,$countryId)
    {
        $orders = new Order();
        $orders=$orders->where('country_id',$countryId)->where('status','delivered');
        if(request()->has('filter') && request('filter') ) {
            $orders = $orders->where('invoice_number',"like","%$request->filter%");
//            $orders = $orders->where(function($q) use($filter){
//                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
//            });

        }
        $app_revenue=null;
        if( isset($request->report_period)) {

//            \Illuminate\Pagination\Paginator::currentPageResolver(function ()  {
//                return 1;
//            });

            if($request->report_period=='custom'){
                $dates=explode(',',$request->custom_period);
                $periodDate=['start_at'=>Carbon::parse($dates[0])->format('Y-m-d 00:00:00'),'end_at'=>Carbon::parse($dates[1])->format('Y-m-d 23:59:59')];
            }else{

            $periodDate=  filterPeriod($request->report_period);
            }
            $app_revenue=Order::where('country_id',$countryId)->where('status','delivered')->where('created_at','>=',$periodDate['start_at'])->where('created_at','<=',$periodDate['end_at'])->sum('app_revenue');
            $orders=$orders->where('created_at','>=',$periodDate['start_at'])->where('created_at','<=',$periodDate['end_at']) ;
            $orders = $orders->orderBy('created_at','desc')->paginate(20);
        }else{
            $app_revenue=Order::where('country_id',$countryId)->where('status','delivered')->sum('app_revenue');
            $orders = $orders->orderBy('created_at','desc')->paginate(20);
         }



        $app_revenue= number_format($app_revenue,2);

//        $orders = $orders->orderBy('created_at','desc')->paginate(20);
        return response()->json(compact('orders','app_revenue'));
    }






}
