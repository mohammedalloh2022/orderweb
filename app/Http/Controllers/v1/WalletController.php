<?php
namespace App\Http\Controllers\v1;
use App\Http\Controllers\Controller;
use App\Models\Wallet;
use App\User;
use Illuminate\Http\Request;

class WalletController extends Controller
{

    public function wallet(Request $request){
        $total_wallet=Wallet::where('driver_id',$request->user('api')->id)->where('accounted',0)->sum('total');

        $total_wallet_2=(double)(number_format((float)$total_wallet,2,'.',','));
        return response()->json([
            'success'=>true,
            'message'=>"Get wallet",
            'wallet'=> $total_wallet_2,
        ], 200);
    }
    public function walletDetails(Request $request){

//        $wallets=User::with('wallets')->findOrFail($request->user('api')->id);
        $total_wallet=Wallet::where('driver_id',$request->user('api')->id)->where('accounted',0)->sum('total');
        $wallets= Wallet::where('driver_id',$request->user('api')->id)->where('accounted',0)->get();
        return response()->json([
            'success'=>true,
            'message'=>"Get wallet",
            'wallets'=>$wallets,
            'total_wallet'=>(double)(number_format((float)$total_wallet,2,'.',',')),
        ], 200);
    }

}
