<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\AdvertiseSlider;
use App\Models\AppContent;
use App\Models\AppSetting;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Avatar;
use Storage;
use Carbon\Carbon;
use DB;
class AdController extends Controller
{
    public function index(Request $request){
      $user=$request->user('api');
      $ads=AdvertiseSlider::where('is_active',true)->get();
        return response()->json(compact('ads'));
    }


}
