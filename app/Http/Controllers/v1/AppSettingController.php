<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\AppContent;
use App\Models\AppSetting;
use App\Models\Country;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Avatar;
use Storage;
use Carbon\Carbon;
use DB;
class AppSettingController extends Controller
{
    public function index(Request $request){
        $settings=AppSetting::get();
        if (isset($settings)){
            $settings=$settings[0];
        }else{
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
        return response()->json(compact('settings'));
    }
    public function clientContent(){

        if(app()->getLocale()=='ar'){

        $privacy_title=AppContent::findOrFail(1)->pluck('client_privacy_title_ar')[0];
        $privacy_content=AppContent::findOrFail(1)->pluck('client_privacy_content_ar')[0];
        }elseif (app()->getLocale()=='en'){

        $privacy_title=AppContent::findOrFail(1)->pluck('client_privacy_title_en')[0];
        $privacy_content=AppContent::findOrFail(1)->pluck('client_privacy_content_en')[0];

        }

        return response()->json(compact('privacy_title','privacy_content'));
    }

    public function driverContent(){

        if(app()->getLocale()=='ar'){
            $privacy_title=AppContent::findOrFail(1)->pluck('driver_privacy_title_ar')[0];
            $privacy_content=AppContent::findOrFail(1)->pluck('driver_privacy_content_ar')[0];
        }elseif (app()->getLocale()=='en'){
            $privacy_title=AppContent::findOrFail(1)->pluck('driver_privacy_title_en')[0];
            $privacy_content=AppContent::findOrFail(1)->pluck('driver_privacy_content_en')[0];
        }
        return response()->json(compact('privacy_title','privacy_content'));
    }
    public function getCountriesList(){
    $countries=Country::get();
     return response()->json(compact('countries'));
    }

}
