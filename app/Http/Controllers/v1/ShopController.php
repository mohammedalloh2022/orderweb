<?php



namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\AppSetting;
use App\Models\CategoriesContent;
use App\Models\Item;
use App\Models\SubCategory;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function shopsCategories($shop){

        $subCategories=SubCategory::where('shop_id',$shop)->where('is_active',1)->whereHas('meals',function ($q){
            $q->where("is_active","==",1);
        })->get();
        return response()->json(compact('subCategories'));

    }
    public function shopsBranches($shop){
        $branches=CategoriesContent::where('parent_id',$shop)->where('is_active',1)->get();
        return response()->json(compact('branches'));
    }

    public function getShopDetails( Request $request,$shop){
        $shop=CategoriesContent::with(['activeOffers','subCategory'=>function($q){
            $q->where('is_active',1);
        }])->find($shop);
        $shop->offers=$shop->activeOffers;
        if($request->lat && $request->lng){
        $shop->branch_distance_km=$this->nearestBranch($request->lat,$request->lng,$shop->id);
        }

        unset($shop->activeOffers);
        $today=Carbon::now()->format('Y-m-d H:i:s');
        return response()->json(compact('shop','today'));
    }

    public function nearestBranch($latitude, $longitude, $shopId)
    {


        if ($latitude && $longitude) {
            $shops = DB::table("categories_contents");
            $shops = $shops->select("*", DB::raw(" 111.045 * acos(cos(radians(" . $latitude . "))
                                * cos(radians(lat)) * cos(radians(lng) - radians(" . $longitude . "))
                                + sin(radians(" . $latitude . ")) * sin(radians(lat))) AS distance"));
            $shops = $shops->where('is_active', 1)->where(function ($q) use ($shopId) {
                $q->where('parent_id', $shopId)->orWhere('id', $shopId);
            });
            $shops = $shops->orderBy('distance', 'asc')->first();
            if($shops){

            return $shops->distance;
            }else{
                return null;
            }
        } else {
            return null;
        }
    }
}
