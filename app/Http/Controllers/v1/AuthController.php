<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\Country;
use App\Notifications\SignupActivate;
use App\Notifications\SignupDrivers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;

use Carbon\Carbon;
use DB;
use Image;
use Validator;
use Avatar;
use File;
use function foo\func;
use Illuminate\Support\Facades\Input;

class AuthController extends Controller
{
    public function signup(Request $request)
    {

        $validator = \Validator::make($request->all(), [
            'name' => 'required|string',
            'email' => 'required|string|email',
//            'email' => 'required|string|email|unique:users',
//            'mobile' => 'required|unique:users',
            'mobile' => 'required',
            'password' => 'required|string|confirmed',
            'address' => 'required',
            'role' => 'required',
//            'country_id' => 'required',
//            'avatar' => 'mimes:jpeg,jpg,png,svg|max:10000'
        ]);

        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }



        \DB::beginTransaction();
        try {

            $register_numberr = 0;
            $reg_numberr = User::where('role',$request->role)->latest()->pluck('reg_no')->first();

            if ( $reg_numberr  ) {
                $register_numberr = (int)$reg_numberr + 1;
            } else if($reg_numberr==null && empty($reg_numberr)) {
                if ($request->role == 'client') {
                    $register_numberr = 10000000001;
                }
                if ($request->role == 'delivery_driver') {
                    $register_numberr = 20000000001;
                }
            }
            $prevouseUser = User::where(function ($q) use ($request) {
                $q->where('email', $request->email)->orWhere('mobile', $request->mobile);
            })->where("role", $request->role)->first();
            $prevouseAminUser=User::where('email',$request->email)->where(function ($q){
                $q->where("role",'shop')->orWhere("role",'admin')->orWhere("role",'branch');
            })->first();
            if ($prevouseUser ||$prevouseAminUser) {
                return response()->json([
                    'success' => false,
                    'message' => 'User already register in our system ',
                ], 422);
            }
//            $user DB::table('users')->update([
//                'name' => $request->name,
//                'email' => $request->email,
//                'mobile' => $request->mobile,
//                'address' => $request->address,
//                'password' => bcrypt($request->password),
//                'activation_token' => time(),
//                'role' => $request->role,
//                'reg_no' => $register_numberr,
//                'active' => $request->role == 'delivery_driver' ? 0 : 1,
//                'is_online' => 0,
//                'is_documented' => 0,
//            ]);
            $user = new User([
                'name' => $request->name,
                'email' => $request->email,
                'mobile' => $request->mobile,
                'address' => $request->address,
                'password' => bcrypt($request->password),
                'activation_token' => time(),
                'role' => $request->role,
                'reg_no' => $register_numberr,
                'active' => $request->role == 'delivery_driver' ? 0 : 1,
                'is_online' => 0,
                'is_documented' => 0,
                'country_id' => @$request->country_id,
            ]);
            $user->save();
            if ($request->role == 'delivery_driver') {
                $users = User::where('role', 'admin')->get();
                foreach ($users as $newuser)
                    if ($newuser) {
                        $message = [
                            'msg' => "New driver register",
                            'title' => "New driver register ",
                            'driver_id' => $user->id,
                            'country_id'=>$user->country_id

                        ];
                        if ($newuser->fcm_token) {
                            sendFCM($newuser->fcm_token, $message);
                        }
                        $newuser->notify(new SignupDrivers($message));
                    }

            }
//        $user->notify(new SignupActivate($user));


            if($request->avatar){
                    $img = Image::make($request->avatar);
                    $extension = explode('/', $img->mime)[1];
                if (in_array($extension, ['jpg','jpeg','gif','bmp','png'])) {
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id);
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath .'/'. $fileNameToStore);
                    $user->avatar = $fileNameToStore;
                    $user->save();
                }else{
                    return response()->json([
                        'success' => false,
                        'message' => 'Supported image types jpg,jpeg,gif,bmp &png',

                    ], 422);
                    $user->forceDelete();
                }
//                    $destinationPath = public_path('uploads/users/'.$user->id);
//                    $extension = strtolower($request->file('avatar')->getClientOriginalExtension());
//                    if (in_array($extension, ['jpg','jpeg','gif','bmp','png'])) {
//                        $fileName = uniqid() .rand(1000,9999). '.' . $extension;
//                        if($request->file('avatar')->move($destinationPath, $fileName)){
//                            $user->avatar = $fileName;
//                            $user->save();
//                        };
//                    }
                }else {
                $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
                Storage::put('users/' . $user->id . '/avatar.png', (string)$avatar);
             }

//            if ($request->hasFile('avatar')) {
//                $fileInfo = pathinfo($this->avatar);
//                $extension=  $fileInfo['extension'];
////                $extension = $request->file('avatar')->getClientOriginalExtension();
//                //uniqid()
//                $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
//                $request->file('avatar')->move(public_path('/'),$fileNameToStore);
////                Storage::putFileAs('users/' . $user->id, $request->file('avatar'), $fileNameToStore);
//                $user->avatar = $fileNameToStore;
//                $user->save();
//            } else {
//                $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
//                Storage::put('users/' . $user->id . '/avatar.png', (string)$avatar);
//            }

            \DB::commit();


        } catch (\Exception $e) {
//            return $e->getMessage();

            \DB::rollback();
            $user->forceDelete();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong',
                'exception' => $e->getMessage(),
            ], 422);
        }
//        if ($request->role == "delivery_driver") {
//        if ($request->role == 'client') {
            return AuthController::login(new Request(
                ['mobile' => $request->mobile,
                    'password' => $request->password,
                    'role' => $request->role,
                    'remember_me' => true,
                ]));
//        }
//        }
        return response()->json([
            'success' => true,
            'message' => 'Successfully created new deliver account!'
        ], 200);
    }

    public function signUpDeliveryAppStep2(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'vehicle_plate' => 'required|string',
//            'id_no' => 'required|integer',
            'vehicle_type' => 'required|string',
//            'insurance_license' => 'required|image|mimes:jpeg,png,jpg,svg|max:10000',
//            'vehicle_pic' => 'required|image|mimes:jpeg,png,jpg,svg|max:10000',
//            'drive_license' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:10000',
//            'vehicle_license' => 'required|image|mimes:jpeg,png,jpg,svg|max:10000',
//            'id_pic' => 'required|image|mimes:jpeg,png,jpg,gif|max:10000',
        ]);

        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }

        DB::beginTransaction();
        try {
            $user = $request->user('api');
            $user->vehicle_no = $request->vehicle_no;
            $user->id_no = $request->id_no;
            $user->vehicle_plate = $request->vehicle_plate;
            $user->vehicle_type = $request->vehicle_type;

                if($request->insurance_license){
                    $img = Image::make($request->insurance_license);
                    $extension = explode('/', $img->mime)[1];
                    if (in_array($extension, ['jpg','jpeg','gif','bmp','png'])) {
                        $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                        $destinationPath = public_path('uploads/users/' . $user->id);
                        File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                        $img->save($destinationPath .'/'. $fileNameToStore);
                        $user->insurance_license = $fileNameToStore;

                    }else{
                        return response()->json([
                            'success' => false,
                            'message' => 'Supported image types jpg,jpeg,gif,bmp &png',

                        ], 422);
//                        $user->forceDelete();
                    }
//                $destinationPath = public_path('uploads/users/'.$user->id);
//                $extension = strtolower($request->file('insurance_license')->getClientOriginalExtension());
//                if (in_array($extension, ['jpg','jpeg','gif','bmp','png'])) {
//                    $fileName = uniqid() .rand(1000,9999). '.' . $extension;
//                    if($request->file('insurance_license')->move($destinationPath, $fileName)){
//                        $user->insurance_license = $fileName;
//                    };
//                }

                // Get just ext
//                $extension = $request->file('insurance_license')->getClientOriginalExtension();
//                //uniqid()
//                $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
//                Storage::putFileAs('users/' . $user->id, $request->file('insurance_license'), $fileNameToStore);
//                $user->insurance_license = $fileNameToStore;
            }

            if($request->drive_license) {
                $img = Image::make($request->drive_license);
                $extension = explode('/', $img->mime)[1];
                if (in_array($extension, ['jpg', 'jpeg', 'gif', 'bmp', 'png'])) {
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id);
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . '/' . $fileNameToStore);
                    $user->drive_license = $fileNameToStore;
//                    $user->save();
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => 'Supported image types jpg,jpeg,gif,bmp &png',

                    ], 422);
//                    $user->forceDelete();
                }
            }
            if($request->vehicle_license) {
                $img = Image::make($request->vehicle_license);
                $extension = explode('/', $img->mime)[1];
                if (in_array($extension, ['jpg', 'jpeg', 'gif', 'bmp', 'png'])) {
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id);
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . '/' . $fileNameToStore);
                    $user->vehicle_license = $fileNameToStore;
//                    $user->save();
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => 'Supported image types jpg,jpeg,gif,bmp &png',

                    ], 422);
//                    $user->forceDelete();
                }
            }

            if($request->id_pic) {
                $img = Image::make($request->id_pic);
                $extension = explode('/', $img->mime)[1];
                if (in_array($extension, ['jpg', 'jpeg', 'gif', 'bmp', 'png'])) {
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id);
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . '/' . $fileNameToStore);
                    $user->id_pic = $fileNameToStore;
//                    $user->save();
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => 'Supported image types jpg,jpeg,gif,bmp &png',

                    ], 422);
//                    $user->forceDelete();
                }
            }
            if($request->vehicle_pic) {
                $img = Image::make($request->vehicle_pic);
                $extension = explode('/', $img->mime)[1];
                if (in_array($extension, ['jpg', 'jpeg', 'gif', 'bmp', 'png'])) {
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id);
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . '/' . $fileNameToStore);
                    $user->vehicle_pic = $fileNameToStore;
//                    $user->save();
                } else {
                    return response()->json([
                        'success' => false,
                        'message' => 'Supported image types jpg,jpeg,gif,bmp &png',

                    ], 422);
//                    $user->forceDelete();
                }
            }




            $user->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong',
                'error'=>$e->getMessage()
            ], 422);
        }
        return $user;


    }


    /**
     * Login user and create token
     *
     * @param  [string] email
     * @param  [string] password
     * @param  [boolean] remember_me
     * @return [string] access_token
     * @return [string] token_type
     * @return [string] expires_at
     */
    public function clientLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|max:255',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        try {
            $user = User::where('mobile', $request->mobile)->where('role','client')->where('active',1)->first();
            if(!$user){
                $user = User::where('mobile',$request->mobile)->where('role','client')->where("active",0)->first();
//                $usercheckMobile = User::where('mobile', $request->mobile)->first();
                if ($user) {
                    return response()->json([
                        "success" => false,
                        'message' => 'Your account not active contact with management '
                    ], 200);
                }else {
                    return response()->json([
                        "success" => false,
                        'message' => 'You must enter valid mobile and password'
                    ], 200);
                }
                return response()->json([
                    "success" => false,
                    'message' => 'Unauthorized to login '
                ], 200);
            }else{
                if (Hash::check($request->password, $user->password)) {
                    $token = $user->createToken('My App');
                    if ($request->remember_me)
                        $token->token->expires_at = Carbon::now()->addWeeks(20);
                    $token->token->save();
                    $response=  [
                        'success' => true,
                        'access_token' =>$token->accessToken,
                        'token_type' => 'Bearer',
                        "user" => $user,
                        'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString()
                    ];
                    return response($response, 200);
                } else {
                    $response = ["message" => "Password mismatch"];
                    return response($response, 422);
                }
            }

        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Unauthorized to login '
//                'message' => $e->getMessage()
            ], 422);
        }
    }

    public function driverLogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|max:255',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        try {
            $user = User::where('mobile', $request->mobile)->where('role','delivery_driver')->where('active',1)->first();
            if(!$user){
                $user = User::where('mobile',$request->mobile)->where('role','delivery_driver')->where("active",0)->first();
//                $usercheckMobile = User::where('mobile', $request->mobile)->first();
                if ($user) {
                    return response()->json([
                        "success" => false,
                        'message' => 'Your account not active contact with management '
                    ], 200);
                }else {
                    return response()->json([
                        "success" => false,
                        'message' => 'You must enter valid mobile and password'
                    ], 200);
                }
                return response()->json([
                    "success" => false,
                    'message' => 'Unauthorized to login '
                ], 200);
            }else{
                if (Hash::check($request->password, $user->password)) {
                    $token = $user->createToken('My App');
                    if ($request->remember_me)
                        $token->token->expires_at = Carbon::now()->addWeeks(20);
                    $token->token->save();
                    $response=  [
                        'success' => true,
                        'access_token' =>$token->accessToken,
                        'token_type' => 'Bearer',
                        "user" => $user,
                        'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString()
                    ];
                    return response($response, 200);
                } else {
                    $response = ["message" => "Password mismatch"];
                    return response($response, 422);
                }
            }

        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => 'Unauthorized to login '
//                'message' => $e->getMessage()
            ], 422);
        }

    }
    public function login (Request $request) {
        $validator = Validator::make($request->all(), [
            'mobile' => 'required|max:255',
            'role' => 'required',
            'password' => 'required|string|min:6',
        ]);
        if ($validator->fails())
        {
            return response(['errors'=>$validator->errors()->all()], 422);
        }
        $user = User::where('mobile', $request->mobile)->where('role',$request->role)->first();
        if ($user) {
            if (Hash::check($request->password, $user->password)) {
                $token = $user->createToken('My App');
                $response=  [
                    'success' => true,
                    'access_token' =>$token->accessToken,
                    'token_type' => 'Bearer',
                    "user" => $user,
                    'expires_at' => Carbon::parse($token->token->expires_at)->toDateTimeString()
                ];
                return response($response, 200);
            } else {
                $response = ["message" => "Password mismatch"];
                return response($response, 422);
            }
        } else {
            $response = ["message" =>'User does not exist'];
            return response($response, 422);
        }
    }

    /**
     * Logout user (Revoke the token)
     *
     * @return [string] message
     */
    public function logout(Request $request)
    {


        $user = User::findOrFail($request->user()->id);
        $user->is_online = 0;
        $user->save();
        $request->user()->token()->revoke();
        $request->user()->token()->delete();
        return response()->json([
            'message' => 'Successfully logged out'
        ]);
    }

    /**
     * Get the authenticated User
     *
     * @return [json] user object
     */
    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function signupActivate($token)
    {
        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            return response()->json([
                'message' => 'This activation token is invalid.'
            ], 404);
        }
        $user->active = true;
        $user->activation_token = '';
        $user->save();
        return $user;
    }

    public function updateProfile(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|unique:users,email,' . $request->user()->id,
            'mobile' => 'required|unique:users,mobile,' . $request->user()->id,
            'address' => '',
            'password' => 'required|string|confirmed',


        ]);

        DB::beginTransaction();
        try {
            $user = $request->user('api');
            if($request->avatar) {
                $img = Image::make($request->avatar);
                $extension = explode('/', $img->mime)[1];
                if (in_array($extension, ['jpg', 'jpeg', 'gif', 'bmp', 'png'])) {
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id);
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . '/' . $fileNameToStore);
                    $user->avatar = $fileNameToStore;
                    $user->save();
                }
            }

            $user->vehicle_no = $request->vehicle_no;
            $user->id_no = $request->id_no;
            $user->vehicle_plate = $request->vehicle_plate;
            $user->vehicle_type = $request->vehicle_type;
            $user->country_id = @$request->country_id;

            if($request->insurance_license) {
                $img = Image::make($request->insurance_license);
                $extension = explode('/', $img->mime)[1];
                if (in_array($extension, ['jpg', 'jpeg', 'gif', 'bmp', 'png'])) {
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id);
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . '/' . $fileNameToStore);
                    $user->insurance_license = $fileNameToStore;
                    $user->save();
                }
            }

            if($request->drive_license) {
                $img = Image::make($request->drive_license);
                $extension = explode('/', $img->mime)[1];
                if (in_array($extension, ['jpg', 'jpeg', 'gif', 'bmp', 'png'])) {
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id);
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . '/' . $fileNameToStore);
                    $user->drive_license = $fileNameToStore;
                    $user->save();
                }
            }
            if($request->vehicle_license) {
                $img = Image::make($request->vehicle_license);
                $extension = explode('/', $img->mime)[1];
                if (in_array($extension, ['jpg', 'jpeg', 'gif', 'bmp', 'png'])) {
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id);
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . '/' . $fileNameToStore);
                    $user->vehicle_license = $fileNameToStore;
                    $user->save();
                }
            }


            if($request->id_pic) {
                $img = Image::make($request->id_pic);
                $extension = explode('/', $img->mime)[1];
                if (in_array($extension, ['jpg', 'jpeg', 'gif', 'bmp', 'png'])) {
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id);
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . '/' . $fileNameToStore);
                    $user->id_pic = $fileNameToStore;
                    $user->save();
                }
            }

            if($request->vehicle_pic) {
                $img = Image::make($request->vehicle_pic);
                $extension = explode('/', $img->mime)[1];
                if (in_array($extension, ['jpg', 'jpeg', 'gif', 'bmp', 'png'])) {
                    $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                    $destinationPath = public_path('uploads/users/' . $user->id);
                    File::exists($destinationPath) or File::makeDirectory($destinationPath, 755, true);
                    $img->save($destinationPath . '/' . $fileNameToStore);
                    $user->vehicle_pic = $fileNameToStore;
                    $user->save();
                }
            }
            $user->email = $request->email;
            $user->mobile = $request->mobile;
            $user->address = $request->address;
            $user->password = bcrypt($request->password);
            $user->save();
            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong',
                'error'=>$e->getMessage()
            ], 422);
        }
        return $user;


    }

    public function changeLanguage(Request $request)
    {
        $request->validate([
            'lang' => 'required',
        ]);
        DB::beginTransaction();
        try {
            $user = $request->user('api');
            if ($user) {
                $user->lang = $request->lang;
                $user->save();

            }
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => "Default lang changed successfully"
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
    }

    public function forgetPasswordRequest(Request $request)
    {
        $request->validate([
            'mobile' => 'required',
        ]);
        DB::beginTransaction();
        $code = random_int(0001, 9999);
        try {
            $user = User::where("mobile", $request->mobile)->first();
            if ($user) {
                $user->reset_password_verify_code = $code;
                $user->save();

            }
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => "Generate reset code done successfully",
                "code" => $code
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
    }

    public function forgetPasswordVerify(Request $request)
    {
        $request->validate([
            'mobile' => 'required',
            'code' => 'required',
        ]);

        try {
            $user = User::where("mobile", $request->mobile)->where('reset_password_verify_code', $request->code)->first();
            if ($user) {
                if (isset($user) && $user->tokens) {

                    foreach ($user->tokens as $token) {
                        $token->revoke();
                    }
                }
                $tokenResult = $user->createToken('Personal Access Token')->accessToken;


                return response()->json([
                    'success' => true,
                    'message' => "Verify code done successfully",
                    'access_token' => $tokenResult

                ], 200);

            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Something going wrong'
                ], 422);
            }


        } catch (\Exception $e) {

            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
    }

    public function resetPassword(Request $request)
    {

        $request->validate([
            'password' => 'required|string|confirmed'
        ]);
        DB::beginTransaction();
        try {
            $user = $request->user();
            if ($user) {
                $user->password = bcrypt($request->password);
                $user->save();
                return response()->json([
                    'success' => true,
                    'message' => "Password changed successfully ",

                ], 200);

            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Something going wrong'
                ], 422);
            }
            DB::commit();

        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
    }

    public function updateDelivery(Request $request)
    {
        $request->validate([
            'lat' => 'required',
            'lng' => 'required',
            'delivery_address' => ''
        ]);
        DB::beginTransaction();
        try {
            $user = $request->user('api');
            $user->lat = $request->lat;
            $user->lng = $request->lng;
            $user->delivery_address = $request->delivery_address;
            $user->save();
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => "location updated successfully"
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }


    }

    public function isOnline(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'is_online' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "Something going wrong",
                    'errors' => $validator->messages()]
                , 422);
        }
        DB::beginTransaction();
        try {
            $user = $request->user();

            $user->is_online = (boolean)$request->is_online;
            $user->save();
            DB::commit();
            return response()->json([
                'success' => true,
                'message' => "Status updated successfully"
            ]);
        } catch (\Exception $e) {
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }


    }

    public function getUserNotifications(Request $request)
    {

        $before12Hours=Carbon::now()->subHours(8)->format('Y-m-d H:m:i');
//        $before12Hours=$now->subHours(12)->toDateTimeString()
        $notifications = $request->user('api')->notifications->where("created_at",">=",$before12Hours);
        return response()->json(compact('notifications'), 200);
    }

    public function sendNotifications(Request $request)
    {
        $newuser = User::findOrFail(50);
        $message = ['msg' => "test notification ",
            'title' => 'notification title'];
//        $newuser->notify(new SignupActivate($message));
        if ($newuser->notify(new SignupActivate($message))) {
            return response()->json([
                'success' => true,
                'message' => "Notification sent successfully"
            ]);
        }
    }

    public function storeUserToken(Request $request)
    {
        $request->validate([
            'token' => 'required',
        ]);
        $user = $request->user('api');
        $user->fcm_token = $request->token;
        $user->save();
        return response()->json([
            'success' => true,
            'message' => "token updated successfully"
        ]);

    }
    public function updateDriverLatAndLng(Request $request)
    {
        $request->validate([
            'driver_id' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);
        $user = User::find($request->driver_id);
        $user->lat = $request->lat;
        $user->lng = $request->lng;
        $user->save();
        return response()->json([
            'success' => true,
            'message' => "Driver location sent  successfully"
        ]);

    }
    public function sendDummyNofitication(Request $request)
    {
        $newuser = User::findOrFail($request->user_id);
        $message = ['msg' => $request->notification_msg,
            'title' => $request->notification_title,
            'order_id' => $request->order_id,
        ];
        sendFCM($newuser->fcm_token, $message);
        $newuser->notify(new SignupActivate($message));

        return response()->json([
            'success' => true,
            'message' => "notification sent successfully"
        ]);
    }
}
