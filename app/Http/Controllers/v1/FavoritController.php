<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\CategoriesContent;
use App\Models\Favorite;
use Illuminate\Http\Request;
use Avatar;
use Storage;
use Carbon\Carbon;
use DB;
class FavoritController extends Controller
{

    public function index(Request $request){
        $favorites=Favorite::with('content')->where('user_id',$request->user()->id)->paginate(10);
        return response()->json(compact('favorites'));
    }

    public function store(Request $request){
        $request->validate([
            'content_id' => 'required',
        ]);

        DB::beginTransaction();
        try{
            $prev=Favorite::where('content_id',$request->content_id)->where('user_id',$request->user()->id)->first();
            if($prev){
                $prev->delete();
            }
            if(!$prev){
                $fav=new Favorite();
                $fav->user_id=$request->user()->id;
                $fav->content_id=$request->content_id;
                $fav->content_type=CategoriesContent::class;
                $fav->save();
            }

            DB::commit();
        }catch (\Exception $e){
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }



        return response()->json([
            'success' => true,
            'message' => 'Successfully created add to favorite!'
        ], 200);
    }


}
