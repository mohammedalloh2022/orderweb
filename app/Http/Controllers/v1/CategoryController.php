<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\AppSetting;
use App\Models\CategoriesContent;
use App\Models\Category;
use Avatar;
use Illuminate\Http\Request;
use Storage;
use DB;

class CategoryController extends Controller
{

    public function index(Request $request)
    {
        // $user = $request->user('api');
        //->where('country_id',$user->country_id);
        //->where('country_id',$user->country_id);
        //where('country_id',$user->country_id)
        //->where('country_id',$user->country_id);
        //->where('country_id',$user->country_id)
        $user_lat=$request->lat;
        $user_lng=$request->lng;
        $shop_wallet_limit=(float)AppSetting::find(1)->pluck('shop_wallet_limit')[0];
        if($user_lat && $user_lng ){
            $shop_distance_limit_km=AppSetting::find(1)->pluck('shop_distance_limit_km')[0];
            $shops = DB::table("categories_contents");
            $shops = $shops->select("*", DB::raw("6371 * acos(cos(radians(" . $user_lat . "))
                                * cos(radians(lat)) * cos(radians(lng) - radians(" . $user_lng . "))
                                + sin(radians(" . $user_lat . ")) * sin(radians(lat))) AS distance"));


            $shops = $shops->having('distance', '<=',$shop_distance_limit_km)->where('parent_id',null)
                ->where('is_active',1);
            $mainShopsids=$shops->pluck('id')->toArray();
            $shopBranches = DB::table("categories_contents");
            $shopBranches = $shopBranches->select("*", DB::raw("6371 * acos(cos(radians(" . $user_lat . "))
                                * cos(radians(lat)) * cos(radians(lng) - radians(" . $user_lng . "))
                                + sin(radians(" . $user_lat . ")) * sin(radians(lat))) AS distance"));


            $shopBranches = $shopBranches->having('distance', '<=',$shop_distance_limit_km)
                ->where('parent_id','!=',"null")->where('is_active',1);
            $branchShopsIds=$shopBranches->pluck('parent_id')->toArray();

            $shopsIds=array_merge($branchShopsIds,$mainShopsids);
            $categories = Category::where('is_active',1)->where(function($q){
                $q->whereHas("shops.meals",function ($q){
                    $q->where('is_active',1);
                })->orWhereHas("shops.offers",function ($q){
                    $q->where('is_active',1)->where('status','active');
                });
            })->whereHas('shops',function ($qq) use($shop_wallet_limit,$shopsIds){
                $qq->where('wallet','>=',$shop_wallet_limit)->where('is_active',1)->whereIn('id',$shopsIds);
            })->get();
            return response()->json(compact('categories'));
        }


    }


    public function shops(Request $request, $category)
    {
        $user_lat=$request->lat;
        $user_lng=$request->lng;
        $user=$request->user('api');
        $shop_wallet_limit=AppSetting::find(1)->pluck('shop_wallet_limit')[0];
        if($user_lat && $user_lng ) {
            $shop_distance_limit_km = AppSetting::find(1)->pluck('shop_distance_limit_km')[0];
            $shops = DB::table("categories_contents");
            $shops = $shops->select("*", DB::raw("6371 * acos(cos(radians(" . $user_lat . "))
                                * cos(radians(lat)) * cos(radians(lng) - radians(" . $user_lng . "))
                                + sin(radians(" . $user_lat . ")) * sin(radians(lat))) AS distance"));


            $shops = $shops->having('distance', '<=',$shop_distance_limit_km)->where('parent_id',null)
                ->where('is_active',1);
            $mainShopsids=$shops->pluck('id')->toArray();
            $shopBranches = DB::table("categories_contents");
            $shopBranches = $shopBranches->select("*", DB::raw("6371 * acos(cos(radians(" . $user_lat . "))
                                * cos(radians(lat)) * cos(radians(lng) - radians(" . $user_lng . "))
                                + sin(radians(" . $user_lat . ")) * sin(radians(lat))) AS distance"));


            $shopBranches = $shopBranches->having('distance', '<=',$shop_distance_limit_km)
                ->where('parent_id','!=',"null")->where('is_active',1);

            $branchShopsIds=$shopBranches->pluck('parent_id')->toArray();
            $shopsIds=array_merge($branchShopsIds,$mainShopsids);
            $shops = CategoriesContent::where(function ($q) {
                    $q->whereHas('meals', function ($qq) {
                        $qq->where('is_active', 1);
                    })->orWhereHas('offers', function ($qq) {
                        $qq->where('is_active',1)->where('status','active');
                    });
                })->whereIn('id',$shopsIds)

                ->where('wallet','>',$shop_wallet_limit)
                ->where('is_active',1)
                ->where('content_id', $category)
                ->where('parent_id',null)
                ->paginate(10);
            return response()->json(compact('shops'));

        }


    }


}
