<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\CategoriesContent;
use App\Models\Contact;
use App\Notifications\NewMessage;
use App\Notifications\SignupActivate;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Avatar;
use Storage;
use Carbon\Carbon;
use DB;
class ContactController extends Controller
{
    public function store(Request $request){


        $validator = \Validator::make($request->all(), [
            'email' => 'required|email',
            'name' => 'required',
            'message' => 'required',
            'subject' => 'required',
            'mobile' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                    'success'=>false,
                    'message'=>"Something going wrong",
                    'errors'=> $validator->messages()]
                , 422);
        }

        DB::beginTransaction();
        try{
            $contact=new Contact();
            $contact->email=$request->email;
            $contact->name=$request->name;
            $contact->message=$request->message;
            $contact->subject=$request->subject;
            $contact->mobile=$request->mobile;
            $contact->user_id=$request->user()->id;
            $contact->save();

            $admins=User::where('role','admin')->get();
            foreach ($admins as $admin){
                $message = [
                    'msg' => "new contact message",
                    'title' => "new contact message",
                    'type' => 'public',
                ];
                $admin->notify(new NewMessage($message));
            }
            DB::commit();
        }catch (\Exception $e){
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
        return response()->json([
            'success' => true,
            'message' => 'Successfully send your review !'
        ], 200);
    }
}
