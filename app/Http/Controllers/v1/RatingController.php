<?php

namespace App\Http\Controllers\v1;

use App\Http\Controllers\Controller;
use App\Models\CategoriesContent;
use App\Models\Order;
use App\Models\Rating;
use App\User;
use Illuminate\Http\Request;
use Avatar;
use Storage;
use Carbon\Carbon;
use DB;
class RatingController extends Controller
{
    public function index(Request $request, $category){
        $ratings= Rating::whereHasMorph('content',CategoriesContent::class)->where('content_id',$category)->with('user')->paginate(10);
//        $ratings=CategoriesContent::with('ratings')->findOrFail($category);
        return response()->json(compact('ratings'));
    }

    public function driversRatings(Request $request){
       $ratings= Rating::whereHasMorph('content',User::class)->where('content_id',$request->user('api')->id)->with('user')->paginate(10);
//        $ratings=CategoriesContent::with('ratings')->findOrFail($category);
        return response()->json(compact('ratings'));
    }

    public function store(Request $request){
        $request->validate([
            'content_id' => 'required',
            'rate' => 'required',
            'review' => 'required',
        ]);

        DB::beginTransaction();
        try{
            $previousOrder=Order::where('shop_id',$request->content_id)->where('user_id',$request->user()->id)->where('status','delivered')->first();

            if(isset($previousOrder) ){
                $fav=new Rating();
                $fav->user_id=$request->user()->id;
                $fav->content_id=$request->content_id;
                $fav->content_type=CategoriesContent::class;
                $fav->rate=$request->rate;
                $fav->review=$request->review;
                $fav->save();
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'You must have at least 1 delivered order from this shop to rate it'
                ], 422);
            }

            DB::commit();

        }catch (\Exception $e){
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
        return response()->json([
            'success' => true,
            'message' => 'Successfully send your review !'
        ], 200);
    }

    public function storeRateDriver(Request $request){
        $request->validate([
            'content_id' => 'required',
            'rate' => 'required',
            'review' => 'required',
        ]);

        DB::beginTransaction();
        try{
            $previousOrder=Order::where('driver_id',$request->content_id)->where('user_id',$request->user()->id)->where('status','delivered')->first();

            if(isset($previousOrder) ){
                $fav=new Rating();
                $fav->user_id=$request->user()->id;
                $fav->content_id=$request->content_id;
                $fav->content_type=User::class;
                $fav->rate=$request->rate;
                $fav->review=$request->review;
                $fav->save();
            }else{
                return response()->json([
                    'success' => false,
                    'message' => 'You must have at least 1  order delivered via this delivery man'
                ], 422);
            }

            DB::commit();

        }catch (\Exception $e){
            DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);
        }
        return response()->json([
            'success' => true,
            'message' => 'Successfully send your review !'
        ], 200);
    }



}
