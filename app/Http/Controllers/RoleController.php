<?php

namespace App\Http\Controllers;

use App\Http\Controllers\v1\AuthController;
use App\Models\CategoriesContent;
use App\User;
use Illuminate\Http\Request;
use Image;
use Storage;
use File;

use DB;
use Validator;
use Avatar;
use Illuminate\Validation\Rule;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
class RoleController extends Controller
{
    public function roles(){
        $roles=Role::where('guard_name','web')->paginate(10);
        return view('roles.index',compact('roles'));
    }

    public function rolesList(){
        $roles=Role::all();
        return response()->json(compact('roles'));
    }
    public function form($role=null){

        return view('roles.form',compact('role'));
    }
    public function getWebPermissions(){
        $permissions= Permission::get();
        return response()->json($permissions,200);
    }
    public function getWebRoles(){
        $roles= Role::where('guard_name','web')->orderBy('created_at','desc')->paginate(10);
        return response()->json($roles);
    }

    public function getWebRolelist(){
        $roles= Role::where('guard_name','web')->orderBy('created_at','desc')->get();
        return response()->json($roles);
    }

    public function getAllRolesPermissions($roleId){
        $role=Role::with("permissions")->findOrFail($roleId);
        return response()->json($role);
    }
    public function storeRole(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|unique:roles,name',
            'permissions' => 'required',
        ]);
        $role = Role::create(['name' => $request->get('name'),'guard_name'=>'web']);
        if(isset($request->permissions)){


//            foreach($request->permissions as $p){
//                $role->givePermissionTo($p['name']);
//            }
            $role->syncPermissions($request->permissions);
        }
        $role->save();
//        $role->save();
        return response()->json([
            'status' => 'success',
            'msg' => 'Submitted successfully .'
        ], 200);
    }

    public function updateRole(Request $request,$roleId)
    {
        //            foreach($request->permissions as $p){
//                $role->givePermissionTo($p['name']);
//            }
        $this->validate($request, [


        ]);
        $role = Role::findOrFail($roleId);
        $role->name=$request->name;
        if(isset($request->permissions)){

            $role->syncPermissions($request->permissions);
        }
        $role->save();

        return response()->json([
            'status' => 'success',
            'msg' => 'Submitted successfully .'
        ], 200);
    }
}
