<?php

namespace App\Http\Controllers;


use App\Models\CategoriesContent;
use App\Models\Category;
use App\Models\City;
use App\Models\Country;
use App\Models\State;
use App\Models\StatePrice;
use http\Url;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\Storage;
use File;

class CityController extends Controller
{

    public function index($country)
    {
        $country = Country::find($country);
        $country_id=null;
        $country_name=null;
        if ($country) {
            $country_id=$country->id;
            $country_name = $country->name;
        }

        return view('countries.city', compact('country_name','country_id'));
    }


    public function list($country)
    {

        $cities = City::city($country)->get();
        return response()->json(compact('cities'));
    }

    public function all()
    {

        $states = City::active()->get();

        return response()->json(compact('states'));
    }
    public function allPrices()
    {
        $statesPrices =StatePrice::get()->pluck('from_state_id')->toArray();
        $statesPrices2 =StatePrice::get()->pluck('to_state_id')->toArray();
        $mergedPrices=array_merge($statesPrices,$statesPrices2);
        $stateHasPriceId= array_unique($mergedPrices);

        $states = State::query()->active()->whereIn('id',$stateHasPriceId)->get();

        return response()->json(compact('states'));
    }

    public function search(Request $request,$country)
    {
        $cities = new City();
        $cities = $cities->where('country_id',$country);
        if (request()->has('filter') && request('filter')) {
            $filter = request('filter');
            $states = $cities->where(function ($q) use ($filter) {
                $q->where('name', 'LIKE', "%$filter%");
            });
        }

        $cities = $cities->orderBy('created_at', 'desc')->paginate(15);

        return response()->json(compact('cities'));
    }

//categories
    public function store(Request $request)
    {
        $request->validate([
            'name_ar' => 'required',
            'name_en' => 'required',
            'country_id' => 'required',
//            'lat' => 'required',
//            'lng' => 'required',
        ]);

        try {
            \DB::beginTransaction();
            $state = new City();
            $state->name_en = $request->name_en;
            $state->name_ar= $request->name_ar;
            $state->country_id = $request->country_id;
            $state->lat = $request->lat;
            $state->lng = $request->lng;
            $state->save();
            \DB::commit();


        } catch (\Exception $e) {
//            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }
        $message = 'تمت العملية بنجاح';
        return response()->json(compact('message'), 200);
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name_ar' => 'required',
            'name_en' => 'required',
//            'country_id' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);


        try {
            \DB::beginTransaction();
            $state = City::find($id);
            $state->name_en = $request->name_en;
            $state->name_ar= $request->name_ar;
//            $state->country_id = $request->country_id;
            $state->lat = $request->lat;
            $state->lng = $request->lng;
            $state->save();
            \DB::commit();


        } catch (\Exception $e) {
//            return $e->getMessage();
            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong'
            ], 422);

        }
        $message = 'تمت العملية بنجاح';
        return response()->json(compact('message'), 200);
    }

    public function destroy(State $state)
    {
        $state->delete();
        $message = 'تمت العملية بنجاح';
        return response()->json(compact('message'));
        return response()->json(['message' => 'Item deleted successfully'], 200);

    }

    public function changeStatus(Request $request)
    {
        $request->validate([
            'id' => 'required',
            'status' => 'required',
        ]);
        $state = City::findOrFail($request->id);
        $state->is_active = $request->status;
        $state->save();
        return response()->json([
            'status' => 'success',
            'msg' => 'تم تغيير الحالة بنجاح'
        ], 200);
    }

}
