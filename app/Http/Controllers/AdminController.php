<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;
use DB;

use Validator;
class AdminController extends Controller
{
    public function showAdminProfile(){

        return view('profile.profile');
    }

    public function updateAdminProfile(Request $request){
        $validator = \Validator::make($request->all(), [
            'name' => 'required|string',
            'mobile' => 'required',
            'email' => Rule::unique('users')->ignore(Auth::id(), 'id'),
//            'role' => 'required',
            'avatar'=>'mimes:jpeg,jpg,png,svg|max:10000'
        ]);


        if ($validator->fails()) {
            return redirect()->back()->withInput($request->all())->withErrors($validator->messages());

        }



        \DB::beginTransaction();
        try {
            $admin =Auth::user();
            $admin->name=$request->name;
            $admin->mobile=$request->mobile;
            if($request->email !=auth()->user()->email){
                $preEmail=User::where('email',$request->email)->first();
                if(!$preEmail){

                     $admin->email=$request->email;
                }else{
                    return redirect()->back()->withInput($request->all())->with(['error'=>"email must be unique "]);

                }
            }
            if ($request->hasFile('avatar')) {
                // Get just ext
                $extension = $request->file('avatar')->getClientOriginalExtension();
                //uniqid()
                $fileNameToStore = rand(1, 99999) . '_' . time() . '.' . $extension;
                Storage::putFileAs('users/'.$admin->id, $request->file('avatar'), $fileNameToStore);
                $admin->avatar = $fileNameToStore;

            }
            $admin->save();

        } catch (\Exception $e) {
            \DB::rollback();
            return $e;
//            return response()->json([
//                'success' => false,
//                'message' => 'Something going wrong'
//            ], 422);
        }

        return redirect()->back()->withInput($request->all())->with(['message'=>"User info updated successfully"]);
    }


    public function showChangePassword(){

        return view('profile.password');
    }

    public function updatePassword(Request $request)
    {
        $request->validate([
            'old_password' => 'required',
            'new_password' => 'required|string|confirmed|min:6|different:old_password'
        ]);

        if (Hash::check($request->old_password, \auth('web')->user()->password) == false) {
            return redirect()->back()->with(['message'=>"Unauthorized"]);
            return response(['message' => 'Unauthorized'], 401);
        }

        $user = auth('web')->user();
        $user->password = Hash::make($request->new_password);
        $user->save();

        return redirect()->back()->with([
            'success'=>true,
            'message'=>"Your password has been updated successfully."]);
//        return response([
//            'message' => 'Your password has been updated successfully.'
//        ]);
    }
}
