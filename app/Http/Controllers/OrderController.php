<?php

namespace App\Http\Controllers;

use App\Models\AppSetting;
use App\Models\CategoriesContent;
use App\Models\Country;
use App\Models\Order;
use App\Notifications\SignupActivate;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Kreait\Firebase\Factory;
//use Kreait\Firebase\ServiceAccount;
class OrderController extends Controller
{
    public function index($countryId)
    {
        $country=Country::findOrFail($countryId);
        if(!auth()->user()->can('Order list '.$country->code)){
            abort(404);
        }
        return view('orders.index',compact('countryId'));
    }

    public function firebase()
    {
        $jsonFile=file_get_contents(public_path('axiomatic-spark-303003-firebase-adminsdk-105ch-b65cb4b0b0.json'));
        $credintial=json_decode($jsonFile,true);
//        $factory = (new Factory)->withServiceAccount($credintial);
//        $factory = (new Factory())
//            ->withDatabaseUri('https://axiomatic-spark-303003-default-rtdb.firebaseio.com/');
//        $serviceAccount1 = new ServiceAccount()::fromJson($jsonFile);
//        $serviceAccount=ServiceAccount::fromJson($jsonFile);
        $firebase = (new Factory)
            ->withServiceAccount($credintial)
            ->withDatabaseUri('https://axiomatic-spark-303003-default-rtdb.firebaseio.com/')
            ->createDatabase();
        return $firebase->getReference('delivery_app_tracking')->getValue();
//
//        $newPost = $database
//            ->getReference('delivery_app_tracking');
////        echo '<pre>';
////        print_r($newPost->getvalue());
        return $newPost->getvalue();
    }
    public function pendingOrderView($countryId){
        $country=Country::findOrFail($countryId);
        if(!auth()->user()->can('Pending order list '.$country->code)){
            abort(404);
        }
        return view('orders.pending',compact('countryId'));
    }
    public function search(Request $request,$countryId)
    {
        $orders = new Order();
        $orders = $orders->where('country_id',$countryId)->with('cancelReason');
        if(!$request->driver_id && !$request->client_id){

            $orders=$orders->where('status','!=','pending');
        }

        $orders = $orders->where(function($q){
            $q->where('type','normal')->orWhere('type','offer');
        })->with("client", "deliveryUser", "shop.branchParent");

        if ($request->driver_id) {
            $orders = $orders->where('driver_id', $request->driver_id);
        }
        if ($request->client_id) {
            $orders = $orders->where('user_id', $request->client_id);
        }
        if(request()->has('store') && $request->store ) {
            $storee = request('store');
            $orders = $orders->where('shop_id',$storee);
        }
        if( isset($request->report_period)) {

//            \Illuminate\Pagination\Paginator::currentPageResolver(function ()  {
//                return 1;
//            });

            if($request->report_period=='custom'){
                $dates=explode(',',$request->custom_period);
                $periodDate=['start_at'=>Carbon::parse($dates[0])->format('Y-m-d 00:00:00'),'end_at'=>Carbon::parse($dates[1])->format('Y-m-d 23:59:59')];
            }else{

                $periodDate=  filterPeriod($request->report_period);
            }
            $orders=$orders->where('created_at','>=',$periodDate['start_at'])->where('created_at','<=',$periodDate['end_at']) ;

        }
        if(request()->has('filter') && request('filter') ) {
            $filter = request('filter');
            $orders=$orders->where('invoice_number','like',"%$filter");
//            $shops = $shops->where(function($q) use($filter){
//                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
//            });

        }
//        if (request()->has('sort')) {
//            $sort = json_decode(request('sort'), true);
////            $categories = $categories->orderBy(($sort['fieldName'] ?? 'id'), $sort['order']);
//        }
        $orders = $orders->orderBy('created_at', 'desc')->paginate(15);
        return response()->json(compact('orders'));
    }

    public function pendingOrderSearch(Request $request,$countryId)
    {
        $orders = new Order();

        $orders=$orders->with('shop.branchParent')->where('status','pending')->where('country_id',$countryId);
        $orders = $orders->where(function($q){
            $q->where('type','normal')->orWhere('type','offer');
        })->with("client", "deliveryUser", "shop");

        if ($request->driver_id) {
            $orders = $orders->where('driver_id', $request->driver_id);
        }
        if ($request->client_id) {
            $orders = $orders->where('user_id', $request->client_id);
        }
        if(request()->has('filter') && request('filter') ) {
            $filter = request('filter');
            $orders=$orders->where('invoice_number','like',"%$filter");
//            $shops = $shops->where(function($q) use($filter){
//                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
//            });

        }
//        if(request()->has('filter') && request('filter') ) {
//            $filter = request('filter');
//            $shops = $shops->where(function($q) use($filter){
//                $q->where('name_ar', 'LIKE', "%$filter%")->orWhere('name_en', 'LIKE', "%$filter%");
//            });
//
//        }
        if (request()->has('sort')) {
            $sort = json_decode(request('sort'), true);
//            $categories = $categories->orderBy(($sort['fieldName'] ?? 'id'), $sort['order']);
        }
        $orders = $orders->orderBy('created_at', 'desc')->paginate(15);
        return response()->json(compact('orders'));
    }


    public function view($order)
    {

        $notifications=auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\SignupActivate');
        foreach ($notifications as $notification){
            if($notification->data['order_id']==$order){

                $notification->markAsRead();
            }

        }

        $order = Order::with('orderItems.item', 'orderItems.extraItems.item', 'shop.branchParent', 'client', 'driver')->findOrFail($order);
        return view('orders.view', compact('order'));
    }
    public function storeView($order)
    {

        $notifications=auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\SignupActivate');
        foreach ($notifications as $notification){
            if($notification->data['order_id']==$order){

                $notification->markAsRead();
            }

        }

        $order = Order::with('orderItems.item', 'orderItems.extraItems.item', 'shop.branchParent', 'client', 'driver')->findOrFail($order);
        return view('store.orderView', compact('order'));
    }

    public function changeStatus(Request $request)
    {
        $order = Order::with('shop')->findOrFail($request->id);
                            $shop = CategoriesContent::find($order->shop_id);

     
        if ($order) {

            $newuser = User::findOrFail($order->user_id);
            if ($request->status == 'cancelled') {
                $shop=CategoriesContent::findOrFail($order->shop_id);
                $order->status = $request->status;
                $order->cancel_reason_manual = $request->reason;
                $order->save();
                $message = [
                    'msg' => "Admin cancel order #".$order->invoice_number."'$request->reason' ",
                    'title' => "Admin cancel order#".$order->invoice_number."'$request->reason'",
                    'order_id' => $order->id,
                    'type' => '4station',
                    'status'=>$order->status,
                    'type_of_receive'=>$order->type_of_receive,
                    'country_id'=>@$order->country_id
                ];
                if ($newuser->fcm_token) {
                    sendFCM($newuser->fcm_token, $message);
                }
                $newuser->notify(new SignupActivate($message));
            }

            if ($order->type_of_receive == 'restaurant') {

                if ($request->status == 'preparing') {
                    $order->status = $request->status;
                    $order->save();
                    $message = [
                        'msg' => "Your order #" . $order->invoice_number . " is under processing ",
                        'title' => "Your order #" . $order->invoice_number . "is under processing",
                        'order_id' => $order->id,
                        'type' => '4station',
                        'status'=>$order->status,
                        'type_of_receive'=>$order->type_of_receive,
                        'country_id'=>@$order->country_id
                    ];
                    if ($newuser->fcm_token) {
                        sendFCM($newuser->fcm_token, $message);
                    }
                    $newuser->notify(new SignupActivate($message));
                }

                if ($request->status == 'ready') {
                    $order->status = $request->status;
                    $order->save();
                    $message = [
                        'msg' => "Your order #" . $order->invoice_number . " is ready ",
                        'title' => "Your order #" . $order->invoice_number . "is ready",
                        'order_id' => $order->id,
                        'type' => '4station',
                        'status'=>$order->status,
                        'type_of_receive'=>$order->type_of_receive,
                        'country_id'=>@$order->country_id
                    ];
                    if ($newuser->fcm_token) {
                        sendFCM($newuser->fcm_token, $message);
                    }
                    $newuser->notify(new SignupActivate($message));
                }

                if ($request->status == 'delivered') {
                    $order->status = $request->status;
                    $order->save();
                    // shop revenue

                    $ordershop=CategoriesContent::with('branchParent')->findOrFail($order->shop_id);
                    if($ordershop->branchParent){
                        $appComitions=$ordershop->branchParent->app_commission;
                    }else{
                        $appComitions=$ordershop->app_commission;
                    }

                    $order->app_shop_commission=((float)$order->sub_total_1*(float)$appComitions/100);
                    $order->app_revenue=((float)$order->sub_total_1*(float)$appComitions/100)+(float)$order->tax;
                    $order->app_commission=$order->app_revenue;
                    if($order->discount){
                        $order->app_revenue-=(float)$order->discount;
                    }
                    $order->shop_revenue=((float)$order->sub_total_1 -((float)$order->sub_total_1*(float)$appComitions/100));
                    $order->save();
                    $shop=CategoriesContent::find($order->shop_id);

                    if ($order->payment_type == 'credit_card' || $order->payment_type == 'paypal' ) {
                        $shop->wallet += (float)$order->shop_revenue;
                        $order->order_store_wallet = number_format((double)$order->shop_revenue,'2','.',',');

                    }else{
                        $shop->wallet -= (float)($order->app_revenue);
                        $order->order_store_wallet = number_format(-(double)$order->app_revenue,'2','.',',');

                    }
                    $shop->save();
                    $order->save();
                    if($shop->parent_id){
                        $parentShop=CategoriesContent::find((int)$shop->parent_id);
                        if ($order->payment_type == 'payment_type' || $order->payment_type == 'paypal' ) {
                            $parentShop->wallet += (float)$order->shop_revenue;

                        }else{
                            $parentShop->wallet -= (float)($order->app_revenue);
                        }

                        $parentShop->save();
                    }
                    $message = [
                        'msg' => "Your order #" . $order->invoice_number . " delivered ",
                        'title' => "Your order #" . $order->invoice_number . " delivered",
                        'order_id' => $order->id,
                        'type' => '4station',
                        'status'=>$order->status,
                        'type_of_receive'=>$order->type_of_receive,
                        'country_id'=>@$order->country_id
                    ];
                    if ($newuser->fcm_token) {
                        sendFCM($newuser->fcm_token, $message);
                    }
                    $newuser->notify(new SignupActivate($message));
                }
            }
            if ($order->type_of_receive == 'home') {

                if ($request->status == 'preparing') {
                    $order->status = $request->status;
                    $order->save();
                    $message = [
                        'msg' => "Your order #" . $order->invoice_number . " is under processing ",
                        'title' => "Your order #" . $order->invoice_number . "is under processing",
                        'order_id' => $order->id,
                        'type' => '4station',
                        'status' => $order->status,
                        'type_of_receive' => $order->type_of_receive,
                        'country_id'=>@$order->country_id
                    ];
                    if ($newuser->fcm_token) {
                        sendFCM($newuser->fcm_token, $message);
                    }
                    $newuser->notify(new SignupActivate($message));
                }

                if ($request->status == 'ready') {
                    $shop=CategoriesContent::find($order->shop_id);

                    $order->status = $request->status;
                    $order->save();
                    $message = [
                        'msg' => "Your order #" . $order->invoice_number . " is ready to delivered",
                        'title' => "Your order #" . $order->invoice_number . "is ready to delivered",
                        'order_id' => $order->id,
                        'type' => '4station',
                        'status' => $order->status,
                        'type_of_receive' => $order->type_of_receive,
                        'country_id'=>@$order->country_id
                    ];
                    if ($newuser->fcm_token) {
                        sendFCM($newuser->fcm_token, $message);
                    }
                    $newuser->notify(new SignupActivate($message));
                    try{



                        $this->sendOrderTodriverDelivery($shop,$order);

                    }catch (\Exception $e){
                        return response()->json([
                            'status' => 'failed',
                            'msg' => 'Failed to send order to drivers',
                            'failed_message' => $e->getMessage(),
//
                        ], 422);
                    }
                }
            }

            return response()->json([
                'status' => 'success',
                'msg' => ' changed successfully .'
            ], 200);
        }
        return response()->json([
            'status' => 'failed',
            'msg' => 'failed'
        ], 422);
    }
    public function resendNotificationToDrivers(Request $request){
        $request->validate([
            'order_id' => 'required',

        ]);

        $order=Order::findOrFail($request->order_id);
        $shop= CategoriesContent::findOrFail($order->shop_id);
        try{
             $this->sendOrderTodriverDelivery($shop,$order);
        }catch (\Exception $e){
            return response()->json([
                'status' => 'failed',
                'msg' => 'failed to send order to drivers',
                'failed_msg' => $e->getMessage(),

            ], 422);
        }

    }
    protected function sendOrderTodriverDelivery($shop,$order){

        $nearestdriverArray=$this->nearsetDriversLocation($shop);
        $wallet_limit=(float)AppSetting::where('country_id',$shop->country_id)->first()->pluck('driver_wallet_limit')[0];

//        $wallet_limit = AppSetting::findOrFail(1)->pluck('driver_wallet_limit')[0];
        $drivers= User::where('is_documented', 1)->where('active', 1)->where('is_online', 1)->where('role', 'delivery_driver')->whereDoesntHave('driverOrder', function ($q) {
            $q->where('status', "on_the_way");
        })->whereDoesntHave('driverPublicOrder', function ($q) {
            $q->where('status', "in_the_way_to_store")->orWhere('status', 'in_the_way_to_client');
        })->where('wallet', '>=', $wallet_limit)->where("orders_balance",">",0)->whereIn('id',$nearestdriverArray)->get();

        if(!empty($drivers)){
            foreach ($drivers as $driver){
                dump($driver->id);

                try{


                    $message = [
                        'msg' => "new order #" . $order->invoice_number,
                        'title' => "new order #" . $order->invoice_number,
                        'order_id' => $order->id,
                        'order_no' => $order->invoice_number,
                        'type' => '4station',
                        "destination_address" => $order->destination_address,
                        'pickup_address_ar' => $order->shop->address_ar,
                        'pickup_address_en' => $order->shop->address_en,
                        'created_at' => strtotime($order->created_at),
                        'status' => $order->status,
                        'type_of_receive' => $order->type_of_receive,
                        'country_id'=>@$order->country_id
                    ];
                    if ($driver->fcm_token) {
                        sendFCM($driver->fcm_token, $message);
                    }
                    $driver->notify(new SignupActivate($message));
                }catch (\Exception $e){
                    \Log::info( $e->getMessage());


                }

            }
        }
    }

    protected function nearsetDriversLocation($shop)
    {

        // $jsonFile=file_get_contents(public_path('axiomatic-spark-303003-firebase-adminsdk-105ch-b65cb4b0b0.json'));
        // dd($jsonFile);
        // $credintial=json_decode($jsonFile,true);
        // $firebase = (new Factory)
        //     ->withServiceAccount($credintial)
        //     ->withDatabaseUri('https://axiomatic-spark-303003-default-rtdb.firebaseio.com/')
        //     ->createDatabase();
            
              $firebase = (new Factory)->withServiceAccount(config('firebase.firebase_file_path'))
            ->withDatabaseUri(config('firebase.database_url')) ->createDatabase();
        $data= $firebase->getReference('delivery_app_tracking')->orderByChild('country_id')->equalTo((int)$shop->country_id)->getValue();

//        $jsonFile=file_get_contents(public_path('axiomatic-spark-303003-firebase-adminsdk-105ch-b65cb4b0b0.json'));
//        $credintial=json_decode($jsonFile,true);
//        $factory = (new Factory)->withServiceAccount($jsonFile);
//        $database = $factory->createDatabase();
//        $data = $database->getReference('delivery_app_tracking')->orderByChild('country_id')->equalTo((int)$shop->country_id)->getValue();
        $onlineDrivers = collect($data)->where('status', '=', 'online');
        $driver_distance_limit_km = (float)AppSetting::where('country_id', $shop->country_id)->get()->pluck('driver_distance_limit_km')->first();
        $subset = $onlineDrivers->filter(function ($item) use ($shop, $driver_distance_limit_km) {
            $distanceBetween = distanceBetweenTwoPoints($shop->lat, $shop->lng, $item['lat'], $item['lng'], 'K');
            return $distanceBetween <= $driver_distance_limit_km;
        });
        $nearestDriver = $subset->all();
        $nearestDriver2 = collect($nearestDriver)->pluck('driver_id');
        return $nearestDriver2;
    }
    public function expertExcel(Request  $request){
        $orders=Order::get();
        $orders=$orders->where('status','!=','pending')->where('country_id',$request->country_id);
        if ($request->driver_id) {
            $orders = $orders->where('driver_id', $request->driver_id);
        }
        if ($request->client_id) {
            $orders = $orders->where('user_id', $request->client_id);
        }
        if(request()->has('store_id') && $request->store_id ) {
            $storee = request('store_id');
            $orders = $orders->where('shop_id',$storee);
        }
        if( isset($request->report_period)) {


            if($request->report_period=='custom'){
                $dates=explode(',',$request->custom_period);
                $periodDate=['start_at'=>Carbon::parse($dates[0])->format('Y-m-d 00:00:00'),'end_at'=>Carbon::parse($dates[1])->format('Y-m-d 23:59:59')];
            }else{

                $periodDate=  filterPeriod($request->report_period);
            }
            $orders=$orders->where('created_at','>=',$periodDate['start_at'])->where('created_at','<=',$periodDate['end_at']) ;

        }
        return  response(view('excel.orders',compact('orders')));


    }
}
