<?php

namespace App\Http\Controllers;

use App\Models\AppSetting;
use App\Models\CategoriesContent;
use App\Models\Country;
use App\Models\Coupon;
use App\Models\Item;
use App\Models\Order;
use App\Models\Rating;
use App\Notifications\SignupActivate;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;


use Illuminate\Validation\Rule;
use Validator;

use DB;
class DeliveryDriverController extends Controller
{
    public function index($countryId){
        $country=Country::findOrFail($countryId);
        if(!auth()->user()->can('Drivers management '.$country->code)){
            abort(404);
        }
        return view('users.drivers.delivery',compact('countryId'));
    }

    public function documentedRequest($countryId){
        $country=Country::findOrFail($countryId);
        if(!auth()->user()->can('New drivers requests '.$country->code)){
            abort(404);
        }
        $notifications=auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\SignupDrivers');
        foreach ($notifications as $notification){
            $notification->markAsRead();
        }

        return view('users.drivers.documentRequests',compact('countryId'));
    }

    public function ratings($driver){
        return view('users.drivers.ratings',compact('driver'));
    }

    public function ratingsList( $driver){
        $reviews = new Rating();
        $reviews=$reviews->where('content_id',$driver)->where('content_type',User::class)->with('user');
        $reviews = $reviews->with('order')->orderBy('created_at','desc')->paginate(15);
        return response()->json(compact('reviews'));
    }



    public function search(Request $request,$countryId){
        $users = new User();
        $users=$users->where('country_id',$countryId)->where('role','delivery_driver')->where('is_documented',1);
        if(request()->has('filter') && request('filter') ) {
            $filter = request('filter');
            $users = $users->where('name', 'LIKE', "%$filter%");
        }
        if(request()->has('name') && request('name') ) {
            $filter = request('name');
            $users = $users->where('name', 'LIKE', "%$filter%");
        }
        if(request()->has('mobile') && request('mobile') ) {
            $filter = request('mobile');
            $users = $users->where('mobile', 'LIKE',$filter);
        }
        if(request()->has('plat_no') && request('plat_no') ) {
            $filter = request('plat_no');
            $users = $users->where('vehicle_plate', 'LIKE', "%$filter%");
        }

        if(request()->has('wallet') && request('wallet') ) {
            $filter = request('wallet');
            if($filter=='exceed_station_wallet'){
                $driver_wallet_limit=AppSetting::where('country_id',$request->country_id)->pluck('driver_wallet_limit')[0];
                if($driver_wallet_limit>=0){

                    $users = $users->where('wallet','>=',$driver_wallet_limit);
                }else{

                    $users = $users->where('wallet','<=',$driver_wallet_limit);
                }
            }
        }

//        if(request()->has('sort')) {
//            $sort = json_decode(request('sort'), true);
//            $users = $users->orderBy(($sort['fieldName'] ?? 'id'), $sort['order']??'desc');
//        }

        if( isset($request->report_period)) {
            if($request->report_period=='custom'){
                $dates=explode(',',$request->custom_period);
                $periodDate=['start_at'=>Carbon::parse($dates[0])->format('Y-m-d 00:00:00'),'end_at'=>Carbon::parse($dates[1])->format('Y-m-d 23:59:59')];
            }else{

                $periodDate=  filterPeriod($request->report_period);
            }
            $users=$users->where('created_at','>=',$periodDate['start_at'])->where('created_at','<=',$periodDate['end_at']) ;

        }

        $users = $users->orderBy('created_at','desc')->paginate(13);
        return response()->json(compact('users'));
    }


    public function documentSearch($countryId){
        $users = new User();
        $users=$users->where('country_id',$countryId)->where('role','delivery_driver')->where('is_documented',0);
        if(request()->has('filter') && request('filter') ) {
            $filter = request('filter');
            $users = $users->where('name', 'LIKE', "%$filter%");
        }
        if(request()->has('sort')) {
            $sort = json_decode(request('sort'), true);
//            $users = $users->orderBy(($sort['fieldName'] ?? 'id'), $sort['name_en']);
        }
        $users = $users->orderBy('created_at','desc')->paginate(13);
        return response()->json(compact('users'));
    }

    public function changeStatus(Request $request)
    {
        $user = User::findOrFail((int)$request->id);
        if ($user) {
            if($request->status=='approve'){
                $user->is_documented = 1;
                $user->active = 1;
                if($user->save()){
                    $message = [
                        'msg' => "Wow! you account has been accepted",
                        'title' => "Administrative message",
                        'type' => 'driver_approved',
                        'country_id' => $user->country_id,
                    ];

                    if ($user->fcm_token) {
                        sendFCM($user->fcm_token, $message);
                    }
                    $user->notify(new SignupActivate($message));

                    $message = [
                        'msg' => "You must contact with management to get order balance ",
                        'title' => "Administrative message",
                        'type' => 'driver_approved',
                        'country_id' => $user->country_id,
                    ];

                    if ($user->fcm_token) {
                        sendFCM($user->fcm_token, $message);
                    }
                    $user->notify(new SignupActivate($message));

                }


            }
            if($request->status == 'reject'){
                $user->is_documented = 0;
                $user->active = 0;
                if($user->save()) {
                    $message = [
                        'msg' => "Your application to be delivery boy is rejected : " . $request->reason,
                        'title' => "4Station",
                        'type' => 'driver_approved',
                    ];

                    if ($user->fcm_token) {
                        sendFCM($user->fcm_token, $message);
                    }
                    sleep(5);
                    $user->delete();
                }
            }

            return response()->json([
                'status' => 'success',
                'msg' => ' changed successfully .'
            ], 200);

        }

        return response()->json([
            'status' => 'failed',
            'msg' => 'failed'
        ], 422);
    }

    public function updateBalance(Request  $request){
        $validator = \Validator::make($request->all(), [
            'order_balance' => 'required',
            'driver_id' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json([
                    'success' => false,
                    'message' => "some data necessary to send",
                    'errors' => $validator->messages()]
                , 422);
        }
        \DB::beginTransaction();
        try {
            $driver = User::findOrFail($request->driver_id);
            $driver->orders_balance=(int)$request->order_balance;
            $driver->save();

            if($driver->save()){
                $message = [
                    'msg' => "Your new order balance:".$driver->orders_balance,
                    'title' => "Administrative message",
                    'type' => 'driver_approved',
                    'country_id' => $driver->country_id,
                ];

                if ($driver->fcm_token) {
                    sendFCM($driver->fcm_token, $message);
                }
                $driver->notify(new SignupActivate($message));
            }

        } catch (\Exception $e) {

            \DB::rollback();
            return response()->json([
                'success' => false,
                'message' => 'Something going wrong',
                "error"=>$e->getMessage()
            ], 422);
        }

    }
    public function exportExcel(Request  $request){
        $users = new User();
        $users=$users->where('country_id',$request->country_id)->where('role','delivery_driver')->where('is_documented',1);
        if(request()->has('filter') && request('filter') ) {
            $filter = request('filter');
            $users = $users->where('name', 'LIKE', "%$filter%");
        }
        if(request()->has('name') && request('name') ) {
            $filter = request('name');
            $users = $users->where('name', 'LIKE', "%$filter%");
        }
        if(request()->has('mobile') && request('mobile') ) {
            $filter = request('mobile');
            $users = $users->where('mobile', 'LIKE',$filter);
        }
        if(request()->has('plat_no') && request('plat_no') ) {
            $filter = request('plat_no');
            $users = $users->where('vehicle_plate', 'LIKE', "%$filter%");
        }

        if(request()->has('wallet') && request('wallet') ) {
            $filter = request('wallet');
            if($filter=='exceed_station_wallet'){

                $driver_wallet_limit=AppSetting::where('country_id',$request->country_id)->pluck('driver_wallet_limit')[0];
                if($driver_wallet_limit>=0){

                    $users = $users->where('wallet','>=',$driver_wallet_limit);
                }else{

                    $users = $users->where('wallet','<=',$driver_wallet_limit);
                }
            }
        }

        if( isset($request->report_period)) {
            if($request->report_period=='custom'){
                $dates=explode(',',$request->custom_period);
                $periodDate=['start_at'=>Carbon::parse($dates[0])->format('Y-m-d 00:00:00'),'end_at'=>Carbon::parse($dates[1])->format('Y-m-d 23:59:59')];
            }else{

                $periodDate=  filterPeriod($request->report_period);
            }
            $users=$users->where('created_at','>=',$periodDate['start_at'])->where('created_at','<=',$periodDate['end_at']) ;

        }

        $drivers = $users->orderBy('created_at','desc')->get();

        return  response(view('excel.drivers',compact('drivers')));
    }

}
