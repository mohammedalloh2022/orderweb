<?php

namespace App\Http\Controllers;

use App\Models\DriverWalletTransaction;
use App\Models\PublicOrder;
use App\Models\Transaction;
use App\Models\Wallet;
use App\Notifications\SignupActivate;
use App\User;
use Illuminate\Http\Request;

class DriverWalletTransactionController extends Controller
{
    public function driverWallet(Request $request,User $driver){

        return view('users.drivers.wallet',compact('driver'));
    }

    public function driverWalletTransaction(Request $request,$driver){
        $wallet_transactions=DriverWalletTransaction::where('driver_id',$driver)->paginate(10);
        return response()->json(compact('wallet_transactions'));
    }
    public function storeWalletTransAction(Request $request,$driver)
    {



        $request->validate([
            'amount' => 'required',
//            'invoice_no' => 'required',

            'type' => 'required',

        ]);


        $user = User::findOrFail((int)$driver);
        if ($user) {
            $prev_wallet=$user->wallet?(float)$user->wallet:0;
//            if($prev_wallet>=0 && $request->type=='withdraw'){
//                $user->public_wallet = ($prev_wallet-(float)$request->amount);
//            }else{
//
//            }
//            $user->wallet = ($prev_wallet+(float)$request->amount);
            $wallet=new Wallet();
            if($request->type=='withdraw'){
                $user->wallet -= ((float)$request->amount);
                $wallet->out=(float)$request->amount;
                $wallet->total=-(float)$request->amount;
                $wallet->driver_id=$user->id;
                $wallet->save();
                $message = [
                    'msg' => "App manger withdraw from your wallet ",
                    'title' => "4station",
                    'public_order_id' => 0,
                    'type' => 'wallet',
                    'country_id'=>@$user->country_id
                ];
                if ($user->fcm_token) {
                    sendFCM($user->fcm_token, $message);
                }

                $user->notify(new SignupActivate($message));

            }else{
                $user->wallet += ((float)$request->amount);
                $wallet->in=(float)$request->amount;
                $wallet->total=(float)$request->amount;
                $wallet->driver_id=$user->id;
                $wallet->save();
                $message = [
                    'msg' => "App manger deposit to your wallet ",
                    'title' => "4station",
                    'public_order_id' => 0,
                    'type' => 'wallet',
                    'country_id' => @$user->country_id,
                ];
                if ($user->fcm_token) {
                    sendFCM($user->fcm_token, $message);
                }

                $user->notify(new SignupActivate($message));
            }

            if($user->wallet == 0.00 || $user->wallet == 0 ){
                $orders= Wallet::where('driver_id',$user->id)->get();
                if($orders){
                    foreach ($orders as $order) {
                        $order->accounted=true;
                        $order->save();
                    }
                }
                $message = [
                    'msg' => "your previous  orders accounted via app manger ",
                    'title' => "4station",
                    'public_order_id' => 0,
                    'type' => 'wallet',
                    'country_id' => @$user->country_id
                ];
                if ($user->fcm_token) {
                    sendFCM($user->fcm_token, $message);
                }

                $user->notify(new SignupActivate($message));
            }
            $user->save();
            $transaction =new DriverWalletTransaction();
            $transaction->prev_wallet=$prev_wallet;
            $transaction->amount=$request->amount;
            $transaction->invoice_no=$request->invoice_no;
            $transaction->next_wallet=$user->wallet;
            $transaction->type=$request->type;
            $transaction->driver_id=$user->id;
            $transaction->save();
            return response()->json([
                'status' => 'success',
                'msg' => ' Transaction stored   successfully .'
            ], 200);
        }
        return response()->json([
            'status' => 'failed',
            'msg' => 'failed'
        ], 422);
    }

}
