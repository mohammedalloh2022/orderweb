<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
class ActiveUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
//    public function handle($request, Closure $next)
//    {
//
//        if ($request->user()->status == User::STATUS_INACTIVE)
//            return response(["message" => trans("You must enter valid token")], 401);
//
//
//        return $next($request);
//    }

    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::guard($guard)->guest() || !$request->user('api')) {
            if ($request->ajax() || $request->wantsJson()) {
                return response('Unauthorized.', 401);
            }
        }

        return $next($request);
    }

}
