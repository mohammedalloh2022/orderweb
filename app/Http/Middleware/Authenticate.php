<?php

namespace App\Http\Middleware;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Exceptions\HttpResponseException;
use Symfony\Component\HttpFoundation\Response;
class Authenticate extends Middleware
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string|null
     */

    protected function unauthenticated($request, array $guards)
    {
        throw new AuthenticationException(
            'Unauthenticated.', $guards, $this->redirectTo($request,$guards)
        );
    }

    protected function redirectTo($request,$guards)
    {
        if($request->is('api/*') || $guards[0]=="api" || $request->expectsJson())
        {
            $array=['success'=>false,'message'=>'Unauthorized Request'];
            throw new HttpResponseException(new Response("Unauthorized Request",401));
        }
        if (! $request->expectsJson()) {
            return route('login');
        }
    }
}
