<?php

namespace App\Http\Middleware;

use Closure;

class Shop
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (auth()->user() && (auth()->user()->role != "shop" && auth()->user()->role != "branch")) {
            abort(403);
        }
        return $next($request);
    }
}
