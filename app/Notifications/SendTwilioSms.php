<?php

namespace App\Notifications;

use App\traits\Sms;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SendTwilioSms extends Notification
{
    use Queueable,Sms;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public $notification=[];
    public function __construct($data)
    {
        $this->notification=$data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['twilio'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function handle(){
        return $this->sendSMSMessage($this->notification['message'],$this->notification['mobile']);
    }
    public function toTwilio()
    {
//        dd($this->notification);
//        return $this->sendSMSMessage($this->notification['message'],$this->notification['mobile']);
//        return (new MailMessage)
//                    ->line('The introduction to the notification.')
//                    ->action('Notification Action', url('/'))
//                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
