<?php
/**
 * Created by PhpStorm.
 * User: WeSSaM
 * Date: 1/4/2021
 * Time: 6:40 م
 */

namespace App\Repositories;

use Twilio\Exceptions\TwilioException;
use Twilio\Rest\Client as TwilioClient;


class TwilloRepository
{
    protected $client;


    protected $twillioNo;

    /**
     * init twillo client class
     *
     * TwilloRepository constructor.
     * @throws \Twilio\Exceptions\ConfigurationException
     * @author WeSSaM
     */
    public function __construct()
    {
        $this->client = new TwilioClient(
            env('TWILLO_ACCOUNT_ID', 'AC09b22a166bf2c6f73686dd623b45487f'),
            env('TWILLO_API_KEY', '7080d6dfd7a3905382b366e7a8ca6572')
        );
    }

    /**
     * @param $recipients
     * @param $body
     * @throws TwilioException
     * @author WeSSaM
     */
    protected function create($recipients, $body)
    {
        $this->client->messages->create(
            '+' . $recipients,
            array(
                'from' => $this->twillioNo,
                'body' => $body
            )
        );
    }
}