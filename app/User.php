<?php

namespace App;

use App\Models\CategoriesContent;
use App\Models\City;
use App\Models\Country;
use App\Models\Favorite;
use App\Models\Order;
use App\Models\PublicOrder;
use App\Models\Rating;
use App\Models\Wallet;
use App\traits\Notifiable;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Storage;

//use Illuminate\Notifications\Notifiable;
class User extends Authenticatable
{

    use Notifiable, HasApiTokens, SoftDeletes, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name', 'email', 'password', 'avatar', 'mobile', 'address', 'role', 'lang',
        'activation_token', 'active', "lat", "lng", "delivery_address",
        "insurance_license",
        "drive_license",
        "vehicle_license",
        "id_pic",
        "vehicle_pic",
        "is_online",
        "reg_no",
        "fcm_token",
        "country_id",
        "city_id"

    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activation_token'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'created_at' => 'datetime:Y-m-d H:m:i'
    ];


    protected $appends = [
        'avatar_url',
        'Insurance_license_url',
        'drive_license_url',
        'vehicle_license_url',
        'id_pic_url',
        'vehicle_pic_url',
        "favorite_restaurants",
        "rate",
        "order_count",
        "city_name",
        "client_order_count",
        "public_wallet_cal"
    ];

    /**
     *
     * @return mixed
     * @author WeSSaM
     */
    public function getCityNameAttribute()
    {
        return $this->city->name;
    }

    public function getAvatarUrlAttribute()
    {
        if ($this->avatar)
            return Storage::url('users/' . $this->id . '/' . $this->avatar);
        else   return Storage::url('users/default.jpg');
    }

    public function getInsuranceLicenseUrlAttribute()
    {
        if ($this->insurance_license)
            return Storage::url('users/' . $this->id . '/' . $this->insurance_license);
        else return null;
    }

    public function getDriveLicenseUrlAttribute()
    {
        if ($this->drive_license)
            return Storage::url('users/' . $this->id . '/' . $this->drive_license);
        else return null;
    }

    public function getVehicleLicenseUrlAttribute()
    {
        if ($this->vehicle_license)
            return Storage::url('users/' . $this->id . '/' . $this->vehicle_license);
        else return null;
    }

    public function getIdPicUrlAttribute()
    {
        if ($this->id_pic)
            return Storage::url('users/' . $this->id . '/' . $this->id_pic);
        else return null;
    }

    public function getVehiclePicUrlAttribute()
    {
        if ($this->vehicle_pic)
            return Storage::url('users/' . $this->id . '/' . $this->vehicle_pic);
        else return null;
    }

    public function favorites()
    {
        return $this->hasMany(Favorite::class, 'user_id');
    }

    public function wallet()
    {
        return $this->hasMany(Wallet::class, 'driver_id')->where('accounted', 0)->sum('total');
    }

    public function scopeWallets()
    {
        return $this->hasMany(Wallet::class, 'driver_id');
    }

    public function ratings()
    {
        return $this->morphMany(Rating::class, 'content');
    }

    public function getRateAttribute()
    {
        if ($this->role == "delivery_driver") {
            $sum = $this->morphMany(Rating::class, 'content')->sum('rate');
            $count = $this->morphMany(Rating::class, 'content')->count();
            if ($count) {
                return $sum / $count;
            } else {
                return 0;
            }
        } else {
            return 0;
        }
    }

    public function getOrderCountAttribute()
    {

        return $this->hasMany(Order::class, 'driver_id')->count();


    }

    public function getClientOrderCountAttribute()
    {
        if ($this->role == "client") {
//            return $this->hasMany(Order::class,'user_id')->where('status','delivered')->count();
            return $this->hasMany(Order::class, 'user_id')->count();
        } else {
            return 0;
        }
    }


    public function driverOrder()
    {
        return $this->hasMany(Order::class, 'driver_id');
    }

    public function driverPublicOrder()
    {
        return $this->hasMany(PublicOrder::class, 'driver_id');
    }

    public function getFavoriteRestaurantsAttribute()
    {
        if ($this->role == "client") {
            return $this->hasMany(Favorite::class, 'user_id')->where('content_type', CategoriesContent::class)->pluck('content_id')->toArray();
        } else {
            return null;
        }

    }

    public function isAdmin()
    {
        return $this->role === 'admin';
    }

    public function isShop()
    {
        return $this->role === 'shop';
    }

    public function isBranch()
    {
        return $this->role === 'branch';
    }

    public function shop()
    {
        return $this->hasOne(CategoriesContent::class, 'user_id');
    }

    public function publicOrders()
    {

        return $this->hasMany(PublicOrder::class, 'driver_id');
//                ->where(function($query){$query->where('status','delivered')->orWhere('status','paid');
//            });

    }

    public function getPublicWalletCalAttribute()
    {
        return $this->public_wallet;
        return $this->hasMany(PublicOrder::class, 'driver_id')->where('status', 'delivered')->where('accounted_with_driver', 0)->sum('driver_revenue');

    }

    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }

    public function city()
    {
        return $this->belongsTo(City::class, 'city_id')->withDefault(["name" => ""]);
    }
}
