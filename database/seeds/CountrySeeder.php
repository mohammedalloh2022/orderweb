<?php

use Illuminate\Database\Seeder;

class CountrySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $countries=[
            [
            "code"=>"SA",
            "iso3"=>"SAU",
            "name_en"=>"Saudi Arabia",
            "name_ar"=>"السعودية",
            "phone_code"=>966,
            "phone_length"=>9,
            "currency_code"=>"SAR",
            "ISO-4217"=>"SAR"
            ],
            [
                "code"=>"EG",
                "iso3"=>"EGY",
                "name_en"=>"Egypt",
                "name_ar"=>"مصر",
                "phone_code"=>20,
                "phone_length"=>10,
                "currency_code"=>"EGP",
                "ISO-4217"=>"EGP"
            ],
            [
                "code"=>"TR",
                "iso3"=>"TUR",
                "name_en"=>"Turkey",
                "name_ar"=>"تركيا",
                "phone_code"=>90,
                "phone_length"=>10,
                'currency_code'=>'TRY',
                "ISO-4217"=>"TRY"
            ],
            [
                "code"=>"MA",
                "iso3"=>"MAR",
                "name_en"=>"Morocco",
                "name_ar"=>"المغرب",
                "phone_code"=>212,
                "phone_length"=>9,
                'currency_code'=>'MAD',
                "ISO-4217"=>"MAD"
            ]
        ];
        foreach ($countries as $country){
            $existCountry=\App\Models\Country::where('code',$country['code'])->first();
            if(!$existCountry){

            \App\Models\Country::create($country);
            }
        }


    }
}
