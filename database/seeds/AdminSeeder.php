<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\User::create([

            'name' => 'admin',
            'email' => 'admin@admin.com',
            'mobile' => '0597773989',
            "role"=>'admin',
            'password' => \Illuminate\Support\Facades\Hash::make(123456),
            'email_verified_at' => now(),
            'remember_token' => Str::random(10),


        ]);
    }
}
