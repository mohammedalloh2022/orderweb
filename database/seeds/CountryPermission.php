<?php

use Illuminate\Database\Seeder;

class CountryPermission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        \Illuminate\Support\Facades\Artisan::call('permission:cache-reset');
        $permissions=[
            ['name'=>'Category list','group'=>'Admin management'],
            ['name'=>'Cancel order reason list','group'=>'Admin management'],
            ['name'=>'Country Management','group'=>'Admin management'],
            ['name'=>'Roles management','group'=>'Admin management'],
            ['name'=>'Admin management','group'=>'Admin management'],
            ['name'=>'App setting','group'=>'Admin management'],
            ['name'=>'App privacy content','group'=>'Admin management'],
            ['name'=>'App Social media list','group'=>'Admin management'],
            ['name'=>'New drivers requests notifications','group'=>'Admin management'],
            ['name'=>'New contact messages notifications','group'=>'Admin management'],
            ['name'=>'New offers requests notifications','group'=>'Admin management'],
            ['name'=>'New orders notifications','group'=>'Admin management'],
        ];
        $countries = \App\Models\Country::get();
        foreach ($countries as $country) {
           $array1= ['name'=>'Super admin dashboard '.$country->code,'group'=>$country->name_en." permissions"];
           $array2= ['name'=>'Store list '.$country->code,'group'=>$country->name_en." permissions"];
           $array3= ['name'=>'Ads list '.$country->code,'group'=>$country->name_en." permissions"];
           $array4= ['name'=>'New offers list '.$country->code,'group'=>$country->name_en." permissions"];
           $array5= ['name'=>'Offers list '.$country->code,'group'=>$country->name_en." permissions"];
           $array6= ['name'=>'Order list '.$country->code,'group'=>$country->name_en." permissions"];
           $array7= ['name'=>'Pending order list '.$country->code,'group'=>$country->name_en." permissions"];
           $array8= ['name'=>'App revenue '.$country->code,'group'=>$country->name_en." permissions"];
           $array9= ['name'=>'App discount '.$country->code,'group'=>$country->name_en." permissions"];
           $array10= ['name'=>'Contact messages '.$country->code,'group'=>$country->name_en." permissions"];
           $array11= ['name'=>'Coupons management '.$country->code,'group'=>$country->name_en." permissions"];
           $array12= ['name'=>'Client management '.$country->code,'group'=>$country->name_en." permissions"];
           $array13= ['name'=>'App setting '.$country->code,'group'=>$country->name_en." permissions"];
           $array14= ['name'=>'Drivers management '.$country->code,'group'=>$country->name_en." permissions"];
           $array15= ['name'=>'New drivers requests '.$country->code,'group'=>$country->name_en." permissions"];
           $array16= ['name'=>'Send notification '.$country->code,'group'=>$country->name_en." permissions"];
           $array17= ['name'=>'Public stores orders '.$country->code,'group'=>$country->name_en." permissions"];
           $array18= ['name'=>'Public drivers management '.$country->code,'group'=>$country->name_en." permissions"];
           $array19= ['name'=>'New drivers requests notifications '.$country->code,'group'=>$country->name_en." permissions"];
           $array20= ['name'=>'New contact messages notifications '.$country->code,'group'=>$country->name_en." permissions"];
           $array21= ['name'=>'New offers requests notifications '.$country->code,'group'=>$country->name_en." permissions"];
           $array22= ['name'=>'New orders notifications '.$country->code,'group'=>$country->name_en." permissions"];
           array_push($permissions,$array1);
           array_push($permissions,$array2);
           array_push($permissions,$array3);
           array_push($permissions,$array4);
           array_push($permissions,$array5);
           array_push($permissions,$array6);
           array_push($permissions,$array7);
           array_push($permissions,$array8);
           array_push($permissions,$array9);
           array_push($permissions,$array10);
           array_push($permissions,$array11);
           array_push($permissions,$array12);
           array_push($permissions,$array13);
           array_push($permissions,$array14);
           array_push($permissions,$array15);
           array_push($permissions,$array16);
           array_push($permissions,$array17);
           array_push($permissions,$array18);
           array_push($permissions,$array19);
           array_push($permissions,$array20);
           array_push($permissions,$array21);
           array_push($permissions,$array22);
        }

        foreach ($permissions as $permission) {


                $p=\Spatie\Permission\Models\Permission::updateOrCreate(['name'=>$permission['name']],['guard_name'=>'web','name' => $permission['name'],'group'=>$permission['group']]);


        }
        $p=\Spatie\Permission\Models\Permission::all();
        $user =\App\User::where('email','abd@gmail.com')->first();
        $user->syncPermissions($p);
        $user->save();

        \Illuminate\Support\Facades\Artisan::call('permission:cache-reset');
    }
}
