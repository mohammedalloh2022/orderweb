<?php

use Illuminate\Database\Seeder;

class RoleSeederPermission extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=3;$i<=48;$i++){
        $permission=     \Spatie\Permission\Models\Permission::findById($i);
            $permission->assignRole([1]);
        }
    }
}
