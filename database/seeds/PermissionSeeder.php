<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [

            /** dashboard **/
            ['name'=>'Super admin dashboard','group'=>'Dashboard'],
//            ['name'=>'Show last client','group'=>'Dashboard'],
//            ['name'=>'Show last orders','group'=>'Dashboard'],
//            ['name'=>'Show drivers google map','group'=>'Dashboard'],
//            ['name'=>'Show statistic','group'=>'Dashboard'],

            /** Category **/
            ['name'=>'Category list','group'=>'Category'],
            ['name'=>'Category add','group'=>'Category'],
            ['name'=>'Category edit','group'=>'Category'],
            ['name'=>'Category active/deactivate','group'=>'Category'],
            /** Category **/
            ['name'=>'ads list','group'=>'Advertisements'],
            ['name'=>'ads add','group'=>'Advertisements'],
            ['name'=>'ads edit','group'=>'Advertisements'],
            ['name'=>'ads active/deactivate','group'=>'Advertisements'],

            ['name'=>'Cancel order reason list','group'=>'Cancel order reasons'],
            ['name'=>'Cancel order reason add','group'=>'Cancel order reasons'],
            ['name'=>'Cancel order reason edit','group'=>'Cancel order reasons'],
            ['name'=>'Cancel order reason active/deactivate','group'=>'Cancel order reasons'],
            /** Stores **/
            ['name'=>'Store list','group'=>'Stores'],
            ['name'=>'Store add','group'=>'Stores'],
            ['name'=>'Store edit','group'=>'Stores'],
//            ['name'=>'store view dashboard','group'=>'Stores'],
            ['name'=>'Store active/deactivate','group'=>'Stores'],

            /** new offer request  **/
            ['name'=>'New offers list','group'=>'New offers'],
            ['name'=>'New offers accept','group'=>'New offers'],
            ['name'=>'New offers reject','group'=>'New offers'],

            /**  Offers management  **/
            ['name'=>'Offers list','group'=>'Offers'],
            ['name'=>'Offers accept','group'=>'Offers'],
            ['name'=>'Offers reject','group'=>'Offers'],
            ['name'=>'Offers active/deactivate','group'=>'Offers'],

            /**  Orders management  **/
            ['name'=>'Order list','group'=>'Orders'],
            ['name'=>'Order change to ready','group'=>'Orders'],
            ['name'=>'Order change to delivered','group'=>'Orders'],
            ['name'=>'Order change to cancel','group'=>'Orders'],
            ['name'=>'Order resend to delivery','group'=>'Orders'],
            /**  Pending orders management  **/
            ['name'=>'New order list','group'=>'Pending orders'],
            ['name'=>'New order change to accept','group'=>'Pending orders'],
            ['name'=>'New order change to cancel','group'=>'Pending orders'],
            ['name'=>'New order resend to delivery drivers','group'=>'Pending orders'],

            /**  users management management  **/
            ['name'=>'Client list','group'=>'Users management'],
            ['name'=>'Drivers list','group'=>'Users management'],
            ['name'=>'New driver list','group'=>'Users management'],

            /**  Admin pages  management  **/
            ['name'=>'App setting','group'=>'Admin management'],
            ['name'=>'App privacy content','group'=>'mobile management'],
            ['name'=>'App Social media list','group'=>'mobile management'],
            ['name'=>'App revenue list','group'=>'Admin management'],
            ['name'=>'App discount list','group'=>'Admin management'],
            ['name'=>'Contacts messages list','group'=>'Admin management'],
            ['name'=>'Coupons list','group'=>'Admin management'],
            ['name'=>'Send notifications','group'=>'Admin management'],
            ['name'=>'Admin management','group'=>'Admin management'],
            ['name'=>'Roles management','group'=>'Admin management'],

            /**  public store  management  **/
            ['name'=>'Public order list ','group'=>'public store management'],
            ['name'=>'Public drivers wallet','group'=>'public store management'],



        ];
        foreach ($permissions as $permission) {
          $exist=  \Spatie\Permission\Models\Permission::where('name',$permission['name'])->first();
          if(!$exist){

            \Spatie\Permission\Models\Permission::create(['guard_name'=>'web','name' => $permission['name'],'group'=>$permission['group']]);
          }
        }
    }
}
