<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryPricesTable extends Migration
{
    public function up()
    {
        Schema::create('delivery_prices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('from')->nullable();
            $table->integer('to')->nullable();
            $table->double('price',10,2)->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('delivery_prices');
    }
}
