<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyOnOrderSuspended extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        Schema::table('orders', function (Blueprint $table) {
////            $table->dropColumn('status');
//        });
        Schema::table('orders', function (Blueprint $table) {
//            $table->enum('status',['pending','preparing','ready',"suspended",'on_the_way','delivered'])->default('pending');
            $table->boolean('sent_to_delivery_boys')->default(false);
            $table->boolean('wallet_calc_with_delivery')->default(false);
            $table->boolean('is_suspended')->default(false);
            $table->boolean('wallet_calc_with_store')->default(false);
            $table->boolean('wallet_calc_with_store_branch')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
