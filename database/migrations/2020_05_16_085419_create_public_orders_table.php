<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;


class CreatePublicOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('public_orders', function (Blueprint $table) {
            $table->id();

            $table->string('place_id')->nullable();
            $table->string('store_name')->nullable();
            $table->string('invoice_number')->nullable();
            $table->enum('status',['pending','in_the_way_to_store','in_the_way_to_client','cancelled','delivered'])->default('pending');
            $table->double('purchase_invoice_value',10,2)->nullable();

            $table->double('delivery_cost',10,2)->nullable();
            $table->double('tax',10,2)->nullable();
            $table->double('total',10,2)->nullable();

            $table->double('store_lat')->nullable();
            $table->double('store_lng')->nullable();
            $table->string('store_address')->nullable();

            $table->double('destination_lat')->nullable();
            $table->double('destination_lng')->nullable();
            $table->string('destination_address')->nullable();

            $table->integer('client_id')->unsigned()->nullable();
            $table->foreign('client_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('driver_id')->unsigned()->nullable();
            $table->foreign('driver_id')->references('id')->on('users')->onDelete('cascade');

//          $table->boolean('client_confirm_Received');
//          $table->dateTime('client_confirm_Received_date');
//          $table->boolean('driver_confirm_Received');

            $table->text('note')->nullable();
            $table->text('cancel_reason')->nullable();
            $table->unsignedInteger('cancel_reasons_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('public_orders');
    }
}
