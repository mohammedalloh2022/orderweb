<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShopTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shop_transactions', function (Blueprint $table) {
            $table->id();
            $table->double('prev_wallet',10,2)->nullable();
            $table->double('current_wallet',10,2)->nullable();
            $table->double('next_wallet',10,2)->nullable();
            $table->double('amount',10,2)->nullable();
            $table->string('invoice_no')->nullable();
            $table->string('order_invoice_number')->nullable();
            $table->enum('type',['deposit','withdraw'])->nullable();
            $table->unsignedInteger('order_id')->nullable();
            $table->unsignedInteger('shop_id')->nullable();
            $table->foreign('order_id')->references('id')->on('orders');
            $table->foreign('shop_id')->references('id')->on('categories_contents');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shop_transactions');
    }
}
