<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('app_contents', function (Blueprint $table) {
            $table->id();
            $table->string('facebook_link')->nullable();
            $table->string('instagram_link')->nullable();
            $table->string('linkedin_link')->nullable();
            $table->string('twitter_link')->nullable();
            $table->string('client_privacy_title_ar')->nullable();
            $table->string('client_privacy_title_en')->nullable();
            $table->text('client_privacy_content_ar')->nullable();
            $table->text('client_privacy_content_en')->nullable();
            $table->string('driver_privacy_title_ar')->nullable();
            $table->string('driver_privacy_title_en')->nullable();
            $table->text('driver_privacy_content_ar')->nullable();
            $table->text('driver_privacy_content_en')->nullable();
            $table->string('mobile')->nullable();
            $table->string('address')->nullable();
            $table->string('email')->nullable();
            $table->timestamps();
        });
        Schema::table('app_settings', function (Blueprint $table) {
            $table->dropColumn('facebook_link');
            $table->dropColumn('instagram_link');

            $table->dropColumn('twitter_link');
            $table->dropColumn('privacy');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('app_contents');
    }
}
