<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyPublicOrderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('public_orders', function (Blueprint $table) {
            $table->double('app_revenue',10,2)->nullable();
            $table->double('driver_revenue',10,2)->nullable();
            $table->double('app_delivery_commission',10,2)->nullable();
            $table->double('app_tax',10,2)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('public_orders', function (Blueprint $table) {
            //
        });
    }
}
