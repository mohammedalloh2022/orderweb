<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyDriversUsersTableAddWalletColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->double('wallet',10,2)->nullable();
            $table->double('public_wallet',10,2)->nullable();
        });
        Schema::table('public_orders', function (Blueprint $table) {
            $table->dropColumn("status");
        });
        Schema::table('public_orders', function (Blueprint $table) {
            $table->enum('status',['pending','in_the_way_to_store','in_the_way_to_client','cancelled','delivered','paid'])->default('pending');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
