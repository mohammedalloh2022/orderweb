<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDriverWalletTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('driver_wallet_transactions', function (Blueprint $table) {
            $table->id();
            $table->double('prev_wallet',10,2)->nullable();
            $table->double('current_wallet',10,2)->nullable();
            $table->double('next_wallet',10,2)->nullable();
            $table->double('amount',10,2)->nullable();
            $table->string('invoice_no')->nullable();
            $table->enum('type',['deposit','withdraw'])->nullable();
            $table->unsignedInteger('driver_id')->nullable();
//            $table->foreign('driver_id')->references('id')->on('users');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('driver_wallet_transactions');
    }
}
