<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyDeliveryPricesTable extends Migration
{
    public function up()
    {
        Schema::table('delivery_prices', function (Blueprint $table) {
            $table->unsignedBigInteger('country_id')->nullable();
        });
    }

    public function down()
    {
        Schema::table('delivery_prices', function (Blueprint $table) {
            //
        });
    }
}
