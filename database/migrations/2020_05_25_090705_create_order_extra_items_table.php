<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderExtraItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_extra_items', function (Blueprint $table) {
            $table->id();
            $table->string('name_ar')->nullable();
            $table->string('name_en')->nullable();
            $table->unsignedInteger('item_id')->index()->nullable();
            $table->unsignedInteger('order_item_id')->index()->nullable();
            $table->double('price',10,2)->nullable();
            $table->integer('qty')->nullable();
            $table->double('total',10,2)->nullable();
            $table->foreign('item_id')->references('id')->on('items');
            $table->foreign('order_item_id')->references('id')->on('order_items');
//            $table->foreign('order_item_id')->references('id')->on('order_items')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_extra_items');
    }
}
