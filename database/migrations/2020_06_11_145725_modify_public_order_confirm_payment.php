<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyPublicOrderConfirmPayment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('public_orders', function (Blueprint $table) {
            $table->dropColumn('status');
        });
        Schema::table('public_orders', function (Blueprint $table) {
            $table->enum('status',['pending','in_the_way_to_store','in_the_way_to_client','cancelled','delivered','invoice_paid'])->default('pending');
            $table->boolean('client_paid_invoice')->default(false);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('public_orders', function (Blueprint $table) {
            //
        });
    }
}
