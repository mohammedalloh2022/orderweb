<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->date('invoice_date')->nullable();
            $table->string('invoice_number')->nullable();
            $table->string('reference_number')->nullable();
            $table->enum('status',['pending','preparing','ready','delivered'])->default('pending');
            $table->enum('paid_status',['pending','approved','rejected'])->default('pending');
            $table->enum('type_of_receive',['home','restaurant'])->default('home');
            $table->enum('type',['normal','offer'])->default('normal');
            $table->double('sub_total_1',10,2)->nullable();
            $table->double('discount',10,2)->nullable();
            $table->double('sub_total_2',10,2)->nullable();
            $table->double('tax',10,2)->nullable();
            $table->double('delivery',10,2)->nullable();
            $table->double('total',10,2)->nullable();
            $table->string('receive')->nullable();
            $table->dateTime('delivered_date')->nullable();
            $table->unsignedInteger('coupon_id')->index()->nullable();
            $table->text('notes')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('driver_id')->unsigned()->nullable();
            $table->foreign('driver_id')->references('id')->on('users')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
