<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateAppSettingsColumn extends Migration
{
    public function up()
    {
        Schema::table('app_settings', function (Blueprint $table) {
            $table->double('client_hand_payment_limit',10,2)->nullable();
        });
    }

    public function down()
    {
        Schema::table('app_settings', function (Blueprint $table) {
            //
        });
    }
}
