<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->enum('type',["normal","offer"])->default("normal");
            $table->string('image')->nullable();
            $table->string('name_ar')->nullable();
            $table->string('name_en')->nullable();
            $table->double('price',10,2)->nullable();
            $table->text('desc_ar')->nullable();
            $table->text('desc_en')->nullable();
            $table->integer('qty')->nullable();
            $table->unsignedInteger('parent_id')->nullable()->index();
            $table->unsignedInteger('classification_id')->nullable()->index();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::table('items', function (Blueprint $table) {
            $table->foreign('parent_id')->references('id')->on('items');
            $table->foreign('classification_id')->references('id')->on('categories_contents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
