<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCategoriesContentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories_contents', function (Blueprint $table) {
            $table->id();
            $table->string('name_ar')->nullable();
            $table->string('name_en')->nullable();
            $table->string('image')->nullable();
            $table->string('cover_image')->nullable();
            $table->string('address_ar')->nullable();
            $table->string('address_en')->nullable();
            $table->double('lat')->nullable();
            $table->double('lng')->nullable();
            $table->time('start_work_at')->nullable();
            $table->time('end_work_at')->nullable();
            $table->unsignedInteger('content_id')->nullable()->index();
            $table->string('content_type')->nullable()->index();
            $table->foreign('content_id')->references('id')->on('categories');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories_contents');
    }
}
