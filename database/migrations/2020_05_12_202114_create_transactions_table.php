<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // driver transaction ...
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->double('prev_wallet',10,2)->nullable();
            $table->double('current_wallet',10,2)->nullable();
            $table->double('next_wallet',10,2)->nullable();
            $table->double('amount',10,2)->nullable();
            $table->string('invoice_no')->nullable();
            $table->enum('type',['deposit','withdraw'])->nullable();
            $table->integer('driver_id')->unsigned()->nullable();
            $table->foreign('driver_id')->references('id')->on('users')->onDelete('cascade');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
