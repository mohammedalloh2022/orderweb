<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->char('code',2)->unique()->nullable();
            $table->char('iso3',3)->nullable();
            $table->char('ISO-4217',3)->nullable();
            $table->char('currency_code',3)->nullable();
            $table->string('name_en')->nullable();
            $table->string('name_ar')->nullable();
            $table->char('phone_code',3)->unique()->nullable();
            $table->tinyInteger('phone_length')->nullable();
            $table->boolean('is_active')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
