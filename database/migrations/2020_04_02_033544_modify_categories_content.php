<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyCategoriesContent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('categories_contents', function (Blueprint $table) {
            $table->unsignedInteger('parent_id')->nullable()->index();
            $table->foreign('parent_id')->references('id')->on('categories_contents');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categories_contents', function (Blueprint $table) {
            //
        });
    }
}
