<?php

use Carbon\Carbon;


function filterPeriod($period)
{
    $start_date = null;
    $end_date = null;
    \Carbon\Carbon::setWeekStartsAt(\Carbon\Carbon::SATURDAY);
    \Carbon\Carbon::setWeekEndsAt(\Carbon\Carbon::FRIDAY);
    switch ($period) {
        case 'today':
            $start_date = Carbon::now()->startOfDay()->format('Y-m-d H:i');
            $end_date = Carbon::now()->endOfDay()->format('Y-m-d H:i');
            break;
        case 'week':
            $start_date = Carbon::now()->startOfWeek()->format('Y-m-d H:i');
            $end_date = Carbon::now()->endOfWeek()->format('Y-m-d H:i');
            break;
        case 'month':
            $start_date = Carbon::now()->startOfMonth()->format('Y-m-d H:i');;
            $end_date = Carbon::now()->endOfMonth()->format('Y-m-d H:i');;
            break;
        case 'year':
            $start_date = Carbon::now()->startOfYear()->format('Y-m-d H:i');
            $end_date = Carbon::now()->endOfYear()->format('Y-m-d H:i');
            break;
    }
    $period = ['start_at' => $start_date, 'end_at' => $end_date];
    return $period;
}

function sendFCMWithTimeExpiration($token, $message)
{

    $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';

    $api_server_key = config('firebase.fcm_server_key');
    $notification = [
        'title' => '4station',
        'body' => $message['msg'],
        "data" => $message,
//        "order_id"=>$message['order_id']?$message['order_id']:$message['public_order_id']??null,
//        "type"=>$message['type']??null,
        'icon' => 'myIcon',
        'sound' => 'mySound',
        "click_action" => "com.webapp.a4_order_station_driver.feture.home.MainActivity",
        "time_to_live" => 30
    ];
    $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];

    $fcmNotification = [
//            'registration_ids' => $this->tokens, //multple token array
        'to' => $token,
        'notification' => $notification,
        'data' => $extraNotificationData
    ];


    $fields = array(
//            'registration_ids' => $token,
        'priority' => 1,
        'notification' => array('title' => ' Qteach ', 'body' => $message, 'sound' => 'Default'),
    );
    $headers = array(
        'Authorization:key=' . $api_server_key,
        'sender:id='.config('firebase.fcm_sender_id'),
        'Content-Type:application/json'
    );

    // Open connection
    $ch = curl_init();
    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
    // Execute post
    $result = curl_exec($ch);
    // Close connection
    curl_close($ch);
    return $result;
}

function sendFcmWeb($token, $message)
{

    $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';

    $api_server_key = config('firebase.fcm_server_key');
    $notification = [
        'title' => '4station',
        'body' => $message['msg'],
        "data" => $message,
        'icon' => 'myIcon',
        'sound' => 'mySound',

        "click_action" => "/manage/orders/view/" . $message['order_id'],

    ];
    $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];

    $fcmNotification = [
//            'registration_ids' => $this->tokens, //multple token array
        'to' => $token,
        'notification' => $notification,
        'data' => $extraNotificationData
    ];


    $fields = array(
//            'registration_ids' => $token,
        'priority' => 1,
        'notification' => array('title' => ' 4orderstation ', 'body' => $message, 'sound' => 'Default'),
    );
    $headers = array(
        'Authorization:key=' . $api_server_key,
        'sender:id='.config('firebase.fcm_sender_id'),
        'Content-Type:application/json'
    );

    // Open connection
    $ch = curl_init();
    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
    // Execute post
    $result = curl_exec($ch);
    // Close connection
    curl_close($ch);
    return $result;
}

function sendFCM($token, $message)
{

    $path_to_firebase_cm = 'https://fcm.googleapis.com/fcm/send';

    $api_server_key = config('firebase.fcm_server_key');
    $notification = [
        'title' => '4station',
        'body' => $message['msg'],
        "data" => $message,

        'icon' => 'myIcon',
        'sound' => 'mySound',
        "click_action" => "com.webapp.a4_order_station_driver.feture.home.MainActivity",

    ];
    $extraNotificationData = ["message" => $notification, "moredata" => 'dd'];

    $fcmNotification = [
//            'registration_ids' => $this->tokens, //multple token array
        'to' => $token,
        'notification' => $notification,
        'data' => $extraNotificationData
    ];


    $fields = array(
//            'registration_ids' => $token,
        'priority' => 1,
        'notification' => array('title' => ' Qteach ', 'body' => $message, 'sound' => 'Default'),
    );
    $headers = array(
        'Authorization:key=' . $api_server_key,
        'sender:id='.config('firebase.fcm_sender_id'),
        'Content-Type:application/json'
    );

    // Open connection
    $ch = curl_init();
    // Set the url, number of POST vars, POST data
    curl_setopt($ch, CURLOPT_URL, $path_to_firebase_cm);
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4);
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
    // Execute post
    $result = curl_exec($ch);
    // Close connection
    curl_close($ch);
    return $result;
}

function distanceBetweenTwoPoints($lat1, $lon1, $lat2, $lon2, $unit)
{

    if (($lat1 == $lat2) && ($lon1 == $lon2)) {
        return 0;
    } else {

        $theta = $lon1 - (double)$lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad((double)$lat2)) + cos(deg2rad($lat1)) * cos(deg2rad((double)$lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
}

function getCheckOutId($amount, $currency)
{
    $url = "https://oppwa.com/v1/checkouts";
    $data = "entityId=8ac9a4c8747f30650174e4f505ed2912" .
        "&amount=$amount" .
        "&currency=$currency" .
        "&paymentType=DB"

//        "&merchantTransactionId=250".
//        "&customer.email=abdallah9said@gmail.com"
    ;
//        "&notificationUrl=http://www.example.com/notify";

    try {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer OGFjOWE0Y2M3NGQ5OGY0ZTAxNzRlNGYyOTdiZjE2OGJ8Y2h3SDlFekN3Zw=='));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if (curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        return json_decode($responseData, true);
    } catch (\Exception $e) {
        return $e->getMessage();
    }

}

function getMadaCheckOutId($amount, $currency)
{
    $url = "https://oppwa.com/v1/checkouts";
    $data = "entityId=8ac9a4c8747f30650174e4f5bb4a291d" .
        "&amount=$amount" .
        "&currency=$currency" .
        "&paymentType=DB";
//        "&notificationUrl=http://www.example.com/notify";

    try {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Authorization:Bearer OGFjOWE0Y2M3NGQ5OGY0ZTAxNzRlNGYyOTdiZjE2OGJ8Y2h3SDlFekN3Zw=='));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);// this should be set to true in production
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $responseData = curl_exec($ch);
        if (curl_errno($ch)) {
            return curl_error($ch);
        }
        curl_close($ch);
        return json_decode($responseData, true);
    } catch (\Exception $e) {
        return $e->getMessage();
    }

}

function heyperPayPaymentStatus($resourcePath)
{
    $url = "https://oppwa.com/" . $resourcePath;

    $url .= "?entityId=8ac9a4c8747f30650174e4f505ed2912";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization:Bearer OGFjOWE0Y2M3NGQ5OGY0ZTAxNzRlNGYyOTdiZjE2OGJ8Y2h3SDlFekN3Zw=='));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);// this should be set to true in production
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $responseData = curl_exec($ch);
    if (curl_errno($ch)) {
        return curl_error($ch);
    }
    curl_close($ch);
    return json_decode($responseData, true);
}

function heyperMadaPayPaymentStatus($resourcePath)
{
    $url = "https://oppwa.com/" . $resourcePath;

    $url .= "?entityId=8ac9a4c8747f30650174e4f5bb4a291d";

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Authorization:Bearer OGFjOWE0Y2M3NGQ5OGY0ZTAxNzRlNGYyOTdiZjE2OGJ8Y2h3SDlFekN3Zw=='));
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);// this should be set to true in production
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    $responseData = curl_exec($ch);
    if (curl_errno($ch)) {
        return curl_error($ch);
    }
    curl_close($ch);
    return json_decode($responseData, true);
}

