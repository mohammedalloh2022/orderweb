@extends('layouts.dashboard')
@section('content')
    <country-list :fetch-data-url="'{{route('country.search')}}'" inline-template>

        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Countries list
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">

                        <a href="{{url()->previous()}}" class="btn btn-clean btn-bold btn-upper btn-font-sm kt-hidden">
                            <i class="la la-long-arrow-left"></i>
                            Back
                        </a>
                        <button type="button" @click="innerVisible = true" class="btn btn-primary">
                            <i class="flaticon2-add-1"></i>
                            New country
                        </button>


                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">



                <div id="kt_table_1_wrapper" class="table table-striped m-table">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">

                        </div>
                        <div class="col-sm-12 col-md-6">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table-component

                                :data="fetchData"
                                :table-class="'table table-hover '"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                sort-by="created_at"
                                {{--                                         :sort-order="name_en"--}}
                                ref="categoriesTable"
                            >
                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="category">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(category)+1}}
                                    </template>
                                </table-column>

                                <table-column label="Name En" show="name_en"></table-column>
                                <table-column label="Name Ar" show="name_ar"></table-column>
                                <table-column label="Code" show="code"></table-column>
                                <table-column label="Currency code" show="currency_code"></table-column>
                                <table-column label="Phone code" show="phone_code"></table-column>
                                <table-column label="Mobile length" show="phone_length"></table-column>

                                <table-column label="Cities ">
                                    <template scope="country">
                                        <a :href="'/country/cities/'+country.id" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                            @{{country.cities_count}}
                                        </a>
                                    </template>
                                </table-column>

                                <table-column label="Active ">
                                    <template scope="category">
                                        <toggle-button
                                            :value="category.is_active==1?true:false"
                                            color="#82C7EB"
                                            :sync="true"
                                            @change="onChangeEventToggleHandler($event,'{!! route('country.change.status') !!}',category.id)"
                                        />
                                    </template>
                                </table-column>



                                <table-column label="actions" :sortable="false" :filterable="false">
                                    <template scope="category">

{{--                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" @click="mountCat(category)">--}}
{{--                                            <i class="la la-edit" aria-hidden="true"></i>--}}
{{--                                        </a>--}}
                                        {{--                                    |--}}
                                        {{--                                    <a class="btn btn-sm btn-clean btn-icon btn-icon-md" @click="deleteAction('categories/',category.id,true)" href="javascript:;">--}}
                                        {{--                                        <i class="la la-trash" aria-hidden="true" style="color:red;"></i>--}}
                                        {{--                                    </a>--}}

                                    </template>
                                </table-column>

                            </table-component>


                        </div>
                    </div>

                </div>






            </div>
            <el-dialog
                @close="clearCategoryModalData"
                width="30%"
                :title="id?'Update  country':'Add new country'"
                :visible.sync="innerVisible"
                append-to-body>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group"
                                 :class="{ 'has-error':  form.validations.cat_name_en }">
                                <label class="control-label">Name (En)</label>
                                <input type="text" class="form-control" placeholder="Name (En)"
                                       v-model="name_en">
                                <span v-if="form.validations.cat_name_en" class="help-block invalid-feedback show">@{{ form.validations.cat_name_en[0] }}</span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group"
                                 :class="{ 'has-error': form.validations.cat_name_ar }">
                                <label class="control-label">Name (Ar)</label>
                                <input type="text" class="form-control" placeholder="Name (Ar)"
                                       v-model="name_ar">
                                <span  v-if="form.validations.cat_name_ar" class="help-block invalid-feedback show">@{{ form.validations.cat_name_ar[0] }}</span>
                                {{--                       @{{ form.validations.cat_name_ar }}--}}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group"
                                 :class="{ 'has-error': form.validations.code }">
                                <label class="control-label">code</label>
                                <input type="text" class="form-control" placeholder="Code ex. SA"
                                       v-model="code">
                                <span  v-if="form.validations.code" class="help-block invalid-feedback show">
                                    @{{ form.validations.code[0] }}</span>

                            </div>
                        </div>
                         <div class="col-md-12">
                                <div class="form-group"
                                     :class="{ 'has-error': form.validations.currency_code }">
                                    <label class="control-label">Currency code</label>
                                    <input type="text" class="form-control" placeholder="Code ex. SAR"
                                           v-model="currency_code">
                                    <span  v-if="form.validations.currency_code" class="help-block invalid-feedback show">
                                        @{{ form.validations.currency_code[0] }}</span>

                                </div>
                            </div>

                        <div class="col-md-12">
                            <div class="form-group"
                                 :class="{ 'has-error': form.validations.phone_code }">
                                <label class="control-label">Phone code</label>
                                <input type="text" class="form-control" placeholder="Phone code ex. 966"
                                       v-model="phone_code">
                                <span  v-if="form.validations.phone_code" class="help-block invalid-feedback show">
                                    @{{ form.validations.phone_code[0] }}</span>

                            </div>
                        </div>


                        <div class="col-md-12">
                            <div class="form-group"
                                 :class="{ 'has-error': form.validations.phone_length }">
                                <label class="control-label">Mobile length</label>
                                <input type="text" class="form-control" placeholder="Mobile # length"
                                       v-model="phone_length">
                                <span  v-if="form.validations.phone_length" class="help-block invalid-feedback show">
                                    @{{ form.validations.phone_length[0] }}</span>

                            </div>
                        </div>



                    </div>


                </div>
                <div slot="footer" class="dialog-footer" v-if="!id">
                    <el-button class="btn btn-primary" class="AddCategory" @click="AddCategory"
                               :disabled="form.disabled">Add</el-button>
                    <el-button @click="innerVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
                <div slot="footer" class="dialog-footer" v-if="id">
                    <el-button class="btn btn-primary" class="updateCategory" @click="updateCategory"
                               :disabled="form.disabled">Update</el-button>
                    <el-button @click="innerVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
            </el-dialog>

        </div>
    </country-list>
@endsection
