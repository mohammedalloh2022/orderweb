@extends('layouts.dashboard')
@section('content')
    <cities-list :fetch-data-url="'{{route('city.search',['country'=>$country_id])}}'"
                :country-id="{{$country_id}}" inline-template>

        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                         مدن دولة {{ @$country_name }}
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">

                        <a href="{{route('country.index')}}" class="btn btn-clean btn-bold btn-upper btn-font-sm ">
                            <i class="la la-long-arrow-left"></i>
                            رجوع
                        </a>
                        <button type="button" @click="innerVisible = true" class="btn btn-primary">
                            <i class="flaticon2-add-1"></i>
                            اضافة مدينة جديدة
                        </button>


                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">



                <div id="kt_table_1_wrapper" class="table table-striped m-table">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">

                        </div>
                        <div class="col-sm-12 col-md-6">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table-component

                                :data="fetchData"
                                :table-class="'table table-hover '"
                                :filter-input-class="'form-control'"
                                filter-placeholder="بحث "
                                :show-caption="false"
                                :filter-no-results="'لا يوجد نتائج'"
                                sort-by="created_at"
                                {{--                                         :sort-order="name_en"--}}
                                ref="citiesTable"
                            >
                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="city">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(city)+1}}
                                    </template>
                                </table-column>
                                <table-column label="اسم المدينة عربي " show="name_ar"></table-column>
                                <table-column label="اسم المدينة انجليزي " show="name_en"></table-column>

                                <table-column label="تاريخ الإضافة" :sortable="false" :filterable="false">
                                    <template scope="city">
                                        @{{dataFormatter(city.created_at)}}
                                    </template>
                                </table-column>

                                <table-column label="الحالة">
                                    <template scope="category">
                                        <toggle-button
                                            :value="category.is_active==1?true:false"
                                            color="#82C7EB"
                                            :sync="true"
                                            @change="onChangeEventToggleHandler($event,'{!! route('city.change.status') !!}',category.id)"
                                        />
                                    </template>
                                </table-column>



                                <table-column label="الاجراءات" :sortable="false" :filterable="false">
                                    <template scope="city">
                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" @click="mountCat(city)">
                                            <i class="la la-edit" aria-hidden="true"></i>
                                        </a>
                                        {{--                                    |--}}
                                        {{--                                        <a class="btn btn-sm btn-clean btn-icon btn-icon-md" @click="deleteAction('cities/',city.id,true)" href="javascript:;">--}}
                                        {{--                                            <i class="la la-trash" aria-hidden="true" style="color:red;"></i>--}}
                                        {{--                                        </a>--}}
                                    </template>
                                </table-column>

                            </table-component>


                        </div>
                    </div>

                </div>






            </div>
            <el-dialog
                :close-on-press-escape="false"
                :close-on-click-modal="false"
                @opened="initMap"
                @close="clearCategoryModalData"
                width="30%"
                :title="id?'تحديث المدينة':'اضافة مدينة جديدة'"
                :visible.sync="innerVisible"
                append-to-body>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group"
                                 :class="{ 'has-error':  form.validations.name_ar }">
                                <label class="control-label">اسم المدينة (عربي) </label>
                                <input type="text" class="form-control" placeholder="اسم المدينة"
                                       v-model="name_ar">
                                <span v-if="form.validations.name_ar" class="help-block invalid-feedback show">@{{ form.validations.name_ar[0] }}</span>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group"
                                 :class="{ 'has-error':  form.validations.name_en }">
                                <label class="control-label">اسم المدينة (انجليزي) </label>
                                <input type="text" class="form-control" placeholder="اسم المدينة"
                                       v-model="name_en">
                                <span v-if="form.validations.name_en" class="help-block invalid-feedback show">@{{ form.validations.name_en[0] }}</span>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <input
                                id="pac-input"
                                class="controls"
                                type="text"
                                placeholder="Search Box"
                            />
                            <div id="mapLocation" style="height:250px"></div>

                        </div>
                    </div>

                </div>
                <div slot="footer" class="dialog-footer" v-if="!id">
                    <el-button class="btn btn-primary" class="AddCategory" @click="AddCategory"
                               :disabled="form.disabled">حفظ</el-button>
                    <el-button @click="innerVisible = false">إلغاء الأمر</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
                <div slot="footer" class="dialog-footer" v-if="id">
                    <el-button class="btn btn-primary" class="updateCategory" @click="updateCategory"
                               :disabled="form.disabled">تحديث</el-button>
                    <el-button @click="innerVisible = false">إلغاء الأمر</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
            </el-dialog>

        </div>
    </cities-list>
@endsection
