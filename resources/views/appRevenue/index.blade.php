@extends('layouts.dashboard')

@section('content')

    <app-revenue-index :fetch-data-url="'{{route('app.revenue.orders.search',['countryId'=>$countryId])}}'" inline-template>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                            App orders revenue
                    </h3>
                </div>

                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <a type="button" class="btn btn-primary" href="{{\Illuminate\Support\Facades\URL::previous()}}">
                            Back
                        </a>
                    </div>
                </div>

            </div>
            <div class="kt-portlet__body">
                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="row">
                        <div class="kt-widget-2">
                            <div class="kt-widget-2__content kt-portlet__space-x">
                                <div class="col-xl-3 col-lg-6 col-md-6 col-6"><div class="kt-widget-2__item"><div class="kt-widget-2__item-title">
                                              App revenue
                                        </div> <div class="kt-widget-2__item-stats"><div class="kt-widget-2__item-info"><div class="kt-widget-2__item-text">
                                                    Total:
                                                </div> <div class="kt-widget-2__item-value">
                                                    @{{app_revenue}}
                                                </div></div> <div class="kt-widget-2__item-chart"><canvas id="kt_widget_general_statistics_chart_1" width="80" height="40" style="display: block;"></canvas>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div class="row col-12">

                        <div class="col-4">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-4 col-form-label">Report</label>
                                <div class="col-8">
                                    <select class="form-control" id="exampleSelect1" v-model="report_period">
                                        <option value="">Select period</option>
                                        <option value="today">Today</option>
                                        <option value="week">Week</option>
                                        <option value="month">Month</option>
                                        <option value="year">Year</option>
                                        <option value="custom">Custom</option>
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="col-4" v-if="report_period=='custom'">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-4 col-form-label">Period date</label>
                                <div class="col-8">
                                    <el-date-picker
                                        v-model="custom_period"
                                        type="daterange"
                                        align="right"
                                        unlink-panels
                                        range-separator="To"
                                        start-placeholder="Start date"
                                        end-placeholder="End date"
                                        value-format="yyyy-MM-dd"
                                    >
                                    </el-date-picker>

                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <button type="button" class="btn btn-primary" @click="refreshTable">
                                Filter
                            </button>
                        </div>



                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table-component
                                :data.sync="fetchAppRevenueData"
                                :table-class="'table table-hover kt-datatable__table'"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                sort-by="created_at"
                                ref="AppRevenue"
                            >
                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                    </template>
                                </table-column>
                                <table-column label="Order No." >
                                    <template scope="order">
                                        <a :href="'/manage/orders/view/'+order.id">
                                            @{{ order.invoice_number }}
                                        </a>
                                    </template>
                                </table-column>
                                <table-column label="Order amount" show="sub_total_1" ></table-column>
                                <table-column label="Discount" show="discount" ></table-column>
                                <table-column label="Total amount" show="total" ></table-column>
                                <table-column label="App revenue" show="app_revenue" ></table-column>
                                <table-column label="Driver revenue" show="driver_revenue" ></table-column>
                                <table-column label="Shop revenue" show="shop_revenue" ></table-column>
                                <table-column label="Date " show="created_at" ></table-column>
                            </table-component>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </app-revenue-index>

@endsection
