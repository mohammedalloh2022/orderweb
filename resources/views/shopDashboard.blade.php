@extends('layouts.dashboard')
@push('js')
    <script>
        jQuery('document').ready(function () {
            initMap()
        });

        function initMap() {
            map = new google.maps.Map(document.getElementById('dashobard-map'), {
                zoom: 15,
                center: {lat: 31.5388035, lng: 34.4863532},
                mapTypeId: 'terrain'
            });
        }
    </script>
@endpush
@section('content')

    <div class="row">





        <div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__body kt-portlet__body--fluid">
                    <div class="kt-widget-20">
                        <div class="kt-widget-20__title">
                            <h4 class="kt-portlet__head-title">Complete Orders</h4>
                        </div>
                        <div class="kt-widget-20__title">
                            <div class="kt-widget-20__label">{{$orders_count}}</div>
                            <img class="kt-widget-20__bg" src="/assets/media/misc/iconbox_bg.png" alt="bg">
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__body kt-portlet__body--fluid">
                    <div class="kt-widget-20">
                        <div class="kt-widget-20__title">
                            <h4 class="kt-portlet__head-title"> Branch</h4>
                        </div>
                        <div class="kt-widget-20__title">
                            <div class="kt-widget-20__label">{{$branch_count}}</div>
                            <img class="kt-widget-20__bg" src="/assets/media/misc/iconbox_bg.png" alt="bg">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__body kt-portlet__body--fluid">
                    <div class="kt-widget-20">
                        <div class="kt-widget-20__title">
                            <h4 class="kt-portlet__head-title">wallet</h4>
                        </div>
                        <div class="kt-widget-20__title">
                            <div class="kt-widget-20__label">{{@$shop->wallet}}</div>
                            <img class="kt-widget-20__bg" src="/assets/media/misc/iconbox_bg.png" alt="bg">
                        </div>
                    </div>
                </div>
            </div>
        </div>


        {{--        <div class="col-lg-4 col-xl-3 order-lg-1 order-xl-1">--}}

        {{--            <!--begin::Portlet-->--}}
        {{--            <div class="kt-portlet kt-portlet--height-fluid">--}}

        {{--                <div class="kt-portlet__body kt-portlet__body--fluid">--}}
        {{--                    <div class="kt-widget-20">--}}
        {{--                        <div class="kt-widget-20__title">--}}
        {{--                            <h4 class="kt-portlet__head-title">Revenue</h4>--}}
        {{--                        </div>--}}
        {{--                        <div class="kt-widget-20__title">--}}
        {{--                            <div class="kt-widget-20__label">17M</div>--}}
        {{--                            <img class="kt-widget-20__bg" src="assets/media/misc/iconbox_bg.png" alt="bg">--}}
        {{--                        </div>--}}
        {{--                    </div>--}}
        {{--                </div>--}}
        {{--            </div>--}}

        {{--            <!--end::Portlet-->--}}
        {{--        </div>--}}
    </div>
    <div class="row">


        <div class="col-lg-6 col-xl-3 order-lg-1 order-xl-1">

            <!--begin::Portlet-->
            <div class="kt-portlet kt-portlet--height-fluid kt-portlet--tabs">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Statistics
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <ul class="nav nav-tabs nav-tabs-line nav-tabs-line-brand nav-tabs-bold" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active show" data-toggle="tab" href="#kt_portlet_tabs_1_1_content"
                                   role="tab" aria-selected="false">
                                    Today
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#kt_portlet_tabs_1_2_content" role="tab"
                                   aria-selected="false">
                                    Week
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#kt_portlet_tabs_1_3_content" role="tab"
                                   aria-selected="true">
                                    Month
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#kt_portlet_tabs_1_4_content" role="tab"
                                   aria-selected="true">
                                    Year
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="tab-content">
                        <div class="tab-pane fade active show" id="kt_portlet_tabs_1_1_content" role="tabpanel">
                            <table class="table table-striped">
                                <tbody>

                                <tr>
                                    <th>Orders</th>
                                    <td>{{$statistics['order_today_count']??0}}</td>
                                </tr>

                                <tr>
                                    <th>Shop income</th>
                                    <td>{{$statistics['order_today_amount']??0}}</td>
                                </tr>
{{--                                <tr>--}}
{{--                                    <th>App revenue</th>--}}
{{--                                    <td>--}}
{{--                                        {{ number_format((float)$statistics['order_today_app_revenue'], 2, '.', '')}}--}}

{{--                                    </td>--}}
{{--                                </tr>--}}
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade" id="kt_portlet_tabs_1_2_content" role="tabpanel">
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th>Orders</th>
                                    <td>{{$statistics['order_week_count']??null}}</td>
                                </tr>

                                <tr>
                                    <th>Shop income</th>
                                    <td>{{$statistics['order_week_amount']??null}}</td>
                                </tr>
{{--                                <tr>--}}
{{--                                    <th>App revenue</th>--}}
{{--                                    <td>--}}
{{--                                        {{number_format($statistics['order_week_app_revenue'], 2, '.', '')}}--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade " id="kt_portlet_tabs_1_3_content" role="tabpanel">
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th>Orders</th>
                                    <td>{{$statistics['order_month_count']??null}}</td>
                                </tr>

                                <tr>
                                    <th>Shop income</th>
                                    <td>{{$statistics['order_month_amount']??null}}</td>
                                </tr>
{{--                                <tr>--}}
{{--                                    <th>App revenue</th>--}}
{{--                                    <td>--}}
{{--                                        {{number_format($statistics['order_month_app_revenue'], 2, '.', '')}}--}}
{{--                                    </td>--}}
{{--                                </tr>--}}
                                </tbody>
                            </table>
                        </div>
                        <div class="tab-pane fade " id="kt_portlet_tabs_1_4_content" role="tabpanel">
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th>Orders</th>
                                    <td>{{$statistics['order_year_count']??null}}</td>
                                </tr>

                                <tr>
                                    <th>Shop income</th>
                                    <td>{{$statistics['order_year_amount']??null}}</td>
                                </tr>
{{--                                <tr>--}}
{{--                                    <th>App revenue</th>--}}
{{--                                    <td>--}}
{{--                                        {{number_format($statistics['order_year_app_revenue'],2,'.','')}}--}}
{{--                                    </td>--}}

{{--                                </tr>--}}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

            <!--end::Portlet-->
        </div>
        <div class="col-lg-12 col-xl-9 order-lg-2 order-xl-1">



                <div class="kt-portlet kt-portlet--height-fluid">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            {{--                        <span class="kt-portlet__head-icon"><i class="flaticon-stopwatch"></i></span>--}}
                            <h3 class="kt-portlet__head-title">Orders</h3>
                        </div>

                    </div>
                    <div class="kt-portlet__body">
                        <div class="kt-portlet__content">
                            <table class="table table-striped ">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Order no.</th>
                                    <th>Client</th>
                                    <th>shop</th>
                                    <th>Status</th>
                                    <th>Total</th>
                                    <th>ordered at</th>
                                </tr>
                                </thead>
                                <tbody>
                                @isset($orders)
                                    @foreach($orders as $key=> $orders)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$orders->invoice_number}}</td>
                                            <td>{{$orders->client->name}}</td>
                                            <td>{{$orders->shop->name}}</td>
                                            <td>{{$orders->status}}</td>
                                            <td>{{$orders->total}}</td>
                                            <td>{{$orders->created_at}}</td>


                                        </tr>
                                    @endforeach
                                @endisset
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>

        </div>

    </div>

@endsection
