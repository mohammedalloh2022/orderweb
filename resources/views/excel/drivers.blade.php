
<table dir="ltr">
    <thead>
    <tr>
        <td>driver name</td>
        <td>Email </td>
        <td>Mobile </td>
        <td>Plate no </td>
        <td>Rate</td>
        <td>Orders</td>
        <td>Wallet</td>
        <td>Register at</td>


    </tr>
    </thead>
    <tbody>
    @foreach($drivers as $driver)
        <tr>
            <td>{{ @$driver->name }}  </td>
            <td>{{ @$driver->email }}  </td>
            <td>{{ @$driver->mobile }}  </td>
            <td>{{ @$driver->vehicle_plate }}  </td>
            <td>{{ @$driver->rate }}  </td>
            <td>{{ @$driver->order_count }}  </td>
            <td>{{ @$driver->wallet }}  </td>
            <td>{{ @$driver->created_at->format('Y-m-d') }}  </td>



        </tr>
    @endforeach
    </tbody>
</table>
