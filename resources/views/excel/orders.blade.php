<!DOCTYPE html>
<html>

<body dir="rtl" >
<table dir="ltr">
    <thead>
            <tr>
                <td>Order number</td>
                <td>Client name</td>
                <td>Driver name</td>
                <td>Shop name</td>
                <td>Type of receive</td>
                <td>Payment type</td>
                <td>Tax</td>
                <td>Discount</td>
                <td>App revenue</td>
                <td>Status</td>
                <td>Total</td>

            </tr>
    </thead>
    <tbody>
    @foreach($orders as $order)
        <tr>
            <td>{!! @$order->invoice_number !!}  </td>
            <td>{{   @$order->client->name}}  </td>
            <td>{{    @$order->driver->name }}  </td>
            <td>{{    @$order->shop->name }}  </td>
            <td>{{   @$order->type_of_receive }}  </td>
            <td>{{   @$order->payment_type }}  </td>
            <td>{{   @$order->tax }}  </td>
            <td>{{   @$order->discount }}  </td>
            <td>{{   @$order->app_revenue }}  </td>
            <td>{{   @$order->status_translation }}  </td>
            <td>{{   @$order->total }}  </td>


        </tr>
    @endforeach
    </tbody>
</table>


</body>
</html>
