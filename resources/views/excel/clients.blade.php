<!DOCTYPE html>
<html>

<body  >
<table dir="ltr">
    <thead>
    <tr>
        <td># </td>
        <td>Reg. # </td>
        <td>Client name</td>
        <td>Email </td>
        <td>Mobile </td>

        <td>Orders</td>

        <td>Register at</td>


    </tr>
    </thead>
    <tbody>
    @foreach($clients as $key=>$client)
        <tr>
            <td>{{ $key+1 }}  </td>
            <td>{{ $client->reg_no }}  </td>
            <td>{{ $client->name }}  </td>
            <td>{{ $client->email }}  </td>
            <td>{{ $client->mobile }}  </td>

            <td>{{ $client->client_order_count }}  </td>

            <td>{{ $client->created_at->format('Y-m-d') }}  </td>



        </tr>
    @endforeach
    </tbody>
</table>


</body>
</html>
