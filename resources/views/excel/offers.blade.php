<!DOCTYPE html>
<html>

<body dir="rtl" lang="en">
<table dir="ltr">
    <thead>
    <tr>
        <td>Store name</td>
        <td>Offer pic </td>
        <td>Name(Ar) </td>
        <td>Name(En) </td>
        <td>Description(Ar) </td>
        <td>Description(En) </td>
        <td>Price</td>
        <td>Status</td>


    </tr>
    </thead>
    <tbody>
    @foreach($offers as $offer)
        <tr>
            <td>{{ @$offer->shop->name }}  </td>
            <td>{{   @$offer->image_url}}  </td>
            <td>{{   @$offer->name_en}}  </td>
            <td>{{   @$offer->name_ar}}  </td>
            <td>{{   @$offer->desc_ar}}  </td>
            <td>{{   @$offer->desc_en}}  </td>
            <td>{{   @$offer->price}}  </td>
            <td>{{   @$offer->status}}  </td>


        </tr>
    @endforeach
    </tbody>
</table>


</body>
</html>
