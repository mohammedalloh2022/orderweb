@extends('layouts.dashboard')
@push('css')
    {{--    <link href="{{asset('assets/css/pages/invoice/invoice-v2.css')}}" rel="stylesheet" type="text/css" />--}}

@endpush


@push('js')
    <script>
        jQuery('document').ready(function () {
            initMap();
        });
        var OrderMap = null;

        function initMap() {
            OrderMap = new google.maps.Map(document.getElementById('public-order-map'), {
                zoom: 15,
                center: {lat: {!! $order->shop->lat!!}, lng: {!! $order->shop->lng !!}},
                mapTypeId: 'terrain'
            });

            var icon = {
                url: '/imgs/storeMarker.png',
                scaledSize: new google.maps.Size(50, 50), // scaled size
            };
            var clienticon = {
                url: '/imgs/user-marker.png',
                scaledSize: new google.maps.Size(50, 50), // scaled size
            };
                {{--var myLatLng = new google.maps.LatLng({!! (float)$order->store_lat!!},{!! (float)$order->store_lng !!});--}}
            var marker = new google.maps.Marker({
                    position: {lat: {!!  $order->shop->lat!!}, lng: {!!  $order->shop->lng !!}},
                    map: OrderMap,
                    title: 'Store location!',
                    draggable: false,
                    icon: icon
                });

            var order_destination_lat ={!!$order->destination_lat?? 'null'!!} ;
            var order_destination_lng ={!! $order->destination_lng ?? 'null' !!};
            if (order_destination_lat != "null" && order_destination_lng != "null") {
                var marker2 = new google.maps.Marker({
                    position: {lat: {!! $order->destination_lat!!}, lng: {!! $order->destination_lng !!}},
                    map: OrderMap,
                    title: 'Client location!',
                    draggable: false,
                    icon: clienticon
                });
            }

        }


    </script>
@endpush


@push('js')
    @push('js')

        <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
{{--        <script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-app.js"></script>--}}

        <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
        <script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-analytics.js"></script>

        <!-- Add Firebase products that you want to use -->
        <script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-auth.js"></script>
        <script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-firestore.js"></script>

{{--        <script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-database.js"></script>--}}


    @endpush
@section('content')
    <div class="row">

        @if($order->shop)
            <div class="col-lg-6 col-xl-4  order-lg-1 order-xl-1">
                <div class="kt-portlet  kt-portlet--height-fluid-half">
                    <div class="kt-widget kt-widget--general-2">

                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">Shop </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">


                            </div>
                        </div>
                        <div class="kt-portlet__body kt-portlet__body--fit bg-white">
                            <div class="kt-widget__top bg-white">

                                <div class="kt-media kt-media--lg kt-media--circle">
                                    <img src="{{$order->shop->logo_url}}" alt="image">
                                </div>
                                <div class="kt-widget__wrapper">
                                    <div class="kt-widget__label">
                                        <a href="#" class="kt-widget__title">
                                            {{$order->shop->name}}
                                        </a>

                                    </div>
                                    <div class="kt-widget__toolbar">
                                        <div class="kt-widget__links">
                                            <div class="kt-widget__cont">
                                                <div class="kt-widget__link">
                                                    <i class="flaticon-time"></i>
                                                    {{$order->shop->start_work_at}}-{{$order->shop->end_work_at}}
                                                </div>
                                                <div class="kt-widget__link mt-1">
                                                    <i class="flaticon2-phone kt-font-success mr-1"></i>
                                                    <a href="#"> {{$order->shop->mobile}}</a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($order->client)
            <div class="col-lg-6 col-xl-4  order-lg-1 order-xl-1">
                <div class="kt-portlet  kt-portlet--height-fluid-half">
                    <div class="kt-widget kt-widget--general-2">

                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">Client </h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">


                            </div>
                        </div>

                        <div class="kt-portlet__body kt-portlet__body--fit bg-white">
                            <div class="kt-widget__top bg-white">

                                <div class="kt-media kt-media--lg kt-media--circle">

                                    <img src="{{$order->client->avatar_url or ''}}" alt="image">

                                </div>
                                <div class="kt-widget__wrapper">
                                    <div class="kt-widget__label">
                                        <a href="#" class="kt-widget__title">
                                            {{$order->client->name}}
                                        </a>

                                    </div>
                                    <div class="kt-widget__toolbar">
                                        <div class="kt-widget__links">
                                            <div class="kt-widget__cont">
                                                <div class="kt-widget__link">


                                                </div>
                                                <div class="kt-widget__link mt-1">
                                                    <i class="flaticon2-phone kt-font-success mr-1"></i>
                                                    <a href="#"> {{$order->client->mobile}}</a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        @endif
        @if($order->driver)
            <div class="col-lg-6 col-xl-4  order-lg-1 order-xl-1">
                <div class="kt-portlet  kt-portlet--height-fluid-half">
                    <div class="kt-widget kt-widget--general-2">

                        <div class="kt-portlet__head">
                            <div class="kt-portlet__head-label">
                                <h3 class="kt-portlet__head-title">Driver</h3>
                            </div>
                            <div class="kt-portlet__head-toolbar">


                            </div>
                        </div>

                        <div class="kt-portlet__body kt-portlet__body--fit bg-white">
                            <div class="kt-widget__top bg-white">

                                <div class="kt-media kt-media--lg kt-media--circle">

                                    <img src="{{$order->driver->avatar_url or ''}}" alt="image">

                                </div>
                                <div class="kt-widget__wrapper">
                                    <div class="kt-widget__label">
                                        <a href="#" class="kt-widget__title">
                                            {{$order->driver->name}}
                                        </a>

                                    </div>
                                    <div class="kt-widget__toolbar">
                                        <div class="kt-widget__links">
                                            <div class="kt-widget__cont">
                                                <div class="kt-widget__link">


                                                </div>
                                                <div class="kt-widget__link mt-1">
                                                    <i class="flaticon2-phone kt-font-success mr-1"></i>
                                                    <a href="#"> {{$order->driver->mobile}}</a>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        @endif

    </div>




    <div class="row">
        <div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
            <div class="kt-portlet kt-portlet--height-fluid kt-portlet--tabs">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Order
                            <small>
                                {{$order->invoice_number}}
                            </small>
                            <span class="badge badge-info">
                                {{ $order->status_translation }}
                            </span>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">

                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="tab-content">
                        <div id="kt_portlet_tabs_1_1_content" role="tabpanel" class="tab-pane fade active show">

                            @if($order->type_of_receive=="home")
                                <order-chat :order-id='{!! $order ? $order->toJson():null !!}'
                                            inline-template>
                                    <div>
                                        <a href="javascript:;" @click="showChat=true" class="btn btn-primary">
                                            <i class="flaticon-chat-1"></i>
                                            Chat
                                        </a>

                                        <el-dialog

                                            @closed="hideDialog"
                                            width="40%"
                                            :title="'Chat'"
                                            :visible.sync="showChat"

                                            append-to-body>
                                            <div>
                                                <div class="kt-blog-post">
                                                    <div class="kt-blog-post__comments">
                                                        <div class=kt-blog-post__threads>


                                                            <div class="kt-blog-post__thread" v-for="item in chat">
                                                                <div class="kt-blog-post__head">

                                                                </div>
                                                                <div class="kt-blog-post__body">
                                                                    <div class="kt-blog-post__top">
                                                                        <div class="kt-blog-post__author">
                                                                            <div class="kt-blog-post__label">
                                                                                <img :src="item.sender_avatar_url"
                                                                                     class="kt-blog-post__image"
                                                                                     style="width: 50px;height: 50px; border-radius: 50%; display: inline-block">
                                                                                <span>
                                                    <span class="badge badge-secondary"></span>
                                                    </span>@{{ dataFormatter(item.time) }}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="kt-blog-post__content">

                                                                        <div
                                                                            class="alert alert-solid-success alert-bold"
                                                                            role="alert">
                                                                            <div class="alert-text"> @{{ item.text }}
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div slot="footer" class="dialog-footer">
                                                <el-button @click="showChat = false">Close</el-button>
                                            </div>
                                        </el-dialog>
                                    </div>
                                </order-chat>
                            @endif
                            <br/>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th>Invoice number</th>
                                    <td>   {{$order->invoice_number??null}}</td>
                                </tr>
                                <tr>
                                    <th>Client</th>
                                    <td>{{$order->client->name??null}}</td>
                                </tr>
                                <tr>
                                    <th>Delivery driver</th>
                                    <td>{{$order->driver->name??null}}</td>
                                </tr>
                                <tr>
                                    <th>Type of receive</th>
                                    <td>{{ $order->type_of_receive}}</td>
                                </tr>
                                <tr>
                                    <th>Payment type</th>
                                    <td>{{ $order->payment_type =='on_delivery'?"Cash":$order->payment_type}}</td>
                                </tr>


                                @if(isset($order->shop->parent_id))

                                    <tr>
                                        <th>Shop</th>
                                        <td>
                                            <a href="/shops/view/{{$order->shop->branchParent->id??null}}"
                                               class="kt-widget-4__item-username">
                                                {{$order->shop->branchParent->name??null}}
                                            </a>

                                        </td>
                                    </tr>


                                    <tr>
                                        <th>Branch</th>
                                        <td>
                                            <a href="/shops/view/{{$order->shop->id??null}}"
                                               class="kt-widget-4__item-username">
                                                {{$order->shop->name??null}}
                                            </a>

                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <th>Shop</th>
                                        <td>
                                            <a href="/shops/view/{{$order->shop->id??null}}"
                                               class="kt-widget-4__item-username">
                                                {{$order->shop->name??null}}
                                            </a>

                                        </td>
                                    </tr>
                                @endif


                                </tbody>
                            </table>


                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-lg-5 col-xl-5 order-lg-1 order-xl-1">
            <div class="kt-portlet kt-portlet--height-fluid kt-portlet--tabs">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Order details
                            <small>
                                {{$order->invoice_number}}
                            </small>
                            <span class="badge badge-info">
                                {{ $order->type }}
                        </span>

                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">

                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="tab-content">
                        <div id="kt_portlet_tabs_1_1_content" role="tabpanel" class="tab-pane fade active show">


{{--                            @if($order->type=="normal")--}}
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Item name</th>
                                        <th>Qty</th>
                                        <th>price</th>
                                        <th>total</th>
                                        {{--                                    <th>Extra items</th>--}}
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @if(!empty($order->orderItems))
                                        @foreach($order->orderItems as $key=>$orderItem)
                                            <tr>
                                                <th>{{$key+1}}
                                                </th>
                                                <td>
                                                    {{$orderItem->item->name??0}}
                                                </td>
                                                <td>
                                                    {{$orderItem->qty??0}}
                                                </td>
                                                <td>
                                                    {{$orderItem->price??0}}
                                                </td>
                                                <td>
                                                    {{$orderItem->total??0}}
                                                </td>

                                            </tr>
                                            @foreach($orderItem->extraItems as $keyy=>$extra )
                                                <tr class="" style="background-color: #ffffff">
                                                    <th>
                                                        +extra
                                                    </th>
                                                    <td>
                                                        {{$extra->item->name??0}}
                                                    </td>

                                                    <td>
                                                        {{$extra->qty??0}}
                                                    </td>
                                                    <td>
                                                        {{$extra->price??0}}
                                                    </td>
                                                    <td>
                                                        {{$extra->total??0}}
                                                    </td>
                                                </tr>
                                            @endforeach
                                        @endforeach

                                        <tr class="spacer" style="background: #ffffff !important">
                                            <td colspan="5" style="padding: 20px"></td>
                                        </tr>
                                        <tr style="border-top: 2px solid gray; margin-top: 5px">
                                            <th colspan="4">Sub total 01</th>
                                            <td>
                                                {{$order->sub_total_1??0}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="4"> Tax</th>
                                            <td>
                                                {{$order->tax??0}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="4">Delivery</th>
                                            <td>
                                                {{$order->delivery??0}}
                                            </td>
                                        </tr>
                                        <tr>
                                            <th colspan="4"> Sub total 02</th>
                                            <td>
                                                {{$order->sub_total_2??0}}
                                            </td>
                                        </tr>

                                        <tr>
                                            <th colspan="4"> Discount</th>
                                            <td>
                                                - {{$order->discount??0}}
                                            </td>
                                        </tr>





                                        <tr>
                                            <th colspan="4"> Total</th>
                                            <td>
                                                {{$order->total??0}}
                                            </td>
                                        </tr>


                                    @endif
                                    </tbody>
                                </table>


{{--                            @endif--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-3 col-xl-3 order-lg-1 order-xl-1">
            <div class="kt-portlet kt-portlet--height-fluid kt-portlet--tabs">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Financial details
                            <small>
                                {{$order->invoice_number}}
                            </small>

                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">

                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="tab-content">
                        <div id="kt_portlet_tabs_1_1_content" role="tabpanel" class="tab-pane fade active show">

                            <table class="table table-striped">
                                {{--                            <thead>--}}
                                {{--                            <tr>--}}
                                {{--                                <th colspan="2" class="text-center">--}}
                                {{--                                    --}}
                                {{--                                </th>--}}
                                {{--                            </tr>--}}
                                {{--                            </thead>--}}
                                <tbody>

                                <tr>
                                    <th> App commission</th>
                                    <td>
                                        {{$order->app_commission??0}}
                                    </td>
                                </tr>



                                <tr>
                                    <th> Delivery driver revenue</th>
                                    <td>
                                        {{$order->driver_revenue??0}}
                                    </td>
                                </tr>

                                {{--                            <tr>--}}
                                {{--                                <th> Delivery driver revenue </th>--}}
                                {{--                                <td>--}}
                                {{--                                    {{$order->driver_revenue??0}}--}}
                                {{--                                </td>--}}
                                {{--                            </tr>--}}

                                <tr>
                                    <th> Shop revenue</th>
                                    <td>
                                        {{$order->shop_revenue??0}}
                                    </td>
                                </tr>
                                <tr>
                                    <th> App revenue</th>
                                    <td>
                                        {{$order->app_revenue??0}}
                                    </td>
                                </tr>

                                <tr>
                                    <th> Order date</th>
                                    <td>
                                        {{$order->created_at??0}}
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-12 col-xl-12 order-lg-2 order-xl-1">

            <!--begin::Portlet-->
            <div class="kt-portlet kt-widget-14">
                <div class="kt-portlet__body">
                    @if($order->status!="on_the_way")
                        <div id="public-order-map" style="height: 500px">

                        </div>
                    @endif
                    @if($order->status=="on_the_way")
                        <order-view-map :order='{!! $order->toJson() !!}' inline-template>
                            <div>
                                <div id="orderMap" style="height: 500px">


                                </div>
                            </div>

                        </order-view-map>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
