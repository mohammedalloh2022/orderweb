@extends('layouts.dashboard')

@section('content')

    <shop-order-list :fetch-data-url="'{{route('mange.orders.search',['countryId'=>$countryId])}}'"
                     :request="'old'"
                     :export-url="'{{route('mange.orders.excel')}}'"
                     :country-id="{{$countryId}}"
                     inline-template>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Orders list
                    </h3>
                </div>

                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <a type="button" class="btn btn-primary" href="{{\Illuminate\Support\Facades\URL::previous()}}">
                            Back
                        </a>

                        <a @click="exportExcel" class="btn  btn-upper btn-success white " style="color:white; margin:0 5px">
                            <i class="fa fa-file-excel"></i> Export excel
                        </a>
                    </div>
                </div>

            </div>
            <div class="kt-portlet__body">
                <div class="row col-12">

                    <div class="col-3">
                        <div class="form-group row">
                            <label for="example-search-input" class="col-3 col-form-label">store</label>
                            <div class="col-8">
                                <multiselect
                                    :multiple="false"
                                    v-model="filter_store"
                                    :options="shopsList"
                                    label="name"
                                    track-by="name"

                                ></multiselect>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group row">
                            <label for="example-search-input" class="col-4 col-form-label">Report</label>
                            <div class="col-8">
                                <select class="form-control" id="exampleSelect1" v-model="report_period">
                                    <option value="">Select period</option>
                                    <option value="today">Today</option>
                                    <option value="week">Week</option>
                                    <option value="month">Month</option>
                                    <option value="year">Year</option>
                                    <option value="custom">Custom</option>
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="col-4" v-if="report_period=='custom'">
                        <div class="form-group row">
                            <label for="example-search-input" class="col-4 col-form-label">Period date</label>
                            <div class="col-8">
                                <el-date-picker
                                    v-model="custom_period"
                                    type="daterange"
                                    align="right"
                                    unlink-panels
                                    range-separator="To"
                                    start-placeholder="Start date"
                                    end-placeholder="End date"
                                    value-format="yyyy-MM-dd"
                                >
                                </el-date-picker>

                            </div>
                        </div>
                    </div>

                    <div class="col-2">
                        <button type="button" class="btn btn-primary" @click="refreshTable" style="margin-left: 5px">
                            Filter
                        </button>
                        <button type="button" class="btn btn-secondary" @click="resetFilter">
                            Reset
                        </button>
                    </div>


                </div>

                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                    <div class="row">
                        <div class="col-sm-12">
                            <table-component
                                :data="fetchData"
                                :table-class="'table table-hover kt-datatable__table table-responsive'"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                sort-by="created_at"
                                :show-filter="true"
                                :cache-lifetime="0"
                                {{--                                         :sort-order="name_en"--}}
                                ref="categoriesTable"
                            >
                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                    </template>
                                </table-column>
                                <table-column label="Order No." >
                                    <template scope="order">
                                        <a :href="'/manage/orders/view/'+order.id">
                                            @{{ order.invoice_number }}
                                        </a>
                                    </template>
                                </table-column>

                                <table-column label="client" :sortable="false" :filterable="false">
                                    <template scope="order">
                                        <div class="kt-widget-4">
                                            <div class="kt-widget-4__item">
                                                <div class="kt-widget-4__item-content">
                                                    <div class="kt-widget-4__item-section" v-if="order.client">
{{--                                                        <div class="kt-widget-4__item-pic">--}}
{{--                                                            <img class=""  :src="order.client.avatar_url"  alt="Avatar">--}}
{{--                                                        </div>--}}
                                                        <div class="kt-widget-4__item-info">
                                                            <a href="#"
                                                               class="kt-widget-4__item-username">
                                                                @{{ order.client.name }}
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </template>
                                </table-column>
                                <table-column label="delivery driver" :sortable="false" :filterable="false">
                                    <template scope="order">
                                        <div class="kt-widget-4" v-if="order.delivery_user">
                                            <div class="kt-widget-4__item">
                                                <div class="kt-widget-4__item-content"  v-if="order.delivery_user">
                                                    <div class="kt-widget-4__item-section">
{{--                                                        <div class="kt-widget-4__item-pic">--}}
{{--                                                            <img class=""  :src="order.delivery_user.avatar_url"  alt="Avatar">--}}
{{--                                                        </div>--}}
                                                        <div class="kt-widget-4__item-info">
                                                            <a href="#"
                                                               class="kt-widget-4__item-username">
                                                                @{{ order.delivery_user.name }}
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </template>
                                </table-column>


                                <table-column label="Shop" :sortable="false" :filterable="false">
                                    <template scope="order">
                                        <div class="kt-widget-4" v-if="order.shop">
                                            <div class="kt-widget-4__item">
                                                <div class="kt-widget-4__item-content">
                                                    <div class="kt-widget-4__item-section" v-if="order.shop">
{{--                                                        <div class="kt-widget-4__item-pic">--}}
{{--                                                            <img v-if='order.shop.logo_url' class="" :src="order.shop.logo_url" alt="shop logo">--}}
{{--                                                        </div>--}}
                                                        <div class="kt-widget-4__item-info">
                                                            <a :href="order.shop.branch_parent?'/shops/view/'+order.shop.branch_parent.id:'/shops/view/'+order.shop.id"
                                                               class="kt-widget-4__item-username">
                                                                @{{ order.shop.name}}
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </template>
                                </table-column>

                                <table-column label="Receive type" show="type_of_receive"></table-column>
                                <table-column label="Payment type" show="payment_type"></table-column>


                                <table-column label="Tax" show="tax"></table-column>
                                <table-column label="Discount" show="discount"></table-column>
                                <table-column label="App revenue" show="app_revenue"></table-column>
                                <table-column label="Total" show="total"></table-column>
                                <table-column label="City" show="client.city_name"></table-column>

                                <table-column label="Status" >
                                    <template scope="order">
                                        <span class="badge badge-secondary">@{{ order.status_translation }}</span>
                                    </template>
                                </table-column>
                                <table-column label="Cancel reason" >
                                    <template scope="order">

                                        <p v-if="order.status=='cancelled'">
                                            <span v-if="order.cancel_reason">@{{order.cancel_reason.reason_en  }}</span>
                                            <span v-else="order.cancel_reason_manual">@{{order.cancel_reason_manual  }}</span>

                                        </p>

                                    </template>
                                </table-column>
                                <table-column label="Order date" show="created_at"></table-column>
                                <table-column label="Delivery At" show="delivery_at">
                                    <template scope="order">
                                        <p>  @{{ order.delivery_at ? moment(order.delivery_at, "YYYY-MM-DD HH:mm:ss").fromNow() : "" }} </p>
                                        <p> @{{ order.delivery_at}}</p>
                                    </template>
                                </table-column>
                                <table-column label="actions" :sortable="false" :filterable="false">
                                    <template scope="order">

                                        <button class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10"
                                                @click="changeStatus('{{route('mange.orders.change.status')}}',order.id,'ready',true)"
                                                v-if="order.status=='preparing' && order.status!='cancelled'">
                                            Ready
                                        </button>
                                        <button class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10"
                                                @click="changeStatus('{{route('mange.orders.change.status')}}',order.id,'delivered',true)"
                                                v-if="order.status=='ready' && order.type_of_receive=='restaurant' && order.status!='cancelled'">
                                            Delivered
                                        </button>

                                        <button class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10"
                                                @click="sendInvitationToDrivers(order.id)"
                                                v-if="order.is_suspended==1 && order.type_of_receive=='home' && order.status!='cancelled' && order.status!='delivered'">
                                            Resend to drivers
                                        </button>
                                        <button class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10"
                                                @click="rejectOrder('{{route('mange.orders.change.status')}}',order.id,'cancelled')"
                                                v-if="order.status!='delivered' && order.status!='on_the_way' && order.status!='cancelled'">
                                            Cancelled
                                        </button>
                                    </template>
                                </table-column>



                            </table-component>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </shop-order-list>

@endsection
