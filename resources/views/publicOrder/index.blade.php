@extends('layouts.dashboard')

@section('content')

<public-order-list :fetch-data-url="'{{route('public.orders.list',['countryId'=>$countryId])}}'" inline-template>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                       Public orders list
                    </h3>
                </div>

                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <a type="button" class="btn btn-primary" href="{{\Illuminate\Support\Facades\URL::previous()}}">
                            Back
                        </a>
                    </div>
                </div>

            </div>
            <div class="kt-portlet__body">


                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                    <div class="row">
                        <div class="col-sm-12">
                            <table-component
                                :data="fetchData"
                                :table-class="'table table-hover kt-datatable__table'"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                sort-by="created_at"
                                {{--                                         :sort-order="name_en"--}}
                                ref="categoriesTable"
                            >
                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                    </template>
                                </table-column>
                                <table-column label="Order No." >
                                    <template scope="order">
                                        <a :href="'/publicOrder/view/'+order.id">
                                            @{{ order.invoice_number }}
                                        </a>
                                    </template>
                                </table-column>
                                <table-column label="Client" :sortable="false" :filterable="false">
                                    <template scope="order">
                                        <div class="kt-widget-4">
                                            <div class="kt-widget-4__item">
                                                <div class="kt-widget-4__item-content">
                                                    <div class="kt-widget-4__item-section" v-if="order.client">
{{--                                                        <div class="kt-widget-4__item-pic">--}}
{{--                                                            <img class=""  :src="order.client.avatar_url"  alt="Avatar">--}}
{{--                                                        </div>--}}
                                                        <div class="kt-widget-4__item-info">
                                                            <a href="#"
                                                               class="kt-widget-4__item-username">
                                                                @{{ order.client.name }}
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </template>
                                </table-column>
                                <table-column label="Store name"  show="store_name"></table-column>
                                <table-column label="Driver" :sortable="false" :filterable="false">
                                    <template scope="order">
                                        <div class="kt-widget-4" v-if="order.driver">
                                            <div class="kt-widget-4__item">
                                                <div class="kt-widget-4__item-content"  v-if="order.driver">
                                                    <div class="kt-widget-4__item-section">
{{--                                                        <div class="kt-widget-4__item-pic">--}}
{{--                                                            <img class=""  :src="order.driver.avatar_url"  alt="Avatar">--}}
{{--                                                        </div>--}}
                                                        <div class="kt-widget-4__item-info">
                                                            <a href="#"
                                                               class="kt-widget-4__item-username">
                                                                @{{ order.driver.name }}
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </template>
                                </table-column>

                                <table-column label="Delivery cost" show="delivery_cost"></table-column>
                                <table-column label="Tax" show="tax"></table-column>
                                <table-column label="Invoice value" show="purchase_invoice_value"></table-column>
                                <table-column label="Status" >
                                    <template scope="order">
                                        <span class="badge badge-secondary">@{{ order.status_translation }}</span>
                                    </template>
                                </table-column>
                                <table-column label="Cancel reason" >
                                    <template scope="order">

                                       <span v-if="order.cancel_reasons_id">
                                           @{{ order.cancel_reason.reason_en }}
                                       </span>
                                        <span v-else-if="order.status =='cancelled'">
                                           No driver pick it.
                                       </span>

                                    </template>
                                </table-column>
                                <table-column label="City" show="client.city_name"></table-column>

                                <table-column label="Is accounted" >
                                    <template scope="order">
                                        <span class="badge badge-success" v-if="order.accounted_with_driver">
                                           True
                                        </span>
                                        <span class="badge badge-secondary" v-else>
                                           false
                                        </span>
                                    </template>
                                </table-column>

                                <table-column label="Created at" show="created_at"></table-column>
                            </table-component>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </public-order-list>

@endsection
