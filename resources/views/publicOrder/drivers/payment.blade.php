@extends('layouts.dashboard')

@section('content')

    <delivery-drivers-public-order-payments
        :fetch-data-url="'{{route('driver.orders.wallet.transaction',$driver)}}'"
        :driver-id="'{!! $driver !!}'" inline-template>

        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                       Driver wallet
                    </h3>
                </div>

                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <a type="button" class="btn btn-primary" href="{{\Illuminate\Support\Facades\URL::previous()}}">
                            Back
                        </a>
                        &nbsp;
                        <button type="button" class="btn btn-primary" @click="caseTransactionDialog=true">
                            Add new transaction
                        </button>
                    </div>
                </div>

            </div>
            <div class="kt-portlet__body">
                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <table-component
                                :data="fetchData"
                                :table-class="'table table-hover kt-datatable__table'"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                :show-filter="false"
                                sort-by="created_at"
                                ref="transactionTable"
                            >
                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                    </template>
                                </table-column>
                                <table-column label="Invoice No." show="invoice_no" ></table-column>
                                <table-column label="Previous wallet" show="prev_wallet" ></table-column>
                                <table-column label="Amount" show="amount" ></table-column>
                                <table-column label="Current wallet" show="next_wallet" ></table-column>
                                <table-column label="Type" show="type" ></table-column>
                                <table-column label="Created at" show="created_at" ></table-column>


                            </table-component>
                        </div>
                    </div>

                </div>


            </div>

            <el-dialog

                :title="'Add transaction'"
                :visible.sync="caseTransactionDialog"
                :close-on-press-escape="false"
                :close-on-click-modal="false"
                :center="true"
                @close="clearTransactionModalData"
                append-to-body>




                <div class="form-group  row">
                    <label for="example-text-input" class="col-3 col-form-label">Invoice No. </label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder=""
                               v-model="invoice_no" @keydown="onlyNumberKe(this)">
                        <span v-if="form.error && form.validations.invoice_no"
                              class="help-block invalid-feedback">@{{ form.validations.invoice_no[0] }}</span>
                    </div>
                </div>
                <div class="form-group  row">
                    <label for="example-text-input" class="col-3 col-form-label">Amount</label>
                    <div class="col-8">
                        <input type="text" class="form-control" placeholder="Transaction amount" min="0"
                               v-model="amount" @keydown="onlyNumberKey(this)">
                        <span v-if="form.error && form.validations.transactionAmount"
                              class="help-block invalid-feedback">@{{ form.validations.transactionAmount[0] }}</span>
                    </div>
                </div>

                <div class=" row">
                    <div class=" col-12 el-col-xs-12  col-sm-12" >
                        <div class="form-group  row">
                            <label for="example-text-input" class="col-3 col-form-label">Payment type</label>
                            <div class="col-8">
                                <select v-model="type" class="form-control">
                                    <option value="deposit">Deposite</option>
                                    <option value="withdraw">Withdraw</option>

                                </select>
                                <span v-if="form.error && form.validations.type"
                                      class="help-block invalid-feedback">@{{ form.validations.type[0] }}
                            </span>

                            </div>
                        </div>
                    </div>
                </div>
                <div slot="footer" class="dialog-footer text-right" >
                    <el-button class="btn btn-primary" class="addUser" @click="addNewTranaction"
                               :disabled="form.disabled">
                        Add transaction
                    </el-button>

                    <el-button @click="caseTransactionDialog = false">
                        Cancel
                    </el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
            </el-dialog>

        </div>
    </delivery-drivers-public-order-payments>

@endsection
