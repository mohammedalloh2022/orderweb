@extends('layouts.dashboard')

@section('content')

    <delivery-drivers-public-order-list :fetch-data-url="'{{route('driver.orders.search',$driver)}}'" inline-template>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Drivers orders list
                    </h3>
                </div>

                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <a type="button" class="btn btn-primary" href="{{\Illuminate\Support\Facades\URL::previous()}}">
                            Back
                        </a>
                    </div>
                </div>

            </div>
            <div class="kt-portlet__body">
                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <table-component
                                :data="fetchData"
                                :table-class="'table table-hover kt-datatable__table'"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                sort-by="created_at"
                                ref="categoriesTable"
                            >
                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                    </template>
                                </table-column>
                                <table-column label="Order No." >
                                    <template scope="order">
                                        <a :href="'/publicOrder/view/'+order.id">
                                            @{{ order.invoice_number }}
                                        </a>
                                    </template>
                                </table-column>

                                <table-column label="Delivery cost" show="delivery_cost" ></table-column>
                                <table-column label="Delivery tax" show="tax" ></table-column>
                                <table-column label="purchase value " show="purchase_invoice_value" ></table-column>
                                <table-column label="order amount" show="total" ></table-column>
                                <table-column label="Driver revenue" show="driver_revenue" ></table-column>
                                <table-column label="App revenue" show="app_revenue" ></table-column>
                                <table-column label="Status" >
                                    <template scope="order">
{{--                                        <span class="badge badge-warning" v-if="order.status =='pending'">@{{ order.status_translation }}</span>--}}
{{--                                        <span class="badge badge-success" v-if="order.status =='delivered'">@{{ order.status_translation }}</span>--}}
                                        <span class="badge badge-secondary">@{{ order.status_translation }}</span>
                                    </template>
                                </table-column>
                                <table-column label="Cancel reason" >
                                    <template scope="order">

                                       <span v-if="order.cancel_reasons_id">
                                           @{{ order.cancel_reason.reason_en }}
                                       </span>


                                    </template>
                                </table-column>
                                <table-column label="Is accounted" >
                                    <template scope="order">
                                        <span class="badge badge-success" v-if="order.accounted_with_driver">
                                           True
                                        </span>
                                        <span class="badge badge-secondary" v-else>
                                           false
                                        </span>
                                    </template>
                                </table-column>

                            </table-component>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </delivery-drivers-public-order-list>

@endsection
