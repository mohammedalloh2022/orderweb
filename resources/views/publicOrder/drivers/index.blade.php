@extends('layouts.dashboard')

@section('content')

    <driver-public-order-list :fetch-data-url="'{{route('driver.public.orders.search',['countryId'=>$countryId])}}'" inline-template>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Public orders list
                    </h3>
                </div>

                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <a type="button" class="btn btn-primary" href="{{\Illuminate\Support\Facades\URL::previous()}}">
                            Back
                        </a>
                    </div>
                </div>

            </div>
            <div class="kt-portlet__body">


                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                    <div class="row">
                        <div class="col-sm-12">
                            <table-component
                                :data="fetchData"
                                :table-class="'table table-hover kt-datatable__table'"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                sort-by="created_at"
                                show-filter="false"
                                {{--                                         :sort-order="name_en"--}}
                                ref="categoriesTable"
                            >
                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                    </template>
                                </table-column>
                                <table-column label="Reg No." show="reg_no" ></table-column>
                                <table-column label="Name" show="name" ></table-column>

                                <table-column label="Count public orders">
                                    <template scope="user">
                                            <a :href="'/publicOrder/driverOrdersList/'+user.id">
                                                @{{ user.public_orders_count }}
                                            </a>
                                    </template>
                                </table-column>

                                <table-column label="City" show="city_name" ></table-column>
                                <table-column label="Total orders amount" show="public_order_sum_total" ></table-column>
                                <table-column label="Driver revenue" show="driver_revenue_sum" ></table-column>
                                <table-column label="App revenue" show="app_revenue_sum" ></table-column>

                                <table-column label="Wallet"  >
                                    <template scope="user">
                                        <a :href="'/publicOrder/driverOrdersList/wallet/'+user.id">
                                            @{{ user.public_wallet_cal ? user.public_wallet_cal:0 }}
                                        </a>
                                    </template>
                                </table-column>

                            </table-component>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </driver-public-order-list>

@endsection
