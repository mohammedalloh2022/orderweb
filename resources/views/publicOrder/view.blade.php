@extends('layouts.dashboard')
@push('css')
    <link href="{{asset('assets/css/pages/invoice/invoice-v2.css')}}" rel="stylesheet" type="text/css" />

@endpush
@push('js')
    <script>
        jQuery('document').ready(function () {
            initMap()
        });
        var map=null;
        function initMap() {
            map = new google.maps.Map(document.getElementById('public-order-map'), {
                zoom: 15,
                center: {lat: {!! (float)$order->store_lat!!}, lng: {!! (float)$order->store_lng !!}},
                mapTypeId: 'terrain'
            });

        var icon = {
            url: '/imgs/storeMarker.png',
            scaledSize: new google.maps.Size(50, 50), // scaled size
        };
        var clienticon = {
            url: '/imgs/user-marker.png',
            scaledSize: new google.maps.Size(50, 50), // scaled size
        };
       {{--var myLatLng = new google.maps.LatLng({!! (float)$order->store_lat!!},{!! (float)$order->store_lng !!});--}}
        var marker  = new google.maps.Marker({
            position: {lat: {!! (float)$order->store_lat!!}, lng: {!! (float)$order->store_lng !!}},
            map: map,
            title: 'Store location!',
            draggable:false,
            icon: icon
        });

        var marker2  = new google.maps.Marker({
            position: {lat: {!! (float)$order->destination_lat!!}, lng: {!! (float)$order->destination_lng !!}},
            map: map,
            title: 'Client location!',
            draggable:false,
            icon: clienticon
        });
        }
        {{--var marker = new google.maps.Marker({--}}
        {{--    position: {lat: '{{(float)$order->store_lat}}', lng:'{{(float)$order->store_lng}}'},--}}
        {{--    map: map,--}}
        {{--    icon: icon--}}
        {{--});--}}

    </script>
@endpush


@push('js')

    <!-- Firebase App (the core Firebase SDK) is always required and must be listed first -->
{{--    <script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-app.js"></script>--}}

    <!-- If you enabled Analytics in your project, add the Firebase SDK for Analytics -->
    <script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-analytics.js"></script>

    <!-- Add Firebase products that you want to use -->
    <script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-auth.js"></script>
    <script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-firestore.js"></script>

{{--    <script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-database.js"></script><script>--}}



    </script>
    <script>



    </script>

@endpush

@section('content')
    <div class="row">
        <div class="col-lg-6 col-xl-6  order-lg-1 order-xl-1">
            <div class="kt-portlet  kt-portlet--height-fluid-half">
                <div class="kt-widget kt-widget--general-2">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Client </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">

                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit bg-white">
                        <div class="kt-widget__top bg-white">

                            <div class="kt-media kt-media--lg kt-media--circle">
                                <img src="{{$order->client->avatar_url}}" alt="image">
                            </div>
                            <div class="kt-widget__wrapper">
                                <div class="kt-widget__label">
                                    <a href="#" class="kt-widget__title">
                                        {{$order->client->name}}
                                    </a>

                                </div>
                                <div class="kt-widget__toolbar">
                                    <div class="kt-widget__links">
                                        <div class="kt-widget__cont">
                                            <div class="kt-widget__link">


                                            </div>
                                            <div class="kt-widget__link mt-1">
                                                <i class="flaticon2-phone kt-font-success mr-1"></i>
                                                <a href="#"> {{$order->client->mobile}}</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-6 col-xl-6  order-lg-1 order-xl-1">
            <div class="kt-portlet  kt-portlet--height-fluid-half">
                <div class="kt-widget kt-widget--general-2">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title">Driver </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">

                        </div>
                    </div>
                    <div class="kt-portlet__body kt-portlet__body--fit bg-white">
                        <div class="kt-widget__top bg-white">
                          @if($order->driver)
                            <div class="kt-media kt-media--lg kt-media--circle">
                                <img src="{{$order->driver->avatar_url}}" alt="image">
                            </div>
                            <div class="kt-widget__wrapper">
                                <div class="kt-widget__label">
                                    <a href="#" class="kt-widget__title">
                                        {{$order->driver->name}}
                                    </a>

                                </div>
                                <div class="kt-widget__toolbar">
                                    <div class="kt-widget__links">
                                        <div class="kt-widget__cont">
                                            <div class="kt-widget__link">


                                            </div>
                                            <div class="kt-widget__link mt-1">
                                                <i class="flaticon2-phone kt-font-success mr-1"></i>
                                                <a href="#"> {{$order->driver->mobile}}</a>
                                            </div>

                                        </div>
                                    </div>
                                </div>

                            </div>
                              @endif
                        </div>


                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-lg-5 col-xl-4 order-lg-1 order-xl-1">
            <div class="kt-portlet kt-portlet--height-fluid kt-portlet--tabs">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Invoice <small>NO.
                                {{$order->invoice_number}}
                            </small>
                            <span class="badge badge-info">
                                {{ $order->status_translation }}
                            </span>
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">

                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="tab-content">
                        <div id="kt_portlet_tabs_1_1_content" role="tabpanel" class="tab-pane fade active show">
                            <public-order-chat :order-id='{!! $order ? $order->toJson():null !!}'
                                        inline-template>
                                <div>
                                    <a href="javascript:;" @click="showChat=true" class="btn btn-circle btn-primary">
                                        <i class="la la-wechat"></i>
                                         chat
                                    </a>

                                    <el-dialog

                                        @closed="hideDialog"
                                        width="40%"
                                        :title="'Chat'"
                                        :visible.sync="showChat"

                                        append-to-body>
                                        <div>
                                            <div class="kt-blog-post">
                                                <div class="kt-blog-post__comments">
                                                    <div class=kt-blog-post__threads>



                                                        <div class="kt-blog-post__thread" v-for="item in chat">
                                                            <div class="kt-blog-post__head">

                                                            </div>
                                                            <div class="kt-blog-post__body">
                                                                <div class="kt-blog-post__top">
                                                                    <div class="kt-blog-post__author">
                                                                        <div class="kt-blog-post__label">
                                                                            <img :src="item.sender_avatar_url" class="kt-blog-post__image"
                                                                                 style="width: 50px;height: 50px; border-radius: 50%; display: inline-block">
                                                                            <span>
                                                    <span class="badge badge-secondary"></span>
                                                    </span>@{{ dataFormatter(item.time) }}
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="kt-blog-post__content">

                                                                    <div class="alert alert-solid-success alert-bold" role="alert">
                                                                        <div class="alert-text">
                                                                            @{{ item.text }}
                                                                            <br/>
                                                                            <img v-if="item.imageUrl" :src="item.imageUrl" class="kt-blog-post__image"
                                                                                 style="width: 150px;height: 150px; display: block">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div slot="footer" class="dialog-footer">
                                            <el-button @click="showChat = false">Close</el-button>
                                        </div>
                                    </el-dialog>
                                </div>
                            </public-order-chat>
                            <table class="table table-striped">
                                <tbody>
                                <tr>
                                    <th>Delivery cost</th>
                                    <td>{{$order->delivery_cost}}</td>
                                </tr>
                                <tr>
                                    <th>Tax</th>
                                    <td>{{$order->tax}}</td>
                                </tr>
                                <tr>
                                    <th>Invoice value</th>
                                    <td>{{$order->purchase_invoice_value}}</td>
                                </tr>
                                <tr>
                                    <th>Total</th>
                                    <td>{{$order->total}}</td>
                                </tr>

                                <tr>
                                    <th>Note:</th>
                                    <td>{{$order->note}}</td>
                                </tr>

                                </tbody>
                            </table>

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th colspan="2" class="text-center">
                                        Attachments
                                    </th>
                                </tr>
                                </thead>
                                <tbody>

                                @if($order->attachments)
                                    @foreach($order->attachments as $attachment)
                                    <tr>
                                        <th>File name :</th>
                                        <td><a href="{{$attachment->image_url}}" target="_blank">
                                                {{$attachment->name}}
                                            </a> </td>
                                    </tr>
                                @endforeach
                                @endif


                                </tbody>
                            </table>
                        </div>
                        </div>
                </div>
            </div>
        </div>


        <div class="col-lg-7 col-xl-8 order-lg-2 order-xl-1">

            <!--begin::Portlet-->
            <div class="kt-portlet kt-widget-14">
                <div class="kt-portlet__body">
                    @if($order->status=='in_the_way_to_client' || $order->status=='in_the_way_to_store' )
                        <public-order-view-map :order='{!! $order->toJson() !!}' inline-template>
                            <div>
                                <div id="orderMap" style="height: 500px">


                                </div>
                            </div>

                        </public-order-view-map>
                    @else
                    <div id="public-order-map" style="height: 500px">

                    </div>
                    <@endif
                </div>
            </div>
        </div>
    </div>

@endsection
