@extends('layouts.dashboard')

@section('content')

    <send-mobile-notification
        :fetch-data-url="'{{route('manage.mobile.notification.search',['countryId'=>$countryId])}}'"
        :country-id="{{$countryId}}"
         inline-template>

        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                       Send notifications
                    </h3>
                </div>

                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <a type="button" class="btn btn-secondary" href="{{\Illuminate\Support\Facades\URL::previous()}}">
                            Back
                        </a>
                        &nbsp;
                        <button type="button" class="btn btn-primary" @click="caseTransactionDialog=true">
                            Send new notification
                        </button>
                    </div>
                </div>

            </div>
            <div class="kt-portlet__body">
                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <table-component
                                :data="fetchData"
                                :table-class="'table table-hover kt-datatable__table'"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                :show-filter="false"
                                sort-by="created_at"
                                ref="transactionTable"
                            >
                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                    </template>
                                </table-column>
                                <table-column label="Message" show="message" ></table-column>
                                <table-column label="Sent to"  >
                                    <template scope="notify">
                                        <span class="badge badge-primary" v-if="notify.sent_to=='all'">All</span>
                                        <span class="badge badge-primary" v-if="notify.sent_to=='all_clients'">All clients</span>
                                        <span class="badge badge-primary" v-if="notify.sent_to=='all_drivers'">All drivers</span>
                                        <span class="badge badge-primary" v-if="notify.sent_to=='custom_clients'">Custom clients</span>
                                        <span class="badge badge-primary" v-if="notify.sent_to=='custom_drivers'">Custom drivers</span>
                                    </template>
                                        </table-column>

                            </table-component>
                        </div>
                    </div>

                </div>


            </div>

            <el-dialog

                :title="'Send notification'"
                :visible.sync="caseTransactionDialog"
                :close-on-press-escape="false"
                :close-on-click-modal="false"
                :center="true"
                @close="clearTransactionModalData"
                append-to-body>

                <div class=" row">
                    <div class=" col-12 el-col-xs-12  col-sm-12" >
                        <div class="form-group  row">
                            <label for="example-text-input" class="col-3 col-form-label">Send to</label>
                            <div class="col-8">
                                <select v-model="sent_to" class="form-control" @change="resetSelected">
                                    <option value="all" selected>All</option>
                                    <option value="all_clients">All clients</option>
                                    <option value="all_drivers">All drivers</option>
                                    <option value="custom_clients">Custom clients</option>
                                    <option value="custom_drivers">Custom drivers</option>

                                </select>
                                <span v-if="form.error && form.validations.sent_to"
                                      class="help-block invalid-feedback">@{{ form.validations.sent_to[0] }}
                            </span>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group  row" v-if="sent_to=='custom_clients'">
                    <label for="example-text-input" class="col-3 col-form-label">Clients list</label>
                    <div class="col-8">
                        <multiselect
                            :multiple="true"
                            v-model="custom_clients"
                            :options="all_clients"
                            label="name"
                            track-by="name"

                        ></multiselect>
                    </div>
                </div>

                <div class="form-group  row" v-if="sent_to=='custom_drivers'">
                    <label for="example-text-input" class="col-3 col-form-label">Drivers list</label>
                    <div class="col-8">
                        <multiselect
                            :multiple="true"
                            v-model="custom_drivers"
                            :options="all_drivers"
                            label="name"
                            track-by="name"

                        ></multiselect>
                    </div>
                </div>
                <div class="form-group  row">
                    <label for="example-text-input" class="col-3 col-form-label">Message</label>
                    <div class="col-8">
                        <textarea type="text" class="form-control" placeholder="Notification message" rows="10"
                                  v-model="message" ></textarea>
                        <span v-if="form.error && form.validations.message"
                              class="help-block invalid-feedback">@{{ form.validations.message[0] }}</span>
                    </div>
                </div>


                <div slot="footer" class="dialog-footer text-right" >
                    <el-button class="btn btn-primary" class="addUser" @click="sendNotification"
                               :disabled="form.disabled">
                        Send
                    </el-button>

                    <el-button @click="caseTransactionDialog = false">
                        Cancel
                    </el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
            </el-dialog>

        </div>
    </send-mobile-notification>

@endsection
