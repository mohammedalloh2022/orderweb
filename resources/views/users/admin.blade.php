@extends('layouts.dashboard')

@push('css')

@endpush
@section('content')

    <delivery-users-list :fetch-data-url="'{{route('mange.admin.search')}}'"
                         :role="'admin'"
                         inline-template>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Admin users
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <button type="button" @click="innerVisible = true" class="btn btn-primary">
                            <i class="flaticon2-add-1"></i>
                            Add new admin
                        </button>


                    </div>
                </div>


            </div>
            <div class="kt-portlet__body">


                <table class="row">

                </table>
                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                    <div class="row">
                        <div class="col-sm-12">
                            <table-component
                                :data="fetchData"
                                :table-class="'table table-hover kt-datatable__table'"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                sort-by="created_at"
                                {{--                                         :sort-order="name_en"--}}
                                ref="adminTable"
                            >

                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="user">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(user)+1}}
                                    </template>
                                </table-column>
                                <table-column label="Reg no." show="reg_no"></table-column>
                                <table-column label="Driver name" :sortable="false" :filterable="false">
                                    <template scope="user">
                                        <div class="kt-widget-4">
                                            <div class="kt-widget-4__item">
                                                <div class="kt-widget-4__item-content">
                                                    <div class="kt-widget-4__item-section">
                                                        <div class="kt-widget-4__item-pic">
                                                            <img class=""  :src="user.avatar_url"  alt="Avatar">
                                                        </div>
                                                        <div class="kt-widget-4__item-info">
                                                            <a href="#"
                                                               class="kt-widget-4__item-username">
                                                                @{{ user.name }}
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </template>
                                </table-column>
                                {{----}}
                                <table-column label="email" show="email"></table-column>
                                <table-column label="Mobile" show="mobile"></table-column>
                                <table-column label="Active ">
                                    <template scope="user">
                                        <toggle-button
                                            :value="user.active==1?true:false"
                                            color="#82C7EB"
                                            :sync="true"
                                            @change="onChangeEventToggleHandler($event,'{!! route('mange.user.change.status') !!}',user.id)"
                                        />
                                    </template>
                                </table-column>
                                {{--                                <table-column label="delivered orders count" ></table-column>--}}
                                {{--                                <table-column label="wallet" show="wallet" ></table-column>--}}
                                <table-column label="created at" show="created_at" ></table-column>
                                <table-column label="actions" :sortable="false" :filterable="false">
                                    <template scope="user">

                                        <a class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                          href="javascript:;" @click="handelViewUser(user)">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                        <a class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                          href="javascript:;" @click="handelEditUser(user)">
                                            <i class="fa fa-user-edit" aria-hidden="true"></i>
                                        </a>
                                        <a class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                           @click="deleteAction('/manage/user/',user.id,true)" href="javascript:;">
                                            <i class="fa fa-trash-alt" aria-hidden="true" style="color:red;"></i>
                                        </a>
                                    </template>
                                </table-column>



                            </table-component>
                        </div>
                    </div>

                </div>



            </div>
            <el-dialog

                @closed="clearModelData"
                width="40%"
                :title="id?'Update admin details':'Add new admin'"
                :visible.sync="innerVisible"

                append-to-body>
                <form class="kt-form kt-form--label-right">
                    <div>
                        <div class="col-12 text-center">
                            {{--                                        <label for="example-text-input" class="col-4 col-form-label">Logo</label>--}}


                            <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                 id="kt_profile_avatar_3">
                                <div class="kt-avatar__holder"
                                >
                                    <img v-if='user_avatar' :src="user_avatar" alt="user avatar" style="    height: 114px;
    width: 114px;"/>
                                </div>
                                <label class="kt-avatar__upload" title="Change avatar">
                                    <i class="fa fa-pen"></i>
                                    <input type='file' name="logo" @change="onFileChange($event,'user_avatar')"
                                           accept=".png, .jpg, .jpeg"/>
                                </label>
                                <span class="clear-image" title="Cancel avatar" v-if="user_avatar && !id"
                                      @click.prevent="removeImage('user_avatar')">
                                            <i class="fa fa-times"></i>
                                </span>
                            </div>
                            <span v-if="form.error && form.validations.user_avatar"
                                  class="help-block invalid-feedback">@{{ form.validations.user_avatar[0] }}</span>
                        </div>
                        <br/>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">name</label>
                            <div class="col-9">
                                <input type="text" class="form-control" placeholder="Name"
                                       v-model="name">
                                <span v-if="form.error && form.validations.name"
                                      class="help-block invalid-feedback">@{{ form.validations.name[0] }}</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">mobile</label>
                            <div class="col-9">
                                <input type="text" class="form-control" placeholder="Mobile"
                                       v-model="mobile" @keydown="onlyNumberKey(this)">
                                <span v-if="form.error && form.validations.mobile"
                                      class="help-block invalid-feedback">@{{ form.validations.mobile[0] }}</span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">address</label>
                            <div class="col-9">
                                <input type="text" class="form-control" placeholder="Address"
                                       v-model="address">
                                <span v-if="form.error && form.validations.address"
                                      class="help-block invalid-feedback">@{{ form.validations.address[0] }}</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Email</label>
                            <div class="col-9">
                                <input type="email" class="form-control" placeholder="Email"
                                       v-model="email"  autocomplete="off">
                                <span v-if="form.error && form.validations.email"
                                      class="help-block invalid-feedback">@{{ form.validations.email[0] }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Password</label>
                            <div class="col-9">
                                <input type="password" class="form-control"
                                       v-model="password">
                                <span v-if="form.error && form.validations.password"
                                      class="help-block invalid-feedback">@{{ form.validations.password[0] }}</span>
                                <span v-if="id"
                                      class="help-block invalid-feedback">Leave password blank if u don't need change it</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Role</label>
                            <div class="col-9">
                                <select v-model="systemRoleId" class="form-control">
                                    <option v-for="role in systemsRoles" :value="role.id">
                                        @{{ role.name }}
                                    </option>
                                </select>
                                <span v-if="form.error && form.validations.systemRoleId"
                                      class="help-block invalid-feedback">@{{ form.validations.systemRoleId[0] }}
                                </span>

                            </div>
                        </div>

                        </div>


                </form>
                <div slot="footer" class="dialog-footer" v-if="!id">
                    <el-button class="btn btn-primary" class="addUser" @click="addUser"
                    :disabled="form.disabled">Create</el-button>
                    <el-button @click="innerVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
                <div slot="footer" class="dialog-footer" v-if="id">
                    <el-button class="btn btn-primary" class="updateUser" @click="addUser">Update</el-button>
                    <el-button @click="innerVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
            </el-dialog>

            <el-dialog

                @closed="clearModelData"
                width="40%"
                :title="id?'View admin details':'View admin details'"
                :visible.sync="innerViewVisible"

                append-to-body>
                <form class="kt-form kt-form--label-right">
                    <div>
                        <div class="col-12 text-center">
                            {{--                                        <label for="example-text-input" class="col-4 col-form-label">Logo</label>--}}


                            <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                 id="kt_profile_avatar_3">
                                <div class="kt-avatar__holder"
                                >
                                    <img v-if='user_avatar' :src="user_avatar" alt="user avatar" style="    height: 114px;
    width: 114px;"/>
                                </div>
{{--                                <label class="kt-avatar__upload" title="Change avatar">--}}
{{--                                    <i class="fa fa-pen"></i>--}}
{{--                                    <input type='file' name="logo" @change="onFileChange($event,'user_avatar')"--}}
{{--                                           accept=".png, .jpg, .jpeg"/>--}}
{{--                                </label>--}}

                            </div>

                        </div>
                        <br/>

                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <td>@{{ name }}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>@{{ email }}</td>
                            </tr>
                            <tr>
                                <th>Mobile</th>
                                <td>@{{ mobile }}</td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td>@{{ address }}</td>
                            </tr>
                            </tbody>
                        </table>





                    </div>


                </form>
                <div slot="footer" class="dialog-footer" v-if="!id">
                    <el-button class="btn btn-primary" class="addUser" @click="addUser"
                               :disabled="form.disabled">Create</el-button>
                    <el-button @click="innerViewVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>

            </el-dialog>

        </div>
    </delivery-users-list>


@endsection
