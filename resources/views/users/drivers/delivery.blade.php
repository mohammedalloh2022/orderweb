@extends('layouts.dashboard')

@push('css')

@endpush
@section('content')

    <delivery-users-list :fetch-data-url="'{{route('mange.delivery.search',['countryId'=>$countryId])}}'"
                         :role="'delivery_driver'"
                         :request="'old'"
                         :country-id="{{$countryId}}"
                         :update-driver-balance-url="'{{route('mange.delivery.update.balance')}}'"
                         :export-url="'{{route('mange.delivery.export.excel')}}'"
                         inline-template>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Delivery drivers
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
{{--                        <button type="button" @click="innerVisible = true" class="btn btn-primary">--}}
{{--                            <i class="flaticon2-add-1"></i>--}}
{{--                            Add new delivery driver--}}
{{--                        </button>--}}

                        <a @click="exportExcel" class="btn  btn-upper btn-success white " style="color:white; margin:0 5px">
                            <i class="fa fa-file-excel"></i> Export excel
                        </a>
                    </div>
                </div>

            </div>
            <div class="kt-portlet__body">


                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="row col-12">
                        <div class="col-4">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-3 col-form-label">Name</label>
                                <div class="col-8">
                                    <input class="form-control"  v-model="name_filter"/>

                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-3 col-form-label">Mobile</label>
                                <div class="col-8">
                                    <input class="form-control"  v-model="mobile_filter"/>

                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-3 col-form-label">Vehicle plat no</label>
                                <div class="col-8">
                                    <input class="form-control"  v-model="plat_no_filter"/>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row col-12">
                        <div class="col-4">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-3 col-form-label">Wallet</label>
                                <div class="col-8">
                                    <select class="form-control"  v-model="wallet_filter">

                                        <option value="" selected>All</option>
                                        <option value="exceed_station_wallet">Exceed 4station limit amount</option>
{{--                                        <option value="exceed_public_wallet">Exceed public limit amount</option>--}}

                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-3 col-form-label">Report</label>
                                <div class="col-8">
                                    <select class="form-control" id="exampleSelect1" v-model="report_period">
                                        <option value="">Select filter</option>
                                        <option value="today">Today</option>
                                        <option value="week">Week</option>
                                        <option value="month">Month</option>
                                        <option value="year">Year</option>
                                        <option value="custom">Custom</option>
                                    </select>

                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-3 col-form-label">City</label>
                                <div class="col-8">
                                    <select class="form-control" id="exampleSelect1" name="city_id">
                                        <option value="">Select filter</option>
                                        <option value="today">Today</option>
                                        <option value="week">Week</option>
                                        <option value="month">Month</option>
                                        <option value="year">Year</option>
                                        <option value="custom">Custom</option>
                                    </select>

                                </div>
                            </div>
                        </div>


                        <div class="col-4" v-if="report_period=='custom'">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-3 col-form-label">Period date</label>
                                <div class="col-9">
                                    <el-date-picker
                                        v-model="custom_period"
                                        type="daterange"
                                        align="right"
                                        unlink-panels
                                        range-separator="To"
                                        start-placeholder="Start date"
                                        end-placeholder="End date"
                                        value-format="yyyy-MM-dd"
                                    >
                                    </el-date-picker>

                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <button type="button" class="btn btn-primary" @click="refreshTable" style="margin-left: 5px">
                                Filter
                            </button>
                            <button type="button" class="btn btn-secondary" @click="resetFilter">
                                Reset
                            </button>
                        </div>



                    </div>
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                            <table-component
                                :data="fetchData"
                                :table-class="'table table-hover kt-datatable__table'"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                :show-filter="false"
                                sort-by="created_at"
                                {{--                                         :sort-order="name_en"--}}
                                ref="adminTable"
                            >

                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="user">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(user)+1}}
                                    </template>
                                </table-column>
                                <table-column label="Reg no." show="reg_no"></table-column>
                                <table-column label="Driver name" :sortable="false" :filterable="false">
                                    <template scope="user">
                                        <div class="kt-widget-4">
                                            <div class="kt-widget-4__item">
                                                <div class="kt-widget-4__item-content">
                                                    <div class="kt-widget-4__item-section">
                                                        <div class="kt-widget-4__item-pic">
                                                            <img class="" :src="user.avatar_url" alt="Avatar">
                                                        </div>
                                                        <div class="kt-widget-4__item-info">
                                                            <a href="#"
                                                               class="kt-widget-4__item-username">
                                                                @{{ user.name }}
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </template>
                                </table-column>
                                <table-column label="email" show="email"></table-column>
                                <table-column label="Mobile" show="mobile"></table-column>
                                <table-column label="Plate no" show="vehicle_plate"></table-column>
                                <table-column label="City" show="city_name"></table-column>
                                <table-column label="Rate">
                                    <template scope="user">
                                        <a :href="'/manage/delivery/ratings/'+user.id">
                                            <star-rating read-only
                                                         :rating="user.rate"
                                                         :round-start-rating="false">
                                            </star-rating>
                                        </a>

                                    </template>
                                </table-column>
                                <table-column label="Order balance">
                                    <template scope="user">

                                        <a href="javascript:;" @click="handelUpdateUserBalance(user)">
                                            @{{ user.orders_balance??0 }}
                                        </a>

                                    </template>
                                </table-column>
                                <table-column label="Orders">
                                    <template scope="user">

                                        <a :href="'/manage/orders/'+user.country_id+'?driver_id='+user.id">
                                            @{{ user.order_count }}
                                        </a>

                                    </template>
                                </table-column>
                                <table-column label="Wallet">
                                    <template scope="user">

                                        <a :href="'/manage/delivery/driver/wallet/'+user.id">
                                            @{{ user.wallet??0 }}
                                        </a>

                                    </template>
                                </table-column>

                                <table-column label="Active ">
                                    <template scope="user">
                                        <toggle-button
                                            :value="user.active==1?true:false"
                                            color="#82C7EB"
                                            :sync="true"
                                            @change="onChangeEventToggleHandler($event,'{!! route('mange.user.change.status') !!}',user.id)"
                                        />
                                    </template>
                                </table-column>
                                {{--                                <table-column label="delivered orders count" ></table-column>--}}
                                {{--                                <table-column label="wallet" show="wallet" ></table-column>--}}

                                <table-column label="Register at" show="created_at"></table-column>
                                <table-column label="actions" :sortable="false" :filterable="false">
                                    <template scope="user">

                                        <a class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                           href="javascript:;" @click="handelViewUser(user)">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                        <a class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                           href="javascript:;" @click="handelEditUser(user)">
                                            <i class="fa fa-user-edit" aria-hidden="true"></i>
                                        </a>
                                        <a class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                           @click="deleteAction('/manage/user/',user.id,true)" href="javascript:;">
                                            <i class="fa fa-trash-alt" aria-hidden="true" style="color:red;"></i>
                                        </a>
                                    </template>
                                </table-column>


                            </table-component>
                        </div>
                    </div>

                </div>


            </div>

            <el-dialog

                @closed="clearUpadteBalanceModelData"
                width="40%"
                :title="'Update driver order balance'"
                :visible.sync="showUpdateBalanceModal"

                append-to-body>
                <form class="kt-form kt-form--label-right">
                    <div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Orders balance</label>
                            <div class="col-9">
                                <input type="number" class="form-control" placeholder="Orders balance"
                                       v-model="order_balance">
                                <span v-if="form.error && form.validations.order_balance"
                                      class="help-block invalid-feedback">@{{ form.validations.order_balance[0] }}</span>
                            </div>
                        </div>

                    </div>



                </form>
                <div slot="footer" class="dialog-footer">
                    <el-button class="btn btn-primary" class="addUser" @click="updateUserBalance"
                               :disabled="form.disabled">Update new balance
                    </el-button>
                    <el-button @click="clearUpadteBalanceModelData"  >Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
            </el-dialog>

            <el-dialog

                @closed="clearModelData"
                width="40%"
                :title="id?'Update driver details':'Add new driver'"
                :visible.sync="innerVisible"

                append-to-body>
                <form class="kt-form kt-form--label-right">
                    <div>
                        <div class="col-12 text-center">
                            {{--                                        <label for="example-text-input" class="col-4 col-form-label">Logo</label>--}}


                            <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                 id="kt_profile_avatar_3">
                                <div class="kt-avatar__holder"
                                >
                                    <img v-if='user_avatar' :src="user_avatar" alt="user avatar" style="    height: 114px;
    width: 114px;"/>
                                </div>
                                <label class="kt-avatar__upload" title="Change avatar">
                                    <i class="fa fa-pen"></i>
                                    <input type='file' name="logo" @change="onFileChange($event,'user_avatar')"
                                           accept=".png, .jpg, .jpeg"/>
                                </label>
                                <span class="clear-image" title="Cancel avatar" v-if="user_avatar && !id"
                                      @click.prevent="removeImage('user_avatar')">
                                            <i class="fa fa-times"></i>
                                </span>
                            </div>
                            <span v-if="form.error && form.validations.user_avatar"
                                  class="help-block invalid-feedback">@{{ form.validations.user_avatar[0] }}</span>
                        </div>
                        <br/>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">name</label>
                            <div class="col-9">
                                <input type="text" class="form-control" placeholder="Name"
                                       v-model="name">
                                <span v-if="form.error && form.validations.name"
                                      class="help-block invalid-feedback">@{{ form.validations.name[0] }}</span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">mobile</label>
                            <div class="col-9">
                                <input type="text" class="form-control" placeholder="Mobile"
                                       v-model="mobile" @keydown="onlyNumberKey(this)">
                                <span v-if="form.error && form.validations.mobile"
                                      class="help-block invalid-feedback">@{{ form.validations.mobile[0] }}</span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">address</label>
                            <div class="col-9">
                                <input type="text" class="form-control" placeholder="Address"
                                       v-model="address">
                                <span v-if="form.error && form.validations.address"
                                      class="help-block invalid-feedback">@{{ form.validations.address[0] }}</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Email</label>
                            <div class="col-9">
                                <input type="email" class="form-control" placeholder="Email"
                                       v-model="email"  autocomplete="off">
                                <span v-if="form.error && form.validations.email"
                                      class="help-block invalid-feedback">@{{ form.validations.email[0] }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Password</label>
                            <div class="col-9">
                                <input type="password" class="form-control"
                                       v-model="password">
                                <span v-if="form.error && form.validations.password"
                                      class="help-block invalid-feedback">@{{ form.validations.password[0] }}</span>
                                <span v-if="id"
                                      class="help-block invalid-feedback">Leave password blank if u don't need change it</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Vehicle type</label>
                            <div class="col-9">
                                <input type="text" class="form-control"
                                       v-model="vehicle_type">
                                <span v-if="form.error && form.validations.vehicle_type"
                                      class="help-block invalid-feedback">@{{ form.validations.vehicle_type[0] }}</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Vehicle plate</label>
                            <div class="col-9">
                                <input type="text" class="form-control"
                                       v-model="vehicle_plate">
                                <span v-if="form.error && form.validations.vehicle_plate"
                                      class="help-block invalid-feedback">@{{ form.validations.vehicle_plate[0] }}</span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Vehicle pic</label>
                            <div class="col-9">
                                <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                     id="kt_profile_avatar_3">
                                    <div class="kt-avatar__holder"
                                    >
                                        <img  :src="vehicle_pic" alt="vehicle pic" style="    height: 114px;
    width: 114px;"/>
                                    </div>
                                    <label class="kt-avatar__upload" title="Change avatar">
                                        <i class="fa fa-pen"></i>
                                        <input type='file' name="logo" @change="onFileChange($event,'vehicle_pic')"
                                               accept=".png, .jpg, .jpeg"/>
                                    </label>
                                    <span class="clear-image" title="Cancel avatar" v-if="vehicle_pic && !id"
                                          @click.prevent="removeImage('vehicle_pic')">
                                            <i class="fa fa-times"></i>
                                </span>
                                </div>
                                <span v-if="form.error && form.validations.vehicle_pic"
                                      class="help-block invalid-feedback">@{{ form.validations.vehicle_pic[0] }}</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Vehicle license pic</label>
                            <div class="col-9">
                                <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                     id="kt_profile_avatar_3">
                                    <div class="kt-avatar__holder"
                                    >
                                        <img  :src="vehicle_license_pic" alt="vehicle license pic" style="    height: 114px;
    width: 114px;"/>
                                    </div>
                                    <label class="kt-avatar__upload" title="Change avatar">
                                        <i class="fa fa-pen"></i>
                                        <input type='file' name="logo" @change="onFileChange($event,'vehicle_license_pic')"
                                               accept=".png, .jpg, .jpeg"/>
                                    </label>
                                    <span class="clear-image" title="Cancel avatar" v-if="vehicle_license_pic && !id"
                                          @click.prevent="removeImage('vehicle_license_pic')">
                                            <i class="fa fa-times"></i>
                                </span>
                                </div>
                                <span v-if="form.error && form.validations.vehicle_pic"
                                      class="help-block invalid-feedback">@{{ form.validations.vehicle_pic[0] }}</span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">License pic</label>
                            <div class="col-9">
                                <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                     id="kt_profile_avatar_3">
                                    <div class="kt-avatar__holder"
                                    >
                                        <img  :src="license_pic" alt="License  pic" style="
                                           height: 114px;width: 114px;"/>
                                    </div>
                                    <label class="kt-avatar__upload" title="Change avatar">
                                        <i class="fa fa-pen"></i>
                                        <input type='file' name="logo" @change="onFileChange($event,'license_pic')"
                                               accept=".png, .jpg, .jpeg"/>
                                    </label>
                                    <span class="clear-image" title="Cancel avatar" v-if="license_pic && !id"
                                          @click.prevent="removeImage('license_pic')">
                                            <i class="fa fa-times"></i>
                                </span>
                                </div>
                                <span v-if="form.error && form.validations.license_pic"
                                      class="help-block invalid-feedback">@{{ form.validations.license_pic[0] }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">Insurance pic</label>
                            <div class="col-9">
                                <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                     id="kt_profile_avatar_3">
                                    <div class="kt-avatar__holder"
                                    >
                                        <img v-if='insurance_pic' :src="insurance_pic" alt="insurance_pic" style="
                                           height: 114px;width: 114px;"/>
                                    </div>
                                    <label class="kt-avatar__upload" title="Change avatar">
                                        <i class="fa fa-pen"></i>
                                        <input type='file' name="logo" @change="onFileChange($event,'insurance_pic')"
                                               accept=".png, .jpg, .jpeg"/>
                                    </label>
                                    <span class="clear-image" title="Cancel avatar" v-if="insurance_pic && !id"
                                          @click.prevent="removeImage('insurance_pic')">
                                            <i class="fa fa-times"></i>
                                </span>
                                </div>
                                <span v-if="form.error && form.validations.insurance_pic"
                                      class="help-block invalid-feedback">@{{ form.validations.insurance_pic[0] }}</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label">ID pic</label>
                            <div class="col-9">
                                <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                     id="kt_profile_avatar_3">
                                    <div class="kt-avatar__holder"
                                    >
                                        <img v-if='id_pic' :src="id_pic" alt="id_pic" style="
                                           height: 114px;width: 114px;"/>
                                    </div>
                                    <label class="kt-avatar__upload" title="Change avatar">
                                        <i class="fa fa-pen"></i>
                                        <input type='file' name="logo" @change="onFileChange($event,'id_pic')"
                                               accept=".png, .jpg, .jpeg"/>
                                    </label>
                                    <span class="clear-image" title="Cancel avatar" v-if="id_pic && !id"
                                          @click.prevent="removeImage('id_pic')">
                                            <i class="fa fa-times"></i>
                                </span>
                                </div>
                                <span v-if="form.error && form.validations.id_pic"
                                      class="help-block invalid-feedback">@{{ form.validations.id_pic[0] }}</span>
                            </div>
                        </div>


                    </div>


                </form>
                <div slot="footer" class="dialog-footer" v-if="!id">
                    <el-button class="btn btn-primary" class="addUser" @click="addUser"
                               :disabled="form.disabled">Create
                    </el-button>
                    <el-button @click="innerVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
                <div slot="footer" class="dialog-footer" v-if="id">
                    <el-button class="btn btn-primary" class="updateUser" @click="addUser">Update</el-button>
                    <el-button @click="innerVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
            </el-dialog>

            <el-dialog
                @closed="clearModelData"
                width="40%"
                :title="id?'View Delivery driver details':'View Delivery details'"
                :visible.sync="innerViewVisible"
                append-to-body>
                <form class="kt-form kt-form--label-right">
                    <div>
                        <div class="col-12 text-center">
                            {{--                                        <label for="example-text-input" class="col-4 col-form-label">Logo</label>--}}


                            <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                 id="kt_profile_avatar_3">
                                <div class="kt-avatar__holder"
                                >
                                    <img v-if='user_avatar' :src="user_avatar" alt="user avatar" style="    height: 114px;
    width: 114px;"/>
                                </div>
                                {{--                                <label class="kt-avatar__upload" title="Change avatar">--}}
                                {{--                                    <i class="fa fa-pen"></i>--}}
                                {{--                                    <input type='file' name="logo" @change="onFileChange($event,'user_avatar')"--}}
                                {{--                                           accept=".png, .jpg, .jpeg"/>--}}
                                {{--                                </label>--}}

                            </div>

                        </div>
                        <br/>

                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <td>@{{ name }}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>@{{ email }}</td>
                            </tr>
                            <tr>
                                <th>Mobile</th>
                                <td>@{{ mobile }}</td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td>@{{ address }}</td>
                            </tr>


                            <tr>
                                <th> Vehicle type</th>
                                <td>
                                    <span v-if="user ">

                                    @{{ user.vehicle_type }}
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <th> Vehicle plate</th>
                                <td>
                                    <span v-if="user">
                                      @{{ user.vehicle_plate }}
                                    </span>
                                </td>
                            </tr>


                            <tr>
                                <th>Vehicle picture</th>
                                <td>
                                    <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                         id="kt_profile_avatar_3">
                                        <div class="kt-avatar__holder"
                                        >
                                            <img v-if='user && user.vehicle_pic_url' :src="user.vehicle_pic_url"
                                                 alt="user avatar" style="    height: 114px;
    width: 114px;"/>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>Vehicle license </th>
                                <td>
                                    <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                         id="kt_profile_avatar_3">
                                        <div class="kt-avatar__holder"
                                        >
                                            <img v-if='user  && user.vehicle_license_url' :src="user.vehicle_license_url"
                                                 alt="user avatar" style="    height: 114px; width: 114px;"/>
                                        </div>
                                    </div>
                                </td>
                            </tr>
  <tr>
                                <th>Driver license </th>
                                <td>
                                    <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                         id="kt_profile_avatar_3">
                                        <div class="kt-avatar__holder"
                                        >
                                            <img v-if='user  && user.drive_license_url' :src="user.drive_license_url"
                                                 alt="user avatar" style="    height: 114px; width: 114px;"/>
                                        </div>
                                    </div>
                                </td>
                            </tr>


                            <tr>
                                <th>Insurance photo</th>
                                <td>
                                    <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                         id="kt_profile_avatar_3">
                                        <div class="kt-avatar__holder"
                                        >
                                            <img v-if='user && user.Insurance_license_url'
                                                 :src="user.Insurance_license_url"
                                                 alt="user avatar" style="    height: 114px; width: 114px;"/>
                                        </div>
                                    </div>
                                </td>
                            </tr>



                            <tr>
                                <th>ID photo</th>
                                <td>
                                    <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                         id="kt_profile_avatar_3">
                                        <div class="kt-avatar__holder"
                                        >
                                            <img v-if='user  && user.id_pic_url' :src="user.id_pic_url"
                                                 alt="user avatar" style="    height: 114px; width: 114px;"/>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            </tbody>
                        </table>


                    </div>


                </form>
                <div slot="footer" class="dialog-footer">

                    <el-button @click="innerViewVisible = false">Close</el-button>
                </div>

            </el-dialog>
        </div>
    </delivery-users-list>


@endsection
