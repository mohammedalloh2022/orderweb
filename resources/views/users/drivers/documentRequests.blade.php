@extends('layouts.dashboard')

@push('css')

@endpush
@section('content')

    <delivery-users-list :fetch-data-url="'{{route('mange.delivery.documentSearch',['countryId'=>$countryId])}}'"
                         :role="'delivery_driver'"
                         :request="'new'"

                         inline-template>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Delivery drivers documentation requests
                    </h3>
                </div>


            </div>
            <div class="kt-portlet__body">


                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                    <div class="row">
                        <div class="col-sm-12">
                            <table-component
                                :data="fetchData"
                                :table-class="'table table-hover kt-datatable__table'"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                sort-by="created_at"
                                {{--                                         :sort-order="name_en"--}}
                                ref="adminTable"
                            >

                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="user">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(user)+1}}
                                    </template>
                                </table-column>
                                <table-column label="Reg no." show="reg_no"></table-column>
                                <table-column label="Driver name" :sortable="false" :filterable="false">
                                    <template scope="user">
                                        <div class="kt-widget-4">
                                            <div class="kt-widget-4__item">
                                                <div class="kt-widget-4__item-content">
                                                    <div class="kt-widget-4__item-section">
                                                        <div class="kt-widget-4__item-pic">
                                                            <img class="" :src="user.avatar_url" alt="Avatar">
                                                        </div>
                                                        <div class="kt-widget-4__item-info">
                                                            <a href="#"
                                                               class="kt-widget-4__item-username">
                                                                @{{ user.name }}
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </template>
                                </table-column>
                                {{----}}


                                <table-column label="email" show="email"></table-column>
                                <table-column label="Mobile" show="mobile"></table-column>
                                <table-column label="City" show="city_name"></table-column>
                                <table-column label="Is documented">
                                    <template scope="user">
                                       <span v-if="user.is_documented==1">
                                            <span class="badge badge-pill badge-success">documented</span>
                                        </span>
                                        <span v-if="user.is_documented==0">
                                            <span class="badge badge-pill badge-warning">pending</span>
                                        </span>
                                    </template>
                                </table-column>
                                <table-column label="actions" :sortable="false" :filterable="false">
                                    <template scope="user">

                                        <button class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10"
                                                @click="changeStatus('{{route('mange.delivery.change.status')}}',user.id,'approve',true)"
                                        >
                                            Approve
                                        </button>
                                        <button class="btn btn-label-danger btn-bold btn-sm btn-icon-h kt-margin-l-10"
                                                @click="rejectDeliveryUser('{{route('mange.delivery.change.status')}}',user.id,'reject',true)"
                                        >
                                            Reject
                                        </button>
                                    </template>
                                </table-column>

                                {{--                                <table-column label="delivered orders count" ></table-column>--}}
                                {{--                                <table-column label="wallet" show="wallet" ></table-column>--}}
                                <table-column label="Register at" show="created_at"></table-column>
                                <table-column label="actions" :sortable="false" :filterable="false">
                                    <template scope="user">

                                        <a class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                           href="javascript:;" @click="handelViewUser(user)">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>

                                        <a class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                           @click="deleteAction('/manage/user/',user.id,true)" href="javascript:;">
                                            <i class="fa fa-trash-alt" aria-hidden="true" style="color:red;"></i>
                                        </a>
                                    </template>
                                </table-column>


                            </table-component>
                        </div>
                    </div>

                </div>


            </div>

            <el-dialog
                @closed="clearModelData"
                width="40%"
                :title="id?'View Delivery driver details':'View Delivery details'"
                :visible.sync="innerViewVisible"
                append-to-body>
                <form class="kt-form kt-form--label-right">
                    <div>
                        <div class="col-12 text-center">
                            {{--                                        <label for="example-text-input" class="col-4 col-form-label">Logo</label>--}}


                            <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                 id="kt_profile_avatar_3">
                                <div class="kt-avatar__holder"
                                >
                                    <img v-if='user_avatar' :src="user_avatar" alt="user avatar" style="    height: 114px;
    width: 114px;"/>
                                </div>
                                {{--                                <label class="kt-avatar__upload" title="Change avatar">--}}
                                {{--                                    <i class="fa fa-pen"></i>--}}
                                {{--                                    <input type='file' name="logo" @change="onFileChange($event,'user_avatar')"--}}
                                {{--                                           accept=".png, .jpg, .jpeg"/>--}}
                                {{--                                </label>--}}

                            </div>

                        </div>
                        <br/>

                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>Name</th>
                                <td>@{{ name }}</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>@{{ email }}</td>
                            </tr>
                            <tr>
                                <th>Mobile</th>
                                <td>@{{ mobile }}</td>
                            </tr>
                            <tr>
                                <th>Address</th>
                                <td>@{{ address }}</td>
                            </tr>



                            <tr>
                                <th> Vehicle type</th>
                                <td>
                                    <span v-if="user ">

                                    @{{ user.vehicle_type }}
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <th> Vehicle plate</th>
                                <td>
                                    <span v-if="user">
                                      @{{ user.vehicle_plate }}
                                    </span>
                                </td>
                            </tr>

                            <tr>
                                <th>Vehicle picture</th>
                                <td>
                                    <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                         id="kt_profile_avatar_3">
                                        <div class="kt-avatar__holder"
                                        >
                                            <img v-if='user && user.vehicle_pic_url' :src="user.vehicle_pic_url"
                                                 alt="user avatar" style="    height: 114px;
    width: 114px;"/>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>Vehicle license </th>
                                <td>
                                    <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                         id="kt_profile_avatar_3">
                                        <div class="kt-avatar__holder"
                                        >
                                            <img v-if='user  && user.vehicle_license_url' :src="user.vehicle_license_url"
                                                 alt="user avatar" style="    height: 114px; width: 114px;"/>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <th>Driver license </th>
                                <td>
                                    <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                         id="kt_profile_avatar_3">
                                        <div class="kt-avatar__holder"
                                        >
                                            <img v-if='user  && user.drive_license_url' :src="user.drive_license_url"
                                                 alt="user avatar" style="    height: 114px; width: 114px;"/>
                                        </div>
                                    </div>
                                </td>
                            </tr>


                            <tr>
                                <th>Insurance photo</th>
                                <td>
                                    <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                         id="kt_profile_avatar_3">
                                        <div class="kt-avatar__holder"
                                        >
                                            <img v-if='user && user.Insurance_license_url'
                                                 :src="user.Insurance_license_url"
                                                 alt="user avatar" style="    height: 114px; width: 114px;"/>
                                        </div>
                                    </div>
                                </td>
                            </tr>



                            <tr>
                                <th>ID photo</th>
                                <td>
                                    <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                         id="kt_profile_avatar_3">
                                        <div class="kt-avatar__holder"
                                        >
                                            <img v-if='user  && user.id_pic_url' :src="user.id_pic_url"
                                                 alt="user avatar" style="    height: 114px; width: 114px;"/>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            </tbody>
                        </table>


                    </div>


                </form>
                <div slot="footer" class="dialog-footer" >

                    <el-button @click="innerViewVisible = false">Close</el-button>
               </div>

            </el-dialog>
        </div>
    </delivery-users-list>


@endsection
