@extends('layouts.dashboard')
@section('content')
    <coupons-list :fetch-data-url="'{{route('coupons.list',['countryId'=>$countryId])}}'"
                  :country-id="{{@$countryId}}"inline-template>

        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Coupons List
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">

                        <a href="{{url()->previous()}}" class="btn btn-clean btn-bold btn-upper btn-font-sm kt-hidden">
                            <i class="la la-long-arrow-left"></i>
                            Back
                        </a>
                        {{--<button type="button" @click="innerVisible = true" class="btn btn-primary">--}}
                            {{--<i class="flaticon2-add-1"></i>--}}
                            {{--New Coupon--}}
                        {{--</button>--}}

                        <a class="btn btn-primary" href="{{route('coupons.create', ['countryId'=> $countryId])}}">
                            <i class="flaticon2-add-1"></i>
                            New Coupon

                        </a>

                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">



                <div id="kt_table_1_wrapper" class="table table-striped m-table">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">

                        </div>
                        <div class="col-sm-12 col-md-6">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table-component

                                :data="fetchData"
                                :table-class="'table table-hover '"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                sort-by="created_at"

                                {{--                                         :sort-order="name_en"--}}
                                ref="couponesTable"
                            >
                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="category">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(category)+1}}
                                    </template>
                                </table-column>

                                <table-column label="Coupon code" show="code"></table-column>
                                <table-column label="max of use" show="max_use_no"></table-column>
                                <table-column label="value" show="value"></table-column>
                                <table-column label="City" show="city_name"></table-column>

                                <table-column label="Expire date" show="expire_at"></table-column>
                                <table-column label="Count orders use it" show="count_of_use">
                                    <template scope="coupon">
                                        <a :href="'/coupons/usedCoupons/'+coupon.id">
                                            @{{ coupon.count_of_use }}
                                        </a>
                                    </template>
                                </table-column>


                                <table-column label="Active ">
                                    <template scope="coupon">
                                        <toggle-button
                                            :value="coupon.active==1?true:false"
                                            color="#82C7EB"
                                            :sync="true"
                                            @change="onChangeEventToggleHandler($event,'{!! route('coupons.change.status') !!}',coupon.id)"
                                        />
                                    </template>
                                </table-column>

                            </table-component>


                        </div>
                    </div>

                </div>

            </div>
            <el-dialog
                @close="clearCouponsModalData"
                width="30%"
                :title="id?'Update  category':'Add new coupon'"
                :visible.sync="innerVisible"
                append-to-body>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group"
                                 :class="{ 'has-error':  form.validations.cat_name_en }">
                                <label class="control-label">Coupon code</label>
                                <input type="text" class="form-control" placeholder="Coupon code"
                                       v-model="code">
                                <span v-if="form.validations.code" class="help-block invalid-feedback show">
                                    @{{ form.validations.code[0] }}</span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group"
                                 :class="{ 'has-error': form.validations.max_use_no }">
                                <label class="control-label">Max number of used</label>
                                <input type="text" class="form-control" placeholder="Max number of use"
                                       v-model="max_use_no">
                                <span  v-if="form.validations.max_use_no" class="help-block invalid-feedback show">
                                    @{{ form.validations.max_use_no[0] }}
                                </span>
                                {{--                       @{{ form.validations.cat_name_ar }}--}}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group"
                                 :class="{ 'has-error': form.validations.value }">
                                <label class="control-label">Discount value</label>
                                <input type="text" class="form-control" placeholder="value"
                                       v-model="value">
                                <span  v-if="form.validations.value" class="help-block invalid-feedback show">
                                    @{{ form.validations.value[0] }}
                                </span>
                                {{--                       @{{ form.validations.cat_name_ar }}--}}
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group"
                                 :class="{ 'has-error': form.validations.expire_date }">
                                <label class="control-label">Expire at</label>

                                <el-date-picker
                                    v-model="expire_date"
                                    type="date"
                                    placeholder="Expire date"
                                    value-format="yyyy-MM-dd">
                                </el-date-picker>

                                <span  v-if="form.validations.expire_date" class="help-block invalid-feedback show">
                                    @{{ form.validations.expire_date[0] }}
                                </span>
                                {{--                       @{{ form.validations.cat_name_ar }}--}}
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-12">


                        </div>
                    </div>

                </div>
                <div slot="footer" class="dialog-footer" v-if="!id">
                    <el-button class="btn btn-primary" class="AddCategory" @click="AddCoupon"
                               :disabled="form.disabled">Add</el-button>
                    <el-button @click="innerVisible = false">Cancel</el-button>
               </div>

            </el-dialog>

        </div>
    </coupons-list>
@endsection
