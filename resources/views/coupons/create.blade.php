@extends('layouts.dashboard')
@section('main')
    <link href="{{asset('panel/css/bootstrap-datepicker3.css')}}" rel="stylesheet" type="text/css"/>

    <div class="container-portlet mt-4">
        <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
            @php
                $item = isset($item) ? $item: null;
                $url =  isset($item)? route('coupons.update',$item->id) : route('coupons.store');
            @endphp
            {{--            {!! Form::open(['method'=>isset($item) ? 'PUT' : 'POST', 'url'=> isset($item)? route('panel.piece.update',$item->id) : route('panel.piece.store')  , 'to'=> route('panel.piece.index') ,  'class'=>'form-horizontal','id'=>'form']) !!}--}}
            <link rel="stylesheet"
                  href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
                  integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
                  crossorigin="anonymous"/>
            <style>
                div#kt_datetimepicker_2-error {
                    display: none !important;
                }
            </style>
            <form id="form" @if(isset($item)) method="PUT" @else  method="POST" @endif action="{{$url}}" to="{{route('coupons.index',$country->id)}}">
            @csrf

            <!--begin::Form-->
                <div class="row">
                    <div class="col-md-8">

                        <!--begin::Portlet-->
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        Coupon Management
                                    </h3>
                                </div>
                            </div>

                            <div class="kt-portlet__body">

                                <input name="country_id" type="hidden" value="{{$country->id}}"/>
                                <div class="form-group row">
                                    <label for="example-text-input"
                                           class="col-3 col-form-label">@lang('lang.coupon.code')</label>
                                    <div class="col-9">
                                        <input class="form-control" type="text" name="code"
                                               value="{{isset($item)?@$item->code:''}}" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input"
                                           class="col-3 col-form-label">@lang('lang.coupon.max_no_of_use')</label>
                                    <div class="col-9">
                                        <input class="form-control" type="number" name="max_use_no"
                                               value="{{isset($item)?@$item->max_use_no:''}}" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input"
                                           class="col-3 col-form-label">@lang('lang.coupon.discount_value')</label>
                                    <div class="col-9">
                                        <input class="form-control" type="number" name="value"
                                               value="{{isset($item)?@$item->value:''}}" required>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="example-text-input"
                                           class="col-3 col-form-label">@lang('lang.coupon.expire_at')</label>
                                    <div class="col-9">
                                        <div class="input-group date">
                                            <input type="text" class="form-control datetimepicker" name="expire_at"
                                                   data-date-format='yyyy-mm-dd' required
                                                   placeholder="@lang('lang.coupon.expire_at')"
                                                   id="kt_datetimepicker_2"/>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="la la-calendar-check-o glyphicon-th"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-3 col-form-label">@lang('lang.city')</label>
                                    <div class="col-9">
                                        <select class="form-control kt-selectpicker" title="@lang('lang.city')"
                                                name="city_id" required>
                                            @foreach($country->cities as $city)
                                                <option value="{{$city->id}}">{{$city->name}}</option>
                                            @endforeach
                                        </select>
                                    </div>

                                </div>


                            </div>

                            <!--end::Portlet-->
                        </div>
                    </div>
                    <div class="col-md-4">

                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        @lang('lang.actions')
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="kt-form__actions">
                                    <div class="row">
                                        <div class="col-12">
                                            <button type="submit" id="m_login_signin_submit"
                                                    class="btn btn-success w-100">
                                                @lang('lang.submit')
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>

                </div>
            </form>

        </div>
    </div>
    @push('js')
        <script src="{{asset('panel/js/sweetalert2.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('panel/js/custom.sweet.js')}}"></script>
        <script src="{{asset('panel/js/jasny-bootstrap.min.js')}}"></script>
        <script src="{{asset('panel/js/jasny.js')}}"></script>
        <script src="{{asset('panel/js/bootstrap-select.js')}}"></script>
        <script src="{{asset('panel/js/post.js')}}"></script>
        {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"--}}
        {{--integrity="sha256-5YmaxAwMjIpMrVlK84Y/+NjCpKnFYa8bWWBbUHSBGfU=" crossorigin="anonymous"></script>--}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
                integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
                crossorigin="anonymous"></script>
        <script>
            $('.datetimepicker').datepicker({
                dateFormat: 'dd-mm-yy'
            });
        </script>
    @endpush
@endsection