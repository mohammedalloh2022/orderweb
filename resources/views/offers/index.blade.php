@extends('layouts.dashboard')

@push('css')

@endpush
@section('content')

    <shop-offers-list :fetch-data-url="'{{route('mange.offers.search',['countryId'=>$countryId])}}'"

                      :request="'new'"
                       inline-template>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Offers Requests
                    </h3>
                </div>

                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">
                        <a type="button" class="btn btn-primary" href="{{\Illuminate\Support\Facades\URL::previous()}}">
                            Back
                        </a>


                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">
                <div class="row col-12">

                    <div class="col-3">
                        <div class="form-group row">
                            <label for="example-search-input" class="col-3 col-form-label">store</label>
                            <div class="col-8">
                                <multiselect
                                    :multiple="false"
                                    v-model="filter_store"
                                    :options="shopsList"
                                    label="name"
                                    track-by="name"

                                ></multiselect>
                            </div>
                        </div>
                    </div>
                    <div class="col-3">
                        <div class="form-group row">
                            <label for="example-search-input" class="col-4 col-form-label">Report</label>
                            <div class="col-8">
                                <select class="form-control" id="exampleSelect1" v-model="report_period">
                                    <option value="">Select period</option>
                                    <option value="today">Today</option>
                                    <option value="week">Week</option>
                                    <option value="month">Month</option>
                                    <option value="year">Year</option>
                                    <option value="custom">Custom</option>
                                </select>

                            </div>
                        </div>
                    </div>
                    <div class="col-4" v-if="report_period=='custom'">
                        <div class="form-group row">
                            <label for="example-search-input" class="col-4 col-form-label">Period date</label>
                            <div class="col-8">
                                <el-date-picker
                                    v-model="custom_period"
                                    type="daterange"
                                    align="right"
                                    unlink-panels
                                    range-separator="To"
                                    start-placeholder="Start date"
                                    end-placeholder="End date"
                                    value-format="yyyy-MM-dd"
                                >
                                </el-date-picker>

                            </div>
                        </div>
                    </div>

                    <div class="col-2">
                        <button type="button" class="btn btn-primary" @click="refreshTable" style="margin-left: 5px">
                            Filter
                        </button>
                        <button type="button" class="btn btn-secondary" @click="resetFilter">
                            Reset
                        </button>
                    </div>


                </div>

                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                    <div class="row">
                        <div class="col-sm-12">
                            <table-component
                                :data="fetchOffersData"
                                :table-class="'table table-hover kt-datatable__table'"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                :show-filter="false"
                                sort-by="created_at"
                                {{--                                         :sort-order="name_en"--}}
                                ref="categoriesTable"
                            >

                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                    </template>
                                </table-column>
                                <table-column label="Shop" :sortable="false" :filterable="false">
                                    <template scope="offer">
                                        <div class="kt-widget-4">
                                            <div class="kt-widget-4__item">
                                                <div class="kt-widget-4__item-content">
                                                    <div class="kt-widget-4__item-section">
                                                        <div class="kt-widget-4__item-pic">
                                                            <img class="" :src="offer.shop.logo_url" alt="shop logo">
                                                        </div>
                                                        <div class="kt-widget-4__item-info">
                                                            <a :href="'/shops/view/'+offer.shop.id"
                                                               class="kt-widget-4__item-username">
                                                                @{{ offer.shop.name}}
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </template>
                                </table-column>

                                <table-column label="Offer pic" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        <div class="kt-media kt-media--lg">
                                            <img :src="shop.image_url" width="200px">
                                        </div>

                                    </template>
                                </table-column>


                                <table-column label="Name (En)" show="name_en"></table-column>
                                <table-column label="Name (Ar)" show="name_ar"></table-column>
                                <table-column label="Desc ar" show="desc_ar"></table-column>
                                <table-column label="Desc en" show="desc_en"></table-column>
                                <table-column label="Price" show="price"></table-column>


                                <table-column label="status" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        <span class="badge badge-warning" v-if="shop.status=='pending'">Pending</span>
                                        <span class="badge badge-success" v-if="shop.status=='active'">Active</span>
                                        <span class="badge badge-danger" v-if="shop.status=='rejected'">Rejected</span>
                                    </template>
                                </table-column>
                                <table-column label="actions" :sortable="false" :filterable="false">
                                    <template scope="shop">

                                        <button class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10"
                                                @click="changeStatus('{{route('mange.offers.change.status')}}',shop.id,'active',true)"
                                                v-if="shop.status=='pending'">
                                            Accept
                                        </button>
                                        <button class="btn btn-label-warning btn-bold btn-sm btn-icon-h kt-margin-l-10"
                                                v-if="shop.status=='pending'"
                                                @click="changeStatus('{{route('mange.offers.change.status')}}',shop.id,'rejected',true)"
                                        >
                                             Reject
                                        </button>
{{--                                        <a class="btn btn-sm btn-clean btn-icon btn-icon-md"--}}
{{--                                           @click="deleteAction('/shops/offers/',shop.id,true)" href="javascript:;">--}}
{{--                                            <i class="la la-trash" aria-hidden="true" style="color:red;"></i>--}}
{{--                                        </a>--}}
                                    </template>
                                </table-column>
                            </table-component>
                        </div>
                    </div>

                </div>



            </div>
        </div>
    </shop-offers-list>


@endsection
