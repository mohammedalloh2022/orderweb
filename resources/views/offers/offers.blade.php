@extends('layouts.dashboard')

@push('css')

@endpush
@section('content')

    <shop-offers-list :fetch-data-url="'{{route('mange.offers.list.search',['countryId'=>$countryId])}}'"
                      :request="'old'"
                      :country-id="{{$countryId}}"
                      inline-template>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                       Stores Offers list
                    </h3>
                </div>


            </div>
            <div class="kt-portlet__body">
                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="row col-12">

                        <div class="col-3">
                                <div class="form-group row">
                                    <label for="example-search-input" class="col-3 col-form-label">store</label>
                                    <div class="col-8">
                                        <multiselect
                                            :multiple="false"
                                            v-model="filter_store"
                                            :options="shopsList"
                                            label="name"
                                            track-by="name"

                                        ></multiselect>
                                    </div>
                                </div>
                        </div>
                        <div class="col-3">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-4 col-form-label">Report</label>
                                <div class="col-8">
                                    <select class="form-control" id="exampleSelect1" v-model="report_period">
                                        <option value="">Select period</option>
                                        <option value="today">Today</option>
                                        <option value="week">Week</option>
                                        <option value="month">Month</option>
                                        <option value="year">Year</option>
                                        <option value="custom">Custom</option>
                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="col-4" v-if="report_period=='custom'">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-4 col-form-label">Period date</label>
                                <div class="col-8">
                                    <el-date-picker
                                        v-model="custom_period"
                                        type="daterange"
                                        align="right"
                                        unlink-panels
                                        range-separator="To"
                                        start-placeholder="Start date"
                                        end-placeholder="End date"
                                        value-format="yyyy-MM-dd"
                                    >
                                    </el-date-picker>

                                </div>
                            </div>
                        </div>

                        <div class="col-2">
                            <button type="button" class="btn btn-primary" @click="refreshTable" style="margin-left: 5px">
                                Filter
                            </button>
                            <button type="button" class="btn btn-secondary" @click="resetFilter">
                                Reset
                            </button>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table-component
                                :data="fetchOffersData"
                                :table-class="'table table-hover kt-datatable__table'"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                sort-by="created_at"
                                {{--                                         :sort-order="name_en"--}}
                                ref="categoriesTable"
                                :show-filter="false"
                            >

                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                    </template>
                                </table-column>
                                <table-column label="Offer pic" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        <div class="kt-media kt-media--lg">
                                            <img :src="shop.image_url" width="200px">
                                        </div>

                                    </template>
                                </table-column>
                                <table-column label="Offer name" show="name"></table-column>
                                <table-column label="Shop" :sortable="false" :filterable="false">

                                    <template scope="offer">
                                        <div class="kt-widget-4">
                                            <div class="kt-widget-4__item">
                                                <div class="kt-widget-4__item-content">
                                                    <div class="kt-widget-4__item-section">
                                                        <div class="kt-widget-4__item-pic">
                                                            <img class="" :src="offer.shop.logo_url" alt="shop logo">
                                                        </div>
                                                        <div class="kt-widget-4__item-info">
                                                            <a :href="'/shops/view/'+offer.shop.id"
                                                               class="kt-widget-4__item-username">
                                                                @{{ offer.shop.name}}
                                                            </a>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </template>
                                </table-column>




{{--                                <table-column label="Name (En)" show="name_en"></table-column>--}}
{{--                                <table-column label="Name (Ar)" show="name_ar"></table-column>--}}
{{--                                <table-column label="Desc ar" show="desc_ar"></table-column>--}}
{{--                                <table-column label="Desc en" show="desc_en"></table-column>--}}
                                <table-column label="Price" show="price"></table-column>

                                <table-column label="offer orders count" >
                                    <template scope="offer">
                                        <a :href="'/manage/offers/orders/'+offer.id">
                                            @{{ offer.offer_orders_count}}
                                        </a>
                                    </template>
                                </table-column>
                                <table-column label="Status" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        <span class="badge badge-warning" v-if="shop.status=='pending'">Pending</span>
                                        <span class="badge badge-success" v-if="shop.status=='active'">Active</span>
                                        <span class="badge badge-danger" v-if="shop.status=='rejected'">Rejected</span>
                                    </template>
                                </table-column>
                                <table-column label="Visibility" >
                                    <template scope="offer">
                                        <toggle-button
                                            v-if="offer.status=='active'"
                                            :value="offer.is_active==1?true:false"
                                            color="#82C7EB"
                                            :sync="true"
                                            @change="onChangeEventToggleHandler($event,'{!! route('shops.meals.change.status') !!}',offer.id)"
                                        />
                                    </template>
                                </table-column>
                                <table-column label="Created at" show="created_at"></table-column>
                                <table-column label="Action">
                                    <template scope="offer">
                                        <a class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                           href="javascript:;" @click="viewOffer(offer)">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                    </template>
                                </table-column>
                            </table-component>
                        </div>
                    </div>

                </div>



            </div>

            <el-dialog
                @closed="clearModelData"
                 width="40%"
                :title="id?'View offer':'View offer'"
                :visible.sync="innerViewVisible"

                append-to-body>
                <form class="kt-form kt-form--label-right">
                    <div>
                        <div class="col-12 text-center">
                        <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                 id="kt_profile_avatar_3">
                                <div class="kt-avatar__holder"
                                >
                                    <img v-if='offerViewObject' :src="offerViewObject.image_url" alt="Offer image" style="    height: 114px;
    width: 114px;"/>
                                </div>

                            </div>

                        </div>
                        <br/>

                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>Name (en)</th>
                                <td>@{{ offerViewObject.name_en }}</td>
                            </tr>
                            <tr>
                                <th>Name (ar)</th>
                                <td>@{{ offerViewObject.name_ar }}</td>
                            </tr>
                            <tr>
                                <th>Description (En)</th>
                                <td>@{{ offerViewObject.desc_en }}</td>
                            </tr>
                            <tr>
                                <th>Description (Ar)</th>
                                <td>@{{ offerViewObject.desc_ar }}</td>
                            </tr>
                            <tr>
                                <th>Price</th>
                                <td>@{{ offerViewObject.price }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>@{{ offerViewObject.status }}</td>
                            </tr>


{{--                            <tr>--}}
{{--                                <th>Shop name</th>--}}
{{--                                <td>@{{ offerViewObject.shop? offerViewObject.shop.name:'' }}</td>--}}
{{--                            </tr>--}}

                            <tr>
                                <th>Created at</th>
                                <td>@{{ offerViewObject.created_at }}</td>
                            </tr>
                            </tbody>
                        </table>





                    </div>


                </form>
                <div slot="footer" class="dialog-footer" >

                    <el-button @click="innerViewVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>

            </el-dialog>


        </div>
    </shop-offers-list>


@endsection
