@extends('layouts.dashboard')
@push('css')

@endpush
@push('js')


{{--    <script type="text/javascript">--}}
{{--        $(document).ready(function () {--}}
{{--            console.log('test');--}}
{{--            $('.summernote').summernote({--}}
{{--                lang: "ar-AR",--}}
{{--                tabsize: 2,--}}
{{--                height: 450,--}}
{{--                // fontName:'SSTArabic-Roman',--}}
{{--                fontsize: '14',--}}
{{--                toolbar: [--}}
{{--                    ['style', ['bold', 'italic', 'underline', 'clear']],--}}
{{--                    ['fontname', ['fontname']],--}}
{{--                    ['fontsize', ['fontsize']],--}}
{{--                    ['color', ['color']],--}}
{{--                    ['para', ['ul', 'ol', 'paragraph']],--}}
{{--                    ['height', ['height']],--}}
{{--                    ['insert', ['table', 'hr']],--}}
{{--                    ['view', ['fullscreen', 'codeview', 'help']],--}}
{{--                ],--}}
{{--                fontSizes: ['12', '14', '16', '18', '20', '22', '24', '32', '34', '36',]--}}

{{--            });--}}
{{--            var max_fields = 15;--}}
{{--            var wrapper = $("#policyAddWrap");--}}
{{--            var add_button = $("#policyAddBtn");--}}
{{--            var x = 1;--}}


{{--            jQuery("#resetCreateNewForm").click(function () {--}}
{{--                jQuery("#createNewTermForm")[0].reset();--}}
{{--                jQuery('#collapseCreateNew').collapse('toggle');--}}
{{--                jQuery('.new_desc_en').summernote('reset');--}}
{{--                jQuery('.new_desc_ar').summernote('reset');--}}
{{--            });--}}


{{--        });--}}
{{--    </script>--}}
@endpush
@section('content')
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title"> Mobile Apps privacy</h3>
                </div>
            </div>
            <form class="kt-form" method="post" action="{{route('app.content.store')}}">
                @csrf
                <div class="kt-portlet__body">
                    @if(\Illuminate\Support\Facades\Session::get('message'))
                        <div class="alert alert-primary" role="alert">
                            <div class="alert-icon">
                                <i class="flaticon2-check-mark"></i>
                            </div>
                            <div class="alert-text">
                                {{  \Illuminate\Support\Facades\Session::get('message')}}
                            </div>
                        </div>
                    @endif
                    <div class="row">
                        <div class="col-12">
                            <div class="kt-portlet__body">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#"
                                           data-target="#kt_tabs_1_1">
                                            Privacy client app</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link " data-toggle="tab" href="#"
                                           data-target="#kt_tabs_1_2">
                                            Privacy delivery app
                                        </a>
                                    </li>
                                </ul>

                                <div class="tab-content">
                                    <div class="tab-pane active" id="kt_tabs_1_1" role="tabpanel">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Title (En)</label>
                                            <input type="text"
                                                   class="form-control @error('client_privacy_title_en')) is-invalid @enderror"
                                                   placeholder="privacy client title en"
                                                   name="client_privacy_title_en"
                                                   required
                                                   value="{{$settings->client_privacy_title_en}}">
                                            @error('client_privacy_title_en')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Title (Ar)</label>
                                            <input type="text"
                                                   class="form-control @error('client_privacy_title_ar')) is-invalid @enderror"
                                                   placeholder="privacy client title en"
                                                   name="client_privacy_title_ar"
                                                   required
                                                   value="{{$settings->client_privacy_title_ar}}">
                                            @error('client_privacy_title_ar')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Content (en)</label>
                                            <textarea class="summernote form-control" name="client_privacy_content_en" rows="15">{{$settings->client_privacy_content_en}}</textarea>
                                            @error('client_privacy_content_en')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Content (ar)</label>
                                            <textarea class="summernote form-control" name="client_privacy_content_ar" rows="15">{{$settings->client_privacy_content_ar}}</textarea>
                                            @error('client_privacy_content_ar')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="tab-pane" id="kt_tabs_1_2" role="tabpanel">
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Title (En)</label>
                                            <input type="text"
                                                   class="form-control @error('driver_privacy_title_en')) is-invalid @enderror"
                                                   placeholder="privacy client title en"
                                                   name="driver_privacy_title_en"
                                                   required
                                                   value="{{$settings->driver_privacy_title_en}}">
                                            @error('driver_privacy_title_en')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Title (Ar)</label>
                                            <input type="text"
                                                   class="form-control @error('driver_privacy_title_ar')) is-invalid @enderror"
                                                   placeholder="privacy client title en"
                                                   name="driver_privacy_title_ar"
                                                   required
                                                   value="{{$settings->driver_privacy_title_ar}}">
                                            @error('driver_privacy_title_ar')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Content (En)</label>
                                            <textarea class="summernote form-control" name="driver_privacy_content_en" rows="15">{{$settings->driver_privacy_content_en}}</textarea>
                                            @error('driver_privacy_content_en')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>

                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Content (Ar)</label>
                                            <textarea class="summernote form-control" name="driver_privacy_content_ar" rows="15">{{$settings->driver_privacy_content_ar}}</textarea>
                                            @error('driver_privacy_content_ar')
                                            <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <a href="/dashboard" class="btn btn-secondary">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection
