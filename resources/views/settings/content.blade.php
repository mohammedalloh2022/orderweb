@extends('layouts.dashboard')
@push('css')
{{--    <link href="{{asset('assets/libs/summernote-bs4.css')}}" rel="stylesheet"/>--}}
@endpush
@push('js')
{{--    <script src="{{asset('assets/libs/summernote-bs4.js')}}" type="text/javascript"></script>--}}

    <script type="text/javascript">
        $(document).ready(function () {
            $('.summernote').summernote({
                lang: "ar-AR",
                tabsize: 2,
                height: 450,
                // fontName:'SSTArabic-Roman',
                fontsize: '14',
                toolbar: [
                    ['style', ['bold', 'italic', 'underline', 'clear']],
                    ['fontname', ['fontname']],
                    ['fontsize', ['fontsize']],
                    ['color', ['color']],
                    ['para', ['ul', 'ol', 'paragraph']],
                    ['height', ['height']],
                    ['insert', ['table', 'hr']],
                    ['view', ['fullscreen', 'codeview', 'help']],
                ],
                fontSizes: ['12', '14', '16', '18', '20', '22', '24', '32', '34', '36',]

            });
            var max_fields = 15;
            var wrapper = $("#policyAddWrap");
            var add_button = $("#policyAddBtn");
            var x = 1;


            jQuery("#resetCreateNewForm").click(function () {
                jQuery("#createNewTermForm")[0].reset();
                jQuery('#collapseCreateNew').collapse('toggle');
                jQuery('.new_desc_en').summernote('reset');
                jQuery('.new_desc_ar').summernote('reset');
            });


        });
    </script>
@endpush
@section('content')

    <div class="row">
        <!--begin::Portlet-->
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">Content management </h3>
                </div>
            </div>
            <!--begin::Form-->
            <form class="kt-form" method="post" action="{{route('store.app.content')}}">
                @csrf
                <div class="kt-portlet__body">
                    <input type="hidden" name="country_id" value="{{@$countryId}}"/>
                    <div class="row">
                        <div class="col-6">
                            @if(\Illuminate\Support\Facades\Session::get('message'))
                                <div class="alert alert-primary" role="alert">
                                    <div class="alert-icon">
                                        <i class="flaticon2-check-mark"></i>
                                    </div>
                                    <div class="alert-text">
                                        {{  \Illuminate\Support\Facades\Session::get('message')}}
                                    </div>
                                </div>
                            @endif
                            <div class="form-group">
                                <label for="exampleInputPassword1">Facebook link</label>
                                <input type="url" class="form-control @error('facebook_link')) is-invalid @enderror"
                                       placeholder="facebook link"
                                       name="facebook_link"
                                       value="{{@$settings?$settings->facebook_link:null}}">

                                @error('facebook_link')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Instagram link</label>
                                <input type="url" class="form-control @error('instagram_link')) is-invalid @enderror"
                                       placeholder="Instagram link"
                                       name="instagram_link"
                                       value="{{@$settings->instagram_link}}">

                                @error('instagram_link')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Linkedin link</label>
                                <input type="url" class="form-control @error('linkedin_link')) is-invalid @enderror"
                                       placeholder="Linkedin link"
                                       name="linkedin_link"
                                       value="{{@$settings->linkedin_link}}">
                                @error('linkedin_link')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>


                            <div class="form-group">
                                <label for="exampleInputPassword1">Twitter link</label>
                                <input type="url" class="form-control @error('twitter_link')) is-invalid @enderror"
                                       placeholder="Twitter link"
                                       name="twitter_link"
                                       value="{{@$settings->twitter_link}}">
                                @error('twitter_link')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Mobile</label>
                                <input type="text" class="form-control @error('mobile')) is-invalid @enderror"
                                       placeholder="4station mobile"
                                       name="mobile"
                                       required
                                       value="{{@$settings->mobile}}">
                                @error('mobile')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Address</label>
                                <input type="text" class="form-control @error('address')) is-invalid @enderror"
                                       placeholder="4station address"
                                       name="address"
                                       required
                                       value="{{@$settings->address}}">
                                @error('address')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Email</label>
                                <input type="text" class="form-control @error('email')) is-invalid @enderror"
                                       placeholder="4station email"
                                       name="email"
                                       required
                                       value="{{@$settings->email}}">
                                @error('email')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                        </div>


                    </div>


                </div>
                <div class="kt-portlet__foot">
                    <div class="kt-form__actions">
                        <button type="submit" class="btn btn-primary">Update</button>
                        <a href="/dashboard" class="btn btn-secondary">Cancel</a>
                    </div>
                </div>
            </form>
            <!--end::Form-->
        </div>

        <!--end::Portlet-->


    </div>
@endsection
