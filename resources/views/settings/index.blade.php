@extends('layouts.dashboard')
@push('js')
    <script type="text/javascript">
        function validate(evt) {
            var theEvent = evt || window.event;

            // Handle paste
            if (theEvent.type === 'paste') {
                key = event.clipboardData.getData('text/plain');
            } else {
                // Handle key press
                var key = theEvent.keyCode || theEvent.which;
                key = String.fromCharCode(key);
            }
            var regex = /[0-9]|\./;
            if( !regex.test(key) ) {
                theEvent.returnValue = false;
                if(theEvent.preventDefault) theEvent.preventDefault();
            }
        }
    </script>
@endpush
@section('content')
    <div class="row">
        <div class="col-md-6">
            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">App settings </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" method="post" action="{{route('app.settings.store')}}"  >
                    @csrf
                    <div class="kt-portlet__body">
                        @if(\Illuminate\Support\Facades\Session::get('message'))
                            <div class="alert alert-primary" role="alert">
                                <div class="alert-icon">
                                    <i class="flaticon2-check-mark"></i>
                                </div>
                                <div class="alert-text">
                                    {{  \Illuminate\Support\Facades\Session::get('message')}}
                                </div>
                            </div>
                        @endif

                        <div class="form-group">
                            <label for="exampleInputPassword1">Delivery driver wallet limit</label>
                            <input type="text" onkeypress='validate(event)'
                                   class="form-control @error('driver_wallet_limit')) is-invalid @enderror"
                                   placeholder="driver wallet limit"
                                   name="driver_wallet_limit"
{{--                                   @keypress="onlyNumberKey($event)"--}}
{{--                                   oninput="this.value = Math.abs(this.value)"--}}
                                   required
                                   value="{{$settings->driver_wallet_limit}}">

                            @error('driver_wallet_limit')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                            <input type="hidden" name="country_id" value="{{@$countryId}}"/>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Shop wallet limit</label>
                            <input type="text" onkeypress='validate(event)' class="form-control @error('shop_wallet_limit')) is-invalid @enderror"
                                   placeholder="shop wallet limit"
                                   name="shop_wallet_limit"
{{--                                   oninput="this.value = Math.abs(this.value)"--}}
                                   required
                                   value="{{$settings->shop_wallet_limit}}">

                            @error('shop_wallet_limit')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">shop distance limit km
                                    </label>
                                <input type="text" onkeypress='validate(event)' class="form-control @error('shop_distance_limit_km')) is-invalid @enderror"
                                       placeholder="shop wallet limit"
                                       name="shop_distance_limit_km"
{{--                                       oninput="this.value = Math.abs(this.value)"--}}
                                       required
                                       value="{{$settings->shop_distance_limit_km}}">

                                @error('shop_distance_limit_km')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">driver distance limit km</label>
                                <input type="text" onkeypress='validate(event)' class="form-control @error('driver_distance_limit_km')) is-invalid @enderror"
                                       placeholder="shop wallet limit"
                                       name="driver_distance_limit_km"
{{--                                       oninput="this.value = Math.abs(this.value)"--}}
                                       required
                                       value="{{$settings->driver_distance_limit_km}}">

                                @error('driver_distance_limit_km')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

{{--                        <div class="form-group">--}}
{{--                            <label for="exampleInputPassword1">Offer periods hours</label>--}}
{{--                            <input type="text" class="form-control @error('offer_periods_hr')) is-invalid @enderror"--}}
{{--                                   placeholder="Offer period per hour"--}}
{{--                                   name="offer_periods_hr"--}}
{{--                                   required--}}
{{--                                   value="{{$settings->offer_periods_hr}}">--}}
{{--                            @error('offer_periods_hr')--}}
{{--                            <div class="invalid-feedback">{{ $message }}</div>--}}
{{--                            @enderror--}}
{{--                        </div>--}}
{{--
                            <div class="form-group">
                                <label for="exampleInputPassword1">App order commission (%)</label>
                                <input type="text" class="form-control @error('app_order_commission')) is-invalid @enderror"
                                       placeholder="App commission per order"
                                       name="app_order_commission"
                                       oninput="this.value = Math.abs(this.value)"
                                       required
                                       value="{{$settings->app_order_commission}}">
                                @error('app_order_commission')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div> --}}
                            <div class="form-group">
                                <label for="exampleInputPassword1">App delivery commission (%)</label>
                                <input type="text" onkeypress='validate(event)' class="form-control @error('app_delivery_commission')) is-invalid @enderror"
                                       placeholder="App commission per order"
                                       name="app_delivery_commission"
{{--                                       oninput="this.value = Math.abs(this.value)"--}}
                                       required
                                       value="{{$settings->app_delivery_commission}}">
                                @error('app_delivery_commission')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Tax (%)</label>
                                <input type="text" onkeypress='validate(event)' class="form-control @error('Tax')) is-invalid @enderror"
                                       placeholder="App commission per order"
                                       name="tax"
{{--                                       oninput="this.value = Math.abs(this.value)"--}}
                                       required
                                       value="{{$settings->tax}}">
                                @error('tax')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Delivery cost</label>
                                <input type="text" onkeypress='validate(event)' class="form-control @error('delivery_cost')) is-invalid @enderror"
                                       placeholder="App commission per order"
                                       name="delivery_cost"
                                       required
{{--                                       oninput="this.value = Math.abs(this.value)"--}}
                                       value="{{$settings->delivery_cost}}">
                                @error('delivery_cost')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">App commission on public delivery cost</label>
                                <input type="text" onkeypress='validate(event)' class="form-control @error('app_commission_public_delivery')) is-invalid @enderror"
                                       placeholder="app commission on public delivery cost"
                                       name="app_commission_public_delivery"
{{--                                       oninput="this.value = Math.abs(this.value)"--}}
                                       required
                                       value="{{$settings->app_commission_public_delivery}}">
                                @error('app_commission_public_delivery')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="form-group">
                                <label for="exampleInputPassword1">Public delivery cost km</label>
                                <input type="text" onkeypress='validate(event)' class="form-control @error('public_delivery_cost_km')) is-invalid @enderror"
                                       placeholder="Public delivery cost per km"
                                       name="public_delivery_cost_km"
{{--                                       oninput="this.value = Math.abs(this.value)"--}}
                                       required
                                       value="{{$settings->public_delivery_cost_km}}">
                                @error('public_delivery_cost_km')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>

                            <div class="form-group">
                                <label for="exampleInputPassword1">Pay upon receipt amount limit</label>
                                <input type="text" onkeypress='validate(event)' class="form-control @error('public_delivery_cost_km')) is-invalid @enderror"
                                       placeholder="Pay upon receipt amount limit"
                                       name="client_hand_payment_limit"
{{--                                       oninput="this.value = Math.abs(this.value)"--}}
                                       required
                                       value="{{$settings->client_hand_payment_limit}}">
                                @error('client_hand_payment_limit')
                                <div class="invalid-feedback">{{ $message }}</div>
                                @enderror
                            </div>
                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-primary">Save</button>
                            <a href="/dashboard" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->


        </div>
    </div>
@endsection
