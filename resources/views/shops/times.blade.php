@extends('layouts.dashboard')

@push('css')

@endpush
@section('content')

    @php
        $business_hours = json_decode($shop->business_hours);
    @endphp
    <link href="{{asset('panel/css/bootstrap-datepicker3.css')}}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.18/jquery.timepicker.min.css"
          integrity="sha512-GgUcFJ5lgRdt/8m5A0d0qEnsoi8cDoF0d6q+RirBPtL423Qsj5cI9OxQ5hWvPi5jjvTLM/YhaaFuIeWCLi6lyQ=="
          crossorigin="anonymous" referrerpolicy="no-referrer"/>
    <div class="container-portlet mt-4">
        <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">

            <link rel="stylesheet"
                  href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
                  integrity="sha512-mSYUmp1HYZDFaVKK//63EcZq4iFWFjxSL+Z3T/aCt4IO9Cejm03q3NKKYN6pFQzY0SBOr8h+eCIAZHPXcpZaNw=="
                  crossorigin="anonymous"/>
            <style>
                div#kt_datetimepicker_2-error {
                    display: none !important;
                }
            </style>
            <form id="form" method="POST" action="{{route('shops.times.update',$shop->id)}}" to="">
            @csrf

            <!--begin::Form-->
                <div class="row">
                    <div class="col-md-8">

                        <!--begin::Portlet-->
                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        Business Hours
                                    </h3>
                                </div>
                            </div>

                            @if(session()->has('error'))
                                <div class="alert alert-danger">
                                    Please enter valid business hours
                                </div>
                            @endif

                            @if(session()->has('success'))
                                <div class="alert alert-success">
                                    Business hours Saved successfully
                                </div>
                            @endif
                            <div class="kt-portlet__body">

                                {{--<input name="country_id" type="hidden" value="{{$country->id}}"/>--}}
                                @for($i = 1 ; $i <= 7 ; $i++)
                                    <div class="form-group row">
                                        <label for="example-text-input"
                                               class="col-4 col-form-label">
                                            @switch($i)
                                                @case(1)
                                                Sat
                                                @break
                                                @case(2)
                                                Sun
                                                @break
                                                @case(3)
                                                Mon
                                                @break
                                                @case(4)
                                                Tue
                                                @break
                                                @case(5)
                                                Wed
                                                @break
                                                @case(6)
                                                Thu
                                                @break
                                                @case(7)
                                                Fri
                                                @break
                                            @endswitch


                                        </label>
                                        @php
                                            $row = isset( $business_hours->$i) ?  $business_hours->$i : null;
                                        @endphp

                                        <div class="col-4">
                                            <select class="form-control kt-selectpicker" title="Open At"
                                                    name="open[{{$i}}]" required>
                                                <option hidden value=""> Open At</option>

                                                @for($j = 1 ; $j <= 24 ; $j++)
                                                    <option value="{{$j}}" @if( isset($row->open)&& $row->open == $j) selected @endif>  {{$j}}:00
                                                    </option>
                                                @endfor
                                            </select>
                                            {{--<div class="input-group date">--}}
                                            {{--<input type="text" class="form-control datetimepicker"--}}
                                            {{--name="from[{{$i}}]"--}}
                                            {{--data-date-format='yyyy-mm-dd' required--}}
                                            {{--placeholder="Open At"--}}
                                            {{--id="kt_datetimepicker_{{$i}}"/>--}}
                                            {{--<div class="input-group-append">--}}
                                            {{--<span class="input-group-text">--}}
                                            {{--<i class="la la-calendar-check-o glyphicon-th"></i>--}}
                                            {{--</span>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                        </div>

                                        <div class="col-4">
                                            <select class="form-control kt-selectpicker" title="Close At"
                                                    name="close[{{$i}}]" required>
                                                <option hidden value=""> Close At</option>
                                                @for($j = 1 ; $j <= 24 ; $j++)
                                                    <option value="{{$j}}"
                                                            @if(isset($row->close)&& $row->close == $j) selected @endif> {{$j}}:00
                                                    </option>
                                                @endfor
                                            </select>
                                            {{--<div class="input-group date">--}}
                                            {{--<input type="text" class="form-control datetimepicker" name="to[{{$i}}]"--}}
                                            {{--data-date-format='yyyy-mm-dd' required--}}
                                            {{--placeholder="Close At"--}}
                                            {{--id="kt_datetimepicker_2"/>--}}
                                            {{--<div class="input-group-append">--}}
                                            {{--<span class="input-group-text">--}}
                                            {{--<i class="la la-calendar-check-o glyphicon-th"></i>--}}
                                            {{--</span>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                        </div>

                                    </div>

                                @endfor


                            </div>

                            <!--end::Portlet-->
                        </div>
                    </div>
                    <div class="col-md-4">

                        <div class="kt-portlet">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">
                                        @lang('lang.actions')
                                    </h3>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="kt-form__actions">
                                    <div class="row">
                                        <div class="col-12">
                                            <button type="submit" id="m_login_signin_submit"
                                                    class="btn btn-success w-100">
                                                @lang('lang.submit')
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>

                </div>
            </form>

        </div>
    </div>
    @push('js')
        <script src="{{asset('panel/js/sweetalert2.min.js')}}" type="text/javascript"></script>
        <script src="{{asset('panel/js/custom.sweet.js')}}"></script>
        <script src="{{asset('panel/js/jasny-bootstrap.min.js')}}"></script>
        <script src="{{asset('panel/js/jasny.js')}}"></script>
        <script src="{{asset('panel/js/bootstrap-select.js')}}"></script>
        <script src="{{asset('panel/js/post.js')}}"></script>
        {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"--}}
        {{--integrity="sha256-5YmaxAwMjIpMrVlK84Y/+NjCpKnFYa8bWWBbUHSBGfU=" crossorigin="anonymous"></script>--}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"
                integrity="sha512-T/tUfKSV1bihCnd+MxKD0Hm1uBBroVYBOYSk1knyvQ9VyZJpc/ALb4P0r6ubwVPSGB2GvjeoMAJJImBG12TiaQ=="
                crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.13.18/jquery.timepicker.min.js"
                integrity="sha512-WHnaxy6FscGMvbIB5EgmjW71v5BCQyz5kQTcZ5iMxann3HczLlBHH5PQk7030XmmK5siar66qzY+EJxKHZTPEQ=="
                crossorigin="anonymous" referrerpolicy="no-referrer"></script>
        {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.8.1/jquery.timepicker.min.css"></script>--}}
        {{--<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timepicker/1.8.1/jquery.timepicker.min.js"></script>--}}
        <script>
            $('.datetimepicker').timepicker();

            // $(function() {
            // });
            // $(document).ready(function () {
            //     $('.datetimepicker').timepicker();
            // });
            // $(document).ready(function(){
            //     $('.datetimepicker').timepicker({});
            // });
            // $('.datetimepicker').datepicker({
            //     dateFormat: 'dd-mm-yy'
            // });
        </script>
    @endpush
@endsection
