<shop-branches-list :fetch-data-url="'{{route('shops.branches',$shop->id)}}'"
                    :shop_id="{!! json_encode($shop->id) !!}"
                    :country-id="{!! json_encode($shop->country_id) !!}"inline-template>
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Branches list
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">

                    <a href="{{url()->previous()}}" class="btn btn-clean btn-bold btn-upper btn-font-sm kt-hidden">
                        <i class="la la-long-arrow-left"></i>
                        Back
                    </a>
                    <button type="button" @click="innerVisible = true" class="btn btn-primary">
                        <i class="flaticon2-add-1"></i>
                        New branch
                    </button>


                </div>
            </div>

        </div>
        <div class="kt-portlet__body">


            <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                <div class="row">
                    <div class="col-sm-12">
                        <table-component
                            :data="fetchData"
                            :table-class="'table table-hover kt-datatable__table'"
                            :filter-input-class="'form-control'"
                            filter-placeholder="Search "
                            :show-caption="false"
                            sort-by="created_at"
                            {{--                                         :sort-order="name_en"--}}
                            ref="categoriesTable"
                        >
                            <table-column label="#" :sortable="false" :filterable="false">
                                <template scope="shop">
                                    @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                </template>
                            </table-column>

                            <table-column label="Branch name (En)" show="name_en"></table-column>
                            <table-column label="Branch name (Ar)" show="name_ar"></table-column>

                            <table-column label="Mobile" show="mobile"></table-column>
                            <table-column label="orders ">
                                <template scope="shop">
                                    <a :href="'/shops/branches/view/'+shop.id"
                                       class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                        @{{ shop.order_count }}
                                    </a>


                                </template>
                            </table-column>
                            <table-column label="Wallet ">
                                <template scope="shop">
                                    @if(auth()->user()->role=='admin')
                                        @{{ shop.wallet ??0}}
                                    @else
                                        <a :href="'/shops/wallet/transactions/'+shop.id">
                                            @{{ shop.wallet ??0}}
                                        </a>
                                    @endif
                                </template>
                            </table-column>
                            <table-column label="Active ">
                                <template scope="shop">
                                    <toggle-button
                                        :value="shop.is_active==1?true:false"
                                        color="#82C7EB"
                                        :sync="true"
                                        @change="onChangeEventToggleHandler($event,'{!! route('shops.change.activity.status') !!}',shop.id)"
                                    />
                                </template>
                            </table-column>

{{--                            <table-column label="work at" :sortable="false" :filterable="false">--}}
{{--                                <template scope="shop">--}}
{{--                                    @{{ shop.start_work_at }} - @{{ shop.end_work_at }}--}}
{{--                                </template>--}}
{{--                            </table-column>--}}

                            <table-column label="actions" :sortable="false" :filterable="false">
                                <template scope="shop">


                                    <a class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                       href="javascript:;" @click="showEditModel(shop)">
                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                    </a>
{{--                                    |--}}
{{--                                    <a class="btn btn-sm btn-clean btn-icon btn-icon-md"--}}
{{--                                       @click="deleteAction('/shops/',shop.id,true)" href="javascript:;">--}}
{{--                                        <i class="fa fa-trash-alt" aria-hidden="true" style="color:red;"></i>--}}
{{--                                    </a>--}}
                                </template>
                            </table-column>



                        </table-component>
                    </div>
                </div>

            </div>


        </div>
        <el-dialog
            @opened="handleMap"
            @close="clearShopModalData"
            width="80%"
            :title="id?'Update shop branch details':'Add new branch'"
            :visible.sync="innerVisible"

            append-to-body>
            <form class="kt-form kt-form--label-right">


                <div class="form-body">
                    <div class="row">
                        <div class="col-7">

                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Name(En)</label>
                                <div class="col-10">
                                    <input type="text" class="form-control" placeholder="Shop name(en)"
                                           v-model="name_en">
                                    <span v-if="form.error && form.validations.name_en"
                                          class="help-block invalid-feedback">@{{ form.validations.name_en[0] }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Name(Ar)</label>
                                <div class="col-10">
                                    <input type="text" class="form-control" placeholder="Shop name(en)"
                                           v-model="name_ar">
                                    <span v-if="form.error && form.validations.name_ar"
                                          class="help-block invalid-feedback">@{{ form.validations.name_ar[0] }}</span>
                                </div>
                            </div>



                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Address(En)</label>
                                <div class="col-10">
                                    <input type="text" class="form-control" placeholder="Address (En)"
                                           v-model="address_en">
                                    <span v-if="form.error && form.validations.address_en"
                                          class="help-block invalid-feedback">@{{ form.validations.address_en[0] }}</span>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Address(Ar)</label>
                                <div class="col-10">
                                    <input type="text" class="form-control" placeholder="Address (Ar)"
                                           v-model="address_ar">
                                    <span v-if="form.error && form.validations.address_ar"
                                          class="help-block invalid-feedback">@{{ form.validations.address_ar[0] }}</span>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">mobile</label>
                                <div class="col-10">
                                    <input type="text" class="form-control" placeholder="Mobile"
                                           v-model="mobile">
                                    <span v-if="form.error && form.validations.mobile"
                                          class="help-block invalid-feedback">@{{ form.validations.mobile[0] }}</span>
                                </div>
                            </div>

                            <div class="form-group row" >
                                <label for="example-text-input" class="col-2 col-form-label">Manger Email</label>
                                <div class="col-10">
                                    <input type="email" class="form-control"
                                           placeholder="User email"
                                           v-model="user_email"  autocomplete="off">
                                    <span v-if="form.error && form.validations.user_email"
                                          class="help-block invalid-feedback">@{{ form.validations.user_email[0] }}</span>
                                </div>
                            </div>

                            <div class="form-group row" >
                                <label for="example-text-input" class="col-2 col-form-label">Manger password</label>
                                <div class="col-10">
                                    <input type="password" class="form-control" placeholder="User password"
                                           v-model="user_password">
                                    <span v-if="form.error && form.validations.user_password"
                                          class="help-block invalid-feedback">@{{ form.validations.user_password[0] }}</span>
                                </div>
                            </div>

                        </div>
                        <div class="col-5">
                            <input
                                id="pac-input"
                                class="controls"
                                type="text"
                                placeholder="Search Box"
                            />
                            <div id="mapLocation" style="height:400px"></div>
                        </div>
                    </div>


                </div>
            </form>
            <div slot="footer" class="dialog-footer" v-if="!id">
                <el-button class="btn btn-primary" class="addShop" @click="add"    :disabled="form.disabled">Create</el-button>
                <el-button @click="innerVisible = false">Cancel</el-button>
                {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
            </div>
            <div slot="footer" class="dialog-footer" v-if="id">
                <el-button class="btn btn-primary" class="updateCategory" @click="update"    :disabled="form.disabled">Update</el-button>
                <el-button @click="innerVisible = false">Cancel</el-button>
                {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
            </div>
        </el-dialog>
    </div>
</shop-branches-list>
