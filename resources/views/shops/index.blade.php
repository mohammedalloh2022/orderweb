@extends('layouts.dashboard')

@push('css')
    <style>
        .clear-image {
            border: 1px solid darkgray;
            text-align: center;
            cursor: pointer;
            align-items: center;
            justify-content: center;
            position: absolute;
            top: auto;
            right: -10px;
            bottom: -5px;
            width: 22px;
            height: 22px;
            border-radius: 50%;
            background-color: #fff;
            box-shadow: 0 0 13px 0 rgba(0, 0, 0, .1);
        }
    </style>
@endpush
@section('content')
<shops-list :fetch-data-url="'{{route('shops.search')}}'" :country-id="'{{ @$countryId?$countryId:null}}'" inline-template>
        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        shops list
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">

                        <a href="{{url()->previous()}}" class="btn btn-clean btn-bold btn-upper btn-font-sm kt-hidden">
                            <i class="la la-long-arrow-left"></i>
                            Back
                        </a>
                        <button type="button" @click="innerVisible = true" class="btn btn-primary">
                            <i class="flaticon2-add-1"></i>
                            New Store
                        </button>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">

                <div class="kt-form kt-fork--label-right kt-margin-t-20 kt-margin-b-10">
                    <div class="row align-items-center">
                        <div class="col-xl-8 order-2 order-xl-1">
                            <div class="row align-items-center">

                            </div>
                        </div>
                        <div class="col-xl-4 order-1 order-xl-2 kt-align-right">
                            <a href="#" class="btn btn-default kt-hidden">
                                <i class="la la-cart-plus"></i> New Order
                            </a>
                            <div class="kt-separator kt-separator--border-dashed kt-separator--space-lg d-xl-none"></div>
                        </div>
                    </div>
                </div>
                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="row col-12">
                        <div class="col-4">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-3 col-form-label">Name</label>
                                <div class="col-8">
                                    <input class="form-control"  v-model="name_filter"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-3 col-form-label">Mobile</label>
                                <div class="col-8">
                                    <input class="form-control"  v-model="mobile_filter"/>

                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row col-12">
                        <div class="col-4">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-3 col-form-label">Wallet</label>
                                <div class="col-8">
                                    <select class="form-control"  v-model="wallet_filter">
                                        <option value="" selected>All</option>
                                        <option value="exceed_station_wallet">Exceed limit amount</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-4">
                            <div class="form-group row">
                                <label for="example-search-input" class="col-3 col-form-label">Category</label>
                                <div class="col-8">
                                    <select class="form-control"  v-model="cat_id">
                                        <option value="">All</option>
                                        <option v-for="cat in categories_list"  :value="cat.id">
                                            @{{ cat.name }}
                                        </option>

                                    </select>

                                </div>
                            </div>
                        </div>
                        <div class="col-4">
                            <button type="button" class="btn btn-primary" @click="refreshTable" style="margin-left: 5px">
                                Filter
                            </button>
                            <button type="button" class="btn btn-secondary" @click="resetFilter">
                                Reset
                            </button>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-sm-12 table-responsive">
                            <table-component
                                 class="kt-datatable__table"
                                :data="fetchData"
                                :table-class="'table table-hover'"
                                :show-caption="false"
                                 sort-by="created_at"
                                :show-filter="false"
                                 ref="categoriesTable"
                            >
                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                    </template>
                                </table-column>
                                <table-column label="Shop" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        {{--                                    <div class="kt-media kt-media--lg">--}}
                                        {{--                                        <img :src="shop.logo_url" width="200px">--}}
                                        {{--                                    </div>--}}
                                        <div class="kt-widget-4">
                                            <div class="kt-widget-4__item">
                                                <div class="kt-widget-4__item-content">
                                                    <div class="kt-widget-4__item-section">
                                                        <div class="kt-widget-4__item-pic">
                                                            <img class="" :src="shop.logo_url" alt="shop logo">
                                                        </div>

                                                        <div class="kt-widget-4__item-info">
                                                            <a :href="'/shops/view/'+shop.id"
                                                               class="kt-widget-4__item-username">
                                                                @{{ shop.name}}
                                                            </a>
                                                            <div class="kt-widget-4__item-desc">

                                                                <star-rating read-only :rating="shop.rate"
                                                                             :round-start-rating="false">
                                                                </star-rating>

                                                                <div style="margin-top: 4px">Reg no.: @{{shop.reg_no}}
                                                                </div>
                                                                {{--                                                                <div>mobile: @{{shop.mobile}}</div>--}}
                                                                <span>@{{shop.created_at}}</span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                    </template>
                                </table-column>
                                {{--                                <table-column label="Reg. no" show="reg_no"></table-column>--}}
                                {{--                                <table-column label="Name En" show="name_en"></table-column>--}}
                                {{--                                <table-column label="Name Ar" show="name_ar"></table-column>--}}
                                <table-column label="Category">
                                    <template scope="shop">
                                       <span v-if="shop.category && shop.category.name">
                                           @{{ shop.category.name }}
                                       </span>
                                    </template>

                                </table-column>
                                <table-column label="Category">
                                    <template scope="shop">
                                       <span v-if="shop.country && shop.country.name_en">
                                           @{{ shop.country.name_en }}
                                       </span>
                                    </template>

                                </table-column>
{{--                                <table-column label="Address " show="address"></table-column>--}}
                                <table-column label="Mobile " show="mobile"></table-column>
                                <table-column label="Branchs ">
                                    <template scope="shop">
                                        <a :href="'/shops/view/'+shop.id+'#tab_branches'">
                                            @{{ shop.branch_count }}
                                        </a>
                                    </template>
                                </table-column>

                                <table-column label="Offers ">
                                    <template scope="shop">
                                        <a :href="'/shops/view/'+shop.id+'#tab_offers'">
                                            @{{ shop.offer_count }}
                                        </a>
                                    </template>
                                </table-column>


                                <table-column label="Categories ">
                                    <template scope="shop">
                                        <a :href="'/shops/view/'+shop.id+'#tab_categories'">
                                            @{{ shop.category_count }}
                                        </a>
                                    </template>
                                </table-column>

{{--                                <table-column label="meals ">--}}
{{--                                    <template scope="shop">--}}
{{--                                        <a :href="'/shops/view/'+shop.id+'#tab_meals'">--}}
{{--                                            @{{ shop.meals_count }}--}}
{{--                                        </a>--}}
{{--                                    </template>--}}
{{--                                </table-column>--}}

                                <table-column label="Orders ">
                                    <template scope="shop">
                                        <a :href="'/shops/view/'+shop.id+'#tab_orders'">
                                            @{{ shop.order_count }}
                                        </a>
                                    </template>
                                </table-column>

                                <table-column label="Wallet ">
                                    <template scope="shop">
                                        <a :href="'/shops/wallet/transactions/'+shop.id">
                                            @{{ shop.wallet.toFixed(2) ??0}}
                                        </a>
                                    </template>
                                </table-column>


{{--                                <table-column label="Active ">--}}
{{--                                    <template scope="shop">--}}
{{--                                        <span v-if="shop.is_active==1">--}}
{{--                                            <span class="badge badge-pill badge-success">active</span>--}}
{{--                                        </span>--}}
{{--                                        <span v-else>--}}
{{--                                              <span class="badge badge-pill badge-danger">inactive</span>--}}
{{--                                        </span>--}}

{{--                                    </template>--}}
{{--                                </table-column>--}}

                                <table-column label="Active ">
                                    <template scope="shop">
                                        <toggle-button
                                            :value="shop.is_active==1?true:false"
                                            color="#82C7EB"
                                            :sync="true"
                                            @change="onChangeEventToggleHandler($event,'{!! route('shops.change.activity.status') !!}',shop.id)"
                                        />
                                    </template>
                                </table-column>
                                <table-column label="work at" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        @{{ shop.start_work_at }} - @{{ shop.end_work_at }}
                                    </template>
                                </table-column>


                                <table-column label="actions" :sortable="false" :filterable="false">
                                    <template scope="shop">

                                        <a :href="'/shops/view/'+shop.id"
                                           class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                            <i class="fa fa-eye" aria-hidden="true"></i>
                                        </a>
                                        |
                                        <a class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                           href="javascript:;" @click="showEditModel(shop)">
                                            <i class="fa fa-edit" aria-hidden="true"></i>
                                        </a>

                                        <a  :href="'/shops/edit/times/'+shop.id" class="btn btn-sm btn-clean btn-icon btn-icon-md">
                                            <i class="far fa-calendar" aria-hidden="true"></i>
                                        </a>
{{--                                        |--}}
{{--                                        <a class="btn btn-sm btn-clean btn-icon btn-icon-md"--}}
{{--                                           @click="deleteAction('/shops/',shop.id,true)" href="javascript:;">--}}
{{--                                            <i class="fa fa-trash-alt" aria-hidden="true" style="color:red;"></i>--}}
{{--                                        </a>--}}
                                    </template>
                                </table-column>

                            </table-component>

                        </div>
                    </div>
                    {{--                <div class="row">--}}

                    {{--                    <div class="col-sm-12 col-md-12">--}}
                    {{--                        <div class="dataTables_paginate paging_simple_numbers" id="kt_table_1_paginate">--}}
                    {{--                            <ul class="pagination  justify-content-center">--}}
                    {{--                                {{$shops->links()}}--}}
                    {{--                            </ul>--}}
                    {{--                        </div>--}}
                    {{--                    </div>--}}
                    {{--                </div>--}}
                </div>


            </div>

            <el-dialog
                @opened="handleMap"
                @close="clearShopModalData"
                width="80%"
                :title="id?'Update shop details':'Add new shop'"
                :visible.sync="innerVisible"
                append-to-body>
                <form class="kt-form kt-form--label-right">


                    <div class="form-body">
                        <div class="row">
                            <div class="col-7">


                                <div class="form-group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Logo</label>
                                    <div class="col-10">

                                        <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                             id="kt_profile_avatar_3">
                                            <div class="kt-avatar__holder"
                                                 style="background-image: url(/uploads/shops/default.png); ">
                                                <img v-if='shop_logo' :src="shop_logo" alt="shop_log" style="    height: 114px;
    width: 114px;"/>
                                            </div>
                                            <label class="kt-avatar__upload" title="Change avatar">
                                                <i class="fa fa-pen"></i>
                                                <input type='file' name="logo" @change="onFileChange($event,'logo')"
                                                       accept=".png, .jpg, .jpeg"/>
                                            </label>
                                            <span class="clear-image" title="Cancel avatar" v-if="shop_logo && !id"
                                                  @click.prevent="removeImage('logo')">
                                            <i class="fa fa-times"></i>
                                        </span>
                                        </div>
                                        <span v-if="form.error && form.validations.shop_logo"
                                              class="help-block invalid-feedback">@{{ form.validations.shop_logo[0] }}</span>

                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Cover</label>
                                    <div class="col-10">

                                        <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                             id="kt_profile_avatar_4">
                                            <div class="kt-avatar__holder"
                                                 style="background-image: url(/uploads/shops/cover_default.png)">
                                                <img v-if='shop_cover' :src="shop_cover" alt="shop_log" style="    height: 114px;
    width: 114px;"/>
                                            </div>
                                            <label class="kt-avatar__upload" title="Change avatar">
                                                <i class="fa fa-pen" for="coverImage"></i>
                                                <input id="coverImage" type='file' name="coverImage"
                                                       v-on:change="onFileChange($event,'cover')"
                                                       accept=".png, .jpg, .jpeg"/>
                                            </label>
                                            <span class="clear-image" title="Cancel avatar" v-if="shop_cover && !id"
                                                  @click.prevent="removeImage('cover')">
                                            <i class="fa fa-times"></i>
                                        </span>
                                        </div>
                                        <span v-if="form.error && form.validations.shop_cover"
                                              class="help-block invalid-feedback">@{{ form.validations.shop_cover[0] }}</span>

                                    </div>
                                </div>
                                    <div class="form-group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Name(En)</label>
                                    <div class="col-10">
                                        <input type="text" class="form-control" placeholder="Shop name(en)"
                                               v-model="name_en">
                                        <span v-if="form.error && form.validations.name_en"
                                              class="help-block invalid-feedback">@{{ form.validations.name_en[0] }}</span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Name(Ar)</label>
                                    <div class="col-10">
                                        <input type="text" class="form-control" placeholder="Shop name(en)"
                                               v-model="name_ar">
                                        <span v-if="form.error && form.validations.name_ar"
                                              class="help-block invalid-feedback">@{{ form.validations.name_ar[0] }}</span>
                                    </div>
                                </div>
                                <div class="form-group row">

                                    <label for="example-text-input" class="col-2 col-form-label">Category</label>
                                    <div class="col-10">
                                        <select class="form-control" id="exampleSelectd" v-model="cat_id">
                                            <option v-for="cat in categories_list" :value="cat.id">
                                                @{{ cat.name }}
                                            </option>
                                        </select>
                                        <span v-if="form.error && form.validations.cat_id"
                                              class="help-block invalid-feedback">
                                               @{{ form.validations.cat_id[0] }}
                                    </span>

                                    </div>
                                </div>
                                            <input type="hidden" v-model="country_id"/>

{{--                                <div class="form-group row">--}}

{{--                                    <label for="example-text-input" class="col-2 col-form-label">Country</label>--}}
{{--                                    <div class="col-10">--}}
{{--                                        <select class="form-control" id="exampleSelectd" v-model="country_id">--}}
{{--                                            <option v-for="country in counties_list" :value="country.id">--}}
{{--                                                @{{ country.name_en }}--}}
{{--                                            </option>--}}
{{--                                        </select>--}}
{{--                                        <span v-if="form.error && form.validations.country_id"--}}
{{--                                              class="help-block invalid-feedback">--}}
{{--                                               @{{ form.validations.country_id[0] }}--}}
{{--                                    </span>--}}

{{--                                    </div>--}}
{{--                                </div>--}}





                                <div class="form-group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Address(En)</label>
                                    <div class="col-10">
                                        <input type="text" class="form-control" placeholder="Address (En)"
                                               v-model="address_en">
                                        <span v-if="form.error && form.validations.address_en"
                                              class="help-block invalid-feedback">@{{ form.validations.address_en[0] }}</span>
                                    </div>
                                </div>


                                <div class="form-group row">
                                    <label for="example-text-input" class="col-2 col-form-label">App commission</label>
                                    <div class="col-10">
                                        <input type="text" class="form-control" placeholder="App commission"
                                               v-model="app_commission">
                                        <span v-if="form.error && form.validations.app_commission"
                                              class="help-block invalid-feedback">@{{ form.validations.app_commission[0] }}</span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Address(Ar)</label>
                                    <div class="col-10">
                                        <input type="text" class="form-control" placeholder="Address (Ar)"
                                               v-model="address_ar">
                                        <span v-if="form.error && form.validations.address_ar"
                                              class="help-block invalid-feedback">@{{ form.validations.address_ar[0] }}</span>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="example-text-input" class="col-2 col-form-label">mobile</label>
                                    <div class="col-10">
                                        <input type="text" class="form-control" placeholder="Mobile"
                                               v-model="mobile">
                                        <span v-if="form.error && form.validations.mobile"
                                              class="help-block invalid-feedback">@{{ form.validations.mobile[0] }}</span>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class=" col-6">
                                        <div class="form-group row">

                                            <label for="example-text-input" class="col-4 col-form-label">Open at</label>
                                            <div class="col-8">
                                                <el-time-select
                                                    v-model="start_work_at"
                                                    :picker-options="{
                                                    start: '00:00',
                                                    step: '00:10',
                                                    end: '23:59'
                                                  }"
                                                    placeholder="Select shop open time">
                                                </el-time-select>

                                                <span v-if="form.error && form.validations.start_work_at"
                                                      class="help-block invalid-feedback">@{{ form.validations.start_work_at[0] }}</span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class=" col-6">
                                        <div class="form-group row">


                                            <label for="example-text-input" class="col-4 col-form-label">Close
                                                at</label>
                                            <div class="col-8">
                                                <el-time-select
                                                    v-model="end_work_at"
                                                    :picker-options="{
                                                    start: '00:00',
                                                    step: '00:10',
                                                    end: '23:59'
                                                  }"
                                                    placeholder="Select shop close time">
                                                </el-time-select>

                                                <span v-if="form.error && form.validations.end_work_at"
                                                      class="help-block invalid-feedback">@{{ form.validations.end_work_at[0] }}</span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" >
                                    <label for="example-text-input" class="col-2 col-form-label">Manger Email</label>
                                    <div class="col-10">
                                        <input type="email" class="form-control" placeholder="User email"
                                               v-model="user_email"  autocomplete="off">
                                        <span v-if="form.error && form.validations.user_email"
                                              class="help-block invalid-feedback">@{{ form.validations.user_email[0] }}</span>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="example-text-input" class="col-2 col-form-label">Manger password</label>
                                    <div class="col-10">
                                        <input type="password" class="form-control" placeholder="User password"
                                               v-model="user_password">
                                        <span v-if="form.error && form.validations.user_password"
                                              class="help-block invalid-feedback">@{{ form.validations.user_password[0] }}</span>
                                        <span v-if="id"
                                              class="help-block invalid-feedback">Leave password blank if you don't need change it</span>

                                    </div>
                                </div>

                            </div>
                            <div class="col-5">
                                <input
                                    id="pac-input"
                                    class="controls"
                                    type="text"
                                    placeholder="Search Box"
                                />
                                <div id="map"  ref="mapLocation" style="height:400px;width: 500px; "></div>
                            </div>
                        </div>


                    </div>
                </form>
                <div slot="footer" class="dialog-footer" v-if="!id">
                    <el-button class="btn btn-primary" class="addShop" @click="add"    :disabled="form.disabled">Create</el-button>
                    <el-button @click="innerVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
                <div slot="footer" class="dialog-footer" v-if="id">
                    <el-button class="btn btn-primary" class="updateCategory" @click="update"    :disabled="form.disabled">Update</el-button>
                    <el-button @click="innerVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
            </el-dialog>

        </div>
    </shops-list>
@endsection
