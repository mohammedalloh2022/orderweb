@extends('layouts.dashboard')

@push('css')

@endpush
@section('content')

<shop-meals-list :fetch-data-url="'{{route('shops.cat.meals',['shop'=>$shop->id,'cat'=>$cat])}}'"
                   :shop-id="'{{$shop->id}}'"
                    :cat-id="'{{$cat}}'"
                  inline-template>
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Meals list
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{\Illuminate\Support\Facades\URL::previous().'#tab_categories'}}" class=" btn btn-default">
                        Back
                    </a>

                    <button type="button" @click="innerVisible = true" class="btn btn-primary">
                        <i class="flaticon2-add-1"></i>
                        New meal
                    </button>


                </div>
            </div>
        </div>
        <div class="kt-portlet__body">


            <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                <div class="row">
                    <div class="col-sm-12">
                        <table-component
                            :data="fetchData"
                            :table-class="'table table-hover kt-datatable__table'"
                            :filter-input-class="'form-control'"
                            filter-placeholder="Search "
                            :show-caption="false"
                            sort-by="created_at"
                            {{--                                         :sort-order="name_en"--}}
                            ref="mealsTable"
                        >
                            <table-column label="#" :sortable="false" :filterable="false">
                                <template scope="shop">
                                    @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                </template>
                            </table-column>
                            <table-column label="Meal pic" :sortable="false" :filterable="false">
                                <template scope="shop">
                                    <div class="kt-media kt-media--lg">
                                        <img :src="shop.image_url" width="200px">
                                    </div>

                                </template>
                            </table-column>
                            <table-column label="Name (En)" show="name_en"></table-column>
                            <table-column label="Name (Ar)" show="name_ar"></table-column>
                            <table-column label="Desc ar" show="desc_ar"></table-column>
                            <table-column label="Desc en" show="desc_en"></table-column>
                            <table-column label="Price" show="price"></table-column>
                            <table-column label="Active ">
                                <template scope="category">
                                    <toggle-button
                                        :value="category.is_active==1?true:false"
                                        color="#82C7EB"
                                        :sync="true"
                                        @change="onChangeEventToggleHandler($event,'{!! route('shops.meals.change.status') !!}',category.id)"
                                    />
                                </template>
                            </table-column>
                            <table-column label="actions" :sortable="false" :filterable="false">
                                <template scope="shop">
                                    <a class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                       @click="showEditModel(shop)" href="javascript:;">
                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                    </a>
                                    {{--                                    |--}}
                                    {{--                                    <a class="btn btn-sm btn-clean btn-icon btn-icon-md"--}}
                                    {{--                                       @click="deleteAction('/shops/meals/',shop.id,true)" href="javascript:;">--}}
                                    {{--                                        <i class="la la-trash" aria-hidden="true" style="color:red;"></i>--}}
                                    {{--                                    </a>--}}
                                </template>
                            </table-column>


                            {{--                                        <table-column label="work at" :sortable="false" :filterable="false">--}}
                            {{--                                            <template scope="shop">--}}
                            {{--                                                @{{ shop.start_work_at }} - @{{ shop.end_work_at }}--}}
                            {{--                                            </template>--}}
                            {{--                                        </table-column>--}}



                        </table-component>
                    </div>
                </div>

            </div>


        </div>

        <el-dialog

            @close="clearMealModalData"
            width="50%"
            :title="id?'Update meal details':'Add new meal'"
            :visible.sync="innerVisible"
            :close-on-press-escape="false"
            :close-on-click-modal="false"
            append-to-body>
            <form class="kt-form kt-form--label-right">

                <div class="col-12 text-center">
                    {{--                                        <label for="example-text-input" class="col-4 col-form-label">Logo</label>--}}


                    <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                         id="kt_profile_avatar_3">
                        <div class="kt-avatar__holder"
                        >
                            <img v-if='meal_image' :src="meal_image" alt="shop_log" style="    height: 114px;
    width: 114px;"/>
                        </div>
                        <label class="kt-avatar__upload" title="Change avatar">
                            <i class="fa fa-pen"></i>
                            <input type='file' name="logo" @change="onFileChange($event,'meal_image')"
                                   accept=".png, .jpg, .jpeg"/>
                        </label>
                        <span class="clear-image" title="Cancel avatar" v-if="meal_image & !id"
                              @click.prevent="removeImage('meal_image')">
                                            <i class="fa fa-times"></i>
                            </span>
                    </div>
                    <span v-if="form.error && form.validations.meal_image"
                          class="help-block invalid-feedback">@{{ form.validations.meal_image[0] }}</span>




                </div>
                <br/>
                <div class="form-body">





                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">Name(En)</label>
                        <div class="col-9">
                            <input type="text" class="form-control" placeholder="name(en)"
                                   v-model="name_en">
                            <span v-if="form.error && form.validations.name_en"
                                  class="help-block invalid-feedback">@{{ form.validations.name_en[0] }}</span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">Name(Ar)</label>
                        <div class="col-9">
                            <input type="text" class="form-control" placeholder="name(en)"
                                   v-model="name_ar">
                            <span v-if="form.error && form.validations.name_ar"
                                  class="help-block invalid-feedback">@{{ form.validations.name_ar[0] }}</span>
                        </div>
                    </div>
                    {{--                            <div class="form-group row">--}}

                    {{--                                <label for="example-text-input" class="col-3 col-form-label">Category</label>--}}
                    {{--                                <div class="col-9">--}}
                    {{--                                    <select class="form-control" id="exampleSelectd" v-model="cat_id">--}}
                    {{--                                        <option v-for="cat in shop_categories_list" :value="cat.id">--}}
                    {{--                                            @{{ cat.name }}--}}
                    {{--                                        </option>--}}
                    {{--                                    </select>--}}
                    {{--                                    <span v-if="form.error && form.validations.cat_id"--}}
                    {{--                                          class="help-block invalid-feedback">--}}
                    {{--                                        @{{ form.validations.cat_id[0] }}--}}
                    {{--                                    </span>--}}

                    {{--                                </div>--}}
                    {{--                            </div>--}}


                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">Description(En)</label>
                        <div class="col-9">
                            <input type="text" class="form-control" placeholder="Description (En)"
                                   v-model="desc_en">
                            <span v-if="form.error && form.validations.desc_en"
                                  class="help-block invalid-feedback">@{{ form.validations.desc_en[0] }}</span>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">Description(Ar)</label>
                        <div class="col-9">
                            <input type="text" class="form-control" placeholder="Description (Ar)"
                                   v-model="desc_ar">
                            <span v-if="form.error && form.validations.desc_ar"
                                  class="help-block invalid-feedback">@{{ form.validations.desc_ar[0] }}</span>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="example-text-input" class="col-3 col-form-label">price</label>
                        <div class="col-9">
                            <input type="text" class="form-control" placeholder="price"
                                   v-model="price">
                            <span v-if="form.error && form.validations.price"
                                  class="help-block invalid-feedback">@{{ form.validations.price[0] }}</span>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-3">

                        </div>
                        <div class="col-9">
                            <button  type="button" class="btn btn-primary" @click="addExtraItem()">
                                Add extra items
                            </button>
                        </div>
                    </div>
                    <div class="row mb-1" v-for="(item,index) in extraItems">
                        <div class="col-3">

                        </div>
                        <div class="col-3">
                            <input type="text" class="form-control" placeholder="name_en" v-model="item.name_en">
                        </div>
                        <div class="col-3">
                            <input type="text" class="form-control" placeholder="name_ar" v-model="item.name_ar">
                        </div>

                        <div class="col-2">
                            <input type="text" class="form-control" placeholder="price"
                                   v-model="item.price"
                                   @keydown="onlyNumberKey(this)">
                        </div>

                        <div class="col-1" v-if="!item.id">
                            <a href="javascript:;" class="btn " @click="deleteExtraItem(item)">
                                <i class="fa fa-trash-alt" aria-hidden="true" style="color:red;"></i>
                            </a>
                        </div>

                    </div>
                    <div class="form-group  row ">
                        <label for="example-text-input" class="col-3 col-form-label">Meal images</label>
                        <div class="col-9">

                        <div class="row">


                        <label class="add-new-image-block" for="attachments">
                            <div class="form-group form-group-last" style="text-align: center">

                                <div class="image-upload " >
                                    <label for="ImagesList" class="icon-container"><span class="fa fa-plus fa-3x gray"></span></label>
                                    <input id="ImagesList" type="file" @change="onfileChangeMultiImage" multiple="multiple"
                                           accept="image/x-png,image/gif,image/jpeg,image/jpg,image/png" >
                                </div>
                            </div>

                        </label>

                        <div class="attachment-holder animated fadeIn"  v-if="imagesList.length"
                             v-for="(base64Attachment, index) in imagesList">
                            <div class="image-preview-block  "  >
                                <img v-if="base64Attachment.image_url" :src="base64Attachment.image_url" class="img-responsive review-img"/>
                                <img v-else :src="base64Attachment" class="img-responsive review-img"/>

                                <span class="" style=" cursor: pointer;" @click="deleteImage(base64Attachment)">
                                            <i class="rmove-icon la la-times-circle"> </i>
                                </span>

                            </div>
                        </div>
                        </div>
                        </div>


                    </div>

                </div>
            </form>
            <div slot="footer" class="dialog-footer" v-if="!id">
                <el-button class="btn btn-primary" class="addShop" @click="add"    :disabled="form.disabled">Add meal</el-button>
                <el-button @click="innerVisible = false">Cancel</el-button>
                {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
            </div>
            <div slot="footer" class="dialog-footer" v-if="id">
                <el-button class="btn btn-primary" class="updateCategory" @click="updateItem"   :disabled="form.disabled">Update</el-button>
                <el-button @click="innerVisible = false">Cancel</el-button>
                {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
            </div>
        </el-dialog>

    </div>
</shop-meals-list>

@endsection
