<shop-category-list :fetch-data-url="'{{route('shops.category.search',$shop->id)}}'"
                    :shop-id="'{{json_encode($shop->id)}}'"

                    inline-template>
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Categories list
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{url()->previous()}}" class="btn btn-clean btn-bold btn-upper btn-font-sm kt-hidden">
                        <i class="la la-long-arrow-left"></i>
                        Back
                    </a>
                    <button type="button" @click="innerVisible = true" class="btn btn-primary">
                        <i class="flaticon2-add-1"></i>
                        New Category
                    </button>


                </div>
            </div>
        </div>
        <div class="kt-portlet__body">


            <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                <div class="row">
                    <div class="col-sm-12">
                        <table-component
                            :data="fetchData"
                            :table-class="'table table-hover kt-datatable__table'"
                            :filter-input-class="'form-control'"
                            filter-placeholder="Search "
                            :show-caption="false"
                            sort-by="created_at"
                            {{--                                         :sort-order="name_en"--}}
                            ref="categoriesTable"
                        >
                            <table-column label="#" :sortable="false" :filterable="false">
                                <template scope="category">
                                    @{{((currentPage-1) * per_page)+ data.indexOf(category)+1}}
                                </template>
                            </table-column>
                            <table-column label="category image" :sortable="false" :filterable="false">
                                <template scope="category">
                                    <div class="kt-media kt-media--lg">
                                        <img :src="category.image_url" width="200px">
                                    </div>

                                </template>
                            </table-column>
                            <table-column label="Name En" show="name_en"></table-column>
                            <table-column label="Name Ar" show="name_ar"></table-column>
                            <table-column label="Meals count" show="meals_count">
                                <template scope="cat">
                                        <a :href="'/shops/meals/'+cat.shop_id+'/'+cat.id">
                                            @{{ cat.meals_count}}
                                        </a>
                                </template>
                            </table-column>

                            <table-column label="Active ">
                                <template scope="category">
                                    <toggle-button
                                        :value="category.is_active==1?true:false"
                                        color="#82C7EB"
                                        :sync="true"
                                        @change="onChangeEventToggleHandler($event,'{!! route('shops.category.change.status') !!}',category.id)"
                                    />
                                </template>
                            </table-column>
                            <table-column label="actions" :sortable="false" :filterable="false">
                                <template scope="category">

                                    <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" @click="mountCat(category)">
                                        <i class="la la-edit" aria-hidden="true"></i>
                                    </a>
{{--                                    |--}}
{{--                                    <a class="btn btn-sm btn-clean btn-icon btn-icon-md" @click="deleteAction('shops/categories/',category.id,true)" href="javascript:;">--}}
{{--                                        <i class="la la-trash" aria-hidden="true" style="color:red;"></i>--}}
{{--                                    </a>--}}

                                </template>
                            </table-column>

                        </table-component>

                    </div>
                </div>

            </div>


        </div>

        <el-dialog
            @close="clearCategoryModalData"
            width="30%"
            :title="id?'Update  category':'Add new category'"
            :visible.sync="innerVisible"
            append-to-body>
            <div class="form-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group"
                             :class="{ 'is-invalid': form.error && form.validations.cat_name_en }">
                            <label class="control-label">Name (En)</label>
                            <input type="text" class="form-control" placeholder="Name (En)"
                                   v-model="cat_name_en">
                            <span v-if="form.error && form.validations.cat_name_en" class="help-block invalid-feedback">@{{ form.validations.cat_name_en[0] }}</span>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group"
                             :class="{ 'is-invalid': form.error && form.validations.cat_name_ar }">
                            <label class="control-label">Name (Ar)</label>
                            <input type="text" class="form-control" placeholder="Name (Ar)"
                                   v-model="cat_name_ar">
                            <span v-if="form.error && form.validations.cat_name_ar" class="help-block invalid-feedback">@{{ form.validations.cat_name_ar[0] }}</span>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">

                        <div class="form-group"
                             :class="{ 'has-error': form.error && form.validations.cat_image }">
                            <label > Cat Image </label>
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail"
                                     style="width: 200px; height: 150px;">
                                    <img :src="cat_image" alt="" style="max-height: 150px"/>
                                </div>

                                <input type="file" id="inputfile" style="display:none;"
                                       @change="onFileChange">
                                <a class="btn blue"
                                   href="javascript:document.getElementById('inputfile').click(); ">
                                    Browse image </a>
                                <button class="btn red" @click.prevent="removeImage">
                                    Remove
                                </button>

                                <div v-if="form.error && form.validations.cat_image"
                                     class="help-block">@{{ form.validations.cat_image[0] }}
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

            </div>
            <div slot="footer" class="dialog-footer" v-if="!id">
                <el-button class="btn btn-primary" class="AddCategory" @click="AddCategory"    :disabled="form.disabled">Add</el-button>
                <el-button @click="innerVisible = false">Cancel</el-button>
                {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
            </div>
            <div slot="footer" class="dialog-footer" v-if="id">
                <el-button class="btn btn-primary" class="updateCategory" @click="updateCategory"    :disabled="form.disabled">Update</el-button>
                <el-button @click="innerVisible = false">Cancel</el-button>
                {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
            </div>
        </el-dialog>
    </div>
</shop-category-list>
