@extends('layouts.dashboard')
@push('js')
    <script>
        jQuery(document).ready(function () {
            if (document.location.hash) {
                // var selector=document.querySelectorAll('.nav-tabs a[href="' + document.location.hash + '"]')[0];
                var selector = jQuery('.nav-tabs a[href="' + document.location.hash + '"]');
                jQuery('.nav-tabs a').each(function (i, obj) {
                    if (jQuery(this).hasClass('active')) {
                        jQuery(this).removeClass('active')
                    }
                });
                jQuery('.tab-pane').removeClass(['show', 'active']);
                selector.addClass('active');
                jQuery(document.location.hash).addClass(['show', 'active']);
            }
        });
    </script>
@endpush
@section('content')

    <shop-view :shop='{!! isset($shop) ? $shop->toJson() : 'null' !!}'
               inline-template>


        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

            <!--begin::Portlet-->
            <div class="kt-portlet kt-widget kt-widget--fit kt-widget--general-3">
                <div class="kt-portlet__body">
                    <div class="kt-widget__top">
                        <div class="kt-media kt-media--xl kt-media--circle">
                            <img src="{{$shop->logo_url}}" alt="image">
                        </div>
                        <div class="kt-widget__wrapper">
                            <div class="kt-widget__label">
                                <a href="#" class="kt-widget__title">
                                    {{$shop->name}}
                                </a>
                                <span class="kt-widget__desc">
                          <star-rating read-only :rating="shop.rate" :round-start-rating="false">
                          </star-rating>
                                    <div>

                                        <span>{{$shop->start_work_at}} - {{$shop->end_work_at}} </span>
                                        <span></span>
                                    </div>
                        </span>
                            </div>

                            <div class="kt-widget__stats">

                                <div class="kt-widget__stat" href="#">
                                    <span class="kt-widget__value">{{$shop->branch_count}}</span>
                                    <span class="kt-widget__caption">Branchs</span>
                                </div>

                                <div class="kt-widget__stat" href="#">
                                    <span class="kt-widget__value">{{$shop->ratings->count()}}</span>
                                    <span class="kt-widget__caption">Reviews</span>
                                </div>

                                <div class="kt-widget__stat" href="#">
                                    <span class="kt-widget__value">{{$shop->category_count}}</span>
                                    <span class="kt-widget__caption">Categories</span>
                                </div>
                                <div class="kt-widget__stat" href="#">
                                    <span class="kt-widget__value">{{$shop->meals_count}}</span>
                                    <span class="kt-widget__caption">Meals</span>
                                </div>
                                <div class="kt-widget__stat" href="#">
                                    <span class="kt-widget__value">{{$shop->offer_count}}</span>
                                    <span class="kt-widget__caption">offers</span>
                                </div>

                                <div class="kt-widget__stat" href="#">
                                    <span class="kt-widget__value">{{$shop->order_count}}</span>
                                    <span class="kt-widget__caption">Orders</span>
                                </div>



                                <div class="kt-widget__stat" >


                                    <span class="kt-widget__value">
                                        <a href="{{route('shop.wallet.transaction.view',$shop->id)}}">

                                                {{$shop->wallet}}

                                        </a>
                                    </span>
                                    <span class="kt-widget__caption">Wallet</span>

                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__foot kt-portlet__foot--fit">
                    <div class="kt-widget__nav ">
                        <ul class="nav nav-tabs nav-tabs-space-xl nav-tabs-line nav-tabs-clear nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand kt-portlet__space-x"
                            role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tab_orders" role="tab">
                                    <i class="flaticon2-calendar-3"></i> Orders
                                </a>
                            </li>
{{--                            <li class="nav-item">--}}
{{--                                <a class="nav-link" data-toggle="tab" href="#tab_meals" role="tab">--}}
{{--                                    <i class="flaticon2-protected"></i> Meals--}}
{{--                                </a>--}}
{{--                            </li>--}}

                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab_categories" role="tab">
                                    <i class="flaticon2-protected"></i> Categories
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab_offers" role="tab">
                                    <i class="flaticon2-protected"></i> Offers
                                </a>
                            </li>
                            {{--                    <li class="nav-item">--}}
                            {{--                        <a class="nav-link" data-toggle="tab" href="#kt_apps_user_edit_tab_3" role="tab">--}}
                            {{--                            <i class="flaticon2-lock"></i> Offers--}}
                            {{--                        </a>--}}
                            {{--                    </li>--}}
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab_branches" role="tab">
                                    <i class="flaticon2-gear"></i> Branchs
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tab_reviews" role="tab">
                                    <i class="flaticon-star" style="color:#ffc107"></i> reviews
                                    ({{$shop->ratings->count()}})
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>

            <!--end::Portlet-->
            <div class="tab-content">
                <div class="tab-pane fade show" id="tab_categories" role="tabpanel">
                    @include('shops.category')
                </div>
                <div class="tab-pane fade show active" id="tab_orders" role="tabpanel">
                    @include('shops.order')
                </div>
                <div class="tab-pane fade show" id="tab_offers" role="tabpanel">
                    @include('shops.offers')
                </div>
{{--                <div class="tab-pane fade show" id="tab_meals" role="tabpanel">--}}
{{--                    @include('shops.meals')--}}
{{--                </div>--}}
                <div class="tab-pane fade show" id="tab_branches" role="tabpanel">
                    @include('shops.branchs')
                </div>
                <div class="tab-pane fade show " id="tab_reviews" role="tabpanel">
                    @include('shops.ratings')
                </div>
            </div>
        </div>
    </shop-view>
@endsection
