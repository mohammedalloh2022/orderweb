
<shop-ratings-list :fetch-data-url="'{{route('shops.reviews',$shop->id)}}'" inline-template>


    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Reviews list
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{url()->previous()}}" class="btn btn-clean btn-bold btn-upper btn-font-sm kt-hidden">
                        <i class="la la-long-arrow-left"></i>
                        Back
                    </a>
                </div>
            </div>

        </div>
        <div class="kt-portlet__body">


            <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                <div class="row">
                    <div class="col-sm-12">
                        <table-component

                            :data="fetchData"
                            :table-class="'table table-hover kt-datatable__table'"
                            :filter-input-class="'form-control'"
                            filter-placeholder="Search "
                            :show-caption="false"
                            sort-by="created_at"

                            :show-filter="false"
                            ref="categoriesTable"
                        >
                            <table-column label="#" :sortable="false" :filterable="false">
                                <template scope="review">
                                    @{{((currentPage-1) * per_page)+ data.indexOf(review)+1}}
                                </template>
                            </table-column>
                            <table-column label="Review by" :sortable="false" :filterable="false">
                                <template scope="shop">
                                    <div class="kt-widget-4">
                                        <div class="kt-widget-4__item">
                                            <div class="kt-widget-4__item-content">
                                                <div class="kt-widget-4__item-section" v-if="shop.user">
                                                    <div class="kt-widget-4__item-pic">
                                                        <img class=""  :src="shop.user.avatar_url"  alt="Avatar">
                                                    </div>
                                                    <div class="kt-widget-4__item-info">
                                                            @{{ shop.user.name }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </template>
                            </table-column>
                            <table-column label="Rate" :sortable="false" :filterable="false">

                                <template scope="shop">
                                    <star-rating read-only :rating="shop.rate" :round-start-rating="false">
                                    </star-rating>
                                </template>
                            </table-column>
                            <table-column label="Review" show="review"></table-column>
                            <table-column label="Review at" show="created_at"></table-column>
                            <table-column label="actions" :sortable="false" :filterable="false">
                                <template scope="review">
                                    <a class="btn btn-sm btn-clean btn-icon btn-icon-md" @click="deleteAction('/shops/rating/',review.id,true)" href="javascript:;">
                                        <i class="la la-trash" aria-hidden="true" style="color:red;"></i>
                                    </a>
                                </template>
                            </table-column>
                        </table-component>

                    </div>
                </div>

            </div>


        </div>
    </div>
</shop-ratings-list>
