<shop-order-list :fetch-data-url="'{{route('shops.orders.search.pending',$shop->id)}}'" inline-template>
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Pending Orders list
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{url()->previous()}}" class="btn btn-clean btn-bold btn-upper btn-font-sm kt-hidden">
                        <i class="la la-long-arrow-left"></i>
                        Back
                    </a>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">


            <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                <div class="row">
                    <div class="col-sm-12">
                        <table-component
                            :data="fetchData"
                            :table-class="'table table-hover kt-datatable__table table-responsive'"
                            :filter-input-class="'form-control'"
                            filter-placeholder="Search "
                            :show-caption="false"
                            sort-by="created_at"
                            {{--                                         :sort-order="name_en"--}}
                            ref="categoriesTable"
                        >
                            <table-column label="#" :sortable="false" :filterable="false">
                                <template scope="shop">
                                    @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                </template>
                            </table-column>
                            <table-column label="Order #." >
                                <template scope="order">
                                    <a :href="'/manage/orders/view/'+order.id">
                                        @{{ order.invoice_number }}
                                    </a>
                                </template>
                            </table-column>
                            <table-column label="client" :sortable="false" :filterable="false">
                                <template scope="order">
                                    <div class="kt-widget-4">
                                        <div class="kt-widget-4__item">
                                            <div class="kt-widget-4__item-content">
                                                <div class="kt-widget-4__item-section" v-if="order.client">
                                                    <div class="kt-widget-4__item-pic">
                                                        <img class=""  :src="order.client.avatar_url"  alt="Avatar">
                                                    </div>
                                                    <div class="kt-widget-4__item-info">
                                                        <a href="#"
                                                           class="kt-widget-4__item-username">
                                                            @{{ order.client.name }}
                                                        </a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </template>
                            </table-column>
                            <table-column label="Shop" :sortable="false" :filterable="false">
                                <template scope="order">
                                    <div class="kt-widget-4" v-if="order.shop">
                                        <div class="kt-widget-4__item">
                                            <div class="kt-widget-4__item-content">
                                                <div class="kt-widget-4__item-section" v-if="order.shop">
                                                    <div class="kt-widget-4__item-pic">
                                                        <img v-if='order.shop.logo_url' class="" :src="order.shop.logo_url" alt="shop logo">
                                                    </div>
                                                    <div class="kt-widget-4__item-info">
                                                        <a :href="'/shops/view/'+order.shop.id"
                                                           class="kt-widget-4__item-username">
                                                            @{{ order.shop.name}}
                                                        </a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </template>
                            </table-column>
                            <table-column label="delivery driver" :sortable="false" :filterable="false">
                                <template scope="order">
                                    <div class="kt-widget-4" v-if="order.delivery_user">
                                        <div class="kt-widget-4__item">
                                            <div class="kt-widget-4__item-content">
                                                <div class="kt-widget-4__item-section" v-if="order.delivery_user">
                                                    <div class="kt-widget-4__item-pic">
                                                        <img class=""  :src="order.delivery_user.avatar_url"  alt="Avatar">
                                                    </div>
                                                    <div class="kt-widget-4__item-info">
                                                        <a href="#"
                                                           class="kt-widget-4__item-username">
                                                            @{{ order.delivery_user.name }}
                                                        </a>

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </template>
                            </table-column>





                            <table-column label="receive type" show="type_of_receive"></table-column>
                            <table-column label="payment type" show="payment_type"></table-column>
                            <table-column label="tax" show="tax"></table-column>
                            <table-column label="discount" show="discount"></table-column>
                            <table-column label="total" show="total"></table-column>
                            <table-column label="Order date" show="created_at"></table-column>
                            <table-column label="type" >
                                <template scope="order">
                                    <span class="badge badge-primary" v-if="order.type=='normal'">Normal</span>
                                    <span class="badge badge-secondary" v-if="order.type=='offer'">Offer</span>
                                </template>
                            </table-column>
                            <table-column label="Status" >
                                <template scope="order">
                                    <span class="badge badge-secondary">@{{ order.status_translation }}</span>
                                </template>
                            </table-column>
                            <table-column label="actions" :sortable="false" :filterable="false">
                                <template scope="order">

                                    <button class="btn btn-label-success btn-bold btn-sm btn-icon-h kt-margin-l-10"
                                            @click="changeStatus('{{route('mange.orders.change.status')}}',order.id,'preparing',true)"
                                            v-if="order.status=='pending'">
                                        Accept order
                                    </button>

                                </template>
                            </table-column>

                            {{--                                        <table-column label="work at" :sortable="false" :filterable="false">--}}
                            {{--                                            <template scope="shop">--}}
                            {{--                                                @{{ shop.start_work_at }} - @{{ shop.end_work_at }}--}}
                            {{--                                            </template>--}}
                            {{--                                        </table-column>--}}



                        </table-component>
                    </div>
                </div>

            </div>


        </div>
    </div>
</shop-order-list>
