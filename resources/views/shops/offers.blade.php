<shop-offers-list :fetch-data-url="'{{route('shops.offers',$shop->id)}}'"
                  :shop-id="{!! json_encode($shop->id) !!}"
                  :country-id="{!! json_encode($shop->country_id) !!}"
                  inline-template>
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Offers list
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <button type="button" @click="innerVisible = true" class="btn btn-primary">
                        <i class="flaticon2-add-1"></i>
                        New offer
                    </button>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="row">
                    <div class="col-sm-12">
                        <table-component
                            :data="fetchOffersData"
                            :table-class="'table table-hover kt-datatable__table'"
                            :filter-input-class="'form-control'"
                            filter-placeholder="Search "
                            :show-caption="false"
                            sort-by="created_at"
                            :show-fitler="false"
                            show-fitler="false"

                            ref="categoriesTable"
                        >
                            <table-column label="#" :sortable="false" :filterable="false">
                                <template scope="shop">
                                    @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                </template>
                            </table-column>
                            <table-column label="Offer pic" :sortable="false" :filterable="false">
                                <template scope="shop">
                                    <div class="kt-media kt-media--lg">
                                        <img :src="shop.image_url" width="200px">
                                    </div>

                                </template>
                            </table-column>
                            <table-column label="Name" show="name"></table-column>
                            <table-column label="Price" show="price"></table-column>
                            <table-column label="offer orders count" >
                                <template scope="offer">
                                    <a :href="'/manage/offers/orders/'+offer.id">
                                        @{{ offer.offer_orders_count}}
                                    </a>
                                </template>
                            </table-column>



                            <table-column label="status" :sortable="false" :filterable="false">
                                <template scope="shop">
                                    <span class="badge badge-warning" v-if="shop.status=='pending'">Pending</span>
                                    <span class="badge badge-success" v-if="shop.status=='active'">Active</span>
                                    <span class="badge badge-danger" v-if="shop.status=='rejected'">Rejected</span>
                                </template>
                            </table-column>

                            <table-column label="Active ">
                                <template scope="item">
                                    <toggle-button
                                        :value="item.is_active==1?true:false"
                                        color="#82C7EB"
                                        :sync="true"
                                        @change="onChangeEventToggleHandler($event,'{!! route('shops.meals.change.status') !!}',item.id)"
                                    />
                                </template>
                            </table-column>
                            <table-column label="actions" :sortable="false" :filterable="false">
                                <template scope="shop">
                                    <a class="btn btn-sm btn-clean btn-icon btn-icon-md"
                                       href="javascript:;" @click="viewOffer(shop)">
                                        <i class="fa fa-eye" aria-hidden="true"></i>
                                    </a>
                                    <a class="btn btn-sm btn-clean btn-icon btn-icon-md" v-if="shop.status=='pending'"
                                       @click="showEditModel(shop)" href="javascript:;">
                                        <i class="fa fa-edit" aria-hidden="true"></i>
                                    </a>
{{--                                    |--}}
{{--                                    <a class="btn btn-sm btn-clean btn-icon btn-icon-md"--}}
{{--                                       @click="deleteAction('/shops/offers/',shop.id,true)" href="javascript:;">--}}
{{--                                        <i class="la la-trash" aria-hidden="true" style="color:red;"></i>--}}
{{--                                    </a>--}}
                                </template>
                            </table-column>
                        </table-component>
                    </div>
                </div>
            </div>
            <el-dialog
                @close="clearMealModalData"
                width="40%"
                :title="id?'Update offer details':'Add offer '"
                :visible.sync="innerVisible"
                append-to-body>
                <form class="kt-form kt-form--label-right">
                    <div class="col-12 text-center">
                        {{--                                        <label for="example-text-input" class="col-4 col-form-label">Logo</label>--}}

                        <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                             id="kt_profile_avatar_3">
                            <div class="kt-avatar__holder"
                            >
                                <img v-if='meal_image' :src="meal_image" alt="shop_log" style="    height: 114px;
    width: 114px;"/>
                            </div>
                            <label class="kt-avatar__upload" title="Change avatar">
                                <i class="fa fa-pen"></i>
                                <input type='file' name="logo" @change="onFileChange($event,'meal_image')"
                                       accept=".png, .jpg, .jpeg"/>
                            </label>
                            <span class="clear-image" title="Cancel avatar" v-if="meal_image &!id"
                                  @click.prevent="removeImage('meal_image')">
                                            <i class="fa fa-times"></i>
                            </span>
                        </div>
                        <span v-if="form.error && form.validations.meal_image"
                              class="help-block invalid-feedback">@{{ form.validations.meal_image[0] }}
                        </span>
                    </div>
                    <br/>
                    <div class="form-body">
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Name(En)</label>
                            <div class="col-9">
                                <input type="text" class="form-control" placeholder="name(en)"
                                       v-model="name_en">
                                <span v-if="form.error && form.validations.name_en"
                                      class="help-block invalid-feedback">@{{ form.validations.name_en[0] }}</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Name(Ar)</label>
                            <div class="col-9">
                                <input type="text" class="form-control" placeholder="name(en)"
                                       v-model="name_ar">
                                <span v-if="form.error && form.validations.name_ar"
                                      class="help-block invalid-feedback">@{{ form.validations.name_ar[0] }}</span>
                            </div>
                        </div>



                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Description(En)</label>
                            <div class="col-9">
                                <input type="text" class="form-control" placeholder="Description (En)"
                                       v-model="desc_en">
                                <span v-if="form.error && form.validations.desc_en"
                                      class="help-block invalid-feedback">@{{ form.validations.desc_en[0] }}</span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Description(Ar)</label>
                            <div class="col-9">
                                <input type="text" class="form-control" placeholder="Description (Ar)"
                                       v-model="desc_ar">
                                <span v-if="form.error && form.validations.desc_ar"
                                      class="help-block invalid-feedback">@{{ form.validations.desc_ar[0] }}</span>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">price</label>
                            <div class="col-9">
                                <input type="text" class="form-control" placeholder="price"
                                       v-model="price">
                                <span v-if="form.error && form.validations.price"
                                      class="help-block invalid-feedback">@{{ form.validations.price[0] }}</span>
                            </div>
                        </div>



                    </div>
                </form>
                <div slot="footer" class="dialog-footer" v-if="!id">
                    <el-button class="btn btn-primary" class="addShop" @click="add"    :disabled="form.disabled">Add offer</el-button>
                    <el-button @click="innerVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
                <div slot="footer" class="dialog-footer" v-if="id">
                    <el-button class="btn btn-primary" class="updateCategory" @click="updateItem"    :disabled="form.disabled">Update</el-button>
                    <el-button @click="innerVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
            </el-dialog>
            <el-dialog
                @closed="clearModelData"
                width="40%"
                :title="id?'View offer':'View offer'"
                :visible.sync="innerViewVisible"

                append-to-body>
                <form class="kt-form kt-form--label-right">
                    <div>
                        <div class="col-12 text-center">
                            <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                 id="kt_profile_avatar_3">
                                <div class="kt-avatar__holder"
                                >
                                    <img v-if='offerViewObject' :src="offerViewObject.image_url" alt="Offer image" style="    height: 114px;
    width: 114px;"/>
                                </div>

                            </div>

                        </div>
                        <br/>

                        <table class="table table-striped">
                            <tbody>
                            <tr>
                                <th>Name (en)</th>
                                <td>@{{ offerViewObject.name_en }}</td>
                            </tr>
                            <tr>
                                <th>Name (ar)</th>
                                <td>@{{ offerViewObject.name_ar }}</td>
                            </tr>
                            <tr>
                                <th>Description (En)</th>
                                <td>@{{ offerViewObject.desc_en }}</td>
                            </tr>
                            <tr>
                                <th>Description (Ar)</th>
                                <td>@{{ offerViewObject.desc_ar }}</td>
                            </tr>
                            <tr>
                                <th>Price</th>
                                <td>@{{ offerViewObject.price }}</td>
                            </tr>
                            <tr>
                                <th>Status</th>
                                <td>@{{ offerViewObject.status }}</td>
                            </tr>


{{--                            <tr>--}}
{{--                                <th>Shop name</th>--}}
{{--                                <td>@{{ offerViewObject.shop? offerViewObject.shop.name:'' }}</td>--}}
{{--                            </tr>--}}

                            <tr>
                                <th>Created at</th>
                                <td>@{{ offerViewObject.created_at }}</td>
                            </tr>
                            </tbody>
                        </table>




                    </div>


                </form>
                <div slot="footer" class="dialog-footer" >

                    <el-button @click="innerViewVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>

            </el-dialog>

        </div>
    </div>
</shop-offers-list>

