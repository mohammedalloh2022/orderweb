@extends('layouts.dashboard')
@push('js')
    <script>
        jQuery(document).ready(function () {
            if (document.location.hash) {
                // var selector=document.querySelectorAll('.nav-tabs a[href="' + document.location.hash + '"]')[0];
                var selector = jQuery('.nav-tabs a[href="' + document.location.hash + '"]');
                jQuery('.nav-tabs a').each(function (i, obj) {
                    if (jQuery(this).hasClass('active')) {
                        jQuery(this).removeClass('active')
                    }
                });
                jQuery('.tab-pane').removeClass(['show', 'active']);
                selector.addClass('active');
                jQuery(document.location.hash).addClass(['show', 'active']);
            }

        });
    </script>
@endpush
@section('content')

    <shop-view :shop='{!! isset($shop) ? $shop->toJson() : 'null' !!}'
               inline-template>


        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">




            <div class="tab-content">


{{--                    <div class="kt-portlet__head bg-white">--}}
{{--                        <div class="kt-portlet__head-label">--}}
{{--                            <h3 class="kt-portlet__head-title">--}}
{{--                                <div class="kt-widget__top  text-center">--}}
{{--                                    <div class="kt-media kt-media--xl kt-media--circle">--}}
{{--                                        <img src="{{$shop->logo_url}}" alt="image">--}}
{{--                                    </div>--}}

{{--                                    <div class="kt-widget__wrapper">--}}
{{--                                        <div class="kt-widget__label">--}}
{{--                                            <span class="kt-widget__title">--}}
{{--                                                {{$shop->name}}--}}
{{--                                                <br/>--}}
{{--                                                Orders : ({{$shop->order_count}})--}}
{{--                                                <br/>--}}
{{--                                                Wallet : <a href="/shops/wallet/transactions/{{$shop->id}}">--}}
{{--                                                            {{ $shop->wallet ??0}}--}}
{{--                                                             </a>--}}
{{--                                            </span>--}}

{{--                                        </div>--}}

{{--                                    </div>--}}
{{--                                </div>--}}
{{--                            </h3>--}}
{{--                        </div>--}}
{{--                    </div>--}}
                    @include('shops.order')
                </div>


        </div>
    </shop-view>
@endsection
