@extends('layouts.dashboard')

@section('content')

    <shop-wallet-payment-transaction
        :fetch-data-url="'{{route('shop.wallet.transaction.list', $shop->id)}}'"
        :shop-id="'{!! $shop->id !!}'" inline-template>

        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        {{$shop->name}} wallet
                    </h3>
                </div>



            </div>
            <div class="kt-portlet__body">
                <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                    <div class="row">
                        <div class="col-sm-12">
                            <table-component
                                :data="fetchData"
                                :table-class="'table table-hover kt-datatable__table'"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                :show-filter="false"
                                sort-by="created_at"
                                ref="transactionTable"
                            >
                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="shop">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                    </template>
                                </table-column>
                                <table-column label="Invoice No." show="invoice_no" ></table-column>
                                <table-column label="Previous wallet" show="prev_wallet" ></table-column>
                                <table-column label="Amount" show="amount" ></table-column>
                                <table-column label="Current wallet" show="next_wallet" ></table-column>
                                <table-column label="Type" show="type" ></table-column>
                                <table-column label="Created at" show="created_at" ></table-column>


                            </table-component>
                        </div>
                    </div>

                </div>


            </div>



        </div>
    </shop-wallet-payment-transaction>

@endsection
