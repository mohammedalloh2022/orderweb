<?php $countries = \App\Models\Country::query()->where('is_active', 1)->get();
?>

@if(auth()->user()->role=='admin')

    @can('Country Management')
        <li class="kt-menu__item {{ request()->is('categories') ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
            <a href="{{route('country.index')}}" class="kt-menu__link ">
                <i class="kt-menu__link-icon flaticon2-copy"></i>
                <span class="kt-menu__link-text">
                @lang('lang.countries_management')
        </span>
            </a>
        </li>
    @endcan
    @can('Category list')
        <li class="kt-menu__item {{ request()->is('categories') ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">
            <a href="{{route('category.index')}}" class="kt-menu__link ">
                <i class="kt-menu__link-icon flaticon2-copy"></i>
                <span class="kt-menu__link-text">
                    @lang('lang.category_management')
                </span>
            </a>
        </li>
    @endcan

    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
                @lang('lang.prices_management')
            </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>

        <div class="kt-menu__submenu {{ request()->is('prices') ? 'kt-menu__item--active' : '' }} "
             kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <?php foreach ($countries as $country):?>


                @can('Public stores orders '.$country->code)
                    <li class="kt-menu__item " aria-haspopup="true">
                        <a href="{{route('prices.index',$country->id)}}" class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">
                                @lang('attribute_prices',['attribute'=>$country->name_en])
                            </span>
                        </a>
                    </li>
                @endcan


                <?php endforeach;?>
            </ul>
        </div>
    </li>




    @can('Cancel order reason list')
        <li class="kt-menu__item {{ request()->is('cancel-reasons') ? 'kt-menu__item--active' : '' }}"
            aria-haspopup="true">
            <a href="{{route('cancel.reason.index')}}" class="kt-menu__link ">
                <i class="kt-menu__link-icon flaticon2-copy"></i>
                <span class="kt-menu__link-text">
            Cancel order reasons
        </span>
            </a>
        </li>
    @endcan
    {{--    @can('Store list')--}}
    {{--        <li class="kt-menu__item {{ request()->is('shops') ? 'kt-menu__item--active' : '' }}" aria-haspopup="true">--}}
    {{--            <a href="{{route('shops.index')}}" class="kt-menu__link ">--}}
    {{--                <i class="kt-menu__link-icon flaticon2-copy"></i>--}}
    {{--                <span class="kt-menu__link-text">--}}
    {{--                Stores management--}}
    {{--            </span>--}}
    {{--            </a>--}}
    {{--        </li>--}}
    {{--    @endcan--}}

    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
             Dashboards
            </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>

        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <?php foreach ($countries as $country):?>
                @can('Super admin dashboard '.$country->code)
                    <li class="kt-menu__item {{ request()->is('dashboard') ? 'kt-menu__item--active' : '' }}"
                        aria-haspopup="true">
                        <a href="{{route('home',['countryId'=>$country->id])}}" class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">
                                        {{$country->name_en}} dashboard
                                 </span>
                        </a>
                    </li>
                @endcan
                <?php endforeach;?>
            </ul>
        </div>
    </li>
    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
             Public ads slider
            </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>

        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <?php foreach ($countries as $country):?>
                @can('Ads list '.$country->code)
                    <li class="kt-menu__item {{ request()->is('advertise') ? 'kt-menu__item--active' : '' }}"
                        aria-haspopup="true">
                        <a href="{{route('advertise.index.country',['countryId'=>$country->id])}}"
                           class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">

                                {{$country->name_en}} ads
                         </span>
                        </a>
                    </li>
                @endcan
                <?php endforeach;?>
            </ul>
        </div>
    </li>

    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
             Stores management
            </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>

        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <?php foreach ($countries as $country):?>

                @can('Store list '.$country->code)
                    <li class="kt-menu__item {{ request()->is('shops') ? 'kt-menu__item--active' : '' }}"
                        aria-haspopup="true">
                        <a href="{{route('shops.country.index',['countryId'=>$country->id])}}" class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">

                                    {{$country->name_en}} stores
                             </span>
                        </a>
                    </li>
                @endcan
                <?php endforeach;?>
            </ul>
        </div>
    </li>

    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
              New Offers requests
            </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>

        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <?php foreach ($countries as $country):?>

                @can('New offers list '.$country->code)
                    <li class="kt-menu__item  {{ request()->is('manage/offers') ? 'kt-menu__item--active' : '' }}"
                        aria-haspopup="true">
                        <a href="{{route('mange.offers.index',['countryId'=>$country->id])}}" class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">
                                  {{$country->name_en}} offers
                            </span>
                        </a>
                    </li>
                @endcan

                <?php endforeach;?>
            </ul>
        </div>
    </li>
    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
               Offers
            </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>

        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <?php foreach ($countries as $country):?>
                @can('Offers list '.$country->code)
                    <li class="kt-menu__item  {{ request()->is('manage/offers/list/search') ? 'kt-menu__item--active' : '' }}"
                        aria-haspopup="true">
                        <a href="{{route('mange.offers.list.index',['countryId'=>$country->id])}}"
                           class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">
                                    {{$country->name_en}} offers
                          </span>
                        </a>
                    </li>
                @endcan
                <?php endforeach;?>
            </ul>
        </div>
    </li>


    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
               Pending orders
            </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>

        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <?php foreach ($countries as $country):?>


                @can('Pending order list '.$country->code)
                    <li class="kt-menu__item   {{ request()->is('manage/orders/pending-list*') ? 'kt-menu__item--active' : '' }}"
                        aria-haspopup="true">
                        <a href="{{route('mange.pending.orders.index',['countryId'=>$country->id])}}"
                           class="kt-menu__link ">

                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">
                                 {{$country->name_en}}  orders
                            </span>
                        </a>
                    </li>
                @endcan
                <?php endforeach;?>
            </ul>
        </div>
    </li>

    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
               Orders management
            </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>

        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <?php foreach ($countries as $country):?>
                @can('Order list '.$country->code)
                    <li class="kt-menu__item   {{ request()->is('manage/orders') ? 'kt-menu__item--active' : '' }}"
                        aria-haspopup="true">
                        <a href="{{route('mange.orders.index',['countryId'=>$country->id])}}" class="kt-menu__link ">

                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">
                             {{$country->name_en}}  Orders
                        </span>
                        </a>
                    </li>
                @endcan

                <?php endforeach;?>
            </ul>
        </div>
    </li>


    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
              App revenue
            </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>

        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <?php foreach ($countries as $country):?>
                @can('App revenue '.$country->code)

                    <li class="kt-menu__item {{ request()->is('manage/revenue') ? 'kt-menu__item--active' : '' }} "
                        aria-haspopup="true">
                        <a href="{{route('app.revenue.orders.index',['countryId'=>$country->id])}}"
                           class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">
                         {{$country->name_en}} revenue
                        </span>
                        </a>
                    </li>
                @endcan
                <?php endforeach;?>
            </ul>
        </div>
    </li>



    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
              App discounts
            </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>

        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <?php foreach ($countries as $country):?>
                @can('App discount '.$country->code)
                    <li class="kt-menu__item {{ request()->is('manage/discounts') ? 'kt-menu__item--active' : '' }} "
                        aria-haspopup="true">
                        <a href="{{route('app.discount.orders.index',['countryId'=>$country->id])}}"
                           class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">
                                  {{$country->name_en}}  discounts
                        </span>
                        </a>
                    </li>
                @endcan

                <?php endforeach;?>
            </ul>
        </div>
    </li>

    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
              Contact messages
            </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>

        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <?php foreach ($countries as $country):?>
                @can('Contact messages '.$country->code)
                    <li class="kt-menu__item {{ request()->is('contacts') ? 'kt-menu__item--active' : '' }}"
                        aria-haspopup="true">
                        <a href="{{route('contacts.index',['countryId'=>$country->id])}}" class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">
                              {{$country->name_en}} messages
                        </span>
                        </a>
                    </li>
                @endcan

                <?php endforeach;?>
            </ul>
        </div>
    </li>
    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
             Coupons management
            </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>

        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <?php foreach ($countries as $country):?>

                @can('Coupons management '.$country->code)

                    <li class="kt-menu__item {{ request()->is('contacts') ? 'kt-menu__item--active' : '' }}"
                        aria-haspopup="true">
                        <a href="{{route('coupons.index',['countryId'=>$country->id])}}" class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">
                                    {{$country->name_en}} Coupons
                            </span>
                        </a>
                    </li>
                @endcan
                <?php endforeach;?>
            </ul>
        </div>
    </li>


    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
            Client management
            </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>

        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <?php foreach ($countries as $country):?>
                @can('Client management '.$country->code)
                    <li class="kt-menu__item  {{ request()->is('manage/client*') ? 'kt-menu__item--active' : '' }} "
                        aria-haspopup="true">
                        <a href="{{route('mange.client.index',['countryId'=>$country->id])}}" class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">
                               {{$country->name_en}} clients
                            </span>
                        </a>
                    </li>
                @endcan

                <?php endforeach;?>
            </ul>
        </div>
    </li>



    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
            Drivers management
            </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>


        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">


                <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true"
                    data-ktmenu-submenu-toggle="hover">
                    <a
                            href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <i class="kt-menu__link-icon flaticon2-copy"></i>
                        <span class="kt-menu__link-text">
                      Drivers management
                    </span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>

                    <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <?php foreach ($countries as $country):?>

                            @can('Drivers management '.$country->code)
                                <li class="kt-menu__item " aria-haspopup="true">
                                    <a href="{{route('mange.delivery.index',['countryId'=>$country->id])}}"
                                       class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon2-copy"></i>
                                        <span class="kt-menu__link-text">
                                            {{$country->name_en}}  management
                                        </span>
                                    </a>
                                </li>
                            @endcan


                            <?php endforeach;?>
                        </ul>
                    </div>
                </li>


                <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true"
                    data-ktmenu-submenu-toggle="hover">
                    <a
                            href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <i class="kt-menu__link-icon flaticon2-copy"></i>
                        <span class="kt-menu__link-text">
                      New Drivers requests
                    </span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>

                    <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <?php foreach ($countries as $country):?>


                            @can('New drivers requests '.$country->code)
                                <li class="kt-menu__item " aria-haspopup="true">
                                    <a href="{{route('mange.delivery.documented',['countryId'=>$country->id])}}"
                                       class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon2-copy"></i>
                                        <span class="kt-menu__link-text">
                                           {{$country->name_en}}requests
                                        </span>
                                    </a>
                                </li>
                            @endcan


                            <?php endforeach;?>
                        </ul>
                    </div>
                </li>


            </ul>
        </div>
    </li>




    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover">
        <a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
                 Send Notifications
            </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>

        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <?php foreach ($countries as $country):?>
                @can('Send notification '.$country->code)
                    <li class="kt-menu__item {{ request()->is('manage/send-notification') ? 'kt-menu__item--active' : '' }}"
                        aria-haspopup="true">
                        <a href="{{route('manage.mobile.notification',['countryId'=>$country->id])}}"
                           class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">
                            {{$country->name_en}}  Notifications
                        </span>
                        </a>
                    </li>
                @endcan


                <?php endforeach;?>
            </ul>
        </div>
    </li>
    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
                     Public Stores Orders
                </span>
            <i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">


                <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">
                                <span class="kt-menu__link"><span class="kt-menu__link-text">

                                    </span>
                                </span>
                </li>


                <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true"
                    data-ktmenu-submenu-toggle="hover">
                    <a
                            href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <i class="kt-menu__link-icon flaticon2-copy"></i>
                        <span class="kt-menu__link-text">
                 Public orders
            </span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>

                    <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <?php foreach ($countries as $country):?>


                            @can('Public stores orders '.$country->code)
                                <li class="kt-menu__item " aria-haspopup="true">

                                    <a href="{{route('public.orders.index',['countryId'=>$country->id])}}"
                                       class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon2-copy"></i>
                                        <span class="kt-menu__link-text">
                                                       {{$country->name_en}}  orders
                                                </span>
                                    </a>
                                </li>
                            @endcan


                            <?php endforeach;?>
                        </ul>
                    </div>
                </li>


                <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true"
                    data-ktmenu-submenu-toggle="hover">
                    <a
                            href="javascript:;" class="kt-menu__link kt-menu__toggle">
                        <i class="kt-menu__link-icon flaticon2-copy"></i>
                        <span class="kt-menu__link-text">
                              Public drivers management
                        </span>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    </a>

                    <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
                        <ul class="kt-menu__subnav">
                            <?php foreach ($countries as $country):?>
                            @can('Public drivers management '.$country->code)
                                <li class="kt-menu__item " aria-haspopup="true">
                                    <a href="{{route('driver.public.orders.list',['countryId'=>$country->id])}}"
                                       class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon2-copy"></i>
                                        <span class="kt-menu__link-text">
                                                 {{$country->name_en}}  Drivers
                                            </span>
                                    </a>
                                </li>
                            @endcan

                            <?php endforeach;?>
                        </ul>
                    </div>
                </li>

            </ul>
        </div>
    </li>

    @if(auth()->user()->can('Admin management') || auth()->user()->can('Roles management'))
        <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                    href="javascript:;" class="kt-menu__link kt-menu__toggle">
                <i class="kt-menu__link-icon flaticon2-copy"></i>
                <span class="kt-menu__link-text">
                 Admin management
             </span>
                <i class="kt-menu__ver-arrow la la-angle-right"></i>
            </a>
            <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
                <ul class="kt-menu__subnav">
                    @can('Admin management')
                        <li class="kt-menu__item " aria-haspopup="true">
                            <a href="{{route('mange.admin.index')}}" class="kt-menu__link ">
                                <i class="kt-menu__link-icon flaticon2-copy"></i>
                                <span class="kt-menu__link-text">
                        Admin management
                    </span>
                            </a>
                        </li>
                    @endcan
                    @can('Roles management')
                        <li class="kt-menu__item " aria-haspopup="true">
                            <a href="{{route('mange.admin.roles.list')}}" class="kt-menu__link ">
                                <i class="kt-menu__link-icon flaticon2-copy"></i>
                                <span class="kt-menu__link-text">
                       System roles
                    </span>
                            </a>
                        </li>
                    @endcan

                </ul>
            </div>
        </li>
    @endif

    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a
                href="javascript:;" class="kt-menu__link kt-menu__toggle">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span
                    class="kt-menu__link-text">
            Settings
        </span><i class="kt-menu__ver-arrow la la-angle-right"></i>
        </a>
        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span class="kt-menu__arrow"></span>
            <ul class="kt-menu__subnav">
                <li class="kt-menu__item  kt-menu__item--parent" aria-haspopup="true">
                <span class="kt-menu__link"><span class="kt-menu__link-text">

                    </span>
                </span>
                </li>
                @can('App setting')
                    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true"
                        data-ktmenu-submenu-toggle="hover">
                        <a
                                href="javascript:;" class="kt-menu__link kt-menu__toggle">
                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">
                            App  settings
                            </span>
                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                        </a>

                        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span
                                    class="kt-menu__arrow"></span>
                            <ul class="kt-menu__subnav">
                                <?php foreach ($countries as $country):?>

                                @can('App setting '.$country->code)
                                    <li class="kt-menu__item " aria-haspopup="true">

                                        <a href="{{route('app.settings',['countryId'=>$country->id])}}"
                                           class="kt-menu__link ">
                                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                                            <span class="kt-menu__link-text">
                                             {{$country->name_en}} settings
                                        </span>
                                        </a>
                                    </li>
                                @endcan


                                <?php endforeach;?>
                            </ul>
                        </div>
                    </li>
                @endcan



                @can('App Social media list')
                    <li class="kt-menu__item kt-menu__item--submenu " aria-haspopup="true"
                        data-ktmenu-submenu-toggle="hover">
                        <a
                                href="javascript:;" class="kt-menu__link kt-menu__toggle">
                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">
                            App content
                            </span>
                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                        </a>

                        <div class="kt-menu__submenu " kt-hidden-height="160" style=""><span
                                    class="kt-menu__arrow"></span>
                            <ul class="kt-menu__subnav">
                                <?php foreach ($countries as $country):?>


                                <li class="kt-menu__item " aria-haspopup="true">

                                    <a href="{{route('app.content',['countryId'=>$country->id])}}"
                                       class="kt-menu__link ">
                                        <i class="kt-menu__link-icon flaticon2-copy"></i>
                                        <span class="kt-menu__link-text">
                                               {{$country->name_en}}  content
                                            </span>
                                    </a>
                                </li>


                                <?php endforeach;?>
                            </ul>
                        </div>
                    </li>
                @endcan


                @can('App privacy content')
                    <li class="kt-menu__item " aria-haspopup="true">

                        <a href="{{route('app.privacy')}}" class="kt-menu__link ">
                            <i class="kt-menu__link-icon flaticon2-copy"></i>
                            <span class="kt-menu__link-text">
                         Mobile apps privacy
                    </span>
                        </a>
                    </li>
                @endcan
            </ul>
        </div>
    </li>

@endif
@if(auth()->user()->role=='shop')


    <li class="kt-menu__item " aria-haspopup="true">
        <a href="{{route('shop.dashboard')}}" class="kt-menu__link ">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
              Dashboard
        </span>
        </a>
    </li>


    <li class="kt-menu__item " aria-haspopup="true">
        <a href="{{route('shop.orders')}}" class="kt-menu__link ">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
                Orders
        </span>
        </a>
    </li>

    <li class="kt-menu__item " aria-haspopup="true">
        <a href="{{route('shop.pending.orders')}}" class="kt-menu__link ">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
                Pending orders
        </span>
        </a>
    </li>


    {{--    <li class="kt-menu__item " aria-haspopup="true">--}}
    {{--        <a href="{{route('shop.prev.orders')}}" class="kt-menu__link ">--}}
    {{--            <i class="kt-menu__link-icon flaticon2-copy"></i>--}}
    {{--            <span class="kt-menu__link-text">--}}
    {{--                  Calculated Orders--}}
    {{--        </span>--}}
    {{--        </a>--}}
    {{--    </li>--}}

    <li class="kt-menu__item " aria-haspopup="true">
        <a href="{{route('shop.category')}}" class="kt-menu__link ">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
                Categories
        </span>
        </a>
    </li>

    <li class="kt-menu__item " aria-haspopup="true">
        <a href="{{route('shop.branch')}}" class="kt-menu__link ">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
                Branches
        </span>
        </a>
    </li>

    <li class="kt-menu__item " aria-haspopup="true">
        <a href="{{route('shop.offers')}}" class="kt-menu__link ">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
              Offers
        </span>
        </a>
    </li>

    <li class="kt-menu__item " aria-haspopup="true">
        <a href="{{route('shop.revenue')}}" class="kt-menu__link ">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
              Shop revenue
        </span>
        </a>
    </li>

    <li class="kt-menu__item " aria-haspopup="true">
        <a href="{{route('shop.branch.payment.transaction')}}" class="kt-menu__link ">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
              Payment branches transactions
        </span>
        </a>
    </li>
    <li class="kt-menu__item " aria-haspopup="true">
        <a href="{{route('shop.transactions')}}" class="kt-menu__link ">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
           Shop transactions
        </span>
        </a>
    </li>
@endif
@if(auth()->user()->role =="branch")


    <li class="kt-menu__item " aria-haspopup="true">
        <a href="{{route('branch.dashboard')}}" class="kt-menu__link ">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
              Dashboard
        </span>
        </a>
    </li>


    <li class="kt-menu__item " aria-haspopup="true">
        <a href="{{route('branch.orders')}}" class="kt-menu__link ">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
                Orders
        </span>
        </a>
    </li>

    <li class="kt-menu__item " aria-haspopup="true">
        <a href="{{route('shop.pending.orders')}}" class="kt-menu__link ">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
                Pending orders
        </span>
        </a>
    </li>
    {{--    <li class="kt-menu__item " aria-haspopup="true">--}}
    {{--        <a href="{{route('shop.prev.orders')}}" class="kt-menu__link ">--}}
    {{--            <i class="kt-menu__link-icon flaticon2-copy"></i>--}}
    {{--            <span class="kt-menu__link-text">--}}
    {{--                Previous orders--}}
    {{--        </span>--}}
    {{--        </a>--}}
    {{--    </li>--}}


    <li class="kt-menu__item " aria-haspopup="true">
        <a href="{{route('shops.branch.view',auth()->user()->shop->id)}}" class="kt-menu__link ">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
              Branch wallet
        </span>
        </a>
    </li>

    <li class="kt-menu__item " aria-haspopup="true">
        <a href="{{route('shop.revenue')}}" class="kt-menu__link ">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
              Branch revenue
        </span>
        </a>
    </li>
    <li class="kt-menu__item " aria-haspopup="true">
        <a href="{{route('shop.transactions')}}" class="kt-menu__link ">
            <i class="kt-menu__link-icon flaticon2-copy"></i>
            <span class="kt-menu__link-text">
              Transactions
        </span>
        </a>
    </li>


@endif
