<!DOCTYPE html>


<html lang="en">

<!-- begin::Head -->
<head>
    <base href="">
    <meta charset="utf-8"/>
    <title>4Station app </title>
    <meta name="description" content="Latest updates and statistic charts">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">


    <!--end::Fonts -->

    <!--begin::Page Vendors Styles(used by this page) -->
{{--    <link href="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.css')}}" rel="stylesheet" type="text/css" />--}}

<!--end::Page Vendors Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css"/>
    @if(\Illuminate\Support\Facades\App::getLocale() == 'ar')
        <link href="{{asset('assets/css/style.bundle.rtl.css')}}" rel="stylesheet" type="text/css"/>
    @else
        <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css"/>
    @endif

<!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="{{asset('assets/css/skins/header/base/light.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/skins/header/menu/light.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/skins/brand/navy.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/css/skins/aside/navy.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('assets/plugins/bootstrap-sweetalert/sweetalert.css') }}" rel="stylesheet"
          type="text/css"/>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDcoLE5fsqEY5OTx5Dd1mQL__9WFmSNt2Q&libraries=geometry,places&v=weekly&language=ar"
            async defer>
    </script>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>

    <!--end::Layout Skins -->
    <link rel="shortcut icon" href="{{asset('assets/media/logos/favicon.css')}}"/>
    <style scoped>

        #description {
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
        }

        #infowindow-content .title {
            font-weight: bold;
        }

        #infowindow-content {
            display: none;
        }

        .pac-container.pac-logo.hdpi {
            z-index: 99999;
        }

        #mapLocation #infowindow-content {
            display: inline;
        }

        .pac-card {
            margin: 10px 10px 0 0;
            border-radius: 2px 0 0 2px;
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            outline: none;
            box-shadow: 0 2px 6px rgba(0, 0, 0, 0.3);
            background-color: #fff;
            font-family: Roboto;
        }

        #pac-container {
            padding-bottom: 12px;
            margin-right: 12px;
        }

        .pac-controls {
            display: inline-block;
            padding: 5px 11px;
        }

        .pac-controls label {
            font-family: Roboto;
            font-size: 13px;
            font-weight: 300;
        }

        #pac-input {
            background-color: #fff;
            font-family: Roboto;
            font-size: 15px;
            font-weight: 300;
            margin-left: 12px;
            padding: 0 11px 0 13px;
            text-overflow: ellipsis;
            width: 400px;
        }

        #pac-input:focus {
            border-color: #4d90fe;
        }

        #title {
            color: #fff;
            background-color: #4d90fe;
            font-size: 25px;
            font-weight: 500;
            padding: 6px 12px;
        }

        #target {
            width: 345px;
        }

        .notification-badge {
            position: absolute;
            top: 1px;
            border-radius: 50%;
            right: 4px;
        }

        .icon-container {
            margin-top: 25%;
        }

        .image-preview-block {
            margin: 5px;
            position: relative;
            width: 90px;
            height: 80px;
        }

        .add-new-image-block {
            cursor: pointer;
            margin: 5px;
            width: 90px;
            height: 90px;
            border: 2px dashed;
            border-color: darkgray;
            text-align: center;
            vertical-align: middle;
        }

        .image-upload > input {
            display: none;
        }

        .fa-camera {
            cursor: pointer;
        }

        /*.image-upload img {*/
        /*    width: 120px;*/
        /*    cursor: pointer;*/
        /*}*/

        .rmove-icon {
            position: absolute;
            display: block;
            top: 7px;
            right: 7px;
            cursor: pointer;
            font-size: 18px !important;
            color: red;
        }

        .review-img {
            width: 90px;
            height: 90px;
            /*min-height: 250px ;*/
            /*margin-bottom: 15px;*/
        }

        .review-img-t {
            width: 350px !important;
            width: 250px !important;
        }

    </style>
    <style>
        [v-cloak] > * {
            display: none
        }

        [v-cloak]::before {
            content: "loading…"
        }

        .vue-star-rating-star {
            width: 25px !important;
            height: 25px !important;
        }

        .table td, .table th {
            vertical-align: middle !important;
        }

        .invalid-feedback {
            display: block !important;
        }

        .page-item a {
            cursor: pointer;
        }

        .pagination a {
            cursor: pointer;
            width: 20px;
            height: 30px;
            margin-left: 2px;
            margin-right: 2px;
            text-align: center;
            vertical-align: middle;
            display: table-cell;
            border: 1px solid whitesmoke;
        }


        .loading {
            position: fixed;
            width: 100%;
            height: 100%;
            z-index: 100;
            background: rgba(0, 0, 0, 0.24);
            top: 0;
        }

        .loading i {
            display: block !important;
            margin: 22% auto;
            font-size: 100px;
            color: #36c6d3;
        }

        .clear-image {
            border: 1px solid darkgray;
            text-align: center;
            cursor: pointer;
            align-items: center;
            justify-content: center;
            position: absolute;
            top: auto;
            right: -10px;
            bottom: -5px;
            width: 22px;
            height: 22px;
            border-radius: 50%;
            background-color: #fff;
            box-shadow: 0 0 13px 0 rgba(0, 0, 0, .1);
        }

    </style>


    @stack('css')
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body @if(\Illuminate\Support\Facades\App::getLocale() == 'ar') style="direction: rtl!important;"
      @endif class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Header Mobile -->
<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
    <div class="kt-header-mobile__logo">
        {{--        <a href="index.html">--}}
        {{--            <img alt="Logo" src="{{asset('assets/media/logos/logo-6.png')}}" />--}}
        {{--        </a>--}}
    </div>
    <div class="kt-header-mobile__toolbar">
        <button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left"
                id="kt_aside_mobile_toggler"><span></span></button>
        <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
                    class="flaticon-more"></i></button>
    </div>
</div>

<!-- end:: Header Mobile -->

<!-- begin:: Root -->
<div class="kt-grid kt-grid--hor kt-grid--root">

    <!-- begin:: Page -->
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <!-- begin:: Aside -->
        <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
        <div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop"
             id="kt_aside">

            <!-- begin::Aside Brand -->
            <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
                <div class="kt-aside__brand-logo">
                    <a href="/">
                        {{--                        <img alt="Logo" src="{{asset('assets/media/logos/logo-6.png')}}" />--}}
                    </a>
                </div>
                <div class="kt-aside__brand-tools">
                    <button class="kt-aside__brand-aside-toggler kt-aside__brand-aside-toggler--left"
                            id="kt_aside_toggler"><span></span></button>
                </div>
            </div>

            <!-- end:: Aside Brand -->

            <!-- begin:: Aside Menu -->
            <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
                <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1"
                     data-ktmenu-dropdown-timeout="500">
                    <ul class="kt-menu__nav ">
                        @include('layouts.menu')
                    </ul>
                </div>
            </div>

            <!-- end:: Aside Menu -->


        </div>

        <!-- end:: Aside -->

        <!-- begin:: Wrapper -->
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            @include('layouts.headers')
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

                <!-- begin:: Subheader -->
                <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                    <div class="kt-container  kt-container--fluid ">
                        {{--                        <div class="kt-subheader__main">--}}
                        {{--                            <h3 class="kt-subheader__title">Dashboard</h3>--}}
                        {{--                            <span class="kt-subheader__separator kt-hidden"></span>--}}
                        {{--                            <div class="kt-subheader__breadcrumbs">--}}
                        {{--                                <a href="#" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>--}}
                        {{--                                <span class="kt-subheader__breadcrumbs-separator"></span>--}}
                        {{--                                <a href="" class="kt-subheader__breadcrumbs-link">--}}
                        {{--                                    Dashboards </a>--}}
                        {{--                                <span class="kt-subheader__breadcrumbs-separator"></span>--}}
                        {{--                                <a href="" class="kt-subheader__breadcrumbs-link">--}}
                        {{--                                    Navy Aside </a>--}}

                        {{--                                <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span>--}}
                        {{--                            </div>--}}
                        {{--                        </div>--}}

                    </div>
                </div>

                <!-- end:: Subheader -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                    <div id="app" v-cloak>

                        @yield('content')

                    </div>

                    @yield('main')


                </div>

                <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            <div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop">
                <div class="kt-container  kt-container--fluid ">
                    <div class="kt-footer__copyright">
                        {{date('Y')}}&nbsp;&copy;&nbsp; Copywrite saved
                    </div>

                </div>
            </div>

            <!-- end:: Footer -->
        </div>

        <!-- end:: Wrapper -->
    </div>

    <!-- end:: Page -->
</div>

<!-- end:: Root -->


<!-- begin:: Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="la la-arrow-up"></i>
</div>

<!-- end:: Scrolltop -->


<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "metal": "#c4c5d6",
                "light": "#ffffff",
                "accent": "#00c5dc",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995",
                "focus": "#9816f4"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>

<!-- end::Global Config -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="{{asset('assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
{{--<script src="{{asset('assets/plugins/custom/fullcalendar/fullcalendar.bundle.js')}}" type="text/javascript"></script>--}}

<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->

{{--<script type="text/javascript"--}}
{{--        src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDcoLE5fsqEY5OTx5Dd1mQL__9WFmSNt2Q&v=3&libraries=geometry">--}}

{{--</script>--}}



{{--<script src="{{asset('assets/js/pages/dashboardTrackingDrivers')}}" type="text/javascript"></script>--}}
<script src="{{ asset('assets/plugins/bootstrap-sweetalert/sweetalert.min.js') }}" type="text/javascript"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-messaging.js"></script>
<script src="https://www.gstatic.com/firebasejs/7.14.6/firebase-database.js"></script>
@stack('js')

<!--end::Page Scripts -->
{{--<script type="text/javascript">--}}
{{--    jQuery(document).ready(function() {--}}
{{--        KTDashboard.init();--}}
{{--    });--}}
{{--</script>--}}
<div id="loading-div" class="loading" style="display: none;">
    <i style="color: #fff;" class="fa fa-spinner fa-pulse fa-3x fa-fw"></i>
    <span class="sr-only">Loading...</span>
</div>

<script>

    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": false,
        "positionClass": "toast-bottom-right",
        "preventDuplicates": false,
        "showDuration": "15000",
        "hideDuration": "1000",
        "timeOut": "5000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
        "progressBar": true,
    };


    // Initialize Firebase
    var firebaseConfig = {
        apiKey: "AIzaSyBqa_2QFsxsPN5rswPSF06tLzTWDBjxS9w",
        authDomain: "orderstation-1156e.firebaseapp.com",
        databaseURL: "https://orderstation-1156e-default-rtdb.firebaseio.com",
        projectId: "orderstation-1156e",
        storageBucket: "orderstation-1156e.appspot.com",
        messagingSenderId: "876199841719",
        appId: "1:876199841719:web:ad1dfeb057dcfb2052e232",
        measurementId: "G-XK4VHBYT7M"
    };

    firebase.initializeApp(firebaseConfig);
    if (!firebase.apps.length) {
        firebase.initializeApp(firebaseConfig);
    }
    const messaging = firebase.messaging();

    let audio = window.audio = new Audio('/sound.mp3');
    messaging.requestPermission()
        .then(function () {
            return messaging.getToken()
        })
        .then(function (token) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            $.ajax({
                url: '{{ URL::to('/save-device-token') }}',
                type: 'POST',
                data: {
                    user_id: {!! json_encode($user_id ?? '') !!},
                    fcm_token: token,

                },
                dataType: 'JSON',
                success: function (response) {
                    // console.log(response)
                },
                error: function (err) {
                    console.log(" Can't do because: " + err);
                },
            });
        })
        .catch(function (err) {
            console.log("Unable to get permission to notify.", err);
        });

    messaging.onMessage(function (payload) {

        toastr.options = {
            onclick: function () {
                window.location = payload.notification.click_action;
            }
        };
        toastr.success(payload.notification.title, payload.notification.body);

        window.audio.play();
    });


</script>


@if(isset($js_file))
    <script src="{{ asset('js/'.$js_file) }}" defer></script>
@else
    <script src="{{ asset('js/app.js') }}" defer></script>
@endif
</body>

<!-- end::Body -->
</html>
