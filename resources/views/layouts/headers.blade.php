<!-- begin:: Header -->
<style>

</style>
<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

    <!-- begin:: Header Menu -->
    <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
    <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper" style="opacity: 1;">
        <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout- ">

        </div>
    </div>

    <!-- end:: Header Menu -->

    <!-- begin:: Header Topbar -->
    <div class="kt-header__topbar">


    @php
        $user=auth()->user();
        $viewOrder=['New orders notifications'];
        $viewOffer=['New offers requests notifications'];
        $viewContactUsNotification=['New contact messages notifications'];
        $viewdriversRoles=['New drivers requests notifications'];
        $countries= \App\Models\Country::get()->pluck('code');
               foreach($countries as $country) {
                array_push($viewOrder,'New orders notifications ' . $country);
                array_push($viewOffer,'New offers requests notifications '.$country);
                array_push($viewContactUsNotification,'New contact messages notifications '.$country);
                array_push($viewdriversRoles,'New drivers requests notifications '.$country);
               }
    @endphp
        <!--begin: Notifications -->
        @if(auth()->user()->role=='admin')

            @if(auth()->user()->hasAnyPermission($viewdriversRoles))
                <div class="kt-header__topbar-item dropdown">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="30px,0px">
                                    <span class="kt-header__topbar-icon">
                                        <i class="flaticon2-bell-alarm-symbol"></i>
                                              <span class="notification-badge badge badge-secondary">{{auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\SignupDrivers')->count()}}</span>

    {{--										<span class="kt-badge kt-badge--dot kt-badge--notify kt-badge--sm kt-badge--brand"></span>--}}
                                    </span>
            </div>
            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-lg">
                <form>
                    <div class="kt-head" style="background-image: url('/assets/media/misc/head_bg_sm.jpg')">
                        <h3 class="kt-head__title">New Drivers</h3>
                        <div class="kt-head__sub"><span class="kt-head__desc">{{auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\SignupDrivers')->count()}} new notifications</span></div>
                    </div>
                    <div class="kt-notification kt-margin-t-30 kt-margin-b-20 kt-scroll ps" data-scroll="true" data-height="270" data-mobile-height="220" style="height: 270px; overflow: hidden;">
                        <?php
                        $notifications=auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\SignupDrivers')->take(10);
                        ?>
                        @foreach($notifications as $notification)
                                @isset($notification->data['country_id'])
                                    <a href="{{route('mange.delivery.documented',['countryId'=>$notification->data['country_id']])}}" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-line-chart kt-font-success"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                #{{$notification->data['title']??''}}
                                            </div>
                                            <div class="kt-notification__item-time">
                                                {{\Carbon\Carbon::parse($notification->created_at)->timezone('Asia/Jerusalem')->format('d-m-Y H:m:i a')}}

                                            </div>
                                        </div>
                                    </a>
                                @endisset
                        @endforeach







                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
                </form>
            </div>
        </div>
            @endif
                @if(auth()->user()->hasAnyPermission($viewContactUsNotification))
                <div class="kt-header__topbar-item dropdown">
                    <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="30px,0px">
                                        <span class="kt-header__topbar-icon">
                                            <i class="flaticon2-bell-alarm-symbol"></i>
                                            <span class="notification-badge badge badge-secondary">{{auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\NewMessage')->count()}}</span>

                                        </span>
                    </div>
                    <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-lg">
                        <form>
                            <div class="kt-head" style="background-image: url(/assets/media/misc/head_bg_sm.jpg)">
                                <h3 class="kt-head__title">New contact messages</h3>
                                <div class="kt-head__sub"><span class="kt-head__desc">{{auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\NewMessage')->count()}} new Messages</span></div>
                            </div>
                            <div class="kt-notification kt-margin-t-30 kt-margin-b-20 kt-scroll ps" data-scroll="true" data-height="270" data-mobile-height="220" style="height: 270px; overflow: hidden;">
                                <?php
                                $notifications=auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\NewMessage')->take(10);
                                ?>
                                @foreach($notifications as $notification)
                                    <a href="{{route('contacts.index',['countryId'=>$notification->data['country_id']])}}" class="kt-notification__item">
                                        <div class="kt-notification__item-icon">
                                            <i class="flaticon2-line-chart kt-font-success"></i>
                                        </div>
                                        <div class="kt-notification__item-details">
                                            <div class="kt-notification__item-title">
                                                #{{$notification->data['title']??''}}
                                            </div>
                                            <div class="kt-notification__item-time">
                                                {{\Carbon\Carbon::parse($notification->created_at)->timezone('Asia/Jerusalem')->format('d-m-Y H:m:i a')}}

                                            </div>
                                        </div>
                                    </a>
                                @endforeach







                                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
                        </form>
                    </div>
                </div>
            @endcan
            @if(auth()->user()->hasAnyPermission($viewOffer))
                <div class="kt-header__topbar-item dropdown">
                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="30px,0px">
                                        <span class="kt-header__topbar-icon">
                                            <i class="flaticon2-bell-alarm-symbol"></i>
                                              <span class="notification-badge badge badge-secondary">{{auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\NewOffer')->count()}}</span>

    {{--										<span class="kt-badge kt-badge--dot kt-badge--notify kt-badge--sm kt-badge--brand"></span>--}}
                                        </span>
                </div>
                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-lg">
                    <form>
                        <div class="kt-head" style="background-image: url(/assets/media/misc/head_bg_sm.jpg)">
                            <h3 class="kt-head__title">New Offers</h3>
                            <div class="kt-head__sub"><span class="kt-head__desc">{{auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\NewOffer')->count()}} new Offers</span></div>
                        </div>
                        <div class="kt-notification kt-margin-t-30 kt-margin-b-20 kt-scroll ps" data-scroll="true" data-height="270" data-mobile-height="220" style="height: 270px; overflow: hidden;">
                            <?php
                            $notifications=auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\NewOffer')->take(10);
                            ?>
                            @foreach($notifications as $notification)

                                <a href="{{route('mange.offers.index',['countryId'=>$notification->data['country_id']])}}" class="kt-notification__item">
                                    <div class="kt-notification__item-icon">
                                        <i class="flaticon2-line-chart kt-font-success"></i>
                                    </div>
                                    <div class="kt-notification__item-details">
                                        <div class="kt-notification__item-title">
                                            new offer
                                        </div>
                                        <div class="kt-notification__item-time">
                                            {{\Carbon\Carbon::parse($notification->created_at)->timezone('Asia/Jerusalem')->format('d-m-Y H:m:i a')}}

                                        </div>
                                    </div>
                                </a>
                            @endforeach







                            <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>
                    </form>
                </div>
            </div>
                @endif
       @endif


        @if(auth()->user()->hasAnyPermission($viewOrder) || auth()->user()->role =="branch" || auth()->user()->role=='shop')


          <div class="kt-header__topbar-item dropdown">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="30px,0px">
									<span class="kt-header__topbar-icon">
										<i class="flaticon2-bell-alarm-symbol"></i>
                                         <span class="notification-badge badge badge-secondary">{{auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\SignupActivate')->count()}}</span>

{{--										<span class="kt-badge kt-badge--dot kt-badge--notify kt-badge--sm kt-badge--brand"></span>--}}
									</span>
            </div>
            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-lg">

                    <div class="kt-head" style="background-image: url(/assets/media/misc/head_bg_sm.jpg)">
                        <h3 class="kt-head__title">New Orders Notifications</h3>
                        <div class="kt-head__sub"><span class="kt-head__desc">{{auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\SignupActivate')->count()}} new notifications</span></div>
                    </div>
                    <div class="kt-notification kt-margin-t-30 kt-margin-b-20 kt-scroll ps " data-scroll="true" data-height="270" data-mobile-height="220" style="height: 270px; overflow: hidden;">
                       <?php
                            $notifications=auth()->user()->notifications->whereNull('read_at')->where('type','App\Notifications\SignupActivate')->take(10);
                        ?>

                               @foreach($notifications as $notification)
                                   @if(isset($notification->data['order_id']))
                               <a href="{{route('mange.orders.store.view.order',$notification->data['order_id'])}}" class="kt-notification__item">
                                   <div class="kt-notification__item-icon">
                                       <i class="flaticon2-line-chart kt-font-success"></i>
                                   </div>
                                   <div class="kt-notification__item-details">
                                       <div class="kt-notification__item-title">
                                           #{{$notification->data['order_no']??''}} New order has been received
                                       </div>
                                       <div class="kt-notification__item-time">
                                           {{\Carbon\Carbon::parse($notification->created_at)->timezone('Asia/Jerusalem')->format('d-m-Y H:m:i a')}}

                                       </div>
                                   </div>
                               </a>
                                   @else
                                        <div class="kt-notification__item">
                                       <div class="kt-notification__item-icon">
                                               <i class="flaticon2-line-chart kt-font-success"></i>
                                           </div>
                                           <div class="kt-notification__item-details">
                                               <div class="kt-notification__item-title">
                                                   {{$notification->data['title']}}
                                               </div>
                                               <div class="kt-notification__item-time">
                                                   {{\Carbon\Carbon::parse($notification->created_at)->timezone('Asia/Jerusalem')->format('d-m-Y H:m:i a')}}

                                               </div>
                                           </div>
                                        </div>

                                   @endif
                               @endforeach













                        <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 0px;"></div></div></div>

            </div>
        </div>


    @endif

        <!--end: Notifications -->

        <!--begin: Quick Actions -->
{{--        <div class="kt-header__topbar-item">--}}
{{--            <div class="kt-header__topbar-wrapper" id="kt_offcanvas_toolbar_quick_actions_toggler_btn">--}}
{{--                <span class="kt-header__topbar-icon"><i class="flaticon2-gear"></i></span>--}}
{{--            </div>--}}
{{--        </div>--}}
        <!--end: Quick Actions -->

        <!--begin: User Bar -->
        <!--begin: User Bar -->
        <div class="kt-header__topbar-item kt-header__topbar-item--user">
            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px" >

                <!--use "kt-rounded" class for rounded avatar style-->
                <div class="kt-header__topbar-user kt-rounded-">
                    <span class="kt-header__topbar-welcome kt-hidden-mobile">Hi,</span>
                    <span class="kt-header__topbar-username kt-hidden-mobile">{{auth()->user()->name}}</span>
                    <img alt="Pic"src="{{auth()->user()->avatar_url}}" class="kt-rounded-" />

                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                    <span class="kt-badge kt-badge--username kt-badge--lg kt-badge--brand kt-hidden kt-badge--bold">S</span>
                </div>
            </div>
            <div class=" dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-sm">
                <div class="kt-user-card kt-margin-b-40 kt-margin-b-30-tablet-and-mobile" style="background-image: url({{asset('assets/media/misc/head_bg_sm.jpg')}})">
                    <div class="kt-user-card__wrapper">
                        <div class="kt-user-card__pic">

                            <!--use "kt-rounded" class for rounded avatar style-->
                            <img alt="Pic" src="{{auth()->user()->avatar_url}}" class="kt-rounded-" />
                        </div>
                        <div class="kt-user-card__details">
                            <div class="kt-user-card__name">{{auth()->user()->name}}</div>
                            <div class="kt-user-card__position"></div>
                        </div>
                    </div>
                </div>
                <ul class="kt-nav kt-margin-b-10">
                    @if(auth()->user()->role =="admin")

                    <li class="kt-nav__item">
                        <a href="{{route('profile.edit')}}" class="kt-nav__link">
                            <span class="kt-nav__link-icon"><i class="flaticon2-calendar-3"></i></span>
                            <span class="kt-nav__link-text">My Profile</span>
                        </a>
                    </li>
                    @endif
                    @if(auth()->user()->role =="shop")

                        <li class="kt-nav__item">
                            <a href="{{route('shop.settings')}}" class="kt-nav__link">
                                <span class="kt-nav__link-icon"><i class="flaticon2-calendar-3"></i></span>
                                <span class="kt-nav__link-text">Shop profile</span>
                            </a>
                        </li>
                    @endif

                    <li class="kt-nav__item">
                        <a href="{{route('change.password')}}" class="kt-nav__link">
                            <span class="kt-nav__link-icon"><i class="flaticon2-lock"></i></span>
                            <span class="kt-nav__link-text">Change password</span>
                        </a>
                    </li>

                    <li class="kt-nav__separator kt-nav__separator--fit"></li>
                    <li class="kt-nav__custom kt-space-between">

                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>

                        {{--                                    <a href="custom/login/login-v1.html" target="_blank" class="btn btn-label-brand btn-upper btn-sm btn-bold">Sign Out</a>--}}
                        {{--                                    <i class="flaticon2-information kt-label-font-color-2" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Click to learn more..."></i>--}}
                    </li>
                </ul>
            </div>
        </div>



    </div>

    <!-- end:: Header Topbar -->
</div>
<!-- end:: Header -->
