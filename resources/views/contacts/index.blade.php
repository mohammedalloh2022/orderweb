@extends('layouts.dashboard')
@section('content')
    <contacts-list :fetch-data-url="'{{route('contacts.search',['countryId'=>$countryId])}}'" inline-template>

        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Contacts list
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">

                        <a href="{{url()->previous()}}" class="btn btn-clean btn-bold btn-upper btn-font-sm kt-hidden">
                            <i class="la la-long-arrow-left"></i>
                            Back
                        </a>



                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">



                <div id="kt_table_1_wrapper" class="table table-striped m-table">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">

                        </div>
                        <div class="col-sm-12 col-md-6">

                        </div>
                    </div>
                    <div class="row">


                        <div class="col-sm-12">
                            <table-component

                                :data="fetchData"
                                :table-class="'table table-hover '"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                sort-by="created_at"
                                :show-filter="false"
                                {{--                                         :sort-order="name_en"--}}
                                ref="categoriesTable"
                            >
                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="category">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(category)+1}}
                                    </template>
                                </table-column>

                                <table-column label="Name" show="name"></table-column>
                                <table-column label="Email" show="email"></table-column>
                                <table-column label="Subject" show="subject"></table-column>
                                <table-column label="Message" show="message"></table-column>
                                <table-column label="From app" >
                                    <template scope="message">
                                        @{{ message.user.role=='client'?message.user.role : 'driver' }}
                                    </template>
                                </table-column>
                                <table-column label="Action" show="message">
                                    <template scope="contact">
                                        <a href="javascript:;" class="btn btn-primary" @click="showContactRepliesList(contact)">
                                            Replies (@{{ contact.replies_count??'' }})
                                        </a>
                                    </template>
                                </table-column>







                            </table-component>


                        </div>
                    </div>

                </div>






            </div>
            <el-dialog
                @close="clearCategoryModalData"
                width="30%"
                :title="'Show replies list'"
                :visible.sync="innerVisible"
                append-to-body>

                <div class="list-group">
                    <a href="#" class="list-group-item list-group-item-action flex-column align-items-start "
                    v-for="replay in repliesList">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1"></h5>
                            <small>@{{ replay.created_at }}</small>
                        </div>
                        <p class="mb-1">
                            @{{ replay.replay }}
                        </p>


                    </a>

                </div>
                <br/>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group"
                                 :class="{ 'has-error':  form.validations.replay }">
                                <label class="control-label">Replay</label>
                                <textarea type="text" class="form-control" placeholder="Replay"
                                       v-model="replay"></textarea>
                                <span v-if="form.validations.replay" class="help-block invalid-feedback show">
                                    @{{ form.validations.replay[0] }}</span>

                            </div>
                        </div>
                    </div>


                </div>




                <div slot="footer" class="dialog-footer" >
                    <el-button class="btn btn-primary" class="AddCategory" @click="sendReplay"
                               :disabled="form.disabled">replay</el-button>
                    <el-button @click="innerVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
            </el-dialog>

        </div>
    </contacts-list>
@endsection
