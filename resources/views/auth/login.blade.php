<!DOCTYPE html>


<html lang="en">

<!-- begin::Head -->
<head>
    <base href="../../">
    <meta charset="utf-8" />
    <title>4Station app</title>
    <meta name="description" content="User login example">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">

    <!--end::Fonts -->

    <!--begin::Page Custom Styles(used by this page) -->
    <link href="{{asset('assets/css/pages/login/login-v2.css')}}" rel="stylesheet" type="text/css" />

    <!--end::Page Custom Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{asset('assets/plugins/global/plugins.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />

    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="{{asset('assets/css/skins/header/base/light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/skins/header/menu/light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/skins/brand/navy.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{asset('assets/css/skins/aside/navy.css')}}" rel="stylesheet" type="text/css" />

    <!--end::Layout Skins -->
{{--    <link rel="shortcut icon" href="{{asset('assets/media/logos/favicon.ico')}}" />--}}
</head>

<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-login-v2--enabled kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid__item   kt-grid__item--fluid kt-grid  kt-grid kt-grid--hor kt-login-v2" id="kt_login_v2">

        <!--begin::Item-->
        <div class="kt-grid__item  kt-grid--hor">


        </div>

        <!--end::Item-->

        <!--begin::Item-->
        <div class="kt-grid__item  kt-grid  kt-grid--ver  kt-grid__item--fluid text-center">

            <!--begin::Body-->
            <div class="kt-login-v2__body">

                <!--begin::Wrapper-->
                <div class="kt-login-v2__wrapper justify-content-center" style="width: 100% !important;">
                    <div class="kt-login-v2__container">

                        <img src="{{asset('imgs/4station.jpeg')}}" alt="" style="width: 150px;height: 150px;border-radius: 50%;margin-top:50px;" class="justify-content-center; ">
                        <div class="kt-login-v2__title">
                            <h3>Sign to Account</h3>
                        </div>
                        @if (session('message'))
                            <div class="alert alert-danger">{{ session('message') }}</div>
                        @endif

                        <!--begin::Form-->
                        <form class="kt-login-v2__form kt-form" method="post" autocomplete="off" action="{{ route('login') }}">
                            @csrf


                            <div class="form-group">
                                <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">

                                <input id="password"  placeholder="Password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>

                            <!--begin::Action-->
                            <div class="kt-login-v2__actions">
{{--                                <a href="#" class="kt-link kt-link--brand">--}}
{{--                                    Forgot Password ?--}}
{{--                                </a>--}}
                                <button type="submit" class="btn btn-brand btn-elevate btn-pill">
                                    {{ __('Login') }}
                                </button>

                            </div>

                            <!--end::Action-->
                        </form>

                        <!--end::Form-->

                        <!--begin::Separator-->
                        <div class="kt-separator kt-separator--space-lg  kt-separator--border-solid"></div>

                        <!--end::Separator-->


                    </div>
                </div>

                <!--end::Wrapper-->


            </div>

            <!--begin::Body-->
        </div>

        <!--end::Item-->

    </div>
</div>

<!-- end:: Page -->

<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "metal": "#c4c5d6",
                "light": "#ffffff",
                "accent": "#00c5dc",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995",
                "focus": "#9816f4"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>

<!-- end::Global Config -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="{{asset('assets/plugins/global/plugins.bundle.js')}}" type="text/javascript"></script>
<script src="{{asset('assets/js/scripts.bundle.js')}}" type="text/javascript"></script>

<!--end::Global Theme Bundle -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{asset('assets/js/pages/custom/user/login.js')}}" type="text/javascript"></script>

<!--end::Page Scripts -->
</body>

<!-- end::Body -->
</html>
