@extends('layouts.dashboard')

@push('css')

@endpush
@section('content')
<shop-settings :shop="{{$shop?json_encode($shop):null}}" inline-template>
    <div>

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Shop update
                    </h3>
                </div>

            </div>
            <div class="kt-portlet__body">
                <div class="kt-portlet__content">
                    <form class="kt-form kt-form--label-right">


                        <div class="form-body">
                            <div class="row">
                                <div class="col-7">

                                    <div class="row">

                                        <div class="col-6">


                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-4 col-form-label">Logo</label>
                                                <div class="col-8">

                                                    <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                                         id="kt_profile_avatar_3">
                                                        <div class="kt-avatar__holder"
                                                             style="background-image: url(/uploads/shops/default.png); ">
                                                            <img v-if='shop_logo' :src="shop_logo" alt="shop_log" style="    height: 114px;
    width: 114px;"/>
                                                        </div>
                                                        <label class="kt-avatar__upload" title="Change avatar">
                                                            <i class="fa fa-pen"></i>
                                                            <input type='file' name="logo" @change="onFileChange($event,'logo')"
                                                                   accept=".png, .jpg, .jpeg"/>
                                                        </label>
                                                        <span class="clear-image" title="Cancel avatar" v-if="shop_logo && !id"
                                                              @click.prevent="removeImage('logo')">
                                            <i class="fa fa-times"></i>
                                        </span>
                                                    </div>
                                                    <span v-if="form.error && form.validations.shop_logo"
                                                          class="help-block invalid-feedback">@{{ form.validations.shop_logo[0] }}</span>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="form-group row">
                                                <label for="example-text-input" class="col-4 col-form-label">Cover</label>
                                                <div class="col-8">

                                                    <div class="kt-avatar kt-avatar--outline kt-avatar--success"
                                                         id="kt_profile_avatar_4">
                                                        <div class="kt-avatar__holder"
                                                             style="background-image: url(/uploads/shops/cover_default.png)">
                                                            <img v-if='shop_cover' :src="shop_cover" alt="shop_log" style="    height: 114px;
    width: 114px;"/>
                                                        </div>
                                                        <label class="kt-avatar__upload" title="Change avatar">
                                                            <i class="fa fa-pen" for="coverImage"></i>
                                                            <input id="coverImage" type='file' name="coverImage"
                                                                   v-on:change="onFileChange($event,'cover')"
                                                                   accept=".png, .jpg, .jpeg"/>
                                                        </label>
                                                        <span class="clear-image" title="Cancel avatar" v-if="shop_cover && !id"
                                                              @click.prevent="removeImage('cover')">
                                            <i class="fa fa-times"></i>
                                        </span>
                                                    </div>
                                                    <span v-if="form.error && form.validations.shop_cover"
                                                          class="help-block invalid-feedback">@{{ form.validations.shop_cover[0] }}</span>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Name(En)</label>
                                        <div class="col-10">
                                            <input type="text" class="form-control" placeholder="Shop name(en)"
                                                   v-model="name_en">
                                            <span v-if="form.error && form.validations.name_en"
                                                  class="help-block invalid-feedback">@{{ form.validations.name_en[0] }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Name(Ar)</label>
                                        <div class="col-10">
                                            <input type="text" class="form-control" placeholder="Shop name(en)"
                                                   v-model="name_ar">
                                            <span v-if="form.error && form.validations.name_ar"
                                                  class="help-block invalid-feedback">@{{ form.validations.name_ar[0] }}</span>
                                        </div>
                                    </div>
{{--                                    <div class="form-group row">--}}

{{--                                        <label for="example-text-input" class="col-2 col-form-label">Category</label>--}}
{{--                                            <div class="col-10">--}}
{{--                                                <select class="form-control" id="exampleSelectd" v-model="cat_id">--}}
{{--                                                    <option v-for="cat in categories_list" :value="cat.id">--}}
{{--                                                        @{{ cat.name }}--}}
{{--                                                    </option>--}}
{{--                                                </select>--}}
{{--                                                <span v-if="form.error && form.validations.cat_id"--}}
{{--                                                      class="help-block invalid-feedback">--}}
{{--                                                   @{{ form.validations.cat_id[0] }}--}}
{{--                                                </span>--}}

{{--                                            </div>--}}
{{--                                    </div>--}}





                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Address(En)</label>
                                        <div class="col-10">
                                            <input type="text" class="form-control" placeholder="Address (En)"
                                                   v-model="address_en">
                                            <span v-if="form.error && form.validations.address_en"
                                                  class="help-block invalid-feedback">@{{ form.validations.address_en[0] }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-2 col-form-label">Address(Ar)</label>
                                        <div class="col-10">
                                            <input type="text" class="form-control" placeholder="Address (Ar)"
                                                   v-model="address_ar">
                                            <span v-if="form.error && form.validations.address_ar"
                                                  class="help-block invalid-feedback">@{{ form.validations.address_ar[0] }}</span>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="example-text-input" class="col-2 col-form-label">mobile</label>
                                        <div class="col-10">
                                            <input type="text" class="form-control" placeholder="Mobile"
                                                   v-model="mobile">
                                            <span v-if="form.error && form.validations.mobile"
                                                  class="help-block invalid-feedback">@{{ form.validations.mobile[0] }}</span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class=" col-6">
                                            <div class="form-group row">

                                                <label for="example-text-input" class="col-4 col-form-label">Open at</label>
                                                <div class="col-8">
                                                    <el-time-select
                                                        v-model="start_work_at"
                                                        :picker-options="{
                                                    start: '05:30',
                                                    step: '00:10',
                                                    end: '23:59'
                                                  }"
                                                        placeholder="Select shop open time">
                                                    </el-time-select>

                                                    <span v-if="form.error && form.validations.start_work_at"
                                                          class="help-block invalid-feedback">@{{ form.validations.start_work_at[0] }}</span>
                                                </div>
                                            </div>
                                        </div>

                                        <div class=" col-6">
                                            <div class="form-group row">


                                                <label for="example-text-input" class="col-4 col-form-label">Close
                                                    at</label>
                                                <div class="col-8">
                                                    <el-time-select
                                                        v-model="end_work_at"
                                                        :picker-options="{
                                                    start: '05:30',
                                                    step: '00:10',
                                                    end: '23:59'
                                                  }"
                                                        placeholder="Select shop close time">
                                                    </el-time-select>

                                                    <span v-if="form.error && form.validations.end_work_at"
                                                          class="help-block invalid-feedback">@{{ form.validations.end_work_at[0] }}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
{{--                                    <div class="form-group row">--}}
{{--                                        <label for="example-text-input" class="col-2 col-form-label">Manger Email</label>--}}
{{--                                        <div class="col-10">--}}
{{--                                            <input type="email" class="form-control" placeholder="User email"--}}
{{--                                                   v-model="user_email">--}}
{{--                                            <span v-if="form.error && form.validations.user_email"--}}
{{--                                                  class="help-block invalid-feedback">@{{ form.validations.user_email[0] }}</span>--}}
{{--                                        </div>--}}
{{--                                    </div>--}}

{{--                                    <div class="form-group row">--}}
{{--                                        <label for="example-text-input" class="col-2 col-form-label">Manger password</label>--}}
{{--                                        <div class="col-10">--}}
{{--                                            <input type="password" class="form-control" placeholder="User password"--}}
{{--                                                   v-model="user_password">--}}
{{--                                            <span v-if="form.error && form.validations.user_password"--}}
{{--                                                  class="help-block invalid-feedback">@{{ form.validations.user_password[0] }}</span>--}}
{{--                                            <span v-if="id"--}}
{{--                                                  class="help-block invalid-feedback">Leave password blank if u don't need change it</span>--}}

{{--                                        </div>--}}
{{--                                    </div>--}}

                                </div>
                                <div class="col-5">
                                    <input
                                        id="pac-input"
                                        class="controls"
                                        type="text"
                                        placeholder="Search Box"
                                    />
                                    <div id="mapLocation" style="height:400px"></div>
                                </div>
                            </div>


                        </div>
                    </form>
                    <div slot="footer" class="dialog-footer" v-if="id">

                        <el-button class="btn btn-primary" class="updateCategory" @click="updateShop">Update</el-button>
                        <a class="btn btn-default" href="{{URL::previous()}}">Cancel</a>
                        {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                    </div>

                </div>
            </div>
        </div>


    </div>

</shop-settings>
@endsection
