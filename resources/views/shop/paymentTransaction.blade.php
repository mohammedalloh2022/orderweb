@extends('layouts.dashboard')

@section('content')

    <shop-branches-list :fetch-data-url="'{{route('shops.branches',$shop->id)}}'"
                    :shop_id="{!! json_encode($shop->id) !!}" inline-template>
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Branches list
                </h3>
            </div>

            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">

                    <a href="{{url()->previous()}}" class="btn btn-clean btn-bold btn-upper btn-font-sm kt-hidden">
                        <i class="la la-long-arrow-left"></i>
                        Back
                    </a>



                </div>
            </div>

        </div>
        <div class="kt-portlet__body">


            <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                <div class="row">
                    <div class="col-sm-12">
                        <table-component
                            :data="fetchData"
                            :table-class="'table table-hover kt-datatable__table'"
                            :filter-input-class="'form-control'"
                            filter-placeholder="Search "
                            :show-caption="false"
                            sort-by="created_at"
                            {{--                                         :sort-order="name_en"--}}
                            ref="categoriesTable"
                        >
                            <table-column label="#" :sortable="false" :filterable="false">
                                <template scope="shop">
                                    @{{((currentPage-1) * per_page)+ data.indexOf(shop)+1}}
                                </template>
                            </table-column>
                            <table-column label="Branch logo" :sortable="false" :filterable="false">
                                <template scope="shop">
                                    <div class="kt-media kt-media--lg">
                                        <img :src="shop.logo_url" width="200px">
                                    </div>

                                </template>
                            </table-column>
                            <table-column label="Branch name" show="name"></table-column>

                            <table-column label="Mobile" show="mobile"></table-column>
                            <table-column label="Wallet ">
                                <template scope="shop">
                                    <a :href="'/shops/wallet/transactions/'+shop.id">
                                        @{{ shop.wallet ??0}}
                                    </a>
                                </template>
                            </table-column>


                        </table-component>
                    </div>
                </div>

            </div>


        </div>

    </div>
</shop-branches-list>
@endsection
