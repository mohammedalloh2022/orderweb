@extends('layouts.dashboard')

@push('css')

@endpush
@section('content')
    @if($page=='order')
        @include('store.orders')
    @endif

    @if($page=='pendingOrders')
        @include('store.pendingOrders')
    @endif

    @if($page=='cat')
        @include('shops.category')
    @endif

    @if($page=='item')
        @include('shops.meals')
    @endif

    @if($page=='branch')
        @include('shops.branchs')
    @endif

    @if($page=='offer')
        @include('shops.offers')
    @endif

    @if($page=='transactions')
        @include('store.branch.payments')
    @endif

@endsection
