@extends('layouts.dashboard')
@section('content')
    <delivery-prices :fetch-data-url="'{{route('prices.search',$countryId)}}'"
                     :country-id='{{@$countryId}}' inline-template>

        <div class="kt-portlet kt-portlet--mobile">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                   Delivery price list
                    </h3>
                </div>
                <div class="kt-portlet__head-toolbar">
                    <div class="kt-portlet__head-wrapper">

                        <a href="{{url()->previous()}}" class="btn btn-clean btn-bold btn-upper btn-font-sm kt-hidden">
                            <i class="la la-long-arrow-left"></i>
                            Back
                        </a>
                        <button type="button" @click="innerVisible = true" class="btn btn-primary">
                            <i class="flaticon2-add-1"></i>
                            Add new price
                        </button>


                    </div>
                </div>
            </div>
            <div class="kt-portlet__body">



                <div id="kt_table_1_wrapper" class="table table-striped m-table">
                    <div class="row">
                        <div class="col-sm-12 col-md-6">

                        </div>
                        <div class="col-sm-12 col-md-6">

                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <table-component

                                :data="fetchData"
                                :table-class="'table table-hover '"
                                :filter-input-class="'form-control'"
                                filter-placeholder="Search "
                                :show-caption="false"
                                sort-by="created_at"
                                :show-filter="false"
                                {{--                                         :sort-order="name_en"--}}
                                ref="categoriesTable"
                            >
                                <table-column label="#" :sortable="false" :filterable="false">
                                    <template scope="category">
                                        @{{((currentPage-1) * per_page)+ data.indexOf(category)+1}}
                                    </template>
                                </table-column>

                                <table-column label="From km" show="from"></table-column>
                                <table-column label="To km" show="to"></table-column>
                                <table-column label="price" show="price"></table-column>
                                <table-column label="actions" :sortable="false" :filterable="false">
                                    <template scope="category">

                                        <a href="javascript:;" class="btn btn-sm btn-clean btn-icon btn-icon-md" @click="mountCat(category)">
                                            <i class="la la-edit" aria-hidden="true"></i>
                                        </a>
                                        |
                                        <a class="btn btn-sm btn-clean btn-icon btn-icon-md" @click="deleteAction('prices/',category.id,true)" href="javascript:;">
                                            <i class="la la-trash" aria-hidden="true" style="color:red;"></i>
                                        </a>

                                    </template>
                                </table-column>

                            </table-component>


                        </div>
                    </div>

                </div>






            </div>
            <el-dialog
                @close="clearCategoryModalData"
                width="30%"
                :title="id?'Update  Delivery price ':'Add new delivery price '"
                :visible.sync="innerVisible"
                append-to-body>
                <div class="form-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group"
                                 :class="{ 'has-error':  form.validations.price_from }">
                                <label class="control-label">From (km)</label>
                                <input type="text" class="form-control" placeholder="from km"
                                       v-model="price_from">
                                <span v-if="form.validations.price_from" class="help-block invalid-feedback show">@{{ form.validations.price_from[0] }}</span>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group"
                                 :class="{ 'has-error': form.validations.price_to }">
                                <label class="control-label">To (km)</label>
                                <input type="text" class="form-control" placeholder="To km"
                                       v-model="price_to">
                                <span  v-if="form.validations.price_to" class="help-block invalid-feedback show">@{{ form.validations.price_to[0] }}</span>
                                {{--                       @{{ form.validations.cat_name_ar }}--}}
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group"
                                 :class="{ 'has-error': form.validations.price }">
                                <label class="control-label">Price</label>
                                <input type="text" class="form-control" placeholder="Price"
                                       v-model="price">
                                <span  v-if="form.validations.price" class="help-block invalid-feedback show">@{{ form.validations.price[0] }}</span>
                                {{--                       @{{ form.validations.cat_name_ar }}--}}
                            </div>
                        </div>

                    </div>


                </div>
                <div slot="footer" class="dialog-footer" v-if="!id">
                    <el-button class="btn btn-primary" class="AddCategory" @click="AddCategory"
                               :disabled="form.disabled">Add</el-button>
                    <el-button @click="innerVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
                <div slot="footer" class="dialog-footer" v-if="id">
                    <el-button class="btn btn-primary" class="updateCategory" @click="updateCategory"
                               :disabled="form.disabled">Update</el-button>
                    <el-button @click="innerVisible = false">Cancel</el-button>
                    {{--                <el-button type="primary" @click="innerVisible = true">open the inner Dialog</el-button>--}}
                </div>
            </el-dialog>

        </div>
    </delivery-prices>
@endsection
