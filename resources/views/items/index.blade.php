@extends('layouts.dashboard')
@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Meals list
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                <div class="row">
                    <div class="col-sm-12 col-md-6">
                    </div>
                    <div class="col-sm-12 col-md-6">
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                        <table
                            class="table table-striped m-table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Meal pic</th>
                                <th>Name(Ar)</th>
                                <th>Name(En)</th>
                                <th>Price</th>
                                <th>Qty</th>
                                <th>Description</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($meals as $key=>$meal)
                                <tr role="row">
                                    <td>{{$key+1}}</td>
                                    <td><img src="{{$meal->image_url}}" width="120"/> </td>
                                    <td>{{$meal->name_ar}}</td>
                                    <td>{{$meal->name_en}}</td>
                                    <td>{{$meal->price}}</td>
                                    <td>{{$meal->qty}}</td>
                                    <td>{{$meal->desc}}</td>

{{--                                    <td>{{$coupon->value}}</td>--}}
{{--                                    <td>{{$coupon->expire_at}}</td>--}}
                                    <td></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-12 col-md-5">
                    </div>
                    <div class="col-sm-12 col-md-7">
                        <div class="dataTables_paginate paging_simple_numbers" id="kt_table_1_paginate">
                            <ul class="pagination">
                                {{$meals->links()}}
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection



