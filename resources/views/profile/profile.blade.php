@extends('layouts.dashboard')
@push('css')


@endpush
@push('js')
    <script type="text/javascript">
        jQuery(document).ready(function () {
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#avatar_profile').attr('src', e.target.result);
                    }

                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                }
            }

            $("#imgInp").change(function() {
                readURL(this);
            });
        });

    </script>
@endpush
@section('content')

    <div class="row">


        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">Update profile  </h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" method="post" action="{{route('profile.update')}}"  enctype="multipart/form-data">
                    @csrf
                    <div class="kt-portlet__body">
                        @if(\Illuminate\Support\Facades\Session::get('message'))
                            <div class="alert alert-primary" role="alert">
                                <div class="alert-icon">
                                    <i class="flaticon2-check-mark"></i>
                                </div>
                                <div class="alert-text">
                                    {{  \Illuminate\Support\Facades\Session::get('message')}}
                                </div>
                            </div>
                        @endif

                            @if(\Illuminate\Support\Facades\Session::get('error'))
                                <div class="alert alert-danger" role="alert">
                                    <div class="alert-icon">
                                        <i class="flaticon2-check-mark"></i>
                                    </div>
                                    <div class="alert-text">
                                        {{  \Illuminate\Support\Facades\Session::get('error')}}
                                    </div>
                                </div>
                            @endif

                        <div class="form-group row">
                            <label for="example-text-input" class="col-3 col-form-label"> </label>
                            <div class="col-12 text-center" >
                                <div class="kt-avatar kt-avatar--outline kt-avatar--danger kt-avatar--circle" id="kt_profile_avatar_4">

                                    <img class="kt-avatar__holder" style="width: 155px;height: 155px"  src="{{auth()->user()->avatar_url}}" id="avatar_profile"/>

                                    <label class="kt-avatar__upload" data-toggle="kt-tooltip" title="Change avatar">
                                        <i class="fa fa-pen"></i>
                                        <input type='file' name="avatar" id="imgInp" accept=".png, .jpg, .jpeg" />
                                    </label>
                                    <span class="kt-avatar__cancel" data-toggle="kt-tooltip" title="Cancel avatar">
                                        <i class="fa fa-times"></i>
                                    </span>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="exampleInputPassword1">Name</label>
                            <input type="text" class="form-control @error('name')) is-invalid @enderror"
                                   placeholder="Name"
                                   name="name"
                                   required
                                   lang="en"
                                   value="{{auth()->user()->name}}"

                            >

                            @error('name')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>


                        <div class="form-group">
                            <label for="exampleInputPassword1">Email</label>
                            <input type="text" class="form-control @error('email')) is-invalid @enderror"
                                   placeholder="Email"
                                   name="email"

                                   value="{{auth()->user()->email}}"

                            >
                            @error('email')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror


                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">Mobile</label>
                            <input type="text" class="form-control @error('mobile')) is-invalid @enderror"
                                   placeholder="Mobile"
                                   name="mobile"
                                   lang="en"
                                   value="{{auth()->user()->mobile}}"

                            >
                            @error('mobile')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror


                        </div>



                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-primary">Update profile</button>
{{--                            <a href="{{route('home')}}" class="btn btn-secondary">Cancel</a>--}}
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->


        </div>
    </div>
@endsection
