@extends('layouts.dashboard')
@push('css')


@endpush
@section('content')

    <div class="row">


        <div class="col-md-6">

            <!--begin::Portlet-->
            <div class="kt-portlet">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">update password</h3>
                    </div>
                </div>

                <!--begin::Form-->
                <form class="kt-form" method="post" action="{{route('update.password')}}" >
                    @csrf
                    <div class="kt-portlet__body">
                        <div class="form-group form-group-last">
                            {{--                        @if($errors->any())--}}
                            {{--                            {{ implode('', $errors->all('<div>:message</div><br/>')) }}--}}
                            {{--                        @endif--}}
                            {{--                        @if($errors->has('firstname'))--}}
                            {{--                            <div class="error">{{ $errors->first('firstname') }}</div>--}}
                            {{--                        @endif--}}

                            @if(\Illuminate\Support\Facades\Session::get('message'))
                                <div class="alert alert-primary" role="alert">
                                    <div class="alert-icon">
                                        <i class="flaticon2-check-mark"></i>
                                    </div>
                                    <div class="alert-text">
                                        {{  \Illuminate\Support\Facades\Session::get('message')}}
                                    </div>
                                </div>
                            @endif


                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Old password</label>
                            <input type="password" class="form-control @error('old_password')) is-invalid @enderror" id="exampleInputPassword1"
                                   placeholder="Old password"
                                   name="old_password"
                                   required>

                            @error('old_password')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1">New password</label>
                            <input type="password" class="form-control @error('new_password')) is-invalid @enderror" id="exampleInputPassword1"
                                   placeholder="New password"
                                   name="new_password"
                                   required>
                            @error('new_password')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="exampleInputPassword1">Confirm new password</label>
                            <input type="password" class="form-control @error('new_password_confirmation')) is-invalid @enderror" id="exampleInputPassword1"
                                   placeholder="New password confirmation"
                                   name="new_password_confirmation"
                                   required>
                            @error('new_password_confirmation')
                            <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>



                    </div>
                    <div class="kt-portlet__foot">
                        <div class="kt-form__actions">
                            <button type="submit" class="btn btn-primary">Change password</button>
                            <a href="{{URL::previous() }}" class="btn btn-secondary">Cancel</a>
                        </div>
                    </div>
                </form>

                <!--end::Form-->
            </div>

            <!--end::Portlet-->


        </div>
    </div>
@endsection
