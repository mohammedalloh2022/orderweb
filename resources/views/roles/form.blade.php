@extends('layouts.dashboard')

@push('css')

@endpush
@section('content')
<role-permission-form
                       :role-id="{{json_encode($role)}}" inline-template>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Role permissions
                </h3>
            </div>

        </div>
        <div class="kt-portlet__body">


            <table class="row">

            </table>
            <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                <div class="row">
                    <div class="col-sm-12">

                        <div class="row">
                            <div id="accordion" class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group row">
                                            <label  class="col-2 col-form-label">Role name</label>
                                            <div class="col-10">
                                                <input type="text"
                                                       v-model="roleName"
                                                       class="form-control"
                                                       name="roleName"
                                                       data-vv-scope="formValidate"
                                                       aria-describedby="emailHelp"
                                                >

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4"  v-for="(permissionGroup,index) in allWebPermissions">
                                        <div class="card "  style="margin-bottom: 10px">
                                            <div class="card-header" :id="'headingOne'+index">
                                                <h5 class="mb-0">
                                                    <a href="" data-toggle="collapse" :data-target="'#collapseOne'+index" aria-expanded="true" :aria-controls="'#collapseOne'+index">
                                                        @{{index}}
                                                    </a>
                                                </h5>
                                            </div>

                                            <div :id="'headingOne'+index" class="collapse show" aria-labelledby="headingOne" :data-parent="'#accordion'+index">
                                                <div class="card-body">
                                                    <div class="row">

                                                        <div >
                                                            <div class="list-group-item" v-for="permission in permissionGroup ">
                                                                <label class="kt-checkbox kt-checkbox--bold kt-checkbox--success">
                                                                    <input type="checkbox" v-model="checkedPermission" :value="permission.id">
                                                                    @{{permission.name}}
                                                                    <span></span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="kt-portlet__foot">

                            <div class="kt-form__actions">
                                <button v-if="roleId" type="button" class="btn btn-primary" @click="updateRole()" :disabled="isDisableAction">
                                  Update
                                </button>
                                <button v-else type="button" class="btn btn-primary" @click="saveRole()" :disabled="isDisableAction">
                                   Save
                                </button>
                            </div>
                        </div>
                    </div>



                    </div>
                </div>




        </div>


    </div>
</role-permission-form>

@endsection
