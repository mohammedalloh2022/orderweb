@extends('layouts.dashboard')

@push('css')

@endpush
@section('content')


    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Rles users
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <a href="{{route('mange.admin.role.edit')}}" type="button" class="btn btn-primary">
                        <i class="flaticon2-add-1"></i>
                        Add new Role
                    </a>
                </div>
            </div>


        </div>
        <div class="kt-portlet__body">


            <table class="row">

            </table>
            <div id="kt_table_1_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">

                <div class="row">
                    <div class="col-sm-12">


                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>
                                    #
                                </th>
                                <th>
                                    Role name
                                </th>
                                <th>
                                    Guard name
                                </th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @if($roles)
                                @foreach($roles as $key=>$role)
                                <tr>
                                    <td>
                                        {{$key+1}}
                                    </td>
                                    <td>
                                        {{$role->name}}

                                    </td>
                                    <td>
                                        {{$role->guard_name	}}

                                    </td>
                                    <td>
                                        <a href="{{route('mange.admin.role.edit',$role->id)}}" >
                                            <i class="fa fa-edit"></i>
                                        </a>

                                    </td>

                                </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                        {{$roles->links()}}
                    </div>
                </div>

            </div>


        </div>


    </div>


@endsection
