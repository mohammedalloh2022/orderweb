<?php

return [
    'invalid_entries' => 'Incorrect Mobile No. or Password',
    'inactive_new_user_msg' => 'Waiting for the administration to approve your request so that you can log in',
    'inactive_user_msg' => 'The user has been disabled, please contact the administration',
    'Unauthorized_user' => 'Unauthorized to login',
    'you_exceeded_allowed_credit_limit' => 'Sorry, you have exceeded the allowed credit limit , Please contact the administration',
    'you_have_not_any_order_balance'=>'Your approved order balance has expired .. Please renew to be able to log in',
    'please_choose_country'=>'Please select country to be able to login',

];
