<?php
/**
 * Created by PhpStorm.
 * User: WeSSaM
 * Date: 29/3/2021
 * Time: 6:51 م
 */
return [
    'countries_management' => 'Countries Management',
    'category_management' => 'Category Management',
    'prices_management' => 'Prices Management',
    'attribute_prices' => ':attribute Prices',
    'city' => 'City',
    'actions' => 'Actions',
    'submit' => 'Submit',
    'coupon' => [
        'code' => 'Coupon Code',
        'discount_value' => 'Discount Value',
        'expire_at' => 'Expire at',
        'max_no_of_use' => 'Max number of used'
    ]
];