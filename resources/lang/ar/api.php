<?php

return [
    'invalid_entries'=>'خطأ في كلمة المرور أو رقم الجوال!',
    'inactive_new_user_msg'=>'بإنتظار موافقة الإدارة على طلبك حتى تستطيع تسجيل الدخول',
    'inactive_user_msg'=>'تم تعطيل المستخدم يرجى مراسلة الإدارة',
    'Unauthorized_user'=>'غير مصرح بتسجيل الدخول',
    'you_exceeded_allowed_credit_limit'=>'عذرا,  تجاوزت الحد الإئتماني المسموح .. يرجى مراجعة الإدارة ',
    'you_have_not_any_order_balance'=>'إنتهى رصيد طلباتك المسموحة .. يرجى التجديد لتتمكن من تسجيل الدخول',
    'please_choose_country'=>'الرجاء إختيار دولة لتتمكن من تسجيل الدخول',
];
