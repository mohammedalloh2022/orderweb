Vue.component('dashboardMap', {
    props:['countryId'],
    data() {

        return {
            googleMap: null,
            marker: null,
            lat: 25.286106,
            lng: 51.534817,
            driversLocations: [],
            firebaseDatabase:null,
            driverMarkers:[],

        }
    },
    created() {

        let vm = this;
        var firebaseConfig = {
            apiKey: "AIzaSyD9tZGY7yW4geQVwUipjCqS7UUiZVAE5kI",
            authDomain: "orderstation-33cc9.firebaseapp.com",
            // databaseURL: "https://orderstation-33cc9.firebaseio.com",
            databaseURL: "https://orderstation-33cc9.firebaseio.com",

            projectId: "orderstation-33cc9",
            storageBucket: "orderstation-33cc9.appspot.com",
            messagingSenderId: "342130242310",
            appId: "1:342130242310:web:7faaa7aba0f9abfd78a487",
            measurementId: "G-K52QM335WZ"
        };
        // Initialize Firebase
        if (!firebase.apps.length) {
            firebase.initializeApp(firebaseConfig);
        }
        // firebase.initializeApp(firebaseConfig);
        // firebase.analytics();
        var ref=firebase.database().ref('delivery_app_tracking');
        ref.orderByChild("country_id").equalTo(this.countryId).on("value", function(snapshot) {
            vm.driversLocations=snapshot.val();
            vm.addMarker();
        });




},
mounted()
{
    this.initMap();

},
methods: {

    initMap()
    {
        if (this.googleMap && this.marker.getPosition().lat() == this.lat) {
            return true;
        }
        // var myLatLng = {lat: this.lat, lng: this.lng};
        var myLatLng = new google.maps.LatLng(this.lat, this.lng);
        // var latlng = new google.maps.LatLng(39.305, -76.617);
        // Create a map object and specify the DOM element
        // for display.
        this.googleMap = new google.maps.Map(document.getElementById('dashobard-map'), {
            center: myLatLng,
            zoom: 6
        });
        //
        // this.marker  = new google.maps.Marker({
        //     position: myLatLng,
        //     map: this.googleMap,
        //     title: 'Event location!',
        //     draggable:true,
        // });

        // this.marker.addListener('dragend',this.handleEvent);
    },
    addMarker(){
        let vm=this;



        for (var i = 0; i < this.driverMarkers.length; i++ ) {
            let marker =this.driverMarkers[i];
            marker.setMap(null);
        }
        vm.driverMarkers=[];
        var infowindow = new google.maps.InfoWindow()
        $.each(this.driversLocations, function(key, value) {
            // markers.push(key);

            let marker = new google.maps.Marker({
                position: {lat:parseFloat(value.lat),lng:parseFloat(value.lng)},
                map: vm.googleMap,
                icon: vm.changeMarker(value.status)
            });
            marker.set("id", key);
            marker.set("name", value.name);
            marker.set("mobile", value.mobile);
            google.maps.event.addListener(marker, 'click', (function(marker, key) {
                return function() {
                    infowindow.setContent('<b>'+marker.name +'</b><br>'+ marker.mobile);
                    infowindow.open(vm.googleMap, marker);
                }
            })(marker, key));


            vm.driverMarkers.push(marker);
        });


    },


    // handleEvent(event){
    //     this.lat=event.latLng.lat()
    //     this.lng=event.latLng.lng()
    // },
   changeMarker(status) {
        let vm=this;
           let icon = null;
           switch (status) {
               case 'busy':
                    icon = {
                       url: vm.colorToBusMarker('busy'),
                       scaledSize: new google.maps.Size(50, 50), // scaled size
                   };
                   return icon;
                   break;
               case 'offline':
                    icon = {
                       url: vm.colorToBusMarker('offline'),
                       scaledSize: new google.maps.Size(50, 50), // scaled size
                   };
                   return icon;
                   break;
               case 'online':
                    icon = {
                       url: vm.colorToBusMarker('online'),
                       scaledSize: new google.maps.Size(50, 50), // scaled size
                   };
                   return icon;
                   break;
       }

    },
    colorToBusMarker(color) {
        switch (color) {
            case 'offline':
                return '/imgs/red_marker.png';
            case 'online':
                return '/imgs/green_marker.png';
            case 'busy':
                // return '/imgs/blue_marker.png';
                return '/imgs/red_marker.png';

        }
    },

    getLocation()
    {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(this.showPosition);
        } else {
            alert("Geolocation is not supported by this browser.");
        }
    }
,
    showPosition(position)
    {
        this.lat = position.coords.latitude;
        this.lng = position.coords.longitude;
        // this.googleMap.setCenter(new google.maps.LatLng(lat, lng));
    }
,
}
})
;
