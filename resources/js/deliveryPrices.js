import { TableComponent, TableColumn } from 'vue-table-component';
import style from 'vue-table-component'
import form from '../js/mixins/form'
Vue.component('deliveryPrices', {
    props:['fetchDataUrl','countryId'],
    mixins:[form],
    data() {
        return {
            id:null,
            price_from: null,
            price_to: null,
            price: null,

            form: {
                disabled: false,
                error: false,
                validations: [],
                message: null,

            },
            currentPage:null,
            per_page:null,
            data:[],

        }
    },
    mounted() {
        // this.getAllServices();
    },
    components: {
        'table-component': TableComponent,
        'table-column': TableColumn
    },
    methods: {
        mountCat(category){
            this.id=category.id;
            this.price_from= category.from;
            this.price_to= category.to;
            this.price= category.price;
            this.innerVisible=true;
        },
        refreshTable:function(){
            this.$refs.categoriesTable.refresh();
        },

        async fetchData({ page, filter, sort }) {
            jQuery('#loading-div').css('display','block');
            const response = await axios.get(this.fetchDataUrl, {params: { page, filter, sort }});
            this.currentPage= response.data.categories.current_page;
            this.per_page= response.data.categories.per_page;
            this.data=response.data.categories.data;
            jQuery('#loading-div').css('display','none');
            return {
                data: response.data.categories.data,
                pagination: {
                    currentPage: response.data.categories.current_page,
                    totalPages: response.data.categories.last_page
                }
            };
        },
        AddCategory() {
            jQuery('#loading-div').css('display','block');
            this.form.disabled = true;
            axios.post('/prices', {
                price_from: this.price_from,
                price_to: this.price_to,
                price: this.price,
                countryId: this.countryId,
            }).then(response => {

                // this.innerVisible=false;
                swal({
                    // title: 'sadfdasf',
                    title: response.data.message,
                    text: "",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: false
                });
                this.clearCategoryModalData();
                this.refreshTable();
                this.form.disabled = false;
                jQuery('#loading-div').css('display','none');
            }).catch(error => {
                this.form.disabled = false;
                this.form.error = true;
                if (error.response &&error.response.data.errors) {
                    this.form.message = 'There are error in entry data';
                    this.form.validations = error.response.data.errors;
                } else if (error.response && error.response.data.message) {
                    this.form.validations = [];
                    this.form.message = error.response.data.message;
                }
                document.body.scrollTop = 0; // For Chrome, Safari and Opera
                document.documentElement.scrollTop = 0; // For IE and Firefox
                jQuery('#loading-div').css('display','none');
            });
        },
        updateCategory() {
            jQuery('#loading-div').css('display','block');
            this.form.disabled = true;
            axios.put('/prices/update/' + this.id, {
                price_from: this.price_from,
                price_to: this.price_to,
                price: this.price,
                countryId: this.countryId,
            }).then(response => {

                // this.innerVisible=false;
                swal({
                    // title: 'sadfdasf',
                    title: response.data.message,
                    text: "",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: false
                });
                this.clearCategoryModalData();
                this.refreshTable();
                this.form.disabled = false;
                jQuery('#loading-div').css('display','none');
            }).catch(error => {
                this.form.disabled = false;
                this.form.error = true;
                if (error.response &&error.response.data.errors) {
                    this.form.message = 'There are error in entry data';
                    this.form.validations = error.response.data.errors;
                } else if (error.response && error.response.data.message) {
                    this.form.validations = [];
                    this.form.message = error.response.data.message;
                }
                document.body.scrollTop = 0; // For Chrome, Safari and Opera
                document.documentElement.scrollTop = 0; // For IE and Firefox
                jQuery('#loading-div').css('display','none');
            });
        },
        clearCategoryModalData() {
            this.innerVisible=false;
            // jQuery('#service_modal').modal('hide');
            this.id = null;
            this.price_from = null;
            this.price_to = null;
            this.price = null;

            this.form.error=false;
            this.form.validations=[];
            this.form.disabled = false;
        },










    },


});
