require('./bootstrap');

window.Vue = require('vue');
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import 'vue-multiselect/dist/vue-multiselect.min.css';


import lang from 'element-ui/lib/locale/lang/en';
import locale from 'element-ui/lib/locale';
// configure language
Vue.use(ElementUI);
locale.use(lang);
import {ToggleButton} from 'vue-js-toggle-button'
Vue.component('ToggleButton', ToggleButton)
import Multiselect from 'vue-multiselect';
Vue.component('multiselect', Multiselect);

require('./components/category');
require('./components/shop_category');
require('./shops/index');
require('./shops/view');
require('./shops/ratings');
require('./shops/branch');
require('./shops/meals');
require('./shops/offers');
require('./shops/category');
require('./shops/orders');
require('./shops/payments');
require('./coupons/index');
require('./delivery_users/index');
require('./delivery_users/ratings');
require('./roles/form');
require('./shop/settings');
require('./publicOrders/index');
require('./publicOrders/drivers/index');
require('./publicOrders/drivers/ordersList');
require('./publicOrders/drivers/payments');
require('./firebase/orderChat');
require('./firebase/publicOrderChat');
require('./firebase/orderViewTrackingMap');
require('./firebase/publicOrderViewTracking');
require('./firebase/dashboardTrackingDrivers');
require('./revenue/index');
require('./transactions/driverWalletTransaction');
require('./notification/index');
require('./contacts/contact');
require('./advertisment/advertisment');
require('./cancelOrderReason/index');
require('./country/index');
require('./deliveryPrices');
// require('./components/cofeeshops/ratings');
const app = new Vue({
    el: '#app',
    methods:{
        onlyNumberKey(evt) {

            // Only ASCII charactar in that range allowed
            var ASCIICode = (evt.which) ? evt.which : evt.keyCode
            if (ASCIICode > 31 && (ASCIICode < 48 || ASCIICode > 57))
                return false;
            return true;
        }
    }
});
