import { TableComponent, TableColumn } from 'vue-table-component';
import style from 'vue-table-component'
import form from '../mixins/form'
Vue.component('shopOrderList', {
    props:['fetchDataUrl','request','countryId','exportUrl'],
    mixins:[form],
    data() {
        return {
            form: {
                disabled: false,
                error: false,
                validations: [],
                message: null,

            },
            currentPage:null,
            per_page:null,
            data:[],
            shopsList:[],
            filter_store:null,
            report_period:'',
            custom_period:'',
            driver_id:null,
            client_id:null,
            cat_id:null,
            wallet:null,


        }
    },

    created(){
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const driver_id = urlParams.get('driver_id');
        const client_id = urlParams.get('client_id');
        const cat_id = urlParams.get('cat_id');
        this.driver_id=driver_id;
        this.client_id=client_id;
        this.cat_id=cat_id;

    },
    mounted() {
        this.getAllShops();
        if(this.request=='new'){
            // this.markReadNotification();
        }
    },
    components: {
        'table-component': TableComponent,
        'table-column': TableColumn,

    },
    methods: {
        exportExcel(){
            let vm =this;
            var driver=this.driver_id ? this.driver_id:'';
            var client=this.client_id ? this.client_id:'';
            var category=this.cat_id ? this.cat_id:'';
            let storeId=this.filter_store?this.filter_store.id:'';
            axios.get(this.exportUrl , {
                    params: {
                        report_period:vm.report_period,
                        custom_period:vm.custom_period,
                        store_id:storeId,
                        driver_id:driver,
                        client_id:client,
                        country_id:vm.countryId
                    }

            }, {
                responseType: 'blob'}
                ).then((response) => {
                const url = URL.createObjectURL(new Blob(["\ufeff",response.data], {
                    type: 'application/vnd.ms-excel'
                }))
                const link = document.createElement('a')
                link.href = url
                link.setAttribute('download', 'orders'+moment().format('Y-m-d H:mm:s'))
                document.body.appendChild(link)
                link.click()
                document.body.removeChild(link)
            });
        },
        rejectOrder(url,order_id,status){
            let vm= this;
            swal({
                title: "Reject order !",
                text: "Write cancel reason:",
                type: "input",
                showCancelButton: true,
                closeOnConfirm: false,
                inputPlaceholder: "Write cancel reason"
            }, function (inputValue) {
                if (inputValue === false) return false;
                if (inputValue === "") {
                    swal.showInputError("You need to write something!");
                    return false
                }
                // swal("Nice!", "You wrote: " + inputValue, "success");
                axios.post(url, {
                    id: order_id,
                    status: status,
                    reason:inputValue
                }).then(res => {

                    vm.refreshTable();

                    // vm.$notify({
                    //     title: 'Success',
                    //     message: "Status changed successfully",
                    //     type: 'success',
                    //     position: 'bottom-right',
                    //     offset: 50
                    // });
                    swal("Nice!", "This order cancelled successfully ");
                    return true
                });
            });
        },
        // markReadNotification(){
        //     let link='/markReadNotification';
        //     axios.post(link, {
        //         target:'newOrders'
        //     }).then(response => {
        //         swal({
        //             title: response.data.message,
        //             text: "",
        //             type: "success",
        //             timer: 2000,
        //             showConfirmButton: false
        //         });
        //
        //         this.form.disabled=false;
        //         jQuery('#loading-div').css('display','none');
        //     }).catch(error => {
        //         this.form.disabled = false;
        //         this.form.error = true;
        //         jQuery('#loading-div').css('display','none');
        //         if (error.response.data.errors) {
        //             this.form.message = 'There are error in entry data';
        //             this.form.validations = error.response.data.errors;
        //         } else if (error.response.data.message) {
        //             this.form.validations = [];
        //             this.form.message = error.response.data.message;
        //         }
        //         document.body.scrollTop = 0; // For Chrome, Safari and Opera
        //         document.documentElement.scrollTop = 0; // For IE and Firefox
        //     });
        // },

        resetFilter(){
            this.filter_store='';
            this.report_period='';
            this.custom_period='';
            this.refreshTable();
        },
        getAllShops(){
            let vm =this;
            axios.get('/shops/list/'+this.countryId).then(res=>{
                vm.shopsList=res.data.shops;
            });
        },
        refreshTable:function(){
            if(this.report_period=='custom' && !this.custom_period){
                swal({
                    title: 'You must enter date period before',
                    text: "",
                    type: "error",
                    timer: 2000,
                    showConfirmButton: false
                });

            }else{
                this.$refs.categoriesTable.refresh();
            }
        },

        async fetchData({ page, filter, sort }) {
            let vm=this;
            jQuery('#loading-div').css('display','block');
            var driver=this.driver_id ? this.driver_id:'';
            var client=this.client_id ? this.client_id:'';
            var category=this.cat_id ? this.cat_id:'';


            // let link=this.fetchDataUrl+"?;
            let storeId=this.filter_store?this.filter_store.id:'';
            let link= vm.fetchDataUrl+"?store="+storeId+"&driver_id="+driver+"&client_id="+client+"&report_period="+vm.report_period+"&custom_period="+vm.custom_period;

            const response = await axios.get(link, {params: { page, filter, sort }});
            this.currentPage= response.data.orders.current_page;
            this.per_page= response.data.orders.per_page;
            this.data=response.data.orders.data;
            this.wallet=response.data.wallet??0;
            jQuery('.shopWallet').html(response.data.totalWallet??0);
            jQuery('#loading-div').css('display','none');
            return {
                data: response.data.orders.data,
                pagination: {
                    currentPage: response.data.orders.current_page,
                    totalPages: response.data.orders.last_page
                }
            };
        },
        sendInvitationToDrivers(order_id) {
            jQuery('#loading-div').css('display','block');

            axios.post("/shops/orders/resendInvitationToDrivers", {
                order_id: order_id
            }).then(res => {

                this.$notify({
                    title: 'Success',
                    message: "Order resend to drivers again",
                    type: 'success',
                    position: 'bottom-right',
                    offset: 50
                });
                jQuery('#loading-div').css('display','none');
            });
        },

    },


});
