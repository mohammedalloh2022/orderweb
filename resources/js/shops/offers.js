import { TableComponent, TableColumn } from 'vue-table-component';
import style from 'vue-table-component'
import form from '../mixins/form'
import item from '../mixins/item'
Vue.component('shopOffersList', {
    props:['fetchDataUrl','shopId','request','countryId'],
    mixins:[form,item],
    data() {
        return {
            form: {
                disabled: false,
                error: false,
                validations: [],
                message: null,
            },
            currentPage:null,
            per_page:null,
            data:[],
            shopsList:[],
            filter_store:null,
            report_period:'',
            custom_period:'',
            innerViewVisible:false,
            offerViewObject:''
        }
    },
    mounted() {
        this.country_id=this.countryId;
        this.type='offer';
        this.getAllShops();
        if(this.request=="new"){
            // this.markReadNotification();
        }
    },
    created() {
        this.type='offer';
        // this.getAllServices();
    },
    components: {
        'table-component': TableComponent,
        'table-column': TableColumn,

    },
    methods: {
        markReadNotification(){
            let link='/markReadNotification';
            axios.post(link, {
                target:'newOffers'
            }).then(response => {
                swal({
                    title: response.data.message,
                    text: "",
                    type: "success",
                    timer: 2000,
                    showConfirmButton: false
                });

                this.form.disabled=false;
                jQuery('#loading-div').css('display','none');
            }).catch(error => {
                this.form.disabled = false;
                this.form.error = true;
                jQuery('#loading-div').css('display','none');
                if (error.response.data.errors) {
                    this.form.message = 'There are error in entry data';
                    this.form.validations = error.response.data.errors;
                } else if (error.response.data.message) {
                    this.form.validations = [];
                    this.form.message = error.response.data.message;
                }
                document.body.scrollTop = 0; // For Chrome, Safari and Opera
                document.documentElement.scrollTop = 0; // For IE and Firefox
            });
        },
        clearModelData(){
        this.innerViewVisible=false;
        this.offerViewObject=false;
        },
        viewOffer(offer){
            this.innerViewVisible=true;
            this.offerViewObject=offer;
        },
        getAllShops(){
          axios.get('/shops/list/'+this.countryId).then(res=>{
            this.shopsList=res.data.shops;
          });
        },
        refreshTable:function(){
            if(this.report_period=='custom' && !this.custom_period){
                swal({
                    title: 'You must enter date period before',
                    text: "",
                    type: "error",
                    timer: 2000,
                    showConfirmButton: false
                });

            }else{
                // this.fetchAppRevenueData({page,filter,sort});
                this.$refs.categoriesTable.refresh();
            }

        },
        resetFilter(){
            this.filter_store='';
            this.report_period='';
            this.custom_period='';
            this.refreshTable();
        },
        async fetchOffersData({ page, filter, sort }) {
            jQuery('#loading-div').css('display','block');
            let vm=this;
           let storeId=this.filter_store?this.filter_store.id:'';
           let link= vm.fetchDataUrl+"?store="+storeId+"&report_period="+vm.report_period+"&custom_period="+vm.custom_period;
            const response = await axios.get(link, {params: { page, filter, sort }});
            this.currentPage= response.data.items.current_page;
            this.per_page= response.data.items.per_page;
            this.data=response.data.items.data;
            jQuery('#loading-div').css('display','none');
            return {
                data: response.data.items.data,
                pagination: {
                    currentPage: response.data.items.current_page,
                    totalPages: response.data.items.last_page
                }
            };
        },
    },


});
