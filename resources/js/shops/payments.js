import { TableComponent, TableColumn } from 'vue-table-component';
import style from 'vue-table-component'
import form from '../mixins/form'
Vue.component('shopWalletPaymentTransaction', {
    props:['fetchDataUrl','shopId','exportUrl'],
    mixins:[form],
    data() {
        return {
            caseTransactionDialog:false,
            invoice_no:null,
            amount:null,
            type:null,
            form: {
                disabled: false,
                error: false,
                validations: [],
                message: null,
            },
            currentPage:null,
            per_page:null,
            data:[],
            driver_id:null,
            client_id:null,
            cat_id:null,
        }
    },
    created(){
        // const queryString = window.location.search;
        // const urlParams = new URLSearchParams(queryString);
        // const driver_id = urlParams.get('driver_id');
        // const client_id = urlParams.get('client_id');
        // const cat_id = urlParams.get('cat_id');
        // this.driver_id=driver_id;
        // this.client_id=client_id;
        // this.cat_id=cat_id;

    },
    mounted() {

    },
    components: {
        'table-component': TableComponent,
        'table-column': TableColumn,

    },
    methods: {

        exportExcel(){
            let vm =this;

            axios.get(this.exportUrl , {
                    params: {
                        shop_id:parseInt(vm.shopId)

                    }

                }, {
                    responseType: 'blob'}
            ).then((response) => {
                const url = URL.createObjectURL(new Blob(["\ufeff",response.data], {
                    type: 'application/vnd.ms-excel'
                }))
                const link = document.createElement('a')
                link.href = url
                link.setAttribute('download', 'payment transactions'+moment().format('Y-m-d H:mm:s'))
                document.body.appendChild(link)
                link.click()
                document.body.removeChild(link)
            });
        },
        refreshTable:function(){
            this.$refs.transactionTable.refresh();
        },
        async fetchData({ page, filter, sort }) {
            jQuery('#loading-div').css('display','none');
            // var driver=this.driver_id ? this.driver_id:'';
            // var client=this.client_id ? this.client_id:'';
            // var category=this.cat_id ? this.cat_id:'';
            // let link=this.fetchDataUrl+"?driver_id="+driver+"&client_id="+client;
            const response = await axios.get(this.fetchDataUrl, {params: { page, filter, sort }});
            this.currentPage= response.data.wallet_transactions.current_page;
            this.per_page= response.data.wallet_transactions.per_page;
            this.data=response.data.wallet_transactions.data;
            jQuery('#loading-div').css('display',' none');
            return {
                data: response.data.wallet_transactions.data,
                pagination: {
                    currentPage: response.data.wallet_transactions.current_page,
                    totalPages: response.data.wallet_transactions.last_page
                }
            };
        },

        addNewTranaction() {
            this.form.disabled = true;
            jQuery('#loading-div').css('display','block');
            axios.post("/shops/wallet/store/"+this.shopId, {
                invoice_no:this.invoice_no,
                amount:this.amount,
                type:this.type,

            }).then(response => {
                this.$notify({
                    title: 'Success',
                    message: response.data.message,
                    type: 'success',
                    position: 'bottom-right',
                    offset: 50
                });
                this.caseTransactionDialog=false;
                this.refreshTable();
                this.clearTransactionModalData();


                this.form.disabled=false;
                this.form.disabled = false;
                this.form.error = false;

                jQuery('#loading-div').css('display','none');
            }).catch(error => {
                this.form.disabled = false;
                this.form.error = true;
                jQuery('#loading-div').css('display','none');
                if (error.response.data.errors) {
                    this.form.message = 'There are error in entry data';
                    this.form.validations = error.response.data.errors;
                } else if (error.response.data.message) {
                    this.form.validations = [];
                    this.form.message = error.response.data.message;
                }
                document.body.scrollTop = 0; // For Chrome, Safari and Opera
                document.documentElement.scrollTop = 0; // For IE and Firefox
            });
        },
        clearTransactionModalData(){
            this.caseTransactionDialog=false;
            this.invoice_no=null;
            this.amount=null;
            this.type=null;

            this.form.error=false;
            this.form.validations=[];
        },

    },

});
