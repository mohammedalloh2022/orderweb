<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get("/", 'HomeController@home')->name('home.index');
Route::get("/check", 'HomeController@check');
// Route::get("/updateCities", function () {
//     $users = \App\User::query()->whereNull('city_id')->get();
//     foreach ($users as $user) {

//         if (!isset($user->country))
//             $country = \App\Models\Country::query()->first();
//          else
//             $country = $user->country;

//         $city = $country->cities()->first();
//         if(isset($city)){
//             $user->city_id = $city->id;
//         $user->save();
//         }

// //        dd($city);
//     }
// });

Route::get('/dashboard/{countryId}', 'HomeController@welcome')->name('home');
Route::get('/testing/{countryId}', 'HomeController@test')->name('home.test');
Route::post('/save-device-token', 'UserFcmController@saveToken')->name('home.save.user.token')->middleware('auth');
Route::post('/sendPush', 'UserFcmController@sendPush')->name('home.save.user.sendPush')
    ->middleware('auth');

Route::get('/dashboard', function () {
    $role = auth()->user()->role;
    switch ($role) {
        case "shop" :
            return redirect()->to('/shop/dashboard');
        case "branch" :
            return redirect()->to('/branch/dashboard');
        case "admin" :
            $country = \App\Models\Country::query()->first();
            return redirect()->to('/dashboard/' . $country->id);
    }
})->middleware('auth');

Route::group(['prefix' => '/shop', 'middleware' => ['auth', 'shop']], function () {
    Route::get('/dashboard', 'HomeController@shopDashbaord')->name('shop.dashboard');
    Route::get('/categories', 'SingleShopController@CateogryIndex')->name('shop.category');
    Route::get('/branch', 'SingleShopController@branch')->name('shop.branch');
    Route::get('/orders', 'SingleShopController@orders')->name('shop.orders');
    Route::get('/pending/orders', 'SingleShopController@pendingOrders')->name('shop.pending.orders');
    Route::get('/offers', 'SingleShopController@offers')->name('shop.offers');
    Route::get('/Transactions', 'SingleShopController@transactions')->name('shop.transactions');
    Route::get('/revenue', 'SingleShopController@revenue')->name('shop.revenue');
    Route::get('/filterRevenue', 'SingleShopController@getRevenueFilter')->name('shop.revenue.filter');
    Route::get('/previousOrder', 'SingleShopController@previousOrderView')->name('shop.prev.orders');
    Route::get('/filterPreviousOrder/{shop}', 'SingleShopController@getPervOrders')->name('shop.prev.orders.filter');
    Route::get('/paymentTransaction', 'SingleShopController@paymentTransaction')->name('shop.branch.payment.transaction');
    Route::get('/settings', 'SingleShopController@settings')->name('shop.settings');
});
Route::group(['prefix' => '/branch', 'middleware' => 'auth'], function () {
    Route::get('/orders', 'SingleShopController@orders')->name('branch.orders');
    Route::get('/pending/orders', 'SingleShopController@pendingOrders')->name('branch.pendings.orders');
    Route::get('/dashboard', 'HomeController@branchDashbaord')->name('branch.dashboard');
});
Route::group(['prefix' => '/publicOrder', 'middleware' => 'auth'], function () {
    Route::get('/orders/{countryId}', 'PublicOrderController@index')->name('public.orders.index');
//    ->middleware('permission:Public order list ');
//    Route::get('/orders','PublicOrderController@index')->name('public.orders.index');
    Route::get('/view/{publicOrder}', 'PublicOrderController@view')->name('public.orders.view');
    Route::get('/ordersList/{countryId}', 'PublicOrderController@search')->name('public.orders.list');
    Route::get('/driverPublicOrders/{countryId}', 'PublicOrderController@driverPublicOrdersList')
        ->name('driver.public.orders.list');
    Route::get('/driverPublicOrders/search/{countryId}', 'PublicOrderController@driverPublicOrdersSearch')->name('driver.public.orders.search');
    Route::get('/driverOrdersList/{driver}', 'PublicOrderController@driverOrdersList')->name('driver.orders.list');
    Route::get('/driverOrdersList/search/{driver}', 'PublicOrderController@driverOrdersSearch')->name('driver.orders.search');
    Route::get('/driverOrdersList/wallet/{driver}', 'PublicOrderController@driverWallet')->name('driver.orders.wallet');
    Route::get('/driverOrdersList/wallet/search/{driver}', 'PublicOrderController@driverWalletTransaction')->name('driver.orders.wallet.transaction');
    Route::post('/driverOrdersList/transaction/{driver}', 'PublicOrderController@storeWalletTransAction')->name('driver.orders.wallet.store.transaction');

});


Route::group(['prefix' => '/country', 'middleware' => 'auth'], function () {
    Route::get('/list', 'CountryController@list')->name('country.list');
    Route::get('/', 'CountryController@index')->name('country.index')->middleware('permission:Country Management');
    Route::get('/search', 'CountryController@search')->name('country.search');
    Route::post('/changeStatus', 'CountryController@changeStatus')->name('country.change.status');
    Route::post('/', 'CountryController@store')->name('country.store');
    Route::put('/update/{id}', 'CountryController@update')->name('country.update');

    Route::group(['prefix' => '/cities', 'middleware' => 'auth'], function () {
        Route::get('/{country}', 'CityController@index')->name('city.index');
        Route::get('/fetch/all', 'CityController@all')->name('city.all');
        Route::get('/fetch/all/prices', 'CityController@allPrices')->name('all.city.prices');
        Route::get('/search/{country}', 'CityController@search')->name('city.search');
        Route::delete('/{state?}', 'CityController@destroy')->name('city.destroy');
        Route::post('/', 'CityController@store')->name('city.store');
        Route::put('/update/{id}', 'CityController@update')->name('city.update');

        Route::post('/changeStatus', 'CityController@changeStatus')
            ->name('city.change.status');
    });

});


Route::group(['prefix' => '/categories', 'middleware' => 'auth'], function () {
    Route::get('/', 'CategoryController@index')->name('category.index')
        ->middleware('auth', 'permission:Category list');
    Route::get('/list', 'CategoryController@list')->name('category.list');
    Route::get('/search', 'CategoryController@search')->name('category.search');
//        ->middleware('auth','permission:Category list');
    Route::delete('/{category?}', 'CategoryController@destroy')->name('category.destroy');
    Route::post('/', 'CategoryController@store')->name('category.store');
//        ->middleware('auth','permission:Category add');
    Route::put('/update/{id}', 'CategoryController@update')->name('category.update');//        ->middleware('permission:Category edit')
    ;
    Route::post('/changeStatus', 'CategoryController@changeStatus')
        ->name('category.change.status');
});
Route::group(['prefix' => '/prices', 'middleware' => 'auth'], function () {
    Route::get('/{countryId}', 'DeliveryPricesController@index')->name('prices.index');
    Route::get('/list', 'DeliveryPricesController@list')->name('prices.list');
    Route::get('/search/{countryId}', 'DeliveryPricesController@search')->name('prices.search');
    Route::delete('/{category?}', 'DeliveryPricesController@destroy')->name('prices.destroy');
    Route::post('/', 'DeliveryPricesController@store')->name('prices.store');
    Route::put('/update/{id}', 'DeliveryPricesController@update')->name('prices.update');
    Route::post('/changeStatus', 'DeliveryPricesController@changeStatus')
        ->name('prices.change.status');
});
Route::group(['prefix' => '/advertise', 'middleware' => 'auth'], function () {
    Route::get('/{countryId}', 'AdvertiseSliderController@index')->name('advertise.index.country');

    Route::get('/list', 'AdvertiseSliderController@list')->name('advertise.list');
//        ->middleware('auth','permission:ads list');
    Route::get('/search/{countryId}', 'AdvertiseSliderController@filter')->name('advertise.search');
//        ->middleware('permission:ads list');
    Route::delete('/{ad?}', 'AdvertiseSliderController@destroy')->name('advertise.destroy');
    Route::post('/', 'AdvertiseSliderController@store')->name('advertise.store');
//        ->middleware('permission:ads add');
    Route::put('/update/{id}', 'AdvertiseSliderController@update')->name('advertise.update');
//        ->middleware('permission:ads edit');
    Route::post('/changeStatus', 'AdvertiseSliderController@changeStatus')
        ->name('advertise.change.status');
});

Route::group(['prefix' => '/cancel-reasons', 'middleware' => 'auth'], function () {
    Route::get('/', 'CancelReasonController@index')->name('cancel.reason.index')
        ->middleware('auth', 'permission:Cancel order reason list');

    Route::get('/list', 'CancelReasonController@list')->name('cancel.reason.list');
//        ->middleware('auth','permission:Cancel order reason list');
    Route::get('/search', 'CancelReasonController@search')->name('cancel.reason.search');
//        ->middleware('permission:Cancel order reason list');
    Route::delete('/{ad?}', 'CancelReasonController@destroy')->name('cancel.reason.destroy');
    Route::post('/', 'CancelReasonController@store')->name('cancel.reason.store');
//        ->middleware('permission:Cancel order reason add');
    Route::put('/update/{id}', 'CancelReasonController@update')->name('cancel.reason.update');
//        ->middleware('permission:Cancel order reason edit');
    Route::post('/changeStatus', 'CancelReasonController@changeStatus')
        ->name('cancel.reason.change.status');
});
Route::group(['prefix' => '/manage', 'middleware' => 'auth'], function () {
    Route::get('/user-export', "UserController@exportUsersExcel")->name('user.export.excel');
    Route::get('/allBusyDrivers', 'UserController@allBusyDrivers')->name('mange.busy.drivers.list');
    Route::get('/exportExcel', 'DeliveryDriverController@exportExcel')->name('mange.delivery.export.excel');

    Route::group(['prefix' => '/send-notifications'], function () {
        Route::get('/{countryId}', 'MobileNotificationController@index')->name('manage.mobile.notification');
        Route::get('/search/{countryId}', 'MobileNotificationController@search')->name('manage.mobile.notification.search');
        Route::post('/send', 'MobileNotificationController@send')->name('manage.mobile.notification.send');

    });
    Route::group(['prefix' => '/offers'], function () {
        Route::get('/{countryId}', 'OffersController@index')->name('mange.offers.index');//        ->middleware('permission:New offers list');
        Route::get('/search/{countryId}', 'OffersController@search')->name('mange.offers.search');
        Route::get('/exportExcel/{countryId}', 'OffersController@exportExcel')->name('mange.offers.excel');
        Route::get('/orders/{offer}', 'OffersController@viewOrderOffers')->name('mange.offers.orders');
        Route::get('/list/{countryId}', 'OffersController@showOffersList')->name('mange.offers.list.index');//        ->middleware('permission:Offers list');
        Route::get('/list/search/{countryId}', 'OffersController@offerList')->name('mange.offers.list.search');
        Route::get('/list/orders/{offer}', 'OffersController@offersOrders')->name('mange.offers.viewOrderOffers');
        Route::post('/changeStatus', 'OffersController@changeStatus')->name('mange.offers.change.status');
    });
    Route::group(['prefix' => '/delivery'], function () {
        Route::get('/{countryId}', 'DeliveryDriverController@index')->name('mange.delivery.index');
        Route::post('/updateDriverBalance', 'DeliveryDriverController@updateBalance')->name('mange.delivery.update.balance');
//            ->middleware('permission:Drivers list');
        Route::get('/documentedRequest/{countryId}', 'DeliveryDriverController@documentedRequest')->name('mange.delivery.documented');//            ->middleware('permission:New driver list');
        Route::get('/search/{countryId}', 'DeliveryDriverController@search')->name('mange.delivery.search');
        Route::get('/documentSearch/{countryId}', 'DeliveryDriverController@documentSearch')->name('mange.delivery.documentSearch');
        Route::post('/isDocumentedUser', 'DeliveryDriverController@changeStatus')->name('mange.delivery.change.status');
        Route::get('/ratings/{driver}', 'DeliveryDriverController@ratings')->name('mange.delivery.ratings.show');
        Route::get('/rating/list/{driver}', 'DeliveryDriverController@ratingsList')->name('mange.delivery.ratings');

        Route::get('/driver/wallet/{driver}', 'DriverWalletTransactionController@driverWallet')->name('delivery.driver.orders.wallet');
        Route::get('/driver/wallet/search/{driver}', 'DriverWalletTransactionController@driverWalletTransaction')->name('delivery.driver.orders.wallet.transaction');
        Route::post('/driver/transaction/{driver}', 'DriverWalletTransactionController@storeWalletTransAction')->name('delivery.driver.orders.wallet.store.transaction');
        Route::get('/allDrivers/{countryId}', 'UserController@allDrivers')->name('mange.drivers.list');
    });
    Route::group(['prefix' => '/client'], function () {
        Route::get('/{countryId}', 'UserController@clientIndex')->name('mange.client.index');
        Route::get('/search/{countryId}', 'UserController@search')->name('mange.client.search');
        Route::get('/export-excel', 'UserController@exportUsersExcel')->name('mange.client.export.excel');
        Route::get('/lastTenClient', 'UserController@lastTenClient')->name('mange.last.ten.client.search');
        Route::get('/all/{countryId}', 'UserController@allClients')->name('mange.client.list');
//        Route::post('/changeStatus', 'UserController@changeStatus')->name('mange.client.change.status');
    });

    Route::post('/user/changeStatus', 'UserController@changeStatus')->name('mange.user.change.status');
    Route::post('/user/create', 'UserController@store')->name('mange.user.store');
    Route::post('/user/update', 'UserController@update')->name('mange.user.update');
    Route::delete('/user/{user}', 'UserController@destroy')->name('mange.user.destroy');
    Route::group(['prefix' => '/admin'], function () {
        Route::get('/', 'UserController@adminIndex')->name('mange.admin.index')->middleware('permission:Admin management');
        Route::get('/search', 'UserController@adminSearch')->name('mange.admin.search');
        Route::group(['middleware' => 'permission:Roles management'], function () {
            Route::get('/roles', 'RoleController@roles')->name('mange.admin.roles.list')->middleware('permission:Roles management');
            Route::get('/roles/list', 'RoleController@rolesList')->name('mange.admin.roles.json.list');
            Route::get('/role/{role?}', 'RoleController@form')->name('mange.admin.role.edit');
            Route::get('/all-web-permission', 'RoleController@getWebPermissions')->name('mange.admin.permissions');
            Route::get('/role-permissions-list/{roleId}', 'RoleController@getAllRolesPermissions')->name('mange.admin.role.permissions');
            Route::post('/roles/store', 'RoleController@storeRole')->name('mange.admin.role.store');
            Route::put('/roles/update/{roleId}', 'RoleController@updateRole')->name('mange.admin.role.update');

        });
    });
    Route::group(['prefix' => '/orders'], function () {
        Route::get('/export', 'OrderController@expertExcel')->name('mange.orders.excel');
        Route::get('/{countryId}', 'OrderController@index')->name('mange.orders.index');
//        ->middleware('permission:Order list');
        Route::get('/pending/search/{countryId}', 'OrderController@pendingOrderSearch')->name('mange.pending.orders.search');
        Route::get('/pending-list/{countryId}', 'OrderController@pendingOrderView')
            ->name('mange.pending.orders.index');
//        ->middleware('permission:New order list');
        Route::get('/search/{countryId}', 'OrderController@search')->name('mange.orders.search');
        Route::get('/view/{order}', 'OrderController@view')->name('mange.orders.view');
        Route::get('/store/view/{order}', 'OrderController@storeView')->name('mange.orders.store.view.order');
        Route::post('/changeStatus', 'OrderController@changeStatus')->name('mange.orders.change.status');
    });

    Route::group(['prefix' => '/revenue'], function () {
        Route::get('/{countryId}', 'AppRevenueController@index')->name('app.revenue.orders.index');
//            ->middleware('permission:App revenue list');

        Route::get('/search/{countryId}', 'AppRevenueController@search')->name('app.revenue.orders.search');
    });
    Route::group(['prefix' => '/discounts'], function () {
        Route::get('/{countryId}', 'AppDiscountsController@index')->name('app.discount.orders.index');
//            ->middleware('permission:App discount list');;
        Route::get('/search/{countryId}', 'AppDiscountsController@search')->name('app.discount.orders.search');
    });
});


Route::group(['prefix' => '/shops', 'middleware' => 'auth'], function () {
    // shop management
    Route::get('/', 'ShopController@index')->name('shops.index');
    Route::get('/fetch-by-Country/{countryId}', 'ShopController@getCountryIndex')->name('shops.country.index');
    Route::get('/search', 'ShopController@search')->name('shops.search');
    Route::get('/list/{countryId}', 'ShopController@shopsList')->name('shops.list');
    Route::get('/view/{shop}', 'ShopController@view')->name('shops.view');
    Route::get('/edit/times/{shop}', 'ShopController@editTimes')->name('shops.times.edit');
    Route::post('/edit/times/{shop}', 'ShopController@updateTimes')->name('shops.times.update');

    Route::post('/status-change', 'ShopController@changeStatus')
        ->name('shops.change.activity.status');
    //->middleware('permission:Store active/deactivate')
    Route::post('/store', 'ShopController@store')->name('shops.store');
    Route::post('/storeBranch', 'ShopController@storeBranch')->name('shops.branch.store');
    Route::post('/updateBranch/{shop}', 'ShopController@updateBranch')->name('shops.branch.update');
    Route::post('/{shop}', 'ShopController@update')->name('shops.update');
    Route::delete('/{shop}', 'ShopController@destroy')->name('shops.destroy');
    // end shop management
    Route::get('/reviews/{shop}', 'ShopController@reviews')->name('shops.reviews');
    Route::delete('/rating/{rate}', 'RatingController@destroy')->name('shops.review.delete');

    Route::group(['prefix' => '/branches'], function () {
        Route::get('/{shop}', 'ShopController@branches')->name('shops.branches');
        Route::get('/view/{shop}', 'ShopController@branchview')->name('shops.branch.view');
        Route::post('/', 'ShopController@store')->name('branch.shops.store');
    });

    Route::group(['prefix' => '/wallet'], function () {
        Route::get('/transactions/{shop}', 'ShopTransactionController@index')->name("shop.wallet.transaction.view");
        Route::get('/export-excel', 'ShopTransactionController@export')->name("shop.wallet.transaction.export.view");
        Route::get('/branch/transactions/{shop}', 'ShopTransactionController@branchIndex')->name("shop.wallet.branch.transaction.view");
        Route::get('/{shopId}', 'ShopTransactionController@walletTransactions')->name("shop.wallet.transaction.list");

        Route::post('/store/{shop}', 'ShopTransactionController@storeWalletTransAction')->name("shop.wallet.transaction.store");
    });

    Route::group(['prefix' => '/meals'], function () {
        Route::get('/{shop}', 'ItemController@meals')->name('shops.meals');
        Route::get('/getMealsByCat/{shop}/{cat}', 'ItemController@getMealsByCat')->name('shops.cat.meals');
        Route::get('/{shop}/{cat}', 'ItemController@mealsByCategory')->name('shops.meals.category');
        Route::post('/store', 'ItemController@store')->name('shops.meals.store');
        Route::put('/update/{item}', 'ItemController@update')->name('shops.meals.update');
        Route::post('/changeStatus', 'ItemController@changeStatus')->name('shops.meals.change.status');
        Route::delete('/{item?}', 'ItemController@destroy')->name('shops.meals.destroy');
//        Route::put('/update/{id}','SubCategoryController@update')->name('shops.category.update');
    });
    Route::group(['prefix' => '/offers'], function () {
        Route::get('/{shop}', 'ItemController@offers')->name('shops.offers');
        Route::put('/update/{item}', 'ItemController@update')->name('shops.offers.update');
        Route::post('/store', 'ItemController@storeOffer')->name('shops.offers.store');
        Route::delete('/{item?}', 'ItemController@destroy')->name('shops.offers.destroy');
    });
    Route::group(['prefix' => '/categories'], function () {
        Route::get('/list/{shop}', 'SubCategoryController@list')->name('shops.category.list');
        Route::get('/{shop}', 'SubCategoryController@search')->name('shops.category.search');
        Route::post('/store', 'SubCategoryController@store')->name('shops.category.store');
        Route::put('/update/{id}', 'SubCategoryController@update')->name('shops.category.update');
        Route::delete('/{category?}', 'SubCategoryController@destroy')->name('shops.category.destroy');
        Route::post('/changeStatus', 'SubCategoryController@changeStatus')->name('shops.category.change.status');
    });
    Route::group(['prefix' => '/orders'], function () {
        Route::get('/{shop}', 'ShopController@orders')->name('shops.orders.search');
        Route::get('/branch/{shop}', 'ShopController@branchOrders')->name('shops.orders.branchSearch');
        Route::get('/pending/{shop}', 'ShopController@pendingOrders')->name('shops.orders.search.pending');
        Route::post('/resendInvitationToDrivers', 'OrderController@resendNotificationToDrivers')->name('shops.orders.invitation');
    });
});

Route::group(['prefix' => '/coupons', 'middleware' => 'auth'], function () {
    Route::get('/usedCoupons/search{coupon}', 'CouponController@orders')->name('orders.used.coupons.search');
    Route::get('/usedCoupons/{coupon}', 'CouponController@viewOrders')->name('orders.used.coupons.view');
    Route::get('/{countryId}', 'CouponController@index')->name('coupons.index');

    Route::get('/{countryId}/create', 'CouponController@create')->name('coupons.create');

    Route::get('/list/{countryId}', 'CouponController@list')->name('coupons.list');
    Route::post('/store', 'CouponController@store')->name('coupons.store');
    Route::post('/changeStatus', 'CouponController@changeStatus')->name('coupons.change.status');
});
Route::group(['prefix' => '/contacts', 'middleware' => 'auth'], function () {
    Route::get('/{countryId}', 'ContactController@index')->name('contacts.index');
    Route::get('/search/{countryId}', 'ContactController@search')->name('contacts.search');
    Route::get('/replies/{contId}', 'ContactController@getReplies')->name('coupons.replies.list');
    Route::post('/reply/{contId}', 'ContactController@reply')->name('coupons.reply');
//    Route::post('/changeStatus','ContactController@changeStatus')->name('coupons.change.status');
});
Route::middleware('auth')->group(function () {
    Route::get('/change/password', 'AdminController@showChangePassword')->name('change.password');
    Route::post('/change/password', 'AdminController@updatePassword')->name('update.password');
    Route::get('/profile', 'AdminController@showAdminProfile')->name('profile.edit');
    Route::post('/profile', 'AdminController@updateAdminProfile')->name('profile.update');
    Route::get('/settings/app/{countryId?}', 'AppSettingController@index')->name('app.settings');
    Route::post('/settings', 'AppSettingController@store')->name('app.settings.store');
    Route::get('/settings/content/{countryId}', 'AppSettingController@content')->name('app.content')->middleware("permission:App Social media list");
    Route::post('/settings/content', 'AppSettingController@contentStore')->name('app.content.store');
    Route::post('/app/settings/content', 'AppSettingController@appContentStore')->name('store.app.content');
    Route::post('/markReadNotification', 'UserController@markReadNotification')->name('app.notifications.read');

    Route::get('/settings/privacy', 'AppSettingController@privacyContent')->name('app.privacy')->middleware("permission:App privacy content");
    Route::post('/settings/privacy', 'AppSettingController@privacyStore')->name('app.privacy.store');
});



