<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//Route::middleware('auth:api')->get("/user", function (Request $request) {
//    return $request->user();
//});
Route::group(['namespace'=>'v1','prefix'=>'v1','middleware'=>'cors'],function(){
    Route::get('/countries-list','AppSettingController@getCountriesList');
    Route::post('/pay', "PaymentTransactionController@payment");
    Route::get('/checkout', "PaymentTransactionController@getCheckOutId");
    Route::get('/paymentStatus', "PaymentTransactionController@paymentStatus");
    Route::group(['prefix' => 'auth'], function () {
        Route::post('/login', 'AuthController@login');
        Route::post('/clientLogin', 'AuthController@clientLogin');
        Route::post('/driverLogin', 'AuthController@driverLogin');
        Route::post('/register', 'AuthController@signup')->name('register.new.user.mobile');

        Route::post('/forgetPasswordRequest', 'AuthController@forgetPasswordRequest');
        Route::post('/forgetPasswordVerify', 'AuthController@forgetPasswordVerify');

        Route::group(['middleware' => ['auth:api']], function() {
            Route::post('/signUpDeliveryAppStep2', 'AuthController@signUpDeliveryAppStep2');
            Route::post('/readyOrNotDriver', 'AuthController@readyOrNotDriver');
            Route::get('/logout', 'AuthController@logout');
            Route::get('/user', 'AuthController@user');
            Route::post('/resetPassword', 'AuthController@resetPassword');
            Route::post('/updateProfile', 'AuthController@updateProfile');
            Route::post('/changeLanguage', 'AuthController@changeLanguage');
            Route::post('/updateDeliveryAddress', 'AuthController@updateDelivery');

        });

    });
    Route::group(['middleware' => ['auth:api']], function() {

        Route::get('/favoriteList', "FavoritController@index");
        Route::post('/storeFavorite', "FavoritController@store");
        Route::post('/rateMajor', "RatingController@store");
        Route::post('/rateDeliveryDriver', "RatingController@storeRateDriver");
        Route::post('/order', "OrderController@placeOrder");
        Route::post('/cancelOrder', "OrderController@clientCancelOrder");
        Route::get('/order/{order}', "OrderController@getSingleOrderDetails");
        Route::get('/orders', "OrderController@index");
        Route::get('/orders/me', "OrderController@getClientOrders");
        Route::get('/ratingList/{category}', "RatingController@index");
        Route::get('/notifications', "AuthController@getUserNotifications");
        Route::post('/fcmToken',"AuthController@storeUserToken");
        Route::post('/contact',"ContactController@store");
    });
    Route::group(['prefix'=>'/public/order/client','middleware'=>'auth:api'],function(){
       Route::get('/ordersList', "PublicOrderController@clientOrdersList");
       Route::post('/placeOrder', "PublicOrderController@placeOrder");
       Route::post('/cancel-order', "PublicOrderController@cancelOrder");
       Route::get('/order/{order}', "PublicOrderController@getOrderDetails");
       Route::get('/cancelReasons', "PublicOrderController@cancelReasons");
       Route::post('/clientConfirmDeliverd', "PublicOrderController@clientConfirmDeliverd");
       Route::post('/sendChatNotify', "PublicOrderController@sendNotificationChatIsBegin");
       Route::post('/clientConfirmPayment', "PublicOrderController@clientConfirmPayment");
    });
    Route::group(['prefix'=>'/public/order/driver','middleware'=>'auth:api'],function(){
        Route::get('/ordersList', "PublicOrderController@driverOrdersList");
        Route::get('/pendingOrders', "PublicOrderController@pendingOrders");
        Route::post('/pickupOrder', "PublicOrderController@pickupOrder");
        Route::post('/sendInvoiceValue', "PublicOrderController@sendInvoiceValue");
        Route::post('/changeToOnTheWay', "PublicOrderController@changeToInTheWayOrder");
        Route::post('/deliveredOrder', "PublicOrderController@deliveredOrder");
        Route::post('/cancelOrder', "PublicOrderController@driverCancelOrder");
        Route::get('/order/{order}', "PublicOrderController@getOrderDetails");
        Route::get('/wallet', "PublicOrderController@driverWallet");
//        Route::get('/rejectOrder', "PublicOrderController@driverWallet");
    });
    Route::post('/sendNotification', "AuthController@sendNotifications");
    Route::post('/sendDummyNotification', "AuthController@sendDummyNofitication");
    Route::group(['middleware' => ['auth:api'],'prefix'=>'delivery'], function() {
        Route::get('/wallet', "WalletController@wallet");
        Route::get('/walletDetails', "WalletController@walletDetails");
//        Route::get('/orders', "OrderController@getDriverOrders");
        Route::get('/order/{id}', "OrderController@getOrderDetials");
        Route::get('/ratings', "RatingController@driversRatings");
        Route::post('/pickupOrder', "OrderController@pickupOrder");
        Route::post('/updateDriverLocation', "AuthController@updateDriverLatAndLng");
        Route::post('/markOrderDelivered', "OrderController@DeliveredOrder");
        Route::post('/isOnline', "AuthController@isOnline");
        Route::get('/myOrders', "OrderController@getDriverOrders");
    });
    Route::post('/checkCoupon',"OrderController@checkCoupon");

    Route::get('/categories',"CategoryController@index");
    Route::get('/ads',"AdController@index");
    Route::get('/shops/{category}',"CategoryController@shops");
    Route::get('/shop/{shop}',"ShopController@getShopDetails");
    Route::get('/shop/categories/{shop}',"ShopController@shopsCategories");
    Route::get('/shop/branches/{shop}',"ShopController@shopsBranches");
    Route::get('/item/{item}',"ItemController@item");
    Route::get('/items/{category}',"ItemController@index");
    Route::get('/items/{shop}/{subcategory}',"ItemController@getItemsBySubCategory");
    Route::get('/offers/slider',"ItemController@offersSliders")->middleware('auth:api');
    Route::get('/offers/{category}',"ItemController@offersByCategory")->middleware('auth:api');;
    Route::get('/settings',"AppSettingController@index");
    Route::get('/clientPrivacy',"AppSettingController@clientContent");
    Route::get('/driverPrivacy',"AppSettingController@driverContent");
//    Route::get('/privacy',"AppSettingController@index");
});



