

// importScripts('https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js');
importScripts('https://www.gstatic.com/firebasejs/7.14.6/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/7.14.6/firebase-messaging.js');
firebase.initializeApp({

    apiKey: "AIzaSyCbuZWc2c5gQRv3uqH_92g4-s7unNv3UGU",
    authDomain: "axiomatic-spark-303003.firebaseapp.com",
    databaseURL: "https://axiomatic-spark-303003-default-rtdb.firebaseio.com",
    projectId: "axiomatic-spark-303003",
    storageBucket: "axiomatic-spark-303003.appspot.com",
    messagingSenderId: "877236599825",
    appId: "1:877236599825:web:c8564cab36d0e0f314c5b0",
    measurementId: "G-YCBV9F0E0R"
});




const messaging2 = firebase.messaging();

messaging2.setBackgroundMessageHandler(function(payload) {
    // toastr.options = { onclick: function () {
    //         window.location='/manage/orders/store/view/'+payload.notification.order_id ;
    //     }};
    // toastr.success(payload.notification.title, payload.notification.msg);
    console.log('[firebase-messaging-sw.js] Received background message ', payload);
    // Customize notification here
    const notificationTitle = payload.notification.title;
    const notificationOptions = {
        body: payload.notification.msg,
        icon: '/firebase-logo.png',
    };

    return self.registration.showNotification(notificationTitle,
        notificationOptions);
});
