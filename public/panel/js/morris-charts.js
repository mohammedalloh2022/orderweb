"use strict";
// Class definition
var KTMorrisChartsDemo = function () {

    var responseArr = JSON.parse($('#kt_morris_3').attr('data-chartData'));
    var data_chart = [];

    $.each(responseArr, function (date, values) {
        var single = {
            date: date,
            'مبيعات العسدسات الاصقة': values.contact_lenses_total_sales,
            'مبيعات الاطارات': values.frames_total_sales,
            'مبيعات النظارات الشمسية': values.sun_glasses_total_sales,
            'مبيعات  الاكسسوارات': values.accessories_total_sales,
        };
        data_chart.push(single);
    });
    /**********************************************/
    var responseArrWeeks = JSON.parse($('#kt_morris_4').attr('data-chartData'));
    var data_chartWeeks = [];

    $.each(responseArrWeeks, function (date, values) {
        var single = {
            date: date,
            'مبيعات العسدسات الاصقة': values.contact_lenses_total_sales,
            'مبيعات الاطارات': values.frames_total_sales,
            'مبيعات النظارات الشمسية': values.sun_glasses_total_sales,
            'مبيعات  الاكسسوارات': values.accessories_total_sales,
        };
        data_chartWeeks.push(single);
    });


    var demo1 = function () {
        // BAR CHART

        new Morris.Bar({
            element: 'kt_morris_3',
            data: data_chart,
            xkey: 'date',
            ykeys: ['مبيعات العسدسات الاصقة', 'مبيعات الاطارات' , 'مبيعات النظارات الشمسية' , 'مبيعات  الاكسسوارات'],
            labels:['مبيعات العسدسات الاصقة', 'مبيعات الاطارات' , 'مبيعات النظارات الشمسية' , 'مبيعات  الاكسسوارات'],
            barColors: ['#2abe81', '#24a5ff' , '#ff1a1a' , '#090910']
        });
    }
    var demo2 = function () {
        // BAR CHART

        new Morris.Bar({
            element: 'kt_morris_4',
            data: data_chartWeeks,
            xkey: 'date',
            ykeys: ['مبيعات العسدسات الاصقة', 'مبيعات الاطارات' , 'مبيعات النظارات الشمسية' , 'مبيعات  الاكسسوارات'],
            labels:['مبيعات العسدسات الاصقة', 'مبيعات الاطارات' , 'مبيعات النظارات الشمسية' , 'مبيعات  الاكسسوارات'],
            barColors: ['#2abe81', '#24a5ff' , '#ff1a1a' , '#090910']
        });
    }


    return {
        // public functions
        init: function () {
            demo1();
            demo2();
        }
    };
}();

jQuery(document).ready(function () {
    KTMorrisChartsDemo.init();
});