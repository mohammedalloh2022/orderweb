"use strict";

// Class definition
var KTAppInbox = function() {
    var asideEl;
    var listEl;
    var viewEl;
    var composeEl;


    return {
        // public functions
        init: function() {
            asideEl = KTUtil.getByID('kt_inbox_aside');
            listEl = KTUtil.getByID('kt_inbox_list');
            viewEl = KTUtil.getByID('kt_inbox_view');
            composeEl = KTUtil.getByID('kt_inbox_compose');

            // init
            KTAppInbox.initList();

        },

        initList: function() {
            // View message
            KTUtil.on(listEl, '.kt-inbox__item', 'click', function(e) {
                var actionsEl = KTUtil.find(this, '.kt-inbox__actions');

                // skip actions click
                if (e.target === actionsEl || (actionsEl && actionsEl.contains(e.target) === true)) {

                    return false;
                }
                location.href=$(this).data('url');

                // demo loading
                var loading = new KTDialog({
                    'type': 'loader',
                    'placement': 'top center',
                    'message': 'جار التحميل ...'
                });
                loading.show();

                setTimeout(function() {
                    loading.hide();

                    // KTUtil.css(listEl, 'display', 'none');
                    // KTUtil.css(viewEl, 'display', 'flex');
                }, 700);


            });

            // Group selection
            KTUtil.on(listEl, '.kt-inbox__toolbar .kt-inbox__check .kt-checkbox input', 'click', function() {
                var items = KTUtil.findAll(listEl, '.kt-inbox__items .kt-inbox__item');

                for (var i = 0, j = items.length; i < j; i++) {
                    var item = items[i];
                    var checkbox = KTUtil.find(item, '.kt-inbox__actions .kt-checkbox input');
                    checkbox.checked = this.checked;

                    if (this.checked) {
                        KTUtil.addClass(item, 'kt-inbox__item--selected');
                    } else {
                        KTUtil.removeClass(item, 'kt-inbox__item--selected');
                    }
                }
            });

            // Individual selection
            KTUtil.on(listEl, '.kt-inbox__item .kt-checkbox input', 'click', function() {
                var item = this.closest('.kt-inbox__item');

                if (item && this.checked) {
                    KTUtil.addClass(item, 'kt-inbox__item--selected');
                } else {
                    KTUtil.removeClass(item, 'kt-inbox__item--selected');
                }
            });
        },

    };
}();

KTUtil.ready(function() {
    KTAppInbox.init();
});
